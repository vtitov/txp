<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\MposTransactions */

$this->title = 'Create Mpos Transactions';
$this->params['breadcrumbs'][] = ['label' => 'Mpos Transactions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mpos-transactions-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
