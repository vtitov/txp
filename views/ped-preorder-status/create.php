<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PedPreorderStatus */

$this->title = \Yii::t('app', 'Добавление статуса предзаказа');
$this->params['breadcrumbs'][] = ['label' => \Yii::t('app', 'Справочник статусов предзаказов'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ped-preorder-status-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
