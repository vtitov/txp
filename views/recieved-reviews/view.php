<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\RecievedReviews */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Полученные отзывы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="recieved-reviews-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?php foreach($statuses as $key => $value): ?>
            <?= Html::a($value, ['change-status', 'id' => $model->id, 'status' => $key], ['class' => $model->status_id == $key ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?php endforeach; ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            'name',
            'email:email',
            'text:ntext',
            'date_recieved',
            [
                'attribute' => 'avatar',
                'value' => Html::img('@web'.\yii\helpers\HtmlPurifier::process($model->avatar)),
                'format' => 'html',
            ]
        ],
    ]) ?>

    <?= Html::a('Разместить на сайте', ['publish-review', 'id' => $model->id], ['class' => 'btn btn-success']) ?>

</div>
