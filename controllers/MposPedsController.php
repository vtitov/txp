<?php

namespace app\controllers;

use app\helpers\Notification;
use app\models\DeviceEventLog;
use app\models\DeviceEventLogSearch;
use app\models\IsoSettingPaymentMap;
use app\models\MposPedsArchive;
use app\models\MposTransactions;
use app\models\Owners;
use app\models\PaymentsMap;
use app\models\PaymentsMapSearch;
use app\models\PaymentTerminalCurrentBatch;
use Yii;
use app\models\MposPeds;
use app\models\MposPedsSearch;
use yii\base\UserException;
use yii\db\Expression;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * MposPedsController implements the CRUD actions for MposPeds model.
 */
class MposPedsController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view', 'view-event'],
                        'roles' => ['pedView'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['create', 'update', 'delete', 'assign-payments', 'reset-conf-category'],
                        'roles' => ['pedEdit'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['remove-ped-from-merchant'],
                        'roles' => ['merchantPedAssign'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all MposPeds models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MposPedsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single MposPeds model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        $paymentsMapSearchModel = new PaymentsMapSearch();
        $paymentsMapDataProvider = $paymentsMapSearchModel->search(Yii::$app->request->queryParams, $model->id);

        $eventLogSearchModel = new DeviceEventLogSearch();
        $eventLogSearchModel->created_at = date('Y-m-d', strtotime('-29 days')) . ' - ' . date('Y-m-d');
        $eventLogDataProvider = $eventLogSearchModel->search(Yii::$app->request->queryParams, $model->serial_number);

        return $this->render('view', [
            'model' => $model,
            'paymentsMapDataProvider' => $paymentsMapDataProvider,
            'paymentsMapSearchModel' => $paymentsMapSearchModel,
            'eventLogSearchModel' => $eventLogSearchModel,
            'eventLogDataProvider' => $eventLogDataProvider,
        ]);
    }

    /**
     * Creates a new MposPeds model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new MposPeds();
        $model->ped_status_id = 2;
        $model->ped_conf_category_id = 1;
        $model->merchant_id = 0;
        $model->ped_caps = 7;//разрешена оплата по магнитной полосе, чипу и бесконтакту (вручную регистрируются только миуры)

        if (Yii::$app->user->identity->owner_id != 1) {
            $model->owner_id = Yii::$app->user->identity->owner_id;
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            $transaction = MposPeds::getDb()->beginTransaction();
            try {
                $model->save(false);
                if ($model->payment_terminal_id != 1) {
                    $model->paymentTerminal->ped_id = $model->id;
                    $model->paymentTerminal->save(false);
                }
                $transaction->commit();
            } catch (\Exception $e) {
                $transaction->rollBack();
                throw $e;
            } catch (\Throwable $e) {
                $transaction->rollBack();
                throw $e;
            }

            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing MposPeds model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $oldPaymentTerminal = $model->paymentTerminal;//при смене платежного терминала меняем группу конфигурации

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            $transaction = MposPeds::getDb()->beginTransaction();
            try {
                if ($model->payment_terminal_id != $oldPaymentTerminal->id) {
                    if ($model->payment_terminal_id == 1) {//Платежный терминал "Не задан"
                        $model->ped_conf_category_id = 1;//группа конфигурации по умолчанию: Root, all devices by default
                    } else {
                        $model->ped_conf_category_id = $model->paymentTerminal->isoSettings->ped_conf_category_id;
                    }

                    $model->save(false);

                    //пед привязывается к платежному терминалу на этапе валидации
                    //здесь привязываем платежный терминал к педу (таблицы ссылаются друг на друга)
                    if ($model->payment_terminal_id != 1) {
                        $model->paymentTerminal->ped_id = $model->id;
                        $model->paymentTerminal->save(false);
                    }//Платежный терминал "Не задан" ни к чему не привязываем

                    if ($oldPaymentTerminal->id != 1) {
                        $oldPaymentTerminal->ped_id = 0;//отвязываем старый платежный терминал от педа
                        $oldPaymentTerminal->save(false);
                    }//Платежный терминал "Не задан" отвязывать не надо, он и не должен быть ни к чему привязан

                } else {
                    $model->save(false);
                }

                //если терминал "не сконфигурирован" - удаляем навешенные платежи
                if ($model->ped_status_id == 3) {
                    PaymentsMap::deleteAll(['ped_id' => $model->id]);
                }

                $transaction->commit();

            } catch (\Exception $e) {
                $transaction->rollBack();
                throw $e;
            } catch (\Throwable $e) {
                $transaction->rollBack();
                throw $e;
            }
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing MposPeds model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        if (MposTransactions::find()->where(['mpos_ped_id' => $id])->exists()) {
            Yii::$app->session->addFlash(
                'error',
                Yii::t('app', 'Устройство не может быть удалено. Для него существуют незакрытые транзакции.')
            );
            return $this->redirect(['view', 'id' => $model->id]);
        }

        if ($model->merchant_id != '0') {
            $merchant = $model->mposMerchant;
        } else {
            $merchant = null;
        }

        $archiveModel = new MposPedsArchive();
        foreach ($model as $key => $value) {
            $archiveModel->$key = $value;
        }
        $archiveModel->delete_date = new Expression('now()');
        $archiveModel->ped_status_id = 2; //устройство отключено
        //$archiveModel->merchant_id = 0; //отвязываем от торговца

        $transaction = MposPeds::getDb()->beginTransaction();
        try {
            $archiveModel->save(false);
            if ($model->paymentTerminal) {
                $model->paymentTerminal->ped_id = 0;//отвязываем платежный терминал от ped'а
                $model->paymentTerminal->save(false, ['ped_id']);

                //если есть незакрытый батч - закрываем
                $batch = PaymentTerminalCurrentBatch::findOne($model->payment_terminal_id);
                if ($batch && $batch->batch_status_id != 4 && $batch->batch_status_id != 5 && $batch->batch_status_id != 6) {
                    $batch->batch_status_id = 6;
                    $batch->save(false, ['batch_status_id']);
                }
            }
            $model->delete();
            $transaction->commit();
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        } catch (\Throwable $e) {
            $transaction->rollBack();
            throw $e;
        }

        if ($merchant) {
            //Yii::t('app', 'Ваше устройство {serialNumber} отключено. Дальнейшая работа невозможна.', ['serialNumber' => $model->serial_number])
            $res = Notification::sendToMerchant($merchant->id, 'device.deleted', ['deviceId' => $model->serial_number]);
            if ($res !== true) {
                Yii::$app->session->addFlash(
                    'error',
                    Yii::t(
                        'app',
                        'Сообщение о переносе устройства в архив не отправлено: {errorMessage}',
                        ['errorMessage' => $res]
                    )
                );
            }
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the MposPeds model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return MposPeds the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        $model = MposPeds::findOne($id);
        $owner_id = Yii::$app->user->identity->owner_id;

        if ($model !== null && ($owner_id == 1 || $model->owner_id == $owner_id)) {
            return $model;
        } else {
            throw new NotFoundHttpException(\Yii::t('app', 'Устройство не найдено.'));
        }
    }

    /*public function actionRemovePedFromMerchant($id)
    {
        $model = $this->findModel($id);

        $transaction = MposPeds::getDb()->beginTransaction();
        try {
            if ($model->payment_terminal_id != 1) {
                $paymentTerminal = $model->paymentTerminal;
            } else {
                $paymentTerminal = null;
            }

            //привязка платежного терминала к торговцу остается,
            //поэтому для сохранения целостности данных, отвязываем ped еще и от платежного терминала
            $model->merchant_id = 0;
            $model->payment_terminal_id = 1;//платежный терминал "не задан"
            $model->save(false, ['merchant_id', 'payment_terminal_id']);

            if ($paymentTerminal) {
                $paymentTerminal->ped_id = 0;
                $paymentTerminal->save(false, ['ped_id']);
            }

            $transaction->commit();
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        } catch (\Throwable $e) {
            $transaction->rollBack();
            throw $e;
        }

        return $this->redirect($_SERVER['HTTP_REFERER']);
    }*/

    public function actionAssignPayments($id)
    {
        $ped = $this->findModel($id);
        $batch = [];

        $payments = IsoSettingPaymentMap::find()
            ->andWhere(['iso_setting_id' => ArrayHelper::getValue($ped->paymentTerminal, 'iso_setting_id')])
            ->all();

        foreach ($payments as $payment) {
            $batch[] = [$id, $payment->payment_id, $payment->currency];
        }

        $transaction = PaymentsMap::getDb()->beginTransaction();
        try {
            PaymentsMap::deleteAll(['ped_id' => $id]);
            if (count($batch) > 0) {
                PaymentsMap::getDb()->createCommand()->batchInsert(PaymentsMap::tableName(), ['ped_id', 'payments_id', 'currency_id'], $batch)->execute();
            }
            $transaction->commit();
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        } catch (\Throwable $e) {
            $transaction->rollBack();
            throw $e;
        }

        return $this->redirect(['view', 'id' => $id]);
    }

    public function actionResetConfCategory($id)
    {
        $model = $this->findModel($id);
        $isoSetting = ArrayHelper::getValue($model->paymentTerminal, 'isoSettings');
        if ($isoSetting !== null) {
            $model->ped_conf_category_id = $isoSetting->ped_conf_category_id;
            $model->save(false, ['ped_conf_category_id']);
        } else {
            throw new UserException(Yii::t('app', 'Не найдена ISO настройка'));
        }
        return $this->redirect(['view', 'id' => $model->id]);
    }

    public function actionViewEvent($id)
    {
        $model = DeviceEventLog::findOne($id);
        if ($model === null) {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }

        //проверяем, что пользователь может просматривать информацию по этому устройству
        $ped = $model->ped;
        $owner_id = Yii::$app->user->identity->owner_id;

        if ($ped !== null && ($owner_id == 1 || $ped->owner_id == $owner_id)) {
            return $this->render('view-event', [
                'model' => $model,
                'ped' => $ped,
            ]);
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }
}