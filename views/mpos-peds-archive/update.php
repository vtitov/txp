<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MposPedsArchive */

$this->title = 'Update Mpos Peds Archive: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Mpos Peds Archives', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="mpos-peds-archive-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
