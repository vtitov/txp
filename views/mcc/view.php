<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Mcc */

$this->title = $model->range_start_code;
$this->params['breadcrumbs'][] = ['label' => \Yii::t('app', 'Справочник МСС-кодов для сайта payBYcard'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mcc-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(\Yii::t('app', 'Редактировать'), ['update', 'id' => $model->range_start_code], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(\Yii::t('app', 'Удалить'), ['delete', 'id' => $model->range_start_code], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => \Yii::t('app', 'Вы действительно хотите удалить эту запись?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'range_start_code',
            'range_end_code',
            'description',
        ],
    ]) ?>

</div>
