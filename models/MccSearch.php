<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Mcc;

/**
 * MccSearch represents the model behind the search form about `app\models\Mcc`.
 */
class MccSearch extends Mcc
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['range_start_code', 'range_end_code', 'description'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Mcc::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'range_start_code', $this->range_start_code])
            ->andFilterWhere(['like', 'range_end_code', $this->range_end_code])
            ->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }
}
