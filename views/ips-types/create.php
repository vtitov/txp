<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\IpsTypes */

$this->title = \Yii::t('app', 'Добавление карты МПС');
$this->params['breadcrumbs'][] = ['label' => \Yii::t('app', 'Справочник карт МПС'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ips-types-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
