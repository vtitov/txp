<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\DeviceEventLog */

$this->title = Yii::t('app', 'Просмотр события');
$this->params['breadcrumbs'][] = ['label' => \Yii::t('app', 'Справочник устройств'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $ped->serial_number, 'url' => ['view', 'id' => $ped->id]];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);

$trustLevels = \yii\helpers\ArrayHelper::map(
    \app\models\TrustLevelLocale::find()
        ->select(['id', 'short_desc'])
        ->where(['locale' => Yii::$app->language])
        ->asArray()
        ->all(),
    'id',
    'short_desc'
);
?>
<div class="device-event-log-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            [
                'attribute' => 'merchant_id',
                'value' => $model->merchant ?
                    Html::a(Html::encode($model->merchant->name), ['mpos-merchants/view', 'id' => $model->merchant_id]) :
                    Html::encode($model->merchant_id),
                'format' => 'html',
            ],
            [
                'attribute' => 'device_id',
                'value' => Html::a(Html::encode($ped->serial_number), ['mpos-peds/view', 'id' => $ped->id]),
                'format' => 'html',
            ],
            [
                'attribute' => 'old_trust_level_id',
                'value' => ArrayHelper::getValue($trustLevels, $model->old_trust_level_id, $model->old_trust_level_id),
            ],
            [
                'attribute' => 'new_trust_level_id',
                'value' => ArrayHelper::getValue($trustLevels, $model->new_trust_level_id, $model->new_trust_level_id),
            ],
            'app_version',
            'mas_version',
            [
                'attribute' => 'event_code_id',
                'value' => ArrayHelper::getValue($model->eventCode, 'desc', $model->event_code_id),
            ],
            [
                'attribute' => 'attest_code',
                'value' => ArrayHelper::getValue($model->attestationCode, 'description', $model->attest_code),
            ],
            [
                'attribute' => 'counter_id',
                'value' => ArrayHelper::getValue($model->counter, 'description', $model->counter_id),
            ],
            'cnt_value',
            'cnt_limit_value',
            'cnt_day',
            [
                'attribute' => 'cnt_currency',
                'value' => ArrayHelper::getValue($model->currency, 'alpha_code', $model->cnt_currency),
            ],
            [
                'attribute' => 'event_source_id',
                'value' => ArrayHelper::getValue($model->eventSource, 'description', $model->event_source_id),
            ],
            'created_at',
            'description',
        ],
    ]) ?>

</div>
