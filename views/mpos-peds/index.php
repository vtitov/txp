<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use app\helpers\RandomImageHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MposPedsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = \Yii::t('app', 'Справочник устройств');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mpos-peds-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <img src="<?= RandomImageHelper::getRandomImageFromDir('img/peds') ?>" height="140px" align="right" style="margin-top: -130px">
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(\Yii::t('app', 'Добавить устройство'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',

            [
                'attribute' => 'owner_id',
                'value' => function($model){
                    if ($model->owner) {
                        return Html::encode($model->owner->name);
                    } else if ($model->owner_id != '') {
                        return '<span class="not-set">(' . Html::encode($model->owner_id) . ')</span>';
                    } else {
                        return null;
                    }
                },
                'filter' => ArrayHelper::map(
                    app\models\Owners::find()
                        ->where(['locale' => Yii::$app->language])
                        ->orderBy(['name' => SORT_ASC])
                        ->asArray()
                        ->all(),
                    'id',
                    'name'
                ),
                'format' => 'html',
                'visible' => (Yii::$app->user->identity->owner_id == 1),
            ],
            [
                'attribute' => 'serial_number',
                'value' => function($model){
                    return Html::a(Html::encode($model->serial_number), ['view', 'id' => $model->id]);
                },
                'format'=>'html',
            ],
            /*[
				'attribute' => 'ped_model_id',
				'value' => 'mposPedModel.name',
                'filter' => ArrayHelper::map(\app\models\MposPedModels::find()->asArray()->all(), 'id', 'name'),
			],*/
            [
                'attribute' => 'merchant_id',
                'value' => function($model){
                    if ($model->mposMerchant){
                        return Html::a(Html::encode($model->mposMerchant->name), ['mpos-merchants/view', 'id' => $model->merchant_id]);
                    } else {
                        return null;
                    }
                },
                'format'=>'html',
            ],
            // 'login_last_date',
            // 'login_attempts_count',
            [
                'attribute' => 'ped_status_id',
                'value' => function($model){
                    return '<font color="' . Html::encode($model->mposPedStatus->color) . '">' . Html::encode($model->mposPedStatus->name) . '</font>';
                },
                'format' => 'html',
                'filter' => ArrayHelper::map(
                    \app\models\PedStatusLocale::find()
                        ->where(['locale' => Yii::$app->language])
                        ->asArray()
                        ->all(),
                    'id',
                    'name'
                )
            ],
            //'mcc',
            //'description',
            [
                'attribute' => 'payment_terminal_id',
                'value' => function($model){
                    if ($model->paymentTerminal){
                        return Html::a(Html::encode($model->paymentTerminal->name), ['payments-terminals/view', 'id' => $model->payment_terminal_id]);
                    } else {
                        return null;
                    }
                },
                'format' => 'html',
            ],
            \app\helpers\DateColumnHelper::getColumn('add_date', $searchModel),
            'app_ver',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
