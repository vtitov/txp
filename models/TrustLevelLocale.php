<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "trust_level_locale".
 *
 * @property int $id
 * @property string $locale
 * @property string $name
 * @property string|null $short_desc
 * @property string|null $desc
 */
class TrustLevelLocale extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'trust_level_locale';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('t2p_monitoring');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'locale', 'name'], 'required'],
            [['id'], 'integer'],
            [['locale'], 'string', 'max' => 5],
            [['name'], 'string', 'max' => 20],
            [['short_desc'], 'string', 'max' => 100],
            [['desc'], 'string', 'max' => 255],
            [['id', 'locale'], 'unique', 'targetAttribute' => ['id', 'locale']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'locale' => Yii::t('app', 'Locale'),
            'name' => Yii::t('app', 'Name'),
            'short_desc' => Yii::t('app', 'Short Desc'),
            'desc' => Yii::t('app', 'Desc'),
        ];
    }
}
