<?php

namespace app\models;

use Yii;
use yii\validators\RegularExpressionValidator;

/**
 * This is the model class for table "priorbank_txp_request".
 *
 * @property int $id
 * @property string $rq_id
 * @property string $unp
 * @property string $name_legal_entity
 * @property string $registered_address
 * @property string $post_address
 * @property int $phone
 * @property string $email
 * @property string $bic
 * @property string $iban
 * @property int $currency
 * @property string $contract_id
 * @property string $contract_date
 * @property string $terminal_id
 * @property string $mcc
 * @property string $pt_description
 * @property string $pt_destination
 * @property int $terminal_status
 * @property string $parse_date
 * @property int $status
 */
class PriorbankTxpRequest extends \yii\db\ActiveRecord
{
    public static function getTerminalStatusses()
    {
        return [
            0 => Yii::t('app', 'Отключение'),
            1 => Yii::t('app', 'Регистрация'),
        ];
    }

    public static function getRequestStatuses()
    {
        return [
            0 => Yii::t('app', 'не обработана'),
            1 => Yii::t('app', 'обработана'),
            2 => Yii::t('app', 'аннулирована'),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'priorbank_txp_request';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('mpos');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['rq_id', 'unp', 'name_legal_entity', 'phone', 'email', 'terminal_id', 'mcc', 'terminal_status'], 'required'],
            [['rq_id'], 'unique'],
            [['phone', 'currency'], 'integer'],
            [['phone'], 'match', 'pattern' => '/^375(29|33|44|25)\d{7}$/'],
            [['rq_id'], 'string', 'max' => 10],
            [['unp'], 'string', 'max' => 12],
            [['name_legal_entity', 'registered_address', 'post_address', 'pt_description', 'pt_destination'], 'string', 'max' => 255],
            [['email', 'contract_id'], 'string', 'max' => 50],
            ['email', 'email'],
            [['contract_date'], 'date', 'format' => 'php:Y-m-d'],
            [['bic'], 'string', 'max' => 11],
            [['iban'], 'string', 'max' => 34],
            [['terminal_id'], 'string', 'max' => 8],
            [['mcc'], 'string', 'max' => 4],
            [['terminal_status'], 'in', 'range' => ['0', '1']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'rq_id' => Yii::t('app', 'ID запроса'),
            'unp' => Yii::t('app', 'УНП'),
            'name_legal_entity' => Yii::t('app', 'Наименование ОТС'),
            'registered_address' => Yii::t('app', 'Юридический адрес'),
            'post_address' => Yii::t('app', 'Почтовый адрес'),
            'phone' => Yii::t('app', 'Мобильный телефон'),
            'email' => Yii::t('app', 'Email'),
            'bic' => Yii::t('app', 'BIC'),
            'iban' => Yii::t('app', 'IBAN'),
            'currency' => Yii::t('app', 'Валюта счета'),
            'contract_id' => Yii::t('app', 'Номер договора'),
            'contract_date' => Yii::t('app', 'Дата заключения договора обслуживания'),
            'terminal_id' => Yii::t('app', 'Платежный терминал'),
            'mcc' => Yii::t('app', 'MCC'),
            'pt_description' => Yii::t('app', 'Описание терминала'),
            'pt_destination' => Yii::t('app', 'Место расположения терминала'),
            'terminal_status' => Yii::t('app', 'Тип заявки'),
            'parse_date' => Yii::t('app', 'Дата вчитывания файла'),
            'status' => Yii::t('app', 'Статус заявки'),
        ];
    }
}
