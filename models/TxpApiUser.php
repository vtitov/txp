<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "txp_api_user".
 *
 * @property int $id
 * @property string $username
 * @property string $password_hash
 * @property int $status
 * @property string|null $description
 * @property string $created_at
 */
class TxpApiUser extends \yii\db\ActiveRecord
{
    public $password;

    public static function getStatuses()
    {
        return [
            0 => \Yii::t('app', 'Заблокирован'),
            1 => \Yii::t('app', 'Активен'),
        ];
    }

    private $_ownerIds;

    public function getOwnerIds()
    {
        if (isset($this->_ownerIds)) {
            return $this->_ownerIds;
        }

        $ownerIds = TxpApiUserOwnerMap::find()->select(['owner_id'])->where(['user_id' => $this->id])->asArray()->all();
        $this->_ownerIds = ArrayHelper::getColumn($ownerIds, 'owner_id');
        return $this->_ownerIds;
    }

    private $_owners;

    public function getOwners()
    {
        if (isset($this->_owners)) {
            return $this->_owners;
        }

        $owners = Owners::find()
            ->select(['id', 'name'])
            ->where([
                'locale' => Yii::$app->language,
                'id' => $this->getOwnerIds()
            ])
            ->orderBy('name')
            ->asArray()
            ->all();

        $this->_owners = ArrayHelper::map($owners, 'id', 'name');
        return $this->_owners;
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'txp_api_user';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('mpos');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['username'], 'required'],
            [['username'], 'unique'],
            [['password'], 'required', 'on' => 'create'],
            [['username', 'password', 'description'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'username' => Yii::t('app', 'Логин'),
            'password' => Yii::t('app', 'Пароль'),
            'status' => Yii::t('app', 'Статус'),
            'description' => Yii::t('app', 'Описание'),
            'created_at' => Yii::t('app', 'Дата регистрации'),
        ];
    }

    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        if (isset($this->password) && $this->password !== '') {
            $this->password_hash = Yii::$app->security->generatePasswordHash($this->password);
        }

        return true;
    }
}
