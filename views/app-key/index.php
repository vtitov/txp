<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AppKeySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Ключи app key');
$this->params['breadcrumbs'][] = $this->title;
$osFilter = \yii\helpers\ArrayHelper::map(
    \app\models\OsAllowed::find()->select(['id', 'name'])->all(),
    'id',
    'name'
);
$css = <<<CSS
tr>td:nth-child(4) {
max-width: 450px;
overflow-wrap: break-word;
}
CSS;
$this->registerCss($css);
?>
<div class="app-key-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php if (Yii::$app->session->hasFlash('appKeyGenerationNoAnswer')) {
        echo '<div class="alert alert-danger">';
        echo Yii::t('app', 'Нет ответа от сервера генерации ключей');
        echo '</div>';
    } else if (Yii::$app->session->hasFlash('appKeyGenerationWrongAnswer')) {
        echo '<div class="alert alert-danger">';
        echo Yii::t('app', 'Некорректный ответа от сервера генерации ключей');
        echo '</div>';
    } ?>

    <p>
        <?= Html::a(Yii::t('app', 'Сгенерировать ключ'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'os_id',
                'value' => function($model) use ($osFilter) {
                    return \yii\helpers\ArrayHelper::getValue($osFilter, $model->os_id, $model->os_id);
                },
                'filter' => $osFilter,
            ],
            'app_ver',
            'pub_key',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update}'
            ],
        ],
    ]); ?>


</div>
