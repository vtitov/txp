<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "transactionslog_archive".
 *
 * @property string $id
 * @property string $dt
 * @property integer $trx_statuses_id
 * @property integer $trx_types_id
 * @property integer $iso_setting_id
 * @property string $payment_id
 * @property string $payment_terminal_id
 * @property string $currency
 * @property integer $amount
 * @property string $bdd
 * @property integer $batch
 * @property integer $reconcilationFlag
 * @property integer $MTI
 * @property string $rrn
 * @property string $ksn
 * @property string $expdate
 * @property string $authcode
 * @property integer $acquircode
 * @property integer $stan
 * @property string $sred
 * @property string $device
 * @property string $response_code
 */
class TransactionslogArchive extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'transactionslog_archive';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('transactionslog');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['dt', 'iso_setting_id', 'payment_terminal_id', 'expdate'], 'required'],
            [['dt', 'bdd'], 'safe'],
            [['trx_statuses_id', 'trx_types_id', 'iso_setting_id', 'payment_id', 'payment_terminal_id', 'amount', 'batch', 'reconcilationFlag', 'MTI', 'rrn', 'expdate', 'acquircode', 'stan'], 'integer'],
            [['currency', 'response_code'], 'string', 'max' => 3],
            [['ksn'], 'string', 'max' => 20],
            [['authcode'], 'string', 'max' => 6],
            [['sred'], 'string', 'max' => 100],
            [['device'], 'string', 'max' => 8]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'dt' => \Yii::t('app', 'Дата'),
            'trx_statuses_id' => \Yii::t('app', 'Статус'),
            'trx_types_id' => \Yii::t('app', 'Тип'),
            'iso_setting_id' => \Yii::t('app', 'Настройка ISO'),
            'payment_id' => \Yii::t('app', 'Платеж'),
            'payment_terminal_id' => \Yii::t('app', 'Платежный терминал'),
            'currency' => \Yii::t('app', 'Валюта'),
            'amount' => \Yii::t('app', 'Сумма'),
            'bdd' => 'Bdd',
            'batch' => 'Batch',
            'reconcilationFlag' => 'Reconcilation Flag',
            'MTI' => 'Mti',
            'rrn' => 'RRN',
            'ksn' => 'Ksn',
            'expdate' => 'Expdate',
            'authcode' => 'Authcode',
            'acquircode' => 'Acquircode',
            'stan' => 'Stan',
            'sred' => 'Sred',
            'device' => \Yii::t('app', 'Пл. терминал'),
            'response_code' => 'RC',
        ];
    }

    public function getTrxStatus()
    {
        return $this->hasOne(TrxStatuses::className(), ['id' => 'trx_statuses_id']);
    }

    public function getTrxType()
    {
        return $this->hasOne(TrxTypes::className(), ['id' => 'trx_types_id']);
    }

    public function getPayment()
    {
        return $this->hasOne(PaymentLocale::className(), ['id' => 'payment_id'])
            ->andOnCondition([PaymentLocale::tableName() . '.locale' => Yii::$app->language]);
    }

    public function getPaymentTerminal()
    {
        return $this->hasOne(PaymentsTerminals::className(), ['id' => 'payment_terminal_id']);
    }

    public function getCurrencyModel()
    {
        return $this->hasOne(Currency::className(), ['id' => 'currency']);
    }

    public function getMposPed()
    {
        return $this->hasOne(MposPeds::className(), ['payment_terminal_id' => 'payment_terminal_id']);
    }

    public function getTransactionArchive()
    {
        return $this->hasOne(TransactionArchive::className(), ['financial_original_trx_id' => 'id']);
    }
}
