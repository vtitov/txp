<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "iso_setting".
 *
 * @property int $id
 * @property string $host
 * @property int $port
 * @property string $name
 * @property string $protocol_class
 * @property string $zpk Ключ шифрования один для всех устройств подключенных к данному процессингу
 * @property string $zak Ключ для MAC, один для всех устройств подключенных к данному процессингу
 * @property int $owner_id
 * @property int $ped_conf_category_id
 * @property int $sred_clear_period_days
 * @property string $zmk_tap_x ключ IBA для перешифровки pinblock zpk2zpk
 * @property int $status
 * @property string $date_mod
 */
class IsoSettings extends \yii\db\ActiveRecord
{
    public static function getStatuses()
    {
        return [
            0 => Yii::t('app', 'неактивна'),
            1 => Yii::t('app', 'активна'),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'iso_setting';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('mpos');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['host', 'name', 'protocol_class', 'zpk', 'zak', 'zmk_tap_x'], 'filter', 'filter' => 'trim'],
            [['zpk', 'zak', 'zmk_tap_x'], 'default', 'value' => null],
            [['host', 'port', 'name', 'protocol_class', 'owner_id', 'ped_conf_category_id'], 'required'],
            [['port', 'owner_id', 'ped_conf_category_id', 'sred_clear_period_days'], 'integer'],
            [['host', 'protocol_class'], 'string', 'max' => 255],
            [['name'], 'string', 'max' => 45],
            [['zpk', 'zak'], 'string', 'max' => 128],
            [['zmk_tap_x'], 'string', 'max' => 49],
            [['ped_conf_category_id'], 'exist', 'skipOnError' => true, 'targetClass' => PedConfCategory::className(), 'targetAttribute' => ['ped_conf_category_id' => 'id']],
            [['owner_id'], 'exist', 'skipOnError' => true, 'targetClass' => Owners::className(), 'targetAttribute' => ['owner_id' => 'id']],
            [['status'], 'in', 'range' => array_keys(static::getStatuses())],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'host' => Yii::t('app', 'Хост'),
            'port' => Yii::t('app', 'Порт'),
            'name' => Yii::t('app', 'Название'),
            'protocol_class' => Yii::t('app', 'Класс протокола'),
            'zpk' => Yii::t('app', 'Зональный PIN ключ (ZPK)'),
            'zak' => Yii::t('app', 'Зональный ключ макирования (ZAK)'),
            'owner_id' => Yii::t('app', 'Владелец'),
            'ped_conf_category_id' => Yii::t('app', 'Группа конфигурации по умолчанию'),
            'sred_clear_period_days' => Yii::t('app', 'Период хранения SRED данных (дней)'),
            'zmk_tap_x' => Yii::t('app', 'Зональный мастер ключ tapXphone'),
            'status' => Yii::t('app', 'Статус'),
            'date_mod' => Yii::t('app', 'Дата последнего изменения'),
        ];
    }

    public function getOwner()
    {
        return $this->hasOne(Owners::className(), ['id' => 'owner_id'])
            ->andOnCondition([Owners::tableName() . '.locale' => Yii::$app->language]);
    }

    public function getPedConfCategory()
    {
        return $this->hasOne(PedConfCategory::className(), ['id' => 'ped_conf_category_id']);
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => false,
                'updatedAtAttribute' => 'date_mod',
                'value' => new Expression('NOW()'),
            ],
        ];
    }
}
