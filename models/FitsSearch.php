<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Fits;

/**
 * FitsSearch represents the model behind the search form about `app\models\Fits`.
 */
class FitsSearch extends Fits
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'ips_types_id'], 'integer'],
            [['fit'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Fits::find()
            ->joinWith('ipsTypes')
            ->with('ipsTypes');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['ips_types_id'] = [
            'asc' => [IpsTypes::tableName() . '.name' => SORT_ASC],
            'desc' => [IpsTypes::tableName() . '.name' => SORT_DESC]
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'ips_types_id' => $this->ips_types_id,
        ]);

        $query->andFilterWhere(['like', 'fit', $this->fit]);

        return $dataProvider;
    }
}
