<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\models\IsoSettingsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = \Yii::t('app', 'Справочник ISO настроек');
$this->params['breadcrumbs'][] = $this->title;

$ownerFilter = ArrayHelper::map(
    \app\models\Owners::find()->andWhere(['locale' => Yii::$app->language])->all(),
    'id',
    'name'
);
?>
<div class="iso-settings-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(\Yii::t('app', 'Добавить ISO настройку'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'host',
            'port',
            'name',
            [
                'attribute' => 'owner_id',
                'value' => function($model) use ($ownerFilter) {
                    return ArrayHelper::getValue($ownerFilter, $model->owner_id, $model->owner_id);
                },
                'filter' => $ownerFilter,
            ],
            [
                'attribute' => 'status',
                'value' => function($model) {
                    return ArrayHelper::getValue(\app\models\IsoSettings::getStatuses(), $model->status, $model->status);
                },
                'filter' => \app\models\IsoSettings::getStatuses(),
            ],
            
            [
                'class' => 'yii\grid\ActionColumn',
                'options' => ['style' => 'width: 70px'],
            ],
        ],
    ]); ?>

</div>
