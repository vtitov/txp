<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Fits */

$this->title = \Yii::t('app', 'Добавление FIT');
$this->params['breadcrumbs'][] = ['label' => \Yii::t('app', 'Справочник FIT'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fits-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
