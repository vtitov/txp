<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MposTransactionsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mpos-transactions-search">

    <?php $form = ActiveForm::begin([
        'id' => 'filterForm',
        'method' => 'post',
    ]); ?>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'date_time')->widget(\kartik\daterange\DateRangePicker::className(), \app\helpers\DateColumnHelper::getWidgetConfig()) ?>

            <?= $form->field($model, 'mpos_ped_id') ?>

            <?= $form->field($model, 'mpos_merchant_id') ?>

            <?= $form->field($model, 'payments_terminal_id') ?>

            <?= $form->field($model, 'payment_id')->dropDownList(
                \yii\helpers\ArrayHelper::map(
                    \app\models\PaymentLocale::find()
                        ->select(['id', 'name'])
                        ->where(['locale' => Yii::$app->language])
                        ->asArray()
                        ->all(),
                    'id',
                    'name'
                ),
                ['prompt' => '']
            ) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'bdd')->widget(\kartik\daterange\DateRangePicker::className(), \app\helpers\DateColumnHelper::getWidgetConfig()) ?>

            <?= $form->field($model, 'batch') ?>

            <?= $form->field($model, 'stan') ?>

            <?= $form->field($model, 'authcode') ?>

            <?= $form->field($model, 'mpos_transaction_status_id')->dropDownList(
                \yii\helpers\ArrayHelper::map(
                    \app\models\TransactionStatusLocale::find()
                        ->select(['id', 'name'])
                        ->where(['locale' => Yii::$app->language])
                        ->asArray()
                        ->all(),
                    'id',
                    'name'
                ),
                ['prompt' => '']
            ) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'amount') ?>

            <?= $form->field($model, 'currency_id')->dropDownList(
                \yii\helpers\ArrayHelper::map(\app\models\Currency::find()->asArray()->all(), 'id', 'alpha_code'),
                ['prompt' => '']
            ) ?>

            <?= $form->field($model, 'card') ?>

            <?= $form->field($model, 'rrn') ?>

            <?= $form->field($model, 'transaction_type_id')->dropDownList(
                \yii\helpers\ArrayHelper::map(
                    \app\models\TransactionTypeLocale::find()
                        ->where(['locale' => Yii::$app->language])
                        ->asArray()
                        ->all(),
                    'id',
                    'desc'
                ),
                ['prompt' => '']
            ) ?>

            <?php if (Yii::$app->user->identity->owner_id == 1): ?>
                <?= $form->field($model, 'bank')->dropDownList(
                    \yii\helpers\ArrayHelper::map(\app\models\Owners::find()->andWhere(['locale' => Yii::$app->language])->all(), 'id', 'name'),
                    ['prompt' => '']
                )->label(\Yii::t('app', 'Банк')) ?>
            <?php endif; ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton(\Yii::t('app', 'Применить'), ['class' => 'btn btn-primary', 'name' => 'action', 'value' => 'render']) ?>
        <?= Html::submitButton(\Yii::t('app', 'Экспорт в CSV'), ['class' => 'btn btn-default', 'name' => 'action', 'value' => 'export']) ?>
        <?php //echo Html::a('Очистить', ['index'], ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
