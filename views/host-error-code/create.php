<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\HostErrorCode */

$this->title = 'Create Host Error Code';
$this->params['breadcrumbs'][] = ['label' => 'Host Error Codes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="host-error-code-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
