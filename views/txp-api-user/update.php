<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TxpApiUser */
/* @var $ownerMap app\models\TxpApiUserOwnerMapBatch */

$this->title = Yii::t('app', 'Редактирование пользователя API: {name}', [
    'name' => $model->username,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Пользователи API'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->username, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Редактирование');
?>
<div class="txp-api-user-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'ownerMap' => $ownerMap,
    ]) ?>

</div>
