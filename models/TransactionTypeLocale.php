<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "transaction_type_locale".
 *
 * @property int $id
 * @property string $locale
 * @property string|null $desc
 */
class TransactionTypeLocale extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'transaction_type_locale';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('mpos');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [];
    }
}
