<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

/**
 * This is the model class for table "fits".
 *
 * @property string $id
 * @property string $fit
 * @property string $ips_types_id
 *
 * @property IpsTypes $ipsTypes
 * @property FitsMap[] $fitsMaps
 * @property FitsGroups[] $fitsGroups
 */
class Fits extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'fit';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('mpos');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fit'], 'required'],
            [['ips_types_id'], 'integer'],
            [['fit'], 'string', 'max' => 19],
            ['fit', 'match', 'pattern' => '|^\d{1,19}$|', 'message' => \Yii::t('app', 'FIT должен состоять только из цифр')],
            [['fit'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fit' => 'FIT',
            'ips_types_id' => \Yii::t('app', 'Карта МПС'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIpsTypes()
    {
        return $this->hasOne(IpsTypes::className(), ['id' => 'ips_types_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFitsMaps()
    {
        return $this->hasMany(FitsMap::className(), ['fits_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFitsGroups()
    {
        return $this->hasMany(FitsGroups::className(), ['id' => 'fits_groups_id'])->viaTable('fits_map', ['fits_id' => 'id']);
    }
}
