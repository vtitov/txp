<?php

namespace app\models;

use app\helpers\DateColumnHelper;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\DeviceEventLog;

/**
 * DeviceEventLogSearch represents the model behind the search form of `app\models\DeviceEventLog`.
 */
class DeviceEventLogSearch extends DeviceEventLog
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['old_trust_level_id', 'new_trust_level_id', 'event_code_id', 'event_source_id'], 'integer'],
            [['created_at', 'merchant_id'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $device_id)
    {
        $query = DeviceEventLog::find()
            ->leftJoin('mpos.' . MposMerchants::tableName(), static::tableName() . '.merchant_id = ' . MposMerchants::tableName() . '.id')
            ->leftJoin(EventCodeLocale::tableName(), static::tableName() . '.event_code_id = ' . EventCodeLocale::tableName() . '.id AND ' . EventCodeLocale::tableName() . '.locale = \'' . \Yii::$app->language . '\'')
            ->leftJoin(TrustLevelLocale::tableName() . ' otl', static::tableName() . '.old_trust_level_id = otl.id AND otl.locale = \'' . \Yii::$app->language . '\'')
            ->leftJoin(TrustLevelLocale::tableName() . ' ntl', static::tableName() . '.new_trust_level_id = ntl.id AND ntl.locale = \'' . \Yii::$app->language . '\'')
            ->with(['merchant'])
            ->andWhere(['device_id' => $device_id]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]],
        ]);

        $dataProvider->sort->attributes['merchant_id'] = [
            'asc' => [MposMerchants::tableName() . '.name' => SORT_ASC],
            'desc' => [MposMerchants::tableName() . '.name' => SORT_DESC]
        ];

        $dataProvider->sort->attributes['old_trust_level_id'] = [
            'asc' => ['otl.short_desc' => SORT_ASC],
            'desc' => ['otl.short_desc' => SORT_DESC]
        ];

        $dataProvider->sort->attributes['new_trust_level_id'] = [
            'asc' => ['ntl.short_desc' => SORT_ASC],
            'desc' => ['ntl.short_desc' => SORT_DESC]
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            static::tableName() . '.old_trust_level_id' => $this->old_trust_level_id,
            static::tableName() . '.new_trust_level_id' => $this->new_trust_level_id,
            static::tableName() . '.event_code_id' => $this->event_code_id,
            static::tableName() . '.event_source_id' => $this->event_source_id,
        ]);

        $query->andFilterWhere(['like', MposMerchants::tableName() . '.name', $this->merchant_id]);

        DateColumnHelper::addFilterParams([static::tableName() . '.created_at'], [$this->created_at], $query);

        return $dataProvider;
    }
}
