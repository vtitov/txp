<?php

namespace app\models;

use app\helpers\Notification;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\validators\RegularExpressionValidator;


/**
 * This is the model class for table "mpos_merchants".
 *
 * @property int $id
 * @property int $owner_id
 * @property string $name
 * @property string $unp
 * @property int $banks_mfo
 * @property int $bank_account
 * @property string $bic
 * @property string $iban
 * @property int $bank_account_currency_id
 * @property string $email
 * @property string $phone
 * @property int $send_mail
 * @property int $merchant_status_id
 * @property string $contract
 * @property string $contract_date
 * @property string $registered_address
 * @property string $post_address
 * @property string $iso_merchant_id
 * @property int $chief_cashier_id
 * @property int $terminal_counter_category_id
 * @property int $merchant_counter_category_id
 * @property string $created_at
 *
 * @property MerchantStatusLocale $mposMerchantStatus
 * @property Currency $currency
 * @property Owners $owner
 * @property MerchantCashier $chiefCashier
 * @property CounterCategory $terminalCounterCategory
 * @property CounterCategory $merchantCounterCategory
 */
class MposMerchants extends \yii\db\ActiveRecord
{
    public static function getNotificationChannels()
    {
        return [
            0 => 'SMS',
            1 => 'Email'
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'merchant';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('mpos');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'registered_address', 'post_address', 'iso_merchant_id', 'email', 'contract', 'unp'], 'filter', 'filter' => 'trim'],
            [['terminal_counter_category_id', 'merchant_counter_category_id'], 'default', 'value' => 0],
            [['name', 'unp', 'bank_account_currency_id', 'email', 'phone', 'owner_id'], 'required'],
            [['phone', 'bank_account_currency_id', 'merchant_status_id', 'owner_id', 'terminal_counter_category_id', 'merchant_counter_category_id'], 'integer'],
            [['owner_id'], 'validateOwnerId'],
            [['name', 'registered_address', 'post_address'], 'string', 'max' => 255],
            [['iso_merchant_id'], 'string', 'max' => 15],
            [['email'], 'string', 'max' => 50],
            [['email'], 'email'],
            [['email'], 'unique', 'targetAttribute' => ['email', 'owner_id'],
                'message' => \Yii::t('app', 'Торговец с таким email уже существует в этом банке-эквайере.')],
            ['contract', 'string', 'max' => 50],
            ['contract_date', 'date', 'format' => 'php:Y-m-d'],
            [['unp'], 'unique', 'targetAttribute' => ['unp', 'owner_id'], 'message' => Yii::t('app', 'Торговец с таким УНП уже существует в выбранном банке-эквайере')],
            [['unp'], 'string', 'min' => 8, 'max' => 15],
            [['bic', 'iban', 'bank_account_currency_id'], 'unique', 'targetAttribute' => ['bic', 'iban', 'bank_account_currency_id', 'owner_id'],
                'when' => function ($model) {
                    return ($model->bic != '' || $model->iban != '');
                },
                'message' => \Yii::t('app', 'Такая комбинация МФО, валюты и номера счета (формат IBAN) уже существует в выбранном банке-эквайере')
            ],
            ['iban', 'validateIban'],
            ['bic', 'validateBic'],
            [['send_mail'], 'in', 'range' => array_keys(static::getNotificationChannels())],
            [['merchant_status_id'], 'exist', 'skipOnError' => true, 'targetClass' => MposMerchantStatuses::className(), 'targetAttribute' => ['merchant_status_id' => 'id']],
            [
                ['terminal_counter_category_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => CounterCategory::className(),
                'targetAttribute' => ['terminal_counter_category_id' => 'id'],
                'when' => function ($model) {
                    return ($model->terminal_counter_category_id != '0');
                },
                'message' => \Yii::t('app', 'Категория не найдена в базе')
            ],
            [
                ['merchant_counter_category_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => CounterCategory::className(),
                'targetAttribute' => ['merchant_counter_category_id' => 'id'],
                'when' => function ($model) {
                    return ($model->merchant_counter_category_id != '0');
                },
                'message' => \Yii::t('app', 'Категория не найдена в базе')
            ],
        ];
    }

    public function validateOwnerId($attribute)
    {
        if (!(Yii::$app instanceof Yii\console\Application)) {
            //если скрипт запущен не через консоль - проверяем что пользователь регистрирует торговца под своим банком-эквайером
            $owner_id = Yii::$app->user->identity->owner_id;
            if ($owner_id != 1) {
                $this->owner_id = $owner_id;
            }
        }
        if (!Owners::find()->where(['id' => $this->owner_id])->exists()) {
            $this->addError($attribute, Yii::t('app', 'Выбран несуществующий владелец'));
        }
    }

    public function validateIban($attribute)
    {

        $modifiedNumber = $this->$attribute = strtoupper(str_replace(" ", "", $this->$attribute));

        if (substr($this->$attribute, 0, 2) == 'BY' && strlen($this->$attribute) != 28) {
            $this->addError($attribute, \Yii::t('app', 'IBAN должен состоять из 28 символов'));
            return;
        }

        $regexpValidator = new RegularExpressionValidator(['pattern' => '|^[A-Z]{2}[0-9]{2}[A-Z0-9]{11,30}$|']);
        if (!$regexpValidator->validate($this->$attribute)) {
            $this->addError($attribute, \Yii::t('app', 'Неверный формат номера счета IBAN'));
            return;
        }

        $modifiedNumber = substr($modifiedNumber, 4) . substr($modifiedNumber, 0, 4);
        $modifiedNumber = str_replace(['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'],
            ['10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31', '32', '33', '34', '35'],
            $modifiedNumber);
        $modifiedNumber = ltrim($modifiedNumber, '0');

        /*
         * Если попытаться привести слишком длинную строку цифр к целому числу, php заменит его на максимальное целое число поддерживаемое данной сборкой (обычно это 2147483647)
         *  поэтому, чтобы получить остаток от деления числа на 97, мы будем менять первые 9 цифр числа на остаток от их деления
         *
         * пример:
         * 999 999 999 999 999 999 999 999  ==
         * ==  (999 999 999 * 10 в 15-й степени)  +  999 999 999 999 999  ==
         * ==  (10 309 278 * 97 + 33) * 10 в 15-й степени  +  999 999 999 999 999  ==        //здесь 33 - остаток от деления 999 999 999 на 97
         * ==  (10 309 278 * 97 * 10 в 15-й степени)  +  (33 * 10 в 15-й степени) + 999 999 999 999 999  ==
         * ==  (10 309 278 * 97 * 10 в 15-й степени)  +  33 999 999 999 999 999
         *
         *  Здесь первое слагаемое делится на 97 без остатка, а значит остаток от деления суммы (исходного числа) будет равен остатку от деления второго слагаемого,
         * что и означает, что для получения остатка можно пользоваться такой заменой
         * */

        while (strlen($modifiedNumber) > 9) {
            $num = substr($modifiedNumber, 0, 9);
            $modifiedNumber = ($num % 97) . substr($modifiedNumber, 9);
            $modifiedNumber = ltrim($modifiedNumber, '0');//обрезаем ведущие ноли, если вдруг они есть
        }

        if (($modifiedNumber % 97) != 1) {
            $this->addError($attribute, \Yii::t('app', 'Неверно введен номер счета IBAN'));
        }
    }

    public function validateBic($attribute)
    {
        $this->$attribute = strtoupper(str_replace(" ", "", $this->$attribute));
        $regexpValidator = new RegularExpressionValidator(['pattern' => '|^[A-Z0-9]{4}[A-Z]{2}[A-Z0-9]{2}([A-Z0-9]{3})?$|']);

        if (!$regexpValidator->validate($this->$attribute)) {
            $this->addError($attribute, \Yii::t('app', 'Неверный формат кода банка'));
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'owner_id' => \Yii::t('app', 'Владелец'),
            'name' => \Yii::t('app', 'Наименование предприятия торговли и сервиса'),
            'unp' => \Yii::t('app', 'УНП'),
            'bic' => \Yii::t('app', 'Код банка (в формате IBAN)'),
            'iban' => \Yii::t('app', 'Расчетный счет (в формате IBAN)'),
            'bank_account_currency_id' => \Yii::t('app', 'Валюта счета'),
            'email' => 'Email',
            'phone' => \Yii::t('app', 'Мобильный телефон (в формате ZZZXXYYYYYYY)'),
            'send_mail' => Yii::t('app', 'Канал отправки кодов'),
            'merchant_status_id' => \Yii::t('app', 'Статус'),
            'contract' => \Yii::t('app', 'Номер договора'),
            'contract_date' => \Yii::t('app', 'Дата заключения договора обслуживания'),
            'post_address' => \Yii::t('app', 'Почтовый адрес'),
            'registered_address' => \Yii::t('app', 'Юридический адрес'),
            'iso_merchant_id' => Yii::t('app', 'ID торговца в процессинговом центре'),
            'terminal_counter_category_id' => \Yii::t('app', 'Категория лимитов терминала'),
            'merchant_counter_category_id' => \Yii::t('app', 'Категория лимитов торговца'),
            'created_at' => \Yii::t('app', 'Дата регистрации'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMposMerchantStatus()
    {
        return $this->hasOne(MerchantStatusLocale::className(), ['id' => 'merchant_status_id'])
            ->andOnCondition([MerchantStatusLocale::tableName() . '.locale' => Yii::$app->language]);
    }

    public function getCurrency()
    {
        return $this->hasOne(Currency::className(), ['id' => 'bank_account_currency_id']);
    }

    public function getOwner()
    {
        return $this->hasOne(Owners::className(), ['id' => 'owner_id'])
            ->andOnCondition([Owners::tableName() . '.locale' => Yii::$app->language]);
    }

    public function getChiefCashier()
    {
        return $this->hasOne(MerchantCashier::className(), ['id' => 'chief_cashier_id']);
    }

    public function getTerminalCounterCategory()
    {
        return $this->hasOne(CounterCategory::className(), ['id' => 'terminal_counter_category_id']);
    }

    public function getMerchantCounterCategory()
    {
        return $this->hasOne(CounterCategory::className(), ['id' => 'merchant_counter_category_id']);
    }

    public static function generatePassword()
    {
        $allowedSymbolsString = "abcdefghijkmnopqrstuwxyzABCDEFGHJKLMNPQRSTUWXYZ123456789-_";
        $password = '';
        $lastAllowedSymbolIndex = strlen($allowedSymbolsString) - 1;
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, $lastAllowedSymbolIndex);
            $password .= $allowedSymbolsString[$n];
        }
        return $password;
    }

    public function sendActivationCodes()
    {
        $codes = ArrayHelper::getColumn(
            PaymentsTerminals::find()
                ->select('activation_code')
                ->where([
                    'merchant_id' => $this->id,
                    'status' => 1,
                    'ped_id' => 0,
                    'is_softpos' => 1
                ])
                ->all(),
            'activation_code');
        $count = count($codes);
        if ($count > 0) {
            $res = Notification::sendToMerchant($this->id, 'merchant.activation.codes', ['codes' => implode(', ', $codes)]);
            if ($res !== true) {
                Yii::$app->session->addFlash(
                    'error',
                    Yii::t(
                        'app',
                        'Сообщение не отправлено: {errorMessage}',
                        ['errorMessage' => $res]
                    )
                );
            }
        }

        Yii::$app->session->setFlash('paymentTerminalActivationCodesSent', $count);
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => false,
                'value' => new Expression('NOW()'),
            ],
        ];
    }
}
