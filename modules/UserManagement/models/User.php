<?php

namespace app\modules\UserManagement\models;

use app\modules\UserManagement\helpers\AwsCognitoHelper;
use Aws\Exception\AwsException;
use Yii;
use yii\base\NotSupportedException;
use yii\rbac\Role;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property int $owner_id
 * @property string $username
 * @property string $auth_key
 * @property string $password_hash
 * @property int $temp_password
 * @property int $temp_user
 * @property string $temp_user_active_from
 * @property string $temp_user_active_to
 * @property int $status
 * @property int $block_reason
 * @property string $description
 * @property string $phone
 * @property string $email
 */
class User extends \yii\db\ActiveRecord implements IdentityInterface
{
    public $password;

    public static function getStatuses()
    {
        return [
            0 => \Yii::t('app', 'Заблокирован'),
            1 => \Yii::t('app', 'Активен'),
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $rules = [
            [['owner_id'], 'required'],
            [['owner_id', 'phone'], 'integer'],
            [['password'], 'required', 'on' => 'create'],
            [['password', 'description', 'email'], 'string', 'max' => 255],
        ];

        $authChannel = Yii::$app->getModule('user-management')->params['authChannel'];

        if ($authChannel == 2) {
            //Для того, чтобы не давать редактировать логин, когда включена авторизация через AWS Cognito,
            //правила валидации 'username' прописываем только для сценария 'create'
            $rules[] = [['username'], 'required', 'on' => 'create'];
            $rules[] = [['username'], 'string', 'max' => 32, 'on' => 'create'];
            $rules[] = [['username'], 'unique', 'on' => 'create'];
            $rules[] = [
                ['password'],
                'match',
                'pattern' => '|^\S*(?=\S{8,})(?=\S*[A-ZА-ЯЁ])(?=\S*[a-zа-яё])(?=\S*[\d])(?=\S*[\=\+\-\^\$\*\.\[\]\{\}\(\)\?\"\!\@\#\%\&\/\\\,\>\<\'\:\;\|\_\~\`])\S*$|',
                'message' => Yii::t('app', 'Пароль должен быть не короче 8 символов, содержать буквы верхнего и нижнего регистра, цифры и спецсимволы')
            ];
        } else {
            $rules[] = [['username'], 'required'];
            $rules[] = [['username'], 'string', 'max' => 32];
            $rules[] = [['username'], 'unique'];
            //эти поля поодерживаются только при авторизации через БД
            if ($authChannel == 0) {
                $rules[] = [['temp_user_active_from', 'temp_user_active_to'], 'safe'];
                $rules[] = [['temp_user'], 'in', 'range' => [0, 1]];
            }
        }

        return $rules;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'owner_id' => \Yii::t('app', 'Владелец'),
            'username' => \Yii::t('app', 'Логин'),
            'auth_key' => 'Auth Key',
            'password_hash' => 'Password Hash',
            'password' => \Yii::t('app', 'Пароль'),
            'temp_password' => \Yii::t('app', 'Временный пароль'),
            'temp_user' => \Yii::t('app', 'Временный пользователь'),
            'temp_user_active_from' => \Yii::t('app', 'Активен с'),
            'temp_user_active_to' => \Yii::t('app', 'Активен по'),
            'status' => \Yii::t('app', 'Статус'),
            'block_reason' => \Yii::t('app', 'Причина блокировки'),
            'description' => \Yii::t('app', 'Описание'),
            'phone' => \Yii::t('app', 'Телефон'),
            'email' => 'Email',
        ];
    }

    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id]);
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username]);
    }

    public function getId()
    {
        return $this->getPrimaryKey();
    }

    public function getAuthKey()
    {
        return $this->auth_key;
    }

    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString(32);
    }

    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        if ($insert) {
            $this->generateAuthKey();
        }

        if (isset($this->password) && $this->password !== '') {
            $this->setPassword($this->password);
            if (!Yii::$app->user->isGuest && Yii::$app->user->id !== $this->id) {
                $this->temp_password = 1;
            }
        }

        return true;
    }

    public function getRoles()
    {
        return $this->hasMany(AuthItem::className(), ['name' => 'item_name'])->viaTable('auth_assignment', ['user_id' => 'id']);
    }

    public function setRoles($roles)
    {
        $userRoles = array_keys(Yii::$app->authManager->getRolesByUser($this->id));
        $rolesToRevoke = array_diff($userRoles, $roles);
        $rolesToAdd = array_diff($roles, $userRoles);

        $transaction = Yii::$app->db->beginTransaction();
        try {
            foreach ($rolesToRevoke as $role) {
                Yii::$app->getAuthManager()->revoke(Yii::$app->getAuthManager()->getRole($role), $this->id);
            }
            foreach ($rolesToAdd as $role) {
                Yii::$app->getAuthManager()->assign(Yii::$app->getAuthManager()->getRole($role), $this->id);
            }
            $transaction->commit();
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        } catch (\Throwable $e) {
            $transaction->rollBack();
            throw $e;
        }

        Yii::$app->session->setFlash('userRolesAssigned');
    }


    /**
     * creates new user in cognito user pool.
     * @return bool whether the user is created successfully
     */
    public function createAwsCognitoUser()
    {
        if ($this->isNewRecord) {
            //вначале должен выполняться $model->save()
            //во первых, он провалидирует данные формы
            //во вторых, так гораздо проще откатить изменения в случае ошибки
            return false;
        }

        Yii::info('Sending AWS adminCreateUser request');

        try {
            $client = AwsCognitoHelper::getClient();
            $requestParams = [
                'DesiredDeliveryMediums' => ['EMAIL'],
                'MessageAction' => 'SUPPRESS',
                'TemporaryPassword' => $this->password,
                'UserPoolId' => Yii::$app->getModule('user-management')->params['awsParams']['AWS_USERPOOL_ID'],
                'Username' => $this->username,
            ];

            /*if (isset($this->phone) && $this->phone != '') {
                $requestParams['UserAttributes'][] = [
                    'Name' => 'phone_number',
                    'Value' => $this->phone,
                ];
                $requestParams['UserAttributes'][] = [
                    'Name' => 'phone_number_verified',
                    'Value' => 'true',
                ];
            }

            if (isset($this->email) && $this->email != '') {
                $requestParams['UserAttributes'][] = [
                    'Name' => 'email',
                    'Value' => $this->email,
                ];
                $requestParams['UserAttributes'][] = [
                    'Name' => 'email_verified',
                    'Value' => 'true',
                ];
            }*/

            $response = $client->adminCreateUser($requestParams);

            if (YII_DEBUG) {
                Yii::info('AWS adminCreateUser response: ' . print_r($response, true));
            }

            return true;

        } catch (AwsException $e) {
            if ($e->getAwsErrorCode() == 'InvalidPasswordException') {
                Yii::warning('AWS Exception: ' . $e->getAwsErrorMessage());
                $this->addError('password', \Yii::t('app', 'Пароль не удовлетворяет требованиям безопасности'));
                return false;
            } else {
                Yii::error($e->getAwsErrorMessage());
                throw $e;
            }
        }
    }

    /**
     * deletes user from cognito user pool.
     * @return bool whether the user was deleted successfully
     */
    public function deleteAwsCognitoUser()
    {
        Yii::info('Sending AWS adminDeleteUser request');

        try {
            $client = AwsCognitoHelper::getClient();
            $response = $client->adminDeleteUser([
                'UserPoolId' => Yii::$app->getModule('user-management')->params['awsParams']['AWS_USERPOOL_ID'],
                'Username' => $this->username,
            ]);

            if (YII_DEBUG) {
                Yii::info('AWS adminDeleteUser response: ' . print_r($response, true));
            }

            return true;

        } catch (AwsException $e) {
            if ($e->getAwsErrorCode() == 'UserNotFoundException') {
                Yii::warning('AWS Exception: ' . $e->getAwsErrorMessage());
                return true;
            } else {
                Yii::error($e->getAwsErrorMessage());
                throw $e;
            }
        }
    }

    /**
     * disables aws cognito user
     * @return bool whether the user was disabled successfully
     */
    public function changeAwsCognitoUserStatus($status)
    {
        Yii::info('Sending AWS '. ($status ? 'adminEnableUser' : 'adminDisableUser').' request');

        try {
            $client = AwsCognitoHelper::getClient();
            $requestParams = [
                'UserPoolId' => Yii::$app->getModule('user-management')->params['awsParams']['AWS_USERPOOL_ID'],
                'Username' => $this->username,
            ];

            if ($status) {
                $response = $client->adminEnableUser($requestParams);
            } else {
                $response = $client->adminDisableUser($requestParams);
            }

            if (YII_DEBUG) {
                Yii::info('AWS '. ($status ? 'adminEnableUser' : 'adminDisableUser').' response: ' . print_r($response, true));
            }

            return true;

        } catch (AwsException $e) {
            Yii::error($e->getAwsErrorMessage());
            throw $e;
        }
    }

    public function updateUserStatusFromAwsCognito() {
        Yii::info('Sending AWS adminGetUser request');

        try {
            $client = AwsCognitoHelper::getClient();
            $response = $client->adminGetUser([
                'UserPoolId' => Yii::$app->getModule('user-management')->params['awsParams']['AWS_USERPOOL_ID'],
                'Username' => $this->username,
            ]);

            if (YII_DEBUG) {
                Yii::info('AWS adminGetUser response: ' . print_r($response, true));
            }

        } catch (AwsException $e) {
            Yii::error($e->getAwsErrorMessage());
            throw $e;
        }

        if ($response->get('Enabled') != $this->status) {//сравнение boolean с int
            $this->status = $response->get('Enabled') ? 1 : 0;
            $this->save(false);
        }
    }

    public function setTemporaryPasswordForAwsCognito() {
        Yii::info('Sending AWS adminSetUserPassword request');

        try {
            $client = AwsCognitoHelper::getClient();
            $response = $client->adminSetUserPassword([
                'Password' => $this->password,
                'Permanent' => false,
                'UserPoolId' => Yii::$app->getModule('user-management')->params['awsParams']['AWS_USERPOOL_ID'],
                'Username' => $this->username,
            ]);

            if (YII_DEBUG) {
                Yii::info('AWS adminSetUserPassword response: ' . print_r($response, true));
            }

            return true;

        } catch (AwsException $e) {
            Yii::error($e->getAwsErrorMessage());
            throw $e;
        }
    }
}
