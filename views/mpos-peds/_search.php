<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MposPedsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mpos-peds-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'mpos_ped_model_id') ?>

    <?= $form->field($model, 'serial_number') ?>

    <?= $form->field($model, 'mpos_merchant_id') ?>

    <?= $form->field($model, 'login') ?>

    <?php // echo $form->field($model, 'password') ?>

    <?php // echo $form->field($model, 'login_last_date') ?>

    <?php // echo $form->field($model, 'login_attempts_count') ?>

    <?php // echo $form->field($model, 'mpos_ped_status_id') ?>

    <?php // echo $form->field($model, 'description') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
