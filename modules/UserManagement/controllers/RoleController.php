<?php

namespace app\modules\UserManagement\controllers;

use Yii;
use app\modules\UserManagement\models\AuthItem;
use app\modules\UserManagement\models\RoleSearch;
use yii\base\DynamicModel;
use yii\filters\AccessControl;
use yii\rbac\Item;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * RoleController implements the CRUD actions for AuthItem model.
 */
class RoleController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view'],
                        'roles' => ['roleView'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['create', 'update', 'manage-role-permissions', 'delete'],
                        'roles' => ['roleEdit'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all AuthItem models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RoleSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single role with permissions.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'role' => $this->findModel($id),
            //'allPermissions' => Yii::$app->authManager->getPermissions(),
            'rolePermissions' => Yii::$app->authManager->getPermissionsByRole($id),
        ]);
    }

    public function actionManageRolePermissions($id)
    {
        $role = $this->findModel($id);
        $model = new DynamicModel(['permissions']);
        $allPermissions = Yii::$app->authManager->getPermissions();
        $model->addRule('permissions', 'each', ['rule' => ['in', 'range' => array_keys($allPermissions)]]);

        if ($model->load(Yii::$app->request->post()) && $model->validate()){
            $rolePermissions = Yii::$app->authManager->getPermissionsByRole($id);
            $permissionsToAdd = array_diff($model->permissions, array_keys($rolePermissions));
            $permissionsToRevoke = array_diff(array_keys($rolePermissions), $model->permissions);
            $transaction = Yii::$app->db->beginTransaction();
            try {
                foreach($permissionsToRevoke as $permission){
                    Yii::$app->getAuthManager()->removeChild($role, Yii::$app->authManager->getPermission($permission));
                }
                foreach($permissionsToAdd as $permission){
                    Yii::$app->getAuthManager()->addChild($role, Yii::$app->authManager->getPermission($permission));
                }
                $transaction->commit();
            } catch (\Exception $e) {
                $transaction->rollBack();
                throw $e;
            } catch (\Throwable $e) {
                $transaction->rollBack();
                throw $e;
            }

            Yii::$app->session->setFlash('rolePermissionsAssigned');
        } else {
            Yii::$app->session->setFlash('invalidDataRecieved');
        }

        return $this->redirect(['view', 'id' => $id]);
    }

    /**
     * Creates a new AuthItem model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        //!!!Сохранение через ActiveRecord->save() заглушено в модели!!!
        $model = new AuthItem();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            $role = Yii::$app->getAuthManager()->createRole($model->name);
            $role->description = $model->description;
            Yii::$app->getAuthManager()->add($role);

            return $this->redirect(['index']);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing role.
     * If update is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        //!!!Сохранение через ActiveRecord->save() заглушено в модели!!!
        $role = $this->findModel($id);//сама роль
        $model = AuthItem::findOne(['name' => $id, 'type' => Item::TYPE_ROLE]);//модель ActiveRecord используется для валидации
        if($model === null){
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $role->name = $model->name;
            $role->description = $model->description;
            Yii::$app->authManager->update($id, $role);
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing AuthItem model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        Yii::$app->authManager->remove($this->findModel($id));

        return $this->redirect(['index']);
    }

    /**
     * Finds the AuthItem model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return AuthItem the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Yii::$app->authManager->getRole($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
