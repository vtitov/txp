<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TransactionslogArchive */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="transactionslog-archive-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'dt')->textInput() ?>

    <?= $form->field($model, 'trx_statuses_id')->textInput() ?>

    <?= $form->field($model, 'trx_types_id')->textInput() ?>

    <?= $form->field($model, 'iso_setting_id')->textInput() ?>

    <?= $form->field($model, 'payment_id')->textInput(['maxlength' => 9]) ?>

    <?= $form->field($model, 'payment_terminal_id')->textInput(['maxlength' => 10]) ?>

    <?= $form->field($model, 'currency')->textInput(['maxlength' => 3]) ?>

    <?= $form->field($model, 'amount')->textInput() ?>

    <?= $form->field($model, 'bdd')->textInput() ?>

    <?= $form->field($model, 'batch')->textInput() ?>

    <?= $form->field($model, 'reconcilationFlag')->textInput() ?>

    <?= $form->field($model, 'MTI')->textInput() ?>

    <?= $form->field($model, 'rrn')->textInput(['maxlength' => 12]) ?>

    <?= $form->field($model, 'ksn')->textInput(['maxlength' => 20]) ?>

    <?= $form->field($model, 'expdate')->textInput(['maxlength' => 4]) ?>

    <?= $form->field($model, 'authcode')->textInput(['maxlength' => 6]) ?>

    <?= $form->field($model, 'acquircode')->textInput() ?>

    <?= $form->field($model, 'stan')->textInput() ?>

    <?= $form->field($model, 'sred')->textInput(['maxlength' => 100]) ?>

    <?= $form->field($model, 'device')->textInput(['maxlength' => 8]) ?>

    <?= $form->field($model, 'response_code')->textInput(['maxlength' => 3]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
