<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\RecievedReviews */

$this->title = 'Update Recieved Reviews: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Recieved Reviews', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="recieved-reviews-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
