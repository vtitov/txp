<?php

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'language' => 'ru',
    'bootstrap' => ['log'],
    'components' => [
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
            // uncomment if you want to cache RBAC items hierarchy
            // 'cache' => 'cache',
        ],
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'NqcPpClvz4D-uiOA7NZCd_E41mENLnOs',
            'csrfCookie' => [
                'httpOnly' => true,
                'secure' => true,
            ],
        ],
        'session' => [
            'cookieParams' => [
                'httpOnly' => true,
                'secure' => true,
            ],
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\modules\UserManagement\models\User',
            'loginUrl' => ['/user-management/auth/login'],
            'enableAutoLogin' => false,//разрешить авторизацию через cookies
            'authTimeout' => 15*60,//логаут после периода бездействия (в секундах), работает только при 'enableAutoLogin' => false
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
            ],
        
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning', 'info', 'trace'],
                    'logFile' => '@runtime/logs/mpos_arm_log_' . date('Y-m-d') . '.log',
                    'logVars' => ['_GET', '_FILES', '_COOKIE', '_SESSION'], //, '_POST', '_SERVER'
                    'maxLogFiles' => 500,
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
	'mpos' => require(__DIR__ . '/mpos.php'),
        't2p_monitoring' => require(__DIR__ . '/t2p_monitoring.php'),
        'paybycard' => require(__DIR__ . '/paybycard.php'),
        'transactionslog' => require(__DIR__ . '/transactionslog.php'),
        'i18n' => [
            'translations' => [
                'app*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    //'basePath' => '@app/messages',
                    'sourceLanguage' => 'ru',
                    'fileMap' => [
                        'app' => 'app.php',
                    ],
                ],
                '*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                ],
            ],
        ],
    ],
	'modules'=>[
        'user-management' => [
            'class' => 'app\modules\UserManagement\Module',
            'params' => [
                //способ авторизации
                //0 - авторизация через пароль, хранящийся в БД
                //1 - двухфакторная через OpenOTP
                //2 - двухфакторная через AWS Cognito
                'authChannel' => 2,

                //AWS settings
                'awsParams' => require(__DIR__ . '/aws_params.php'),

                //OpenOTP settings
                'openOtpUrl' => 'https://mfa.pcidss.loc/websrvs/wsdl.php?websrv=openotp',


                //Эти настройки играют роль только когда вЫключена двухфакторная авторизация
                //при включенной двухфакторной авторизации всеми этими параметрами управляет домен
                /*
                 * шаблон сложности пароля
                 *
                 * примеры:
                 * '|^\S*(?=\S{7,})(?=\S*[a-zA-Zа-яА-ЯёЁ])(?=\S*[\d])\S*$|' - Пароль должен быть не короче 7 символов и содержать буквы и цифры
                 * '|^.{1,}$|' - Пароль должен содержать хотя бы 1 любой символ
                 * */
                //'passwordPattern' => '|^\S*(?=\S{7,})(?=\S*[a-zA-Zа-яА-ЯёЁ])(?=\S*[\d])\S*$|',
                'passwordPattern' => '|^.{4,}$|',
                //Сообщение о недостаточной сложности пароля
                'weakPasswordMessage' => 'Пароль должен быть не короче 4 символов',
                //Сколько предыдущих паролей пользователя должно отличаться от его нового пароля
                'passwordHistoryLength' => 1,
                //срок действия пароля в днях (0 - бессрочный)
                'passwordExpire' => 0,
                //кол-во неверных вводов пароля, после которых пользователь блокируется (0 - не блокировать)
                'failedLoginAttemptsCountBeforeLock' => 3,
                //период блокировки, после неудачных вводов парлей (в секундах)
                'failedLoginAttemptsLockTime' => 60,
            ],
        ],
	],
    'params' => $params,
    'on beforeRequest' => function () {
        $language = Yii::$app->request->cookies->getValue('mpos-arm-language');
        if (in_array($language, Yii::$app->params['availableTranslations'])) {
        Yii::$app->language = $language;
        } else {
        Yii::$app->language = 'ru';
        }
    },
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = 'yii\debug\Module';

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = 'yii\gii\Module';
}

return $config;
