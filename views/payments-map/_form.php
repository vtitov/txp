<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\PaymentsMap */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="payments-map-form">

    <?php $form = ActiveForm::begin(); ?>

    <!--?= $form->field($model, 'ped_id')->textInput(['maxlength' => 10]) ?-->

    <!--?= $form->field($model, 'payments_id')->textInput(['maxlength' => 9]) ?-->

    <?= $form->field($model, 'currency_id')->dropDownList(
        ArrayHelper::map(\app\models\Currency::find()->asArray()->all(), 'id', 'alpha_code')
    ) ?>

    <?= $form->field($model, 'fits_groups_id')->dropDownList(
        ArrayHelper::map(\app\models\FitsGroups::find()->asArray()->all(), 'id', 'name')
    ) ?>

    <?= $form->field($model, 'fits_groups_id_sign_allowed')->dropDownList(
        ArrayHelper::map(\app\models\FitsGroups::find()->asArray()->all(), 'id', 'name')
    ) ?>

    <?= $form->field($model, 'fits_groups_id_fallback_allowed')->dropDownList(
        ArrayHelper::map(\app\models\FitsGroups::find()->asArray()->all(), 'id', 'name')
    ) ?>

    <div class="form-group">
        <?= Html::submitButton(\Yii::t('app', 'Сохранить'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary', 'id' => 'saveButton']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
