<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\MposPeds */
/* @var $form yii\widgets\ActiveForm */

$confCategories = \app\models\PedConfCategory::find()->all();
$confCategoryDropdown = [];
foreach ($confCategories as $cat) {
    $confCategoryDropdown[$cat->id] = $cat->name;
    if ($cat->desc != '') {
        $confCategoryDropdown[$cat->id] .= ' (' . $cat->desc . ')';
    }
}
?>

<div class="mpos-peds-form">

    <?php $form = ActiveForm::begin(); ?>


    <?= $form->field($model, 'ped_model_id')->dropDownList(
        ArrayHelper::map(app\models\MposPedModels::find()->asArray()->all(), 'id', 'name')
    ) ?>

    <?= $form->field($model, 'serial_number')->textInput(['maxlength' => true, 'disabled' => !$model->isNewRecord])->label(\Yii::t('app', 'Серийный номер')) ?>

    <?= $form->field($model, 'ped_status_id')->dropDownList(
        ArrayHelper::map(
            app\models\PedStatusLocale::find()
                ->where(['locale' => Yii::$app->language])
                ->asArray()
                ->all(),
            'id',
            'name'
        )
    ) ?>

    <?= $form->field($model, 'owner_id')->dropDownList(
        ArrayHelper::map(app\models\Owners::find()->andWhere(['locale' => Yii::$app->language])->all(), 'id', 'name'),
        ['prompt' => '', 'disabled' => Yii::$app->user->identity->owner_id != 1]
    ) ?>

    <?= $form->field($model, 'paymentTerminalName')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ped_conf_category_id')->dropDownList($confCategoryDropdown) ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'mcc')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'region')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'district')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'locality')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'location')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'counter_category_id')->dropDownList(
        ArrayHelper::map(app\models\CounterCategory::find()->select(['id', 'name'])->asArray()->all(), 'id', 'name'),
        ['prompt' => '']
    ) ?>

    <h3><?= Yii::t('app', 'Разрешенные способы оплаты') ?></h3>
    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'magStripe')->checkbox() ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'emv')->checkbox() ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'contactless')->checkbox() ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'manualPanEntry')->checkbox() ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton(\Yii::t('app', 'Сохранить'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary', 'id' => 'submit-button']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
