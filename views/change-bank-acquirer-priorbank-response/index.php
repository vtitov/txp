<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ChangeBankAcquirerPriorbankResponseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Change Bank Acquirer Priorbank Responses';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="change-bank-acquirer-priorbank-response-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Change Bank Acquirer Priorbank Response', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'request_id',
            'file_number',
            'name_legal_entity',
            'unp',
            // 'email:email',
            // 'pos_info',
            // 'contract',
            // 'contract_date',
            // 'trading_name',
            // 'phone_trading_name',
            // 'mcc',
            // 'country',
            // 'region',
            // 'locality',
            // 'address',
            // 'terminal_id',
            // 'merchant_id',
            // 'parse_date',
            // 'status',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
