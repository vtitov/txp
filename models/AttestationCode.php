<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "attestation_code".
 *
 * @property int $attestation_code
 * @property string $name
 * @property string|null $description
 * @property int $risk_severity_id
 * @property int|null $importance
 * @property string $scope
 * @property int $in_request
 */
class AttestationCode extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'attestation_code';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('t2p_monitoring');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            /*'attestation_code' => Yii::t('app', 'Attestation Code'),
            'name' => Yii::t('app', 'Name'),
            'description' => Yii::t('app', 'Description'),
            'risk_severity_id' => Yii::t('app', 'Risk Severity ID'),
            'importance' => Yii::t('app', 'Importance'),
            'scope' => Yii::t('app', 'Scope'),
            'in_request' => Yii::t('app', 'In Request'),*/
        ];
    }
}
