<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use app\helpers\RandomImageHelper;
use yii\helpers\ArrayHelper;

//use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MposPeds */

$this->title = $model->serial_number;
$this->params['breadcrumbs'][] = ['label' => \Yii::t('app', 'Справочник устройств'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$trustLevelFilter = ArrayHelper::map(
    \app\models\TrustLevelLocale::find()
        ->select(['id', 'short_desc'])
        ->where(['locale' => Yii::$app->language])
        ->asArray()
        ->all(),
    'id',
    'short_desc'
);
$eventCodeFilter = ArrayHelper::map(
    \app\models\EventCodeLocale::find()->select(['id', 'desc'])->where(['locale' => Yii::$app->language])->asArray()->all(),
    'id',
    'desc'
);

$this->registerCss('.glyphicon-ok{color:green}.glyphicon-remove{color:red}');
?>
    <div class="mpos-peds-view">

        <h1><?= Html::encode($this->title) ?></h1>
        <img src="<?= RandomImageHelper::getRandomImageFromDir('img/peds') ?>" height="140px" align="right"
             style="margin-top: -130px">

        <p>
            <?= Html::a(\Yii::t('app', 'Редактировать'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(\Yii::t('app', 'Удалить'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => \Yii::t('app', 'Вы действительно хотите удалить эту запись?'),
                    'method' => 'post',
                ],
            ]) ?>
        </p>

        <div class="row">

            <div class="col-md-6">

                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        //'id',

                        'serial_number',
                        [
                            'attribute' => 'ped_model_id',
                            'value' => $model->mposPedModel->name . (
                                $model->mposPedModel->pts_appr_number != '' ?
                                    '<span style="color: gray; margin-left: 20px"> PTS SSC Approval Number: '
                                    . $model->mposPedModel->pts_appr_number
                                    . ', ' . Yii::t('app', 'окончание: ')
                                    . $model->mposPedModel->pts_exp_date . '</span>' :
                                    ''),
                            'format' => 'html',
                        ],
                        [
                            'attribute' => 'owner_id',
                            'value' => $model->owner ? $model->owner->name : $model->owner_id,
                        ],
                        [
                            'attribute' => 'merchant_id',
                            'value' => $model->mposMerchant ?
                                Html::a(Html::encode($model->mposMerchant->name), ['mpos-merchants/view', 'id' => $model->merchant_id]) :
                                null,
                            'format' => 'html',
                        ],
                        [
                            'attribute' => 'ped_status_id',
                            'value' => $model->mposPedStatus->name,
                        ],
                        [
                            'attribute' => 'payment_terminal_id',
                            'value' => Html::a(Html::encode($model->paymentTerminalName), ['payments-terminals/view', 'id' => $model->payment_terminal_id]) .
                                Html::img('@web/img/owners/' . $model->getPaymentTerminalOwner() . '.png', ['style' => 'margin-left: 20px']),
                            'format' => 'html',
                            'label' => \Yii::t('app', 'Платежный терминал'),
                        ],
                        'description',
                        'add_date',
                        'mcc',
                        [
                            'attribute' => 'mcc',
                            'value' => $model->mcc ? (($mcc = \app\models\Mcc::find()->andWhere("range_start_code <= $model->mcc")->andWhere("range_end_code >= $model->mcc")->one()) ? $mcc->description : '') : '',
                            'label' => \Yii::t('app', 'Описание MCC-кода'),
                        ],
                        'region',
                        'district',
                        'locality',
                        'location',
                        [
                            'attribute' => 'ped_conf_category_id',
                            'value' => getConfCategoryHtml($model),
                            'format' => 'html',
                        ],
                        [
                            'attribute' => 'ped_caps',
                            'label' => Yii::t('app', 'Разрешенные способы оплаты'),
                            'value' => getPedCapsHtml($model),
                            'format' => 'html',
                        ],
                        'app_ver',
                        [
                            'attribute' => 'os_id',
                            'value' => \yii\helpers\ArrayHelper::getValue($model->os, 'name', $model->os_id),
                        ],
                        [
                            'attribute' => 'counter_category_id',
                            'value' => \yii\helpers\ArrayHelper::getValue($model->counterCategory, 'name'),
                        ],
                    ],
                ]) ?>

            </div>

            <div class="col-md-6">

                <?php if ($model->deviceProfile === null): ?>
                    <div class="alert alert-warning">
                        <?= Yii::t('app', 'Профиль устройства не найден') ?>
                    </div>
                <?php else: ?>
                    <?= DetailView::widget([
                        'model' => $model->deviceProfile,
                        'attributes' => [
                            [
                                'attribute' => 'mask',
                                'value' => str_replace(';', ";\n", $model->deviceProfile->mask),
                                'format' => 'ntext',
                            ],
                            //'mask_hash',
                            //'status',
                            'current_locale',
                            'created_at',
                            [
                                'attribute' => 'trust_level_id',
                                'value' => \yii\helpers\ArrayHelper::getValue(
                                    $model->deviceProfile->trustLevel,
                                    'short_desc',
                                    $model->deviceProfile->trust_level_id
                                ),
                            ],
                        ],
                    ]) ?>
                <?php endif; ?>

            </div>

        </div>

    </div>

<?php
$sm = new \app\models\MposTransactionsSearch();
$sm->mpos_ped_id = $model->serial_number;
$f = \yii\widgets\ActiveForm::begin([
    'action' => ['mpos-transactions/index'],
    'method' => 'post',
]);
echo $f->field($sm, 'mpos_ped_id')->hiddenInput()->label(false);
echo Html::submitButton(\Yii::t('app', 'Просмотр транзакций по терминалу'), ['class' => 'btn btn-primary', 'name' => 'action', 'value' => 'render']);
\yii\widgets\ActiveForm::end();
?>

    <div class="payments-map">

        <h2><?= Yii::t('app', 'Добавленные платежи') ?></h2>

        <?= Html::a(count($paymentsMapDataProvider->getModels()) > 0 ?
                \Yii::t('app', 'Переназначить платежи') : \Yii::t('app', 'Назначить платежи'),
            ['assign-payments', 'id' => $model->id], ['class' => 'btn btn-success']) ?>

        <?= GridView::widget([
            'dataProvider' => $paymentsMapDataProvider,
            //'filterModel' => $paymentsMapSearchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                //'ped_id',
                [
                    'attribute' => 'payments_id',
                    'value' => 'payments.name',
                ],
                [
                    'attribute' => 'currency_id',
                    'value' => 'currency.alpha_code',
                ],
                [
                    'attribute' => 'fits_groups_id',
                    'value' => 'fitsGroups.name',
                ],
                [
                    'attribute' => 'fits_groups_id_sign_allowed',
                    'value' => 'fitsGroupsSign.name',
                ],
                [
                    'attribute' => 'fits_groups_id_fallback_allowed',
                    'value' => 'fitsGroupsFallback.name',
                ],

                [
                    'class' => 'yii\grid\ActionColumn',
                    'controller' => 'payments-map',
                    'template' => '{update}',
                ],
            ],
        ]); ?>

    </div>


    <div class="device-event-log">

        <h2><?= Yii::t('app', 'Журнал событий') ?></h2>

        <?= GridView::widget([
            'dataProvider' => $eventLogDataProvider,
            'filterModel' => $eventLogSearchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                //'id',
                [
                    'attribute' => 'merchant_id',
                    'value' => function ($model) {
                        if ($model->merchant) {
                            return Html::a(
                                Html::encode($model->merchant->name),
                                ['mpos-merchants/view', 'id' => $model->merchant_id]
                            );
                        } else {
                            return $model->merchant_id;
                        }
                    },
                    'format' => 'html'
                ],
                //'device_id',
                [
                    'attribute' => 'old_trust_level_id',
                    'value' => function ($model) use ($trustLevelFilter) {
                        return ArrayHelper::getValue($trustLevelFilter, $model->old_trust_level_id, $model->old_trust_level_id);
                    },
                    'filter' => $trustLevelFilter,
                ],
                [
                    'attribute' => 'new_trust_level_id',
                    'value' => function ($model) use ($trustLevelFilter) {
                        return ArrayHelper::getValue($trustLevelFilter, $model->new_trust_level_id, $model->new_trust_level_id);
                    },
                    'filter' => $trustLevelFilter,
                ],
                //'app_version',
                //'mas_version',
                [
                    'attribute' => 'event_code_id',
                    'value' => function ($model) use ($eventCodeFilter) {
                        return ArrayHelper::getValue($eventCodeFilter, $model->event_code_id, $model->event_code_id);
                    },
                    'filter' => $eventCodeFilter,
                ],
                //'attest_code',
                //'counter_id',
                //'cnt_value',
                //'cnt_limit_value',
                //'cnt_day',
                //'cnt_currency',
                //'event_source_id',
                \app\helpers\DateColumnHelper::getColumn('created_at', $eventLogSearchModel),
                //'description',

                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{view-event}',
                    'buttons' => [
                        'view-event' => function($url, $model, $key) {
                            return Html::a('<span class="glyphicon glyphicon-eye-open"/>', $url);
                        }
                    ]
                ],
            ],
        ]); ?>

    </div>


<?php
function getConfCategoryHtml($model)
{
    if ($model->pedConfCategory === null) {
        return '<span class="not-set">(' . \Yii::t('app', 'неизвестная группа конфигурации') . ')</span>';
    }

    $confCategoryName = $model->pedConfCategory->name;
    if ($model->pedConfCategory->desc != '') {
        $confCategoryName .= ' (' . Html::encode($model->pedConfCategory->desc) . ')';
    }

    $isoSetting = \yii\helpers\ArrayHelper::getValue($model->paymentTerminal, 'isoSettings');
    if ($isoSetting && $isoSetting->ped_conf_category_id != $model->ped_conf_category_id) {
        return Html::encode($confCategoryName) .
        \yii\bootstrap\Alert::widget([
            'options' => [
                'class' => 'alert-warning',
            ],
            'closeButton' => false,
            'body' => Html::encode(
                    Yii::t('app', 'Для устройства выбрана группа конфигурации, отличная от той, что задана по умолчанию для ISO настройки')
                ) . '<br>' . Html::a(
                    Html::encode(Yii::t('app', 'Восстановить значение по умолчанию')),
                    ['reset-conf-category', 'id' => $model->id],
                    ['class' => 'btn btn-warning', 'align' => 'right']
                ),
        ]);
    } else {
        return Html::encode($confCategoryName);
    }
}

/*
 * returns html string for displaying ped_caps property
 * @param MposPeds $model
 * @return string
 * */
function getPedCapsHtml($model)
{
    $html = '';
    foreach (['magStripe', 'emv', 'contactless', 'manualPanEntry'] as $attribute) {
        $html .= $model->$attribute ? '<span class="glyphicon glyphicon-ok"/>' : '<span class="glyphicon glyphicon-remove"/>';
        $html .= ' ' . Html::encode($model->attributeLabels()[$attribute]) . '<br>';
    }
    return $html;
}

?>