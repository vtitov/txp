<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\FitsMap */

$this->title = $model->fits_id;
$this->params['breadcrumbs'][] = ['label' => 'Fits Maps', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fits-map-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'fits_id' => $model->fits_id, 'fits_groups_id' => $model->fits_groups_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'fits_id' => $model->fits_id, 'fits_groups_id' => $model->fits_groups_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'fits_id',
            'fits_groups_id',
        ],
    ]) ?>

</div>
