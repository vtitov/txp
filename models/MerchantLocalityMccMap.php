<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "merchant_locality_mcc_map".
 *
 * @property string $mcc
 * @property string $merchant_locality_id
 */
class MerchantLocalityMccMap extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'merchant_locality_mcc_map';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('paybycard');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mcc', 'merchant_locality_id'], 'required'],
            [['merchant_locality_id'], 'integer'],
            [['mcc'], 'string', 'max' => 4],
            [['mcc'], 'match', 'pattern' => '/^\d{4}$/', 'message' => 'MCC-код должен состоять из четырех цифр'],
            ['mcc', 'unique', 'targetAttribute' => ['mcc', 'merchant_locality_id'], 'message' => 'Такой MCC-код уже есть в этом населенном пункте'],
            ['mcc', 'validateMcc'],

        ];
    }

    public function validateMcc($attribute)
    {
        $mcc = Mcc::find()
            ->where(['>=', 'range_end_code', $this->$attribute])
            ->andWhere(['<=', 'range_start_code', $this->$attribute])
            ->limit(1)
            ->one();

        if ($mcc === null){
            $this->addError($attribute, 'MCC-код не найден в базе данных');
        }

    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'mcc' => 'МСС',
            'merchant_locality_id' => 'Населенный пункт',
        ];
    }
}
