<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\FitsGroups */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => \Yii::t('app', 'Справочник групп FIT'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fits-groups-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(\Yii::t('app', 'Редактировать'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(\Yii::t('app', 'Удалить'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => \Yii::t('app', 'Вы действительно хотите удалить эту запись?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            'name',
            [
                'attribute' => 'fits_groups_types_id',
                'value' => $model->fitsGroupsTypes->fit_name,
            ],
            'description',
        ],
    ]) ?>

</div>

<div class="assigned-fits">

    <h1><?= Html::encode(\Yii::t('app', 'Включенные в группу FIT')) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $fitsDataProvider,
        'filterModel' => $fitsSearchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'fits_id',
                'value' => 'fits.fit',
                'label' => 'FIT',
            ],
            //'fits_groups_id',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{delete}',
                'controller' => 'fits-map',
            ],
        ],
    ]); ?>

</div>

<div class="unassigned-fits">

    <h1><?= Html::encode(\Yii::t('app', 'Добавление FIT в группу')) ?></h1>

    <?php $form = ActiveForm::begin([
        'action' => ['fits-map/create'],
    ]); ?>

    <?= $form->field($fitToBeAssigned, 'fits_groups_id')->hiddenInput(['value' => $model->id])->label(false) ?>

    <?= $form->field($fitToBeAssigned, 'fits_id')->dropDownList(ArrayHelper::map(\app\models\Fits::find()->orderBy(['fit' => SORT_ASC])->asArray()->all(), 'id', 'fit'))->label('FIT') ?>

    <div class="form-group">
        <?= Html::submitButton(\Yii::t('app', 'Сохранить'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
