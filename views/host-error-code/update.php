<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\HostErrorCode */

$this->title = 'Update Host Error Code: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Host Error Codes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id, 'iso_settings_id' => $model->iso_settings_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="host-error-code-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
