<?php

namespace app\modules\UserManagement\models;

use Yii;

/**
 * This is the model class for table "password_history".
 *
 * @property string $id
 * @property string $user_id
 * @property string $password_hash
 * @property string $date
 */
class PasswordHistory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'password_history';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            /*[['user_id', 'password_hash'], 'required'],
            [['user_id'], 'integer'],
            [['date'], 'safe'],
            [['password_hash'], 'string', 'max' => 255],*/
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'password_hash' => 'Password Hash',
            'date' => 'Date',
        ];
    }
}
