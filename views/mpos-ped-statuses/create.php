<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\MposPedStatuses */

$this->title = \Yii::t('app', 'Добавление статуса устройства');
$this->params['breadcrumbs'][] = ['label' => \Yii::t('app', 'Статусы устройств'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mpos-ped-statuses-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
