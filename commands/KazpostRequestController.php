<?php
namespace app\commands;

use app\helpers\Notification;
use app\models\KazpostTxpRequest;
use app\models\MerchantCashier;
use app\models\MerchantsArmUsers;
use app\models\MposMerchants;
use app\models\PaymentsTerminals;
use Yii;
use yii\console\Controller;
use yii\db\Expression;

class KazpostRequestController extends Controller
{
    //здесь храним список файлов на ftp сервере из соответствующих директорий
    private $files = [
        'arch' => [],
        'error' => []
    ];
    private $tmpFile;
    private $ftpConn;

    public function actionIndex()
    {
        set_time_limit(0);

        //временный файл, в который будем сохранять данные из файлов, расположенных на ftp
        $this->tmpFile = @fopen('php://temp', 'r+');
        if ($this->tmpFile === false) {
            $errorText = "Не удалось создать временный файл";
            Yii::error($errorText);
            $this->reportError($errorText);
            ftp_close($this->ftpConn);
            return;
        }

        $this->ftpConnect();

        $this->parseTxpRequest($this->getFtpFileList('kazpost'));
    }

    private function parseTxpRequest($files)
    {
        foreach ($files as $file) {

            if ($file == 'kazpost/arch' || $file == 'kazpost/error') {
                continue;
            }

            if (preg_match('/^.*\.xml$/', $file)) {

                $fileContent = $this->readXmlFile($file);

                //сталкивался с тем, что во время записи файла есть момент когда его содержимое пусто
                //хоть это было и не по ftp, но здесь эта проверка тоже мешать не будет
                if ($fileContent === "") {
                    continue;
                }

                $requests = @simplexml_load_string($fileContent);
                if ($requests === false) {
                    Yii::error('XML parse error. File content: ' . $fileContent);
                    $this->moveFile($file, 'error');
                    $this->reportError("Не удалось распарсить XML-файл " . $file);
                    continue;
                }

                foreach ($requests->Request as $xml) {
                    $requestModel = new KazpostTxpRequest();

                    $requestModel->rq_id = trim((string)$xml->RqId);

                    $requestModel->unp = trim((string)$xml->Merchant->UNP);
                    $requestModel->name_legal_entity = trim((string)$xml->Merchant->NameLegalEntity);
                    $requestModel->registered_address = trim((string)$xml->Merchant->RegisteredAddress);
                    $requestModel->post_address = trim((string)$xml->Merchant->PostAddress);
                    $requestModel->phone = trim((string)$xml->Merchant->Phone);
                    $requestModel->email = trim((string)$xml->Merchant->Email);
                    $requestModel->bic = trim((string)$xml->Merchant->BIC);
                    $requestModel->iban = trim((string)$xml->Merchant->IBAN);
                    $requestModel->currency = trim((string)$xml->Merchant->Currency);
                    $requestModel->contract_id = trim((string)$xml->Merchant->ContractId);
                    $requestModel->contract_date = trim((string)$xml->Merchant->ContractDate);
                    $requestModel->terminal_id = trim((string)$xml->Terminal->TerminalId);
                    $requestModel->terminal_status = trim((string)$xml->Terminal->Status);
                    $requestModel->mcc = trim((string)$xml->Terminal->MCC);
                    $requestModel->pt_description = trim((string)$xml->Terminal->PTDescription);
                    $requestModel->pt_destination = trim((string)$xml->Terminal->PTDestination);

                    $requestModel->parse_date = new Expression('now()');
                    $requestModel->status = 0;

                    if ($requestModel->save()) {
                        if ($requestModel->terminal_status == '0') {
                            $paymentTerminal = PaymentsTerminals::find()->where([
                                'name' => $requestModel->terminal_id,
                                'iso_setting_id' => Yii::$app->params['kazpostDefaultIsoSettingId'],
                            ])->one();
                            if ($paymentTerminal !== null) {
                                $transaction = KazpostTxpRequest::getDb()->beginTransaction();
                                try {
                                    $paymentTerminal->status = 3;//отключен
                                    $requestModel->status = 1;//заявка обработана

                                    $paymentTerminal->save(false, ['status']);
                                    $requestModel->save(false, ['status']);

                                    $transaction->commit();
                                } catch (\Exception $e) {
                                    $transaction->rollBack();
                                    Yii::error('Failed to disable payment terminal: ' . $e->getMessage());
                                    $this->reportError(
                                        Yii::t(
                                            'app',
                                            'Файл {fileName}, запрос {rqId}. Не удалось отключить платежный терминал: {errorMessage}',
                                            ['fileName' => $file, 'rqId' => $requestModel->rq_id, 'errorMessage' => $e->getMessage()]
                                        )
                                    );
                                    continue;
                                } catch (\Throwable $e) {
                                    $transaction->rollBack();
                                    Yii::error('Failed to disable payment terminal: ' . $e->getMessage());
                                    $this->reportError(
                                        Yii::t(
                                            'app',
                                            'Файл {fileName}, запрос {rqId}. Не удалось отключить платежный терминал: {errorMessage}',
                                            ['fileName' => $file, 'rqId' => $requestModel->rq_id, 'errorMessage' => $e->getMessage()]
                                        )
                                    );
                                    continue;
                                }
                            } else {
                                Yii::error('Failed to disable payment terminal: payment terminal not found');
                                $this->reportError(
                                    Yii::t(
                                        'app',
                                        'Файл {fileName}, запрос {rqId}. Не удалось отключить платежный терминал: терминал не найден в базе',
                                        ['fileName' => $file, 'rqId' => $requestModel->rq_id]
                                    )
                                );
                                continue;
                            }
                        } else if ($requestModel->terminal_status == '1') {

                            $isNewMerchant = false;

                            $merchant = MposMerchants::find()
                                ->where([
                                    'unp' => $requestModel->unp,
                                    'owner_id' => 6,
                                ])->one();

                            if ($merchant === null) {
                                $isNewMerchant = true;
                                $merchant = new MposMerchants();
                                $merchant->owner_id = 6;
                                $merchant->name = $requestModel->name_legal_entity;
                                $merchant->unp = $requestModel->unp;
                                $merchant->banks_mfo = 0;
                                $merchant->bic = $requestModel->bic;
                                $merchant->bank_account = 0;
                                $merchant->iban = $requestModel->iban;
                                $merchant->bank_account_currency_id = ($requestModel->currency == '' ? 398 : $requestModel->currency);
                                $merchant->email = ($requestModel->email == '' ? $requestModel->unp . '@kazpost.kz' : $requestModel->email);
                                $merchant->phone = $requestModel->phone;
                                $merchant->merchant_status_id = 1;//торговец активен
                                $merchant->contract = $requestModel->contract_id;
                                $merchant->contract_date = $requestModel->contract_date;
                                $merchant->registered_address = $requestModel->registered_address;
                                $merchant->post_address = $requestModel->post_address;

                                $cashier = new MerchantCashier();
                                $cashier->login = $merchant->phone;
                                $cashier->password = sha1($merchant->phone);
                                $cashier->merchant_cashier_status_id = 1;//кассир активен

                                $cabinetAccount = new MerchantsArmUsers();
                                $cabinetAccount->username = $merchant->email;
                                $cabinetAccount->email = $merchant->email;
                                $cabinetAccount->setPassword(MposMerchants::generatePassword());
                                $cabinetAccount->generateAuthKey();
                                $cabinetAccount->status = 0;//казахи не пользуются лк, но чтобы ничего не ломать, заводим отключенную учетку
                            }

                            $paymentTerminal = new PaymentsTerminals();
                            $paymentTerminal->iso_setting_id = Yii::$app->params['kazpostDefaultIsoSettingId'];
                            $paymentTerminal->name = $requestModel->terminal_id;
                            $paymentTerminal->description = $requestModel->pt_description;
                            $paymentTerminal->mcc = $requestModel->mcc;
                            $paymentTerminal->status = 1;//активен
                            $paymentTerminal->unp = $requestModel->unp;
                            $paymentTerminal->email = $requestModel->email;
                            $paymentTerminal->is_softpos = 1;

                            $requestModel->status = 1;//заявка обработана

                            $transaction = KazpostTxpRequest::getDb()->beginTransaction();
                            try {
                                if ($isNewMerchant) {
                                    if (!$merchant->save()) {
                                        $transaction->rollBack();
                                        Yii::error('Merchant data saving error: ' . print_r($merchant->errors, true));
                                        $this->reportError(
                                            Yii::t(
                                                'app',
                                                'Файл {fileName}, запрос {rqId}. Не удалось сохранить данные торговца: {errorMessage}',
                                                ['fileName' => $file, 'rqId' => $requestModel->rq_id, 'errorMessage' => print_r($merchant->errors, true)]
                                            )
                                        );
                                        continue;
                                    }

                                    $cashier->merchant_id = $merchant->id;
                                    if (!$cashier->save()) {
                                        $transaction->rollBack();
                                        Yii::error('Cashier data saving error: ' . print_r($cashier->errors, true));
                                        $this->reportError(
                                            Yii::t(
                                                'app',
                                                'Файл {fileName}, запрос {rqId}. Не удалось сохранить данные кассира: {errorMessage}',
                                                ['fileName' => $file, 'rqId' => $requestModel->rq_id, 'errorMessage' => print_r($cashier->errors, true)]
                                            )
                                        );
                                        continue;
                                    }

                                    $merchant->chief_cashier_id = $cashier->id;
                                    $merchant->save(false, ['chief_cashier_id']);

                                    $cabinetAccount->merchant_id = $merchant->id;
                                    if (!$cabinetAccount->save()) {
                                        $transaction->rollBack();
                                        Yii::error('Cabinet account data saving error: ' . print_r($cabinetAccount->errors, true));
                                        $this->reportError(
                                            Yii::t(
                                                'app',
                                                'Файл {fileName}, запрос {rqId}. Не удалось сохранить авторизационные данные для личного кабинета торговца: {errorMessage}',
                                                ['fileName' => $file, 'rqId' => $requestModel->rq_id, 'errorMessage' => print_r($cabinetAccount->errors, true)]
                                            )
                                        );
                                        continue;
                                    }
                                }


                                if (!$paymentTerminal->save()) {
                                    $transaction->rollBack();
                                    Yii::error('Payment terminal data saving error: ' . print_r($paymentTerminal->errors, true));
                                    $this->reportError(
                                        Yii::t(
                                            'app',
                                            'Файл {fileName}, запрос {rqId}. Не удалось сохранить данные платежному терминала: {errorMessage}',
                                            ['fileName' => $file, 'rqId' => $requestModel->rq_id, 'errorMessage' => print_r($paymentTerminal->errors, true)]
                                        )
                                    );
                                    continue;
                                }

                                $requestModel->save(false, ['status']);

                                $transaction->commit();

                            } catch (\Exception $e) {
                                $transaction->rollBack();
                                Yii::error('Merchant or payment terminal data saving error: ' . $e->getMessage());
                                $this->reportError(
                                    Yii::t(
                                        'app',
                                        'Файл {fileName}, запрос {rqId}. Не удалось перенести данные из заявки на автоматическую регистрацию в рабочую БД: {errorMessage}',
                                        ['fileName' => $file, 'rqId' => $requestModel->rq_id, 'errorMessage' => $e->getMessage()]
                                    )
                                );
                                continue;
                            } catch (\Throwable $e) {
                                Yii::error('Merchant or payment terminal data saving error: ' . $e->getMessage());
                                $this->reportError(
                                    Yii::t(
                                        'app',
                                        'Файл {fileName}, запрос {rqId}. Не удалось перенести данные из заявки на автоматическую регистрацию в рабочую БД: {errorMessage}',
                                        ['fileName' => $file, 'rqId' => $requestModel->rq_id, 'errorMessage' => $e->getMessage()]
                                    )
                                );
                                continue;
                            }

                        } else {
                            Yii::error('Looks like there is some kind of validation error. Model with such status should not be saved.');
                        }

                    } else {
                        Yii::error("File " . $file . ", request " . $requestModel->rq_id . " save failed.\nValidation errors: " . print_r($requestModel->errors, true));
                        $this->reportError("Не удалось сохранить данные запроса " . $requestModel->rq_id . " из файла " . $file . " в базу: " . print_r($requestModel->errors, true));
                        continue;
                    }
                }

                $this->moveFile($file, 'arch');

            } else {
                $this->reportError('Неизвестный формат имени файла: ' . $file);
            }
        }
    }

    private function readXmlFile($file)
    {
        $r = ftp_fget($this->ftpConn, $this->tmpFile, $file, FTP_ASCII);
        if ($r === false) {
            $errorText = "Не удалось скачать файл с FTP сервера";
            Yii::error($errorText);
            $this->reportError($errorText);
            ftp_close($this->ftpConn);
            exit;
        }

        $r = rewind($this->tmpFile);
        if ($r === false) {
            $errorText = "Ошибка при сбросе курсора файлового указателя";
            Yii::error($errorText);
            $this->reportError($errorText);
            ftp_close($this->ftpConn);
            exit;
        }

        $xmlString = '';
        while (!feof($this->tmpFile)) {
            $xmlString .= fread($this->tmpFile, 1024);
        }

        return $xmlString;
    }

    private function reportError($errorText)
    {
        foreach (Yii::$app->params['xmlParseErrorEmailNotification'] as $email) {
            Notification::sendToAddress(
                Notification::TYPE_EMAIL,
                $email,
                $errorText,
                null,
                'Ошибка вчитывания xml-файла от Казпочты'
            );
        }
    }

    private function ftpConnect()
    {
        // FTP connection
        $this->ftpConn = ftp_connect(Yii::$app->params['serverFtpForKazpost']);
        if ($this->ftpConn === false) {
            $errorText = "Не удалось подключиться к FTP серверу";
            Yii::error($errorText);
            $this->reportError($errorText);
            return;
        }

        // FTP login
        $r = @ftp_login($this->ftpConn, Yii::$app->params['userFtpForKazpost'], Yii::$app->params['passwordFtpForKazpost']);
        if ($r === false) {
            $errorText = "Не удалось авторизоваться на FTP сервере";
            Yii::error($errorText);
            $this->reportError($errorText);
            ftp_close($this->ftpConn);
            return;
        }

        //passive mode
        $r = ftp_pasv($this->ftpConn, true);
        if ($r === false) {
            $errorText = "Не удалось включить пассивный режим FTP";
            Yii::error($errorText);
            $this->reportError($errorText);
            ftp_close($this->ftpConn);
            return;
        }
    }

    private function getFtpFileList($folder)
    {
        $files = ftp_nlist($this->ftpConn, $folder);
        if ($files === false) {
            $errorText = "Не удалось получить список файлов от FTP сервера";
            Yii::error($errorText);
            $this->reportError($errorText);
            ftp_close($this->ftpConn);
            exit;
        }
        return $files;
    }

    private function moveFile($filePath, $to)
    {
        if (count($this->files[$to]) == 0) {//список файлов не был получен с сервера
            $this->files[$to] = ftp_nlist($this->ftpConn, 'kazpost/' . $to);
            if ($this->files[$to] === false) {
                $errorText = "Не удалось получить список файлов от FTP сервера";
                Yii::error($errorText);
                $this->reportError($errorText);
                ftp_close($this->ftpConn);
                fclose($this->tmpFile);
                die;
            }
        }

        $newFilePath = 'kazpost/' . $to . substr($filePath, 7);//обрезаем старый путь ('kazpost') и формируем новый ('kazpost/arch' или 'kazpost/error')
        if (!in_array($newFilePath, $this->files[$to])) {
            $this->moveFileOnFTP($filePath, $newFilePath, $to);
        } else {
            $i = 1;
            while (true) {
                $renamedFilePath = substr($newFilePath, 0, -4) . '(' . $i . ')' . '.xml';
                if (!in_array($renamedFilePath, $this->files[$to])) {
                    $this->moveFileOnFTP($filePath, $renamedFilePath, $to);
                    break;
                } else {
                    $i++;
                }
            }
        }
    }

    private function moveFileOnFTP($oldPath, $newPath, $newDirectory)
    {
        if (ftp_rename($this->ftpConn, $oldPath, $newPath)) {
            $this->files[$newDirectory][] = $newPath;//обновляем информацию о списке файлов на сервере
            $this->truncateTempFile();
        } else {
            $errorText = "Не удалось переместить файл на FTP сервере";
            Yii::error($errorText);
            $this->reportError($errorText);
            ftp_close($this->ftpConn);
            fclose($this->tmpFile);
            die;
        }
    }

    private function truncateTempFile()
    {
        if (!ftruncate($this->tmpFile, 0)) {
            $errorText = "Не удалось очистить временный файл";
            Yii::error($errorText);
            $this->reportError($errorText);
            ftp_close($this->ftpConn);
            fclose($this->tmpFile);
            die;
        }
    }
}
