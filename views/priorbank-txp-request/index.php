<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PriorbankTxpRequestSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Заявки на подключение tapXphone Приорбанк');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="priorbank-txp-request-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'rq_id',
            'unp',
            'name_legal_entity',
            //'registered_address',
            //'post_address',
            'phone',
            //'email:email',
            //'bic',
            //'iban',
            //'currency',
            //'contract_id',
            //'contract_date',
            'terminal_id',
            //'mcc',
            //'pt_description',
            //'pt_destination',
            [
                'attribute' => 'terminal_status',
                'value' => function ($model) {
                    return \yii\helpers\ArrayHelper::getValue(
                        \app\models\PriorbankTxpRequest::getTerminalStatusses(),
                        $model->terminal_status,
                        $model->terminal_status
                    );
                },
                'filter' => \app\models\PriorbankTxpRequest::getTerminalStatusses(),
            ],
            \app\helpers\DateColumnHelper::getColumn('parse_date', $searchModel),
            [
                'attribute' => 'status',
                'value' => function ($model) {
                    return \yii\helpers\ArrayHelper::getValue(
                        \app\models\PriorbankTxpRequest::getRequestStatuses(),
                        $model->status,
                        $model->status
                    );
                },
                'filter' => \app\models\PriorbankTxpRequest::getRequestStatuses(),
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}',
            ],
        ],
    ]); ?>


</div>
