<?php

namespace app\controllers;

use app\models\TransactionCountAcquirerReportSearch;
use app\models\TransactionDetailedReportRbiSearch;
use app\models\TransactionIpsReportSearch;
use yii\base\DynamicModel;
use yii\base\UserException;
use yii\filters\AccessControl;
use yii\web\Controller;
use Yii;
use yii\web\NotFoundHttpException;

class ReportController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['get-sum-report'],
                        'roles' => ['runMonthSumReport'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['transaction-ips-report'],
                        'roles' => ['runTransactionIpsReport'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['transaction-count-acquirer-report'],
                        'roles' => ['runTransactionCountAcquirerReport'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['transaction-detailed-report-rbi'],
                        'roles' => ['runTransactionDetailedReportRbi'],
                    ],
                ],
            ],
        ];
    }

    public function actionGetSumReport()
    {
        $model = new DynamicModel(['bank', 'year', 'month']);
        $model->addRule(['bank', 'year', 'month'], 'required')
            ->addRule(['bank'], 'in', ['range' => ['0', '1', '2']])//0 - бб (mPOS), 1 - приор (mPOS), 2 - приор (SoftPOS)
            ->addRule(['year'], 'match', ['pattern' => '/^\d{4}$/'])
            ->addRule(['month'], 'match', ['pattern' => '/^(0[1-9]|1[012])$/']);

        $model->year = date('Y', strtotime('-1 month'));
        $model->month = date('m', strtotime('-1 month'));

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            if ($model->bank == '0') {
                $result = Yii::$app->db->createCommand('
SELECT SUM(tmp.amt) AS total_income, tmp.cid AS curr FROM
(SELECT ta.payment_id pid,
	(IF(ta.payment_id=200000002, -1, 1)*SUM(ta.amount)) amt,
	ta.currency_id cid
 FROM mpos.transaction_archive ta, mpos.payment_terminal_settlement_log sl, mpos.payment_terminal pt
 WHERE
	ta.payments_terminal_id=sl.payment_terminal_id AND ta.bdd=sl.bdd AND ta.batch=sl.bddn
	AND sl.sett_date_time>=:startDate AND sl.sett_date_time<:endDate -- Беларусбанк
 	AND (sl.sett_result=500 OR sl.sett_result=501) -- Беларусбанк
	AND ta.financial_reverse_trx_id=0
	AND ta.mpos_transaction_status_id=1 -- complete
	AND ta.payments_terminal_id=pt.id
	AND pt.iso_setting_id=19 -- Беларусбанк mPOS
	AND ta.payments_terminal_id NOT IN (1,35,1026,1416)
 GROUP BY ta.payment_id, ta.currency_id) tmp
GROUP BY tmp.cid', [
                    ':startDate' => date('Y-m-d', strtotime('-1 day', strtotime($model->year . '-' . $model->month . '-01'))) . ' 01:58:00',
                    ':endDate' => date('Y-m-t', strtotime($model->year . '-' . $model->month . '-01')) . ' 01:58:00', //'t' - кол-во дней в месяце, date('Y-m-t') дает последний день месяца
                ])->queryOne();

                return $this->render('result', [
                    'model' => $model,
                    'result' => $result,
                ]);

            } else if ($model->bank == '1') {
                $result = Yii::$app->db->createCommand('
SELECT SUM(tmp2.merchant_total_income) AS `total_income`, SUM(merchant_ops_counter) AS `ops_counter` FROM (
 SELECT  tmp.munp unp,  SUM(tmp.amt) AS merchant_total_income, SUM(tmp.cnt) AS merchant_ops_counter
 FROM
  (SELECT m.unp munp,
	(IF(ta.mpos_transaction_status_id=3, 0 ,(IF(ta.payment_id=200000003,-1,1))) * SUM(ta.amount)) amt,
	(IF(ta.mpos_transaction_status_id=3, 2 , 1) * COUNT(*)) cnt
   FROM mpos.transaction_archive ta,mpos.payment_terminal pt, mpos.merchant m
   WHERE
	(ta.date_time >= :startDate AND ta.date_time < :endDate)
	AND ta.mpos_transaction_status_id IN (1,3) -- complete
	AND ta.payments_terminal_id=pt.id
	AND pt.iso_setting_id=22 -- Приобанк mPOS
	AND ta.mpos_merchant_id=m.id
    GROUP BY m.unp, ta.mpos_transaction_status_id, ta.payment_id) tmp
 GROUP BY tmp.munp) tmp2;', [
                    ':startDate' => date('Y-m-d', strtotime('-1 day', strtotime($model->year . '-' . $model->month . '-01'))) . ' 00:00:00',
                    ':endDate' => date('Y-m-t', strtotime($model->year . '-' . $model->month . '-01')) . ' 00:00:00', //'t' - кол-во дней в месяце, date('Y-m-t') дает последний день месяца
                ])->queryOne();

                return $this->render('result', [
                    'model' => $model,
                    'result' => $result,
                ]);
            } else if ($model->bank == '2') {
                $result = Yii::$app->db->createCommand('
SELECT SUM(tmp2.merchant_total_income) AS `total_income`, SUM(merchant_ops_counter) AS `ops_counter` FROM (
 SELECT  tmp.munp unp,  SUM(tmp.amt) AS merchant_total_income, SUM(tmp.cnt) AS merchant_ops_counter
 FROM
  (SELECT m.unp munp,
	(IF(ta.mpos_transaction_status_id=3, 0 ,(IF(ta.payment_id=200000003,-1,1))) * SUM(ta.amount)) amt,
	(IF(ta.mpos_transaction_status_id=3, 2 , 1) * COUNT(*)) cnt
   FROM mpos.transaction_archive ta,mpos.payment_terminal pt, mpos.merchant m
   WHERE
	(ta.date_time >= :startDate AND ta.date_time < :endDate)
	AND ta.mpos_transaction_status_id IN (1,3) -- complete
	AND ta.payments_terminal_id=pt.id
	AND pt.iso_setting_id=25 -- Приобанк SoftPOS
	AND ta.mpos_merchant_id=m.id
    GROUP BY m.unp, ta.mpos_transaction_status_id, ta.payment_id) tmp
 GROUP BY tmp.munp) tmp2;', [
                    ':startDate' => date('Y-m-d', strtotime('-1 day', strtotime($model->year . '-' . $model->month . '-01'))) . ' 00:00:00',
                    ':endDate' => date('Y-m-t', strtotime($model->year . '-' . $model->month . '-01')) . ' 00:00:00', //'t' - кол-во дней в месяце, date('Y-m-t') дает последний день месяца
                ])->queryOne();

                return $this->render('result', [
                    'model' => $model,
                    'result' => $result,
                ]);
            } else {
                throw new UserException('Неизвестный банк');
            }
        }

        return $this->render('form', [
            'model' => $model,
        ]);
    }

    public function actionTransactionIpsReport()
    {
        $searchModel = new TransactionIpsReportSearch();
        if (Yii::$app->request->isPost) {
            $dataProvider = $searchModel->search(Yii::$app->request->post());
        } else {
            $dataProvider = null;
        }


        return $this->render('transaction-ips-report', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionTransactionCountAcquirerReport()
    {
        $dailyDataProvider = null;
        $totalDataProvider = null;

        $searchModel = new TransactionCountAcquirerReportSearch();

        if (Yii::$app->request->isPost) {
            if (Yii::$app->request->post('action') == 'render') {
                list($dailyDataProvider, $totalDataProvider) = $searchModel->search(Yii::$app->request->post());

            } else if (Yii::$app->request->post('action') == 'export') {
                $outputFile = $searchModel->export(Yii::$app->request->post());
                if ($outputFile === false) {
                    Yii::$app->session->addFlash('error', Yii::t('app', 'Не удалось сформировать файл. Данные фильтра некорректны.'));
                } else {
                    $fs = fstat($outputFile);
                    header("Content-type: text/csv; charset=utf-8");
                    header("Content-Length: " . $fs['size']);
                    header("Content-Disposition: attachment; filename=transaction_count_report.csv");
                    header('Cache-Control: no-store, no-cache, must-revalidate, max-age=0');
                    rewind($outputFile);
                    while (!feof($outputFile)) {
                        echo fread($outputFile, 52428800);
                    }
                    fclose($outputFile);
                    exit();
                }

            } else {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
        }

        return $this->render('transaction-count-acquirer-report', [
            'searchModel' => $searchModel,
            'dailyDataProvider' => $dailyDataProvider,
            'totalDataProvider' => $totalDataProvider,
        ]);
    }

    public function actionTransactionDetailedReportRbi()
    {
        $searchModel = new TransactionDetailedReportRbiSearch();
        $dataProvider = null;

        if (Yii::$app->request->get('TransactionDetailedReportRbiSearch')) {
            if (Yii::$app->request->get('action') == 'export') {
                $outputFile = $searchModel->export(Yii::$app->request->queryParams);
                if ($outputFile === false) {
                    Yii::$app->session->addFlash('error', Yii::t('app', 'Не удалось сформировать файл. Данные фильтра некорректны.'));
                } else {
                    $fs = fstat($outputFile);
                    header("Content-type: text/csv; charset=utf-8");
                    header("Content-Length: " . $fs['size']);
                    header("Content-Disposition: attachment; filename=transaction_detailed_report.csv");
                    header('Cache-Control: no-store, no-cache, must-revalidate, max-age=0');
                    rewind($outputFile);
                    while (!feof($outputFile)) {
                        echo fread($outputFile, 52428800);
                    }
                    fclose($outputFile);
                    exit();
                }

            } else {
                $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            }
        }

        return $this->render('transaction-detailed-report-rbi', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
} 