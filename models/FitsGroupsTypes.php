<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "fits_groups_types".
 *
 * @property string $id
 * @property string $user_name
 * @property string $fit_name
 * @property string $description
 *
 * @property FitsGroups[] $fitsGroups
 */
class FitsGroupsTypes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'fit_group_type';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('mpos');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fit_name'], 'string', 'max' => 15],
            ['fit_name', 'required'],
            ['fit_name', 'unique'],
            [['description'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fit_name' => \Yii::t('app', 'Название'),
            'description' => \Yii::t('app', 'Описание'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFitsGroups()
    {
        return $this->hasMany(FitsGroups::className(), ['fits_groups_types_id' => 'id']);
    }
}
