<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "payment".
 *
 * @property string $id
 * @property string $name
 * @property string $iso_type
 * @property string $description
 */
class Payments extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payment';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('mpos');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'name'], 'required'],
            [['id'], 'integer'],
            [['name'], 'string', 'max' => 99],
            [['iso_type'], 'string', 'max' => 2],
            [['description'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => \Yii::t('app', 'Название'),
            'iso_type' => \Yii::t('app', 'Тип ISO'),
            'description' => \Yii::t('app', 'Описание'),
        ];
    }
}
