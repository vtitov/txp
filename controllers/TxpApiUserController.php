<?php

namespace app\controllers;

use app\models\TxpApiUserOwnerMapBatch;
use Yii;
use app\models\TxpApiUser;
use app\models\TxpApiUserSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TxpApiUserController implements the CRUD actions for TxpApiUser model.
 */
class TxpApiUserController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view'],
                        'roles' => ['txpApiUserView'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['create', 'update', 'change-status', 'delete'],
                        'roles' => ['txpApiUserEdit'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all TxpApiUser models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TxpApiUserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TxpApiUser model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new TxpApiUser model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TxpApiUser();
        $model->scenario = 'create';

        $ownerMap = new TxpApiUserOwnerMapBatch();

        if ($model->load(Yii::$app->request->post()) && $ownerMap->load(Yii::$app->request->post())) {

            $transaction = Yii::$app->db->beginTransaction();
            try {
                if ($model->save() && $ownerMap->insertNewOwners($model->id)) {
                    $transaction->commit();
                    return $this->redirect(['view', 'id' => $model->id]);
                } else {
                    $transaction->rollBack();
                }
            } catch (\Exception $e) {
                $transaction->rollBack();
                throw $e;
            } catch (\Throwable $e) {
                $transaction->rollBack();
                throw $e;
            }
        }

        return $this->render('create', [
            'model' => $model,
            'ownerMap' => $ownerMap,
        ]);
    }

    /**
     * Updates an existing TxpApiUser model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $ownerMap = new TxpApiUserOwnerMapBatch();
        $ownerMap->ownerIds = $model->getOwnerIds();

        if ($model->load(Yii::$app->request->post()) && $ownerMap->load(Yii::$app->request->post())) {
            $transaction = Yii::$app->db->beginTransaction();
            try {
                if ($model->save() && TxpApiUserOwnerMapBatch::deleteCurrentOwners($model->id) && $ownerMap->insertNewOwners($model->id)) {
                    $transaction->commit();
                    return $this->redirect(['view', 'id' => $model->id]);
                } else {
                    $transaction->rollBack();
                }
            } catch (\Exception $e) {
                $transaction->rollBack();
                throw $e;
            } catch (\Throwable $e) {
                $transaction->rollBack();
                throw $e;
            }
        }

        return $this->render('update', [
            'model' => $model,
            'ownerMap' => $ownerMap,
        ]);
    }

    public function actionChangeStatus($id)
    {
        $model = $this->findModel($id);
        $model->status = 1 - $model->status;

        $model->save();

        return $this->redirect(['view', 'id' => $id]);
    }

    /**
     * Deletes an existing TxpApiUser model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        $transaction = Yii::$app->db->beginTransaction();
        try {
            if ($model->delete() && TxpApiUserOwnerMapBatch::deleteCurrentOwners($model->id)) {
                $transaction->commit();
            } else {
                $transaction->rollBack();
            }
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        } catch (\Throwable $e) {
            $transaction->rollBack();
            throw $e;
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the TxpApiUser model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TxpApiUser the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TxpApiUser::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
