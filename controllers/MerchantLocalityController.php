<?php

namespace app\controllers;

use app\models\MerchantLocalityMccMap;
use app\models\MerchantLocalityMccMapSearch;
use Yii;
use app\models\MerchantLocality;
use app\models\MerchantLocalitySearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * MerchantLocalityController implements the CRUD actions for MerchantLocality model.
 */
class MerchantLocalityController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view'],
                        'roles' => ['merchantLocalityView'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['create', 'update', 'delete', 'add-mcc', 'delete-mcc'],
                        'roles' => ['merchantLocalityEdit'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'delete-mcc' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all MerchantLocality models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MerchantLocalitySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single MerchantLocality model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $searchModel = new MerchantLocalityMccMapSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $id);

        return $this->render('view', [
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new MerchantLocality model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new MerchantLocality();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing MerchantLocality model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing MerchantLocality model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionAddMcc($merchant_locality_id)
    {
        $model = new MerchantLocalityMccMap();
        $model->merchant_locality_id = $merchant_locality_id;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->merchant_locality_id]);
        } else {
            return $this->render('add-mcc', [
                'model' => $model,
            ]);
        }
    }

    public function actionDeleteMcc($mcc, $merchant_locality_id)
    {
        $mccLocalityMap = MerchantLocalityMccMap::findOne([
            'mcc' => $mcc, 'merchant_locality_id' => $merchant_locality_id
        ]);

        if ($mccLocalityMap !== null) {
            $mccLocalityMap->delete();
        }

        return $this->redirect(['view', 'id' => $merchant_locality_id]);
    }

    /**
     * Finds the MerchantLocality model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return MerchantLocality the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MerchantLocality::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
