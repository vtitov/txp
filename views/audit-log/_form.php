<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AuditLog */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="audit-log-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'mpos_id')->textInput() ?>

    <?= $form->field($model, 'audit_event_id')->textInput() ?>

    <?= $form->field($model, 'audit_event_details')->textInput(['maxlength' => 500]) ?>

    <?= $form->field($model, 'rq_time')->textInput() ?>

    <?= $form->field($model, 'rs_time')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
