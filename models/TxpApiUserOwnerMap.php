<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "txp_api_user_owner_map".
 *
 * @property int $id
 * @property int $user_id
 * @property int $owner_id
 */
class TxpApiUserOwnerMap extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'txp_api_user_owner_map';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('mpos');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [];
    }
}
