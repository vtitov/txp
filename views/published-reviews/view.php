<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\PublishedReviews */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Опубликованные отзывы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="published-reviews-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы действительно хотите удалить эту запись?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            //'recieved_review_id',
            'name',
            'text:ntext',
            'date_recieved',
            [
                'attribute' => 'avatar',
                'value' => Html::img('@web'.$model->avatar),
                'format' => 'html',
            ]
        ],
    ]) ?>

</div>
