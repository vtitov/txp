<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\KazpostTxpRequest */

$this->title = $model->rq_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Заявки на подключение tapXphone Казпочта'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);

$this->registerCss("a.dropdown-toggle{color:black};");
$this->registerCss("a.dropdown-toggle:hover{text-decoration:none};");
?>
<div class="kazpost-txp-request-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            'rq_id',
            'unp',
            'name_legal_entity',
            'registered_address',
            'post_address',
            'phone',
            'email:email',
            'bic',
            'iban',
            'currency',
            'contract_id',
            'contract_date',
            'terminal_id',
            'mcc',
            'pt_description',
            'pt_destination',
            [
                'attribute' => 'terminal_status',
                'value' => \yii\helpers\ArrayHelper::getValue(
                    \app\models\KazpostTxpRequest::getTerminalStatusses(),
                    $model->terminal_status,
                    $model->terminal_status
                )
            ],
            'parse_date',
            [
                'attribute' => 'status',
                'value' => \yii\helpers\ArrayHelper::getValue(
                        \app\models\KazpostTxpRequest::getRequestStatuses(),
                        $model->status,
                        $model->status
                    ) . ' <div class="dropdown btn btn-default"><a href="#" data-toggle="dropdown" class="dropdown-toggle">' .
                    Yii::t('app', 'Изменить') . ' <b class="caret"></b></a>' .
                    \yii\bootstrap\Dropdown::widget([
                        'items' => $statusDropdownItems,
                    ]) . '</div>',
                'format' => 'raw',
            ],
        ],
    ]) ?>

</div>
