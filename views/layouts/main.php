﻿<?php
use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>

<style>
    .navbar-right.nav a {
        padding-left: 7px;
        padding-right: 7px;
    }

    /*.container {
        width: 100%;
    }*/
</style>

<?php $this->beginBody() ?>
<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => \Yii::t('app', 'mPOS Администрирование'),
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    $userPermissions = Yii::$app->authManager->getPermissionsByUser(Yii::$app->user->id);
    $mposMenuItems = [
        ['label' => \Yii::t('app', 'Справочник устройств'), 'url' => ['/mpos-peds/index'], 'visible' => isset($userPermissions['pedView'])],
        ['label' => \Yii::t('app', 'Архив устройств'), 'url' => ['/mpos-peds-archive/index'], 'visible' => isset($userPermissions['pedArchiveView'])],
        ['label' => \Yii::t('app', 'Справочник Торговцев'), 'url' => ['/mpos-merchants/index'], 'visible' => isset($userPermissions['merchantView'])],
        ['label' => \Yii::t('app', 'Справочник платежных терминалов'), 'url' => ['/payments-terminals/index'], 'visible' => isset($userPermissions['paymentTerminalView'])],
        ['label' => \Yii::t('app', 'Журнал транзакций'), 'url' => ['/mpos-transactions/index'], 'visible' => isset($userPermissions['transactionView'])],
        ['label' => 'TransactionsLog', 'url' => ['/transactionslog/index'], 'visible' => isset($userPermissions['transactionslogView'])],
        ['label' => 'TransactionsLogArchive', 'url' => ['/transactionslog-archive/index'], 'visible' => isset($userPermissions['transactionslogArchiveView'])],
        ['label' => \Yii::t('app', 'Незакрытые бизнес-дни'), 'url' => ['/payment-terminal-current-batch/index'], 'visible' => isset($userPermissions['paymentTerminalCurrentBatchView'])],
        ['label' => \Yii::t('app', 'История закрытия бизнес-дней'), 'url' => ['/payment-terminal-settlement-log/index'], 'visible' => isset($userPermissions['paymentTerminalSettlementLogView'])],
        ['label' => \Yii::t('app', 'Журнал аудита'), 'url' => ['/audit-log/index'], 'visible' => isset($userPermissions['auditLogView'])]
    ];
    $connectServiceRequestMenuItems = [
        ['label' => \Yii::t('app', 'Просмотр предзаказов на устройства'), 'url' => ['/mpos-peds-preorder/index'], 'visible' => isset($userPermissions['pedPreorderView'])],
        ['label' => \Yii::t('app', 'Заявки на подключение tapXphone Казпочта'), 'url' => ['/kazpost-txp-request/index'], 'visible' => isset($userPermissions['kazpostTxpRequestView'])],
        ['label' => \Yii::t('app', 'Заявки на подключение tapXphone Приорбанк'), 'url' => ['/priorbank-txp-request/index'], 'visible' => isset($userPermissions['priorTxpRequestView'])],
        ['label' => \Yii::t('app', 'Заявки на подключение mPOS Приорбанк'), 'url' => ['/ped-preorder-priorbank-response/index'], 'visible' => isset($userPermissions['priorMposRequestView'])]
    ];
    $configMenuItems = [
        ['label' => \Yii::t('app', 'Справочник производителей'), 'url' => ['/mpos-ped-manufacturers/index'], 'visible' => isset($userPermissions['pedManufacturerView'])],
        ['label' => \Yii::t('app', 'Справочник моделей'), 'url' => ['/mpos-ped-models/index'], 'visible' => isset($userPermissions['pedModelView'])],
        ['label' => \Yii::t('app', 'Справочник статусов устройств'), 'url' => ['/mpos-ped-statuses/index'], 'visible' => isset($userPermissions['pedStatusView'])],
        ['label' => \Yii::t('app', 'Справочник банков'), 'url' => ['/banks/index'], 'visible' => isset($userPermissions['bankView'])],
        ['label' => \Yii::t('app', 'Справочник статусов Торговцев'), 'url' => ['/mpos-merchant-statuses/index'], 'visible' => isset($userPermissions['merchantStatusView'])],
        ['label' => \Yii::t('app', 'Справочник настроек ISO'), 'url' => ['/iso-settings/index'], 'visible' => isset($userPermissions['isoSettingView'])],
        ['label' => \Yii::t('app', 'Справочник платежей'), 'url' => ['/payments/index'], 'visible' => isset($userPermissions['paymentView'])],
        ['label' => \Yii::t('app', 'Справочник карт МПС'), 'url' => ['/ips-types/index'], 'visible' => isset($userPermissions['ipsTypeView'])],
        ['label' => \Yii::t('app', 'Справочник FIT'), 'url' => ['/fits/index'], 'visible' => isset($userPermissions['fitView'])],
        ['label' => \Yii::t('app', 'Справочник групп FIT'), 'url' => ['/fits-groups/index'], 'visible' => isset($userPermissions['fitGroupView'])],
        ['label' => \Yii::t('app', 'Справочник типов групп FIT'), 'url' => ['/fits-groups-types/index'], 'visible' => isset($userPermissions['fitGroupTypeView'])],
        ['label' => \Yii::t('app', 'Справочник статусов предзаказов'), 'url' => ['/ped-preorder-status/index'], 'visible' => isset($userPermissions['pedPreorderStatusView'])],
        ['label' => \Yii::t('app', 'Справочник ошибок'), 'url' => ['/client-error/index'], 'visible' => isset($userPermissions['clientErrorView'])],
        ['label' => \Yii::t('app', 'Справочник Response Code\'ов'), 'url' => ['/host-error-code/index'], 'visible' => isset($userPermissions['hostErrorCodeView'])],
        ['label' => \Yii::t('app', 'Справочник МСС-кодов для сайта payBYcard'), 'url' => ['/mcc/index'], 'visible' => isset($userPermissions['mccView'])],
        ['label' => \Yii::t('app', 'Справочник МСС-кодов платежных терминалов'), 'url' => ['/mcc-locale/index'], 'visible' => isset($userPermissions['mccView'])],
        ['label' => \Yii::t('app', 'Ключи app key'), 'url' => ['/app-key/index'], 'visible' => isset($userPermissions['appKeyView'])]
    ];
    $sendingMenuItems = [
        ['label' => \Yii::t('app', 'Оповещение Торговцам'), 'url' => ['/sending-messages/message-to-merchants'], 'visible' => isset($userPermissions['messageMerchant'])],
        ['label' => \Yii::t('app', 'Оповещение Пользователям'), 'url' => ['/sending-messages/message-to-users'], 'visible' => isset($userPermissions['messageUser'])],
        ['label' => \Yii::t('app', 'SMS на произвольный номер'), 'url' => ['/sending-messages/send-sms'], 'visible' => isset($userPermissions['messageSms'])],
        ['label' => \Yii::t('app', 'Неотправленные Торговцам сообщения'), 'url' => ['/sending-messages/view-unsend-messages'], 'visible' => isset($userPermissions['messageMerchant'])]
    ];
    $reportMenuItems = [
        ['label' => \Yii::t('app', 'Итоговая сумма за месяц'), 'url' => ['/report/get-sum-report'], 'visible' => isset($userPermissions['runMonthSumReport'])],
        ['label' => \Yii::t('app', 'Транзакции в разрезе МПС'), 'url' => ['/report/transaction-ips-report'], 'visible' => isset($userPermissions['runTransactionIpsReport'])],
        ['label' => \Yii::t('app', 'Отчет по количеству транзакций'), 'url' => ['/report/transaction-count-acquirer-report'], 'visible' => isset($userPermissions['runTransactionCountAcquirerReport'])],
        ['label' => \Yii::t('app', 'Детальный отчет по транзакциям'), 'url' => ['/report/transaction-detailed-report-rbi'], 'visible' => isset($userPermissions['runTransactionCountAcquirerReport'])],
    ];
    $paybycardMenuItems = [
        ['label' => \Yii::t('app', 'Новости сайта'), 'url' => ['/news/index'], 'visible' => isset($userPermissions['newsView'])],
        ['label' => \Yii::t('app', 'Полученные отзывы'), 'url' => ['/recieved-reviews/index'], 'visible' => isset($userPermissions['reviewView'])],
        ['label' => \Yii::t('app', 'Опубликованные отзывы'), 'url' => ['/published-reviews/index'], 'visible' => isset($userPermissions['reviewView'])],
        ['label' => \Yii::t('app', 'Населенные пункты и MCC'), 'url' => ['/merchant-locality/index'], 'visible' => isset($userPermissions['merchantLocalityView'])]
    ];
    $userMenuItems = [
        ['label' => \Yii::t('app', 'Пользователи'), 'url' => ['/user-management/user/index'], 'visible' => isset($userPermissions['userView'])],
        ['label' => \Yii::t('app', 'Роли'), 'url' => ['/user-management/role/index'], 'visible' => isset($userPermissions['roleView'])],
        ['label' => \Yii::t('app', 'Изменить пароль'), 'url' => ['/user-management/auth/change-password'],
            'visible' => (Yii::$app->getModule('user-management')->params['authChannel'] == 0 || Yii::$app->getModule('user-management')->params['authChannel'] == 2)
        ],
        ['label' => \Yii::t('app', 'Пользователи API'), 'url' => ['/txp-api-user/index'], 'visible' => isset($userPermissions['txpApiUserView'])],
    ];
    $languageMenuItems = [];
    $languageMenuName = '';
    foreach (Yii::$app->params['availableTranslations'] as $lang) {
        if (Yii::$app->language != $lang) {
            $languageMenuItems[] = ['label' => strtoupper($lang), 'url' => ['/site/change-language', 'language' => $lang]];
        } else {
            $languageMenuName = strtoupper($lang);
        }
    }
    echo \yii\bootstrap\Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            ['label' => 'mPOS', 'items' => $mposMenuItems, 'visible' => hasVisibleItems($mposMenuItems)],
            ['label' => \Yii::t('app', 'Заявки'), 'items' => $connectServiceRequestMenuItems, 'visible' => hasVisibleItems($connectServiceRequestMenuItems)],
            ['label' => \Yii::t('app', 'Конфигурация'), 'items' => $configMenuItems, 'visible' => hasVisibleItems($configMenuItems)],
            ['label' => \Yii::t('app', 'Оповещение'), 'items' => $sendingMenuItems, 'visible' => hasVisibleItems($sendingMenuItems)],
            ['label' => \Yii::t('app', 'Отчеты'), 'items' => $reportMenuItems, 'visible' => hasVisibleItems($reportMenuItems)],
            ['label' => 'payBYcard', 'items' => $paybycardMenuItems, 'visible' => hasVisibleItems($paybycardMenuItems)],
            ['label' => \Yii::t('app', 'Пользователи'), 'items' => $userMenuItems, 'visible' => !Yii::$app->user->isGuest],
            Yii::$app->user->isGuest ?
                ['label' => \Yii::t('app', 'Войти'), 'url' => ['/user-management/auth/login']] :
                ['label' => \Yii::t('app', 'Выход ({username})', ['username' => Yii::$app->user->identity->username]),
                    'url' => ['/user-management/auth/logout'],
                    'linkOptions' => ['data-method' => 'post']
                ],
            ['label' => $languageMenuName, 'items' => $languageMenuItems]
        ],
    ]);
    NavBar::end();

    function hasVisibleItems($menuItems) {
        foreach ($menuItems as $item) {
            if ($item['visible'] == true) {
                return true;
            }
        }
        return false;
    }
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; IBA <?= date('Y') ?></p>
        <!--p class="pull-right"><--?= Yii::powered() ?></p-->
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
