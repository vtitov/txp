<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TxpApiUserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Пользователи API');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="txp-api-user-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Добавить пользователя API'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'username',
            //'password_hash',
            [
                'attribute' => 'status',
                'value' => function ($model) {
                    return '<span class="glyphicon ' . ($model->status ? 'glyphicon-ok' : 'glyphicon-remove') . '">' .
                    \app\modules\UserManagement\models\User::getStatuses()[$model->status] . '</span>';
                },
                'format' => 'html',
                'filter' => \app\modules\UserManagement\models\User::getStatuses(),
            ],
            'description',
            \app\helpers\DateColumnHelper::getColumn('created_at', $searchModel),

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
