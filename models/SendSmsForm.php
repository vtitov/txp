<?php

namespace app\models;

use yii\base\Model;

class SendSmsForm extends Model{

    public $phone;
    public $text;

    public function rules()
    {
        return [
            [['phone', 'text'], 'required'],
            ['phone', 'match', 'pattern' => '/^\d+$/',
                'message' => \Yii::t('app', 'Мобильный телефон должен состоять только из цифр')],
        ];
    }

    public function attributeLabels()
    {
        return [
            'phone' => \Yii::t('app', 'Телефон'),
            'text' => \Yii::t('app', 'Сообщение'),
        ];
    }
} 