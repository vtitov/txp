<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "device_event_log".
 *
 * @property int $id
 * @property int|null $merchant_id
 * @property string $device_id
 * @property int $old_trust_level_id
 * @property int $new_trust_level_id
 * @property string|null $app_version
 * @property string|null $mas_version
 * @property int $event_code_id
 * @property int|null $attest_code
 * @property int|null $counter_id
 * @property int|null $cnt_value
 * @property int|null $cnt_limit_value
 * @property int|null $cnt_day
 * @property int|null $cnt_currency
 * @property int $event_source_id
 * @property string $created_at
 * @property string|null $description
 *
 * @property AttestationCodeLocale $attestationCode
 * @property Counter $counter
 * @property EventCodeLocale $eventCode
 * @property EventSourceLocale $eventSource
 * @property MposMerchants $merchant
 * @property MposPeds $ped
 * @property Currency $currency
 */
class DeviceEventLog extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'device_event_log';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('t2p_monitoring');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'merchant_id' => Yii::t('app', 'Торговец'),
            'device_id' => Yii::t('app', 'Устройство'),
            'old_trust_level_id' => Yii::t('app', 'Предыдущий уровень доверия'),
            'new_trust_level_id' => Yii::t('app', 'Новый уровень доверия'),
            'app_version' => Yii::t('app', 'Версия мобильного приложения'),
            'mas_version' => Yii::t('app', 'Версия системы мониторинга'),
            'event_code_id' => Yii::t('app', 'Событие'),
            'attest_code' => Yii::t('app', 'Тип проверки'),
            'counter_id' => Yii::t('app', 'Счетчик'),
            'cnt_value' => Yii::t('app', 'Значение счетчика'),
            'cnt_limit_value' => Yii::t('app', 'Лимит счетчика'),
            'cnt_day' => Yii::t('app', 'Период лимита (дней)'),
            'cnt_currency' => Yii::t('app', 'Валюта счетчика'),
            'event_source_id' => Yii::t('app', 'Источник события'),
            'created_at' => Yii::t('app', 'Дата'),
            'description' => Yii::t('app', 'Описание'),
        ];
    }

    /**
     * Gets query for [[AttestationCodeLocale]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAttestationCode()
    {
        return $this->hasOne(AttestationCodeLocale::className(), ['attestation_code' => 'attest_code'])
            ->andOnCondition([AttestationCodeLocale::tableName() . '.locale' => Yii::$app->language]);
    }

    /**
     * Gets query for [[Counter]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCounter()
    {
        return $this->hasOne(Counter::className(), ['id' => 'counter_id']);
    }

    /**
     * Gets query for [[EventCodeLocale]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEventCode()
    {
        return $this->hasOne(EventCodeLocale::className(), ['id' => 'event_code_id'])
            ->andOnCondition([EventCodeLocale::tableName() . '.locale' => Yii::$app->language]);
    }

    /**
     * Gets query for [[EventSourceLocale]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEventSource()
    {
        return $this->hasOne(EventSourceLocale::className(), ['id' => 'event_source_id'])
            ->andOnCondition([EventSourceLocale::tableName() . '.locale' => Yii::$app->language]);
    }

    public function getMerchant()
    {
        return $this->hasOne(MposMerchants::className(), ['id' => 'merchant_id']);
    }

    public function getPed()
    {
        return $this->hasOne(MposPeds::className(), ['serial_number' => 'device_id']);
    }

    public function getCurrency()
    {
        return $this->hasOne(Currency::className(), ['id' => 'cnt_currency']);
    }
}
