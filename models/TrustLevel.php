<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "trust_level".
 *
 * @property int $id
 * @property string $name
 * @property string|null $short_desc
 * @property string|null $desc
 * @property int|null $att_request_period
 * @property int|null $codes_number_min
 * @property int|null $codes_number_max
 */
class TrustLevel extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'trust_level';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('t2p_monitoring');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            /*'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Название'),
            'short_desc' => Yii::t('app', 'Краткое описание'),
            'desc' => Yii::t('app', 'Описание'),
            'att_request_period' => Yii::t('app', 'Att Request Period'),
            'codes_number_min' => Yii::t('app', 'Codes Number Min'),
            'codes_number_max' => Yii::t('app', 'Codes Number Max'),*/
        ];
    }
}
