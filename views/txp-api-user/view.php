<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\TxpApiUser */

$this->title = $model->username;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Пользователи API'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="txp-api-user-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="row">
        <div class="col-md-8">

            <p>
                <?= Html::a(Yii::t('app', 'Редактировать'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                <?= Html::a(Yii::t('app', 'Удалить'), ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => Yii::t('app', 'Вы действительно хотите удалить данного пользователя?'),
                        'method' => 'post',
                    ],
                ]) ?>
            </p>

            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    //'id',
                    'username',
                    //'password_hash',
                    [
                        'attribute' => 'status',
                        'value' => \app\modules\UserManagement\models\User::getStatuses()[$model->status] . ' ' .
                            Html::a($model->status ? \Yii::t('app', 'Заблокировать') : \Yii::t('app', 'Разблокировать'),
                                ['change-status', 'id' => $model->id],
                                ['class' => $model->status ? 'btn btn-warning' : 'btn btn-success']
                            ),
                        'format' => 'raw',
                    ],
                    'description',
                    'created_at',
                ],
            ]) ?>
        </div>
        <div class="col-md-4">

            <h3 style="margin-top: 8px"><?= Html::encode(Yii::t('app', 'Банки-эквайеры')) ?></h3>

            <table class="table table-striped table-bordered">

                <?php foreach ($model->getOwners() as $owner): ?>

                    <tr><td><?= Html::encode($owner) ?></td></tr>

                <?php endforeach; ?>

            </table>

        </div>
    </div>

</div>
