<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "iso_setting_payment_map".
 *
 * @property int $iso_setting_id
 * @property int $payment_id
 * @property int $currency
 */
class IsoSettingPaymentMap extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'iso_setting_payment_map';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('mpos');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['payment_id', 'currency'], 'required'],
            [['payment_id', 'currency'], 'integer'],
            [['payment_id'], 'exist', 'skipOnError' => true, 'targetClass' => Payments::className(), 'targetAttribute' => ['payment_id' => 'id']],
            [['currency'], 'exist', 'skipOnError' => true, 'targetClass' => Currency::className(), 'targetAttribute' => ['currency' => 'id']],
            [['payment_id', 'currency'], 'unique', 'targetAttribute' => ['iso_setting_id', 'payment_id', 'currency'], 'message' => Yii::t('app', 'Такой платеж и валюта уже добавлены для данной настройки ISO')],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'iso_setting_id' => Yii::t('app', 'Настройка ISO'),
            'payment_id' => Yii::t('app', 'Платеж'),
            'currency' => Yii::t('app', 'Валюта'),
        ];
    }
}
