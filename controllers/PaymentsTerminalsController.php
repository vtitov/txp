<?php

namespace app\controllers;

use app\helpers\Notification;
use app\models\MposPeds;
use app\models\MposTransactions;
use app\models\PaymentTerminalCurrentBatch;
use app\models\TransactionArchive;
use Yii;
use app\models\PaymentsTerminals;
use app\models\PaymentsTerminalsSearch;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PaymentsTerminalsController implements the CRUD actions for PaymentsTerminals model.
 */
class PaymentsTerminalsController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view'],
                        'roles' => ['paymentTerminalView'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['create', 'update', 'delete'],
                        'roles' => ['paymentTerminalEdit'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all PaymentsTerminals models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PaymentsTerminalsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single PaymentsTerminals model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new PaymentsTerminals model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new PaymentsTerminals();
        $model->is_softpos = 1;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            //Сообщение для приора Yii::t('app', 'Уважаемый клиент! Ваше устройство Prior SoftPos (tapXphone) на {description} готово к работе. Для активации приложения введите логин и МПИН, которыми является Ваш email. Коды инициаизации и активации терминала придут после ввода логина и МПИН', ['description' => $model->description]),
            $res = Notification::sendToMerchant($model->merchant_id, 'terminal.registration', ['description' => $model->description]);
            if ($res !== true) {
                Yii::$app->session->addFlash(
                    'error',
                    Yii::t(
                        'app',
                        'Сообщение о регистрации платежного терминала не отправлено: {errorMessage}',
                        ['errorMessage' => $res]
                    )
                );
            }
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing PaymentsTerminals model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if ($id == 1) {//техническая запись, нельзя редактировать
            throw new NotFoundHttpException(Yii::t('app', 'Страница не найдена'));
        }

        $model = $this->findModel($id);
        $model->unp = ArrayHelper::getValue($model->merchant, 'unp');
        $model->email = ArrayHelper::getValue($model->merchant, 'email');

        //при смене iso настройки платежного терминала, автоматом меняем группу конфигураций устройства
        $oldIsoSettingId = $model->iso_setting_id;
        //при отключении платежного терминала, блокируем привязанное к нему устройство и закрываем батч
        $oldStatus = $model->status;

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            $transaction = PaymentsTerminals::getDb()->beginTransaction();
            try {
                $model->save(false);

                if ($oldIsoSettingId != $model->iso_setting_id && $model->ped_id != 0 && $model->ped) {
                    $model->ped->ped_conf_category_id = ArrayHelper::getValue($model->isoSettings, 'ped_conf_category_id');
                    $model->ped->save(false, ['ped_conf_category_id']);
                }

                if ($oldStatus == 1 && $model->status != 1) {
                    if ($model->ped_id != 0 && $model->ped) {
                        $model->ped->ped_status_id = 2;//отключено
                        $model->ped->save(false, ['ped_status_id']);
                    }

                    $batch = PaymentTerminalCurrentBatch::findOne($model->id);
                    if ($batch && $batch->batch_status_id != 4 && $batch->batch_status_id != 5 && $batch->batch_status_id != 6) {
                        $batch->batch_status_id = 6;
                        $batch->save(false, ['batch_status_id']);
                    }
                }

                $transaction->commit();
            } catch (\Exception $e) {
                $transaction->rollBack();
                throw $e;
            } catch (\Throwable $e) {
                $transaction->rollBack();
                throw $e;
            }

            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing PaymentsTerminals model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if ($id == 1) {//техническая запись, нельзя удалять
            throw new NotFoundHttpException(Yii::t('app', 'Страница не найдена'));
        }

        $model = $this->findModel($id);

        if (MposPeds::find()->where(['payment_terminal_id' => $model->id])->exists()) {
            Yii::$app->session->addFlash(
                'error',
                Yii::t('app', 'Нельзя удалить платежный терминал, к которому привязано устройство')
            );
            return $this->redirect(['view', 'id' => $model->id]);
        }

        if (
            MposTransactions::find()->where(['payments_terminal_id' => $model->id])->exists() ||
            TransactionArchive::find()->where(['payments_terminal_id' => $model->id])->exists()
        ) {
            Yii::$app->session->addFlash(
                'error',
                Yii::t('app', 'Нельзя удалить платежный терминал, в базе есть проведенные через него транзакции')
            );
            return $this->redirect(['view', 'id' => $model->id]);
        }

        if (PaymentTerminalCurrentBatch::find()->where(['payment_terminal_id' => $model->id])->exists()) {
            Yii::$app->session->addFlash(
                'error',
                Yii::t('app', 'Нельзя удалить платежный терминал, для него уже был открыт бизнес-день')
            );
            return $this->redirect(['view', 'id' => $model->id]);
        }

        $model->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the PaymentsTerminals model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return PaymentsTerminals the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        $owner_id = Yii::$app->user->identity->owner_id;
        $model = PaymentsTerminals::findOne($id);

        if ($model !== null && ($owner_id == 1 || ArrayHelper::getValue($model->isoSettings, 'owner_id') == $owner_id)) {
            return $model;
        } else {
            throw new NotFoundHttpException(\Yii::t('app', 'Страница не найдена'));
        }
    }
}
