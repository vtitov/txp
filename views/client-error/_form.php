<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ClientError */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="client-error-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id')->textInput() ?>

    <?= $form->field($model, 'message')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'locale')->dropDownList(\app\models\ClientError::$_LOCALES, ['prompt' => '']) ?>

    <div class="form-group">
        <?= Html::submitButton(\Yii::t('app', 'Сохранить'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
