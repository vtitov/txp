<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PedPreorderPriorbankResponse */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ped-preorder-priorbank-response-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'preorder_id')->textInput() ?>

    <?= $form->field($model, 'file_number')->textInput() ?>

    <?= $form->field($model, 'name_legal_entity')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'unp')->textInput() ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'pos_info')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'contract')->textInput(['maxlength' => 50]) ?>

    <?= $form->field($model, 'contract_date')->textInput() ?>

    <?= $form->field($model, 'trading_name')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'phone_trading_name')->textInput(['maxlength' => 12]) ?>

    <?= $form->field($model, 'mcc')->textInput(['maxlength' => 4]) ?>

    <?= $form->field($model, 'country')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'region')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'locality')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'address')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'terminal_id')->textInput(['maxlength' => 8]) ?>

    <?= $form->field($model, 'merchant_id')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'parse_date')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
