<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PaymentTerminalCurrentBatchSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="payment-terminal-current-batch-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'payment_terminal_id') ?>

    <?= $form->field($model, 'bdd') ?>

    <?= $form->field($model, 'bddn') ?>

    <?= $form->field($model, 'trx_counter') ?>

    <?= $form->field($model, 'batch_status_id') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
