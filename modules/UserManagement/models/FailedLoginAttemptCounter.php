<?php

namespace app\modules\UserManagement\models;

use Yii;

/**
 * This is the model class for table "login_attempt".
 *
 * @property string $id
 * @property string $username
 * @property int $attempt_count
 * @property string $lock_date
 */
class FailedLoginAttemptCounter extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'failed_login_attempt_counter';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['username'], 'required'],
            [['attempt_count'], 'integer'],
            [['lock_date'], 'safe'],
            [['username'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'attempt_count' => 'Attempt Count',
            'lock_date' => 'Lock Date',
        ];
    }
}
