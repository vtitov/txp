<?php
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\helpers\Html;

$this->title = \Yii::t('app', 'Рассылка сообщений пользователям');
?>

    <style>
        textarea, input{
            margin-top: 20px;
        }
        textarea{
            width: 100%;
        }
        .form-group {
            margin-top: 20px;
        }
    </style>

    <h1><?= Html::encode($this->title) ?></h1>

<?php $form = ActiveForm::begin(['action' => Url::to(['sending-messages/send-message-to-users'])]); ?>

    <input type="text" name="subject" class="form-control"
           placeholder="<?= Yii::t('app', 'Введите тему сообщения') ?>">

    <textarea name="text" class="form-control" rows="5"
              placeholder="<?= Yii::t('app', 'Введите текст сообщения') ?>"
              style="width: 100%" required></textarea>

    <div class="form-group">
        <?= Html::submitButton(\Yii::t('app', 'Разослать всем Пользователям'), ['class' => 'btn btn-success']) ?>
    </div>

<?php ActiveForm::end(); ?>