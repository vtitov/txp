<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\FitsMap */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="fits-map-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'fits_id')->textInput(['maxlength' => 10]) ?>

    <?= $form->field($model, 'fits_groups_id')->textInput(['maxlength' => 10]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
