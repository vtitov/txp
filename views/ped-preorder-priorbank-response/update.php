<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PedPreorderPriorbankResponse */

$this->title = 'Update Ped Preorder Priorbank Response: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Ped Preorder Priorbank Responses', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ped-preorder-priorbank-response-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
