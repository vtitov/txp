<?php
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\helpers\Html;

$this->title = Yii::t('app', 'Неотправленные Торговцам сообщения');
?>

    <h1><?= Html::encode($this->title) ?></h1>

<?php if ($count > 0): ?>

    <div class="alert alert-info">
        <p><?= Yii::t('app', 'Количество писем в очереди: ') . $count ?></p>
    </div>

<?php else: ?>

    <div class="alert alert-success">
        <p><?= Yii::t('app', 'Все сообщения были разосланы') ?></p>
    </div>

<?php endif; ?>