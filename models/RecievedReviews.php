<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "recieved_reviews".
 *
 * @property string $id
 * @property string $name
 * @property string $email
 * @property string $text
 * @property string $date_recieved
 */
class RecievedReviews extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'recieved_reviews';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('paybycard');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'email', 'text'], 'required'],
            [['text'], 'string'],
            [['date_recieved'], 'safe'],
            [['name', 'email', 'avatar'], 'string', 'max' => 255],
            ['email', 'email'],
            ['status_id', 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя',
            'email' => 'Email',
            'text' => 'Текст сообщения',
            'date_recieved' => 'Дата получения',
            'status_id' => 'Статус',
            'avatar' => 'Аватар',
        ];
    }

    public function getReviewStatus()
    {
        return $this->hasOne(RecievedReviewsStatuses::className(), ['id' => 'status_id']);
    }
}
