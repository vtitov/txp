<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\PaymentsTerminals;

/**
 * PaymentsTerminalsSearch represents the model behind the search form about `app\models\PaymentsTerminals`.
 */
class PaymentsTerminalsSearch extends PaymentsTerminals
{
    public $ownerId;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'iso_setting_id', 'ownerId'], 'integer'],
            [['name', 'description', 'zpk', 'mcc', 'merchant_id', 'ped_id'], 'safe'],
            ['status', 'in', 'range' => [1, 2, 3], 'message' => \Yii::t('app', 'Неизвестный статус')]
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $merchantId = null)
    {
        $query = PaymentsTerminals::find()
            ->joinWith(['isoSettings.owner', 'paymentTerminalStatus', 'merchant', 'ped']);

        if (Yii::$app->user->identity->owner_id != 1) {
            $query->andWhere([IsoSettings::tableName() . '.owner_id' => Yii::$app->user->identity->owner_id]);
        }

        if ($merchantId !== null) {
            $query->andWhere([static::tableName() . '.merchant_id' => $merchantId]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]],
        ]);

        $dataProvider->sort->attributes['iso_setting_id'] = [
            'asc' => [IsoSettings::tableName() . '.name' => SORT_ASC],
            'desc' => [IsoSettings::tableName() . '.name' => SORT_DESC]
        ];

        $dataProvider->sort->attributes['ownerId'] = [
            'asc' => [Owners::tableName() . '.name' => SORT_ASC],
            'desc' => [Owners::tableName() . '.name' => SORT_DESC]
        ];

        $dataProvider->sort->attributes['status'] = [
            'asc' => [PaymentTerminalStatusLocale::tableName() . '.name' => SORT_ASC],
            'desc' => [PaymentTerminalStatusLocale::tableName() . '.name' => SORT_DESC]
        ];

        $dataProvider->sort->attributes['merchant_id'] = [
            'asc' => [MposMerchants::tableName() . '.name' => SORT_ASC],
            'desc' => [MposMerchants::tableName() . '.name' => SORT_DESC]
        ];

        $dataProvider->sort->attributes['ped_id'] = [
            'asc' => [MposPeds::tableName() . '.serial_number' => SORT_ASC],
            'desc' => [MposPeds::tableName() . '.serial_number' => SORT_DESC]
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            static::tableName() . '.id' => $this->id,
            static::tableName() . '.iso_setting_id' => $this->iso_setting_id,
            IsoSettings::tableName() . '.owner_id' => $this->ownerId,
            static::tableName() . '.status' => $this->status,
        ]);

        $query->andFilterWhere(['like', static::tableName() . '.name', $this->name])
            ->andFilterWhere(['like', static::tableName() . '.zpk', $this->zpk])
            ->andFilterWhere(['like', static::tableName() . '.description', $this->description])
            ->andFilterWhere(['like', MposMerchants::tableName() . '.name', $this->merchant_id])
            ->andFilterWhere(['like', static::tableName() . '.mcc', $this->mcc])
            ->andFilterWhere(['like', MposPeds::tableName() . '.serial_number', $this->ped_id]);

        return $dataProvider;
    }
}
