<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "edi_sending".
 *
 * @property string $id
 * @property string $name
 * @property string $email
 * @property int $status
 * @property string $date_send
 * @property resource $message_text
 */
class EdiSending extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'edi_sending';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('paybycard');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'email'], 'required'],
            /*[['status'], 'integer'],
            [['date_send'], 'safe'],
            [['message_text'], 'string'],*/
            [['name'], 'string', 'max' => 255],
            [['email'], 'email'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'email' => 'Email',
            'status' => 'Status',
            'date_send' => 'Date Send',
            'message_text' => 'Message Text',
        ];
    }
}
