<?php

namespace app\helpers;

use Yii;
use yii\helpers\ArrayHelper;

class Notification
{
    const TYPE_SMS = 'sms';
    const TYPE_EMAIL = 'email';

    /*
     * Sends message to merchant
     * @param int $merchantId ID of merchant, who will receive message
     * @param string $templateName message template name
     * @param Array $params params, that will be placed into message template
     * @return boolean|string returns true if message was successfully sent, or error message in other case
     * */
    public static function sendToMerchant($merchantId, $templateName, $params = null)
    {
        $url = ArrayHelper::getValue(Yii::$app->params, 'notificationUrl', '');
        if ($url == '') {
            Yii::error("Message wasn't sent, notificationUrl parameter is not set");
            return Yii::t('app', 'Сообщение не отправлено, т.к. не задан URL для отправки сообщений');
        }
        $url .= 'send.do';

        Yii::info("Sending message to merchant " . $merchantId);

        $data = [
            'merchantId' => $merchantId,
            'localeSource' => Yii::$app->language,
            'templateName' => $templateName,
        ];

        if ($params !== null) {
            $data['messageParams'] = $params;
        }

        return static::send($url, $data);
    }

    /*
     * Sends message to address (phone or email)
     * @param string $type Notification::TYPE_SMS or Notification::TYPE_EMAIL
     * @param string $address email or phone number
     * @param int|null $ownerId acquirer id
     * @param string|null $emailSubject email subject
     * @return boolean|string returns true if message was successfully sent, or error message in other case
     * */
    public static function sendToAddress($type, $address, $message, $ownerId = null, $emailSubject = null)
    {
        $url = ArrayHelper::getValue(Yii::$app->params, 'notificationUrl', '');
        if ($url == '') {
            Yii::error("Message wasn't sent, notificationUrl parameter is not set");
            return Yii::t('app', 'Сообщение не отправлено, т.к. не задан URL для отправки сообщений');
        }
        $url .= 'send_directly.do';

        Yii::info("Sending " . $type . " to " . $address);

        $data = [
            'channel' => $type,
            'body' => $message,
            'to' => $address,
        ];

        if ($type == static::TYPE_EMAIL && $emailSubject !== null) {
            $data['subject'] = $emailSubject;
        }

        if ($ownerId !== null) {
            $data['ownerId'] = $ownerId;
        }

        return static::send($url, $data);
    }

    /*
     * Sends request to notification service
     * @param string $url
     * @param Array $data
     * @return boolean|string returns true if message was successfully sent, or error message in other case
     * */
    private static function send($url, $data)
    {
        $curl = curl_init();

        if ($curl === false) {
            Yii::error('cURL initialization error');
            return Yii::t('app', 'Ошибка инициализации модуля cURL');
        }

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));

        $out = curl_exec($curl);
        if ($out === false) {
            Yii::error($error = curl_error($curl));
            return $error;
        }
        curl_close($curl);

        $response = json_decode($out, true);

        if ($response === null || !isset($response['status'])) {
            Yii::error('Failed parse notification response. Response: ' . $out);
            return Yii::t('app', 'Не удалось обработать ответ от сервера отправки сообщений');
        } else if ($response['status'] == 'SUCCESS') {
            Yii::info('Notification response: ' . print_r($out, true));
            return true;
        } else {
            Yii::info('Notification response: ' . print_r($out, true));
            if (isset($response['message'])) {
                return $response['message'];
            } else {
                return $response['status'];
            }
        }
    }
} 