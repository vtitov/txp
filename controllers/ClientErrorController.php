<?php

namespace app\controllers;

use Yii;
use app\models\ClientError;
use app\models\ClientErrorSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ClientErrorController implements the CRUD actions for ClientError model.
 */
class ClientErrorController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view'],
                        'roles' => ['clientErrorView'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['create', 'update', 'delete'],
                        'roles' => ['clientErrorEdit'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ClientError models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ClientErrorSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ClientError model.
     * @param integer $id
     * @param string $locale
     * @return mixed
     */
    public function actionView($id, $locale)
    {
        return $this->render('view', [
            'model' => $this->findModel($id, $locale),
        ]);
    }

    /**
     * Creates a new ClientError model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ClientError();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id, 'locale' => $model->locale]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing ClientError model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @param string $locale
     * @return mixed
     */
    public function actionUpdate($id, $locale)
    {
        $model = $this->findModel($id, $locale);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id, 'locale' => $model->locale]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing ClientError model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @param string $locale
     * @return mixed
     */
    public function actionDelete($id, $locale)
    {
        $this->findModel($id, $locale)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ClientError model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @param string $locale
     * @return ClientError the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id, $locale)
    {
        if (($model = ClientError::findOne(['id' => $id, 'locale' => $locale])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
