<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\TransactionslogArchive */

$this->title = 'Create Transactionslog Archive';
$this->params['breadcrumbs'][] = ['label' => 'Transactionslog Archives', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="transactionslog-archive-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
