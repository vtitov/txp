<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\modules\UserManagement\models\LoginOtpTokenForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;

$this->title = \Yii::t('app', 'Вход в систему');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">
    <h1><?= Html::encode($this->title) ?></h1>

    <?php
    if ($flash = Yii::$app->session->getFlash('loginOtpFormError')) {
        echo Alert::widget([
            'options' => [
                'class' => 'alert-danger',
            ],
            'body' => $flash,
        ]);
    }

    if ($flash = Yii::$app->session->getFlash('loginFormSuccess')) {
        echo Alert::widget([
            'options' => [
                'class' => 'alert-info',
            ],
            'body' => $flash,
        ]);
    }

    if ($timeout > 0) {
        echo Yii::t('app', 'Осталось {timeout} секунд', ['timeout' => '<label id="timer_value">' . $timeout . '</label>']);
    }
    ?>

    <?php $form = ActiveForm::begin([
        'id' => 'login-form',
        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
            'labelOptions' => ['class' => 'col-lg-1 control-label'],
        ],
    ]); ?>

    <?= $form->field($model, 'username')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'domain')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'session')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'otp')->passwordInput() ?>

    <div class="form-group">
        <div class="col-lg-offset-1 col-lg-11">
            <?= Html::submitButton(\Yii::t('app', 'Войти'), ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>

<?php
$js = "var seconds = $timeout;
function timer_text() {
    if (seconds >= 0) {
        document.getElementById('timer_value').innerHTML = seconds;
        seconds -= 1;
        setTimeout(timer_text, 1000);
    } else {
        document.getElementById('timer_value').innerHTML = '0';
    }
}
timer_text();";
$this->registerJs($js);
?>
