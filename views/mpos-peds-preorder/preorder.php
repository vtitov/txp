<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\MposPedsPreorder */

$this->title = \Yii::t('app', 'Онлайн заявка на приобретение устройств');
?>
<div class="mpos-peds-preorder-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
