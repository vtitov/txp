<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\HostErrorCode */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Справочник Response Code\'ов'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="host-error-code-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <!--<p>
        <?/*= Html::a('Update', ['update', 'id' => $model->id, 'iso_settings_id' => $model->iso_settings_id], ['class' => 'btn btn-primary']) */?>
        <?/*= Html::a('Delete', ['delete', 'id' => $model->id, 'iso_settings_id' => $model->iso_settings_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) */?>
    </p>-->

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'iso_settings_id',
                'value' => $model->isoSettings->name,
            ],
            'descr',
        ],
    ]) ?>

</div>
