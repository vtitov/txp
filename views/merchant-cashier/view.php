<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\MerchantCashier */

$this->title = \Yii::t('app', 'Кассир') . ' ' . $model->description . ' (' . $model->login . ')';
$this->params['breadcrumbs'][] = ['label' => \Yii::t('app', 'Справочник Торговцев'), 'url' => ['mpos-merchants/index']];
$this->params['breadcrumbs'][] = ['label' => $model->merchant->name, 'url' => ['mpos-merchants/view', 'id' => $model->merchant_id]];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="merchant-cashier-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?php
            echo Html::a(\Yii::t('app', 'Установить пароль по умолчанию'), ['reset-password', 'id' => $model->id], [
                'class' => 'btn btn-success',
                'data' => [
                    'confirm' => \Yii::t('app', 'Вы действительно хотите установить пароль по умолчанию?'),
                    'method' => 'post',
                ],
            ]);

        if ($model->merchant->chief_cashier_id != $model->id) {
            echo ' ';
            echo Html::a(\Yii::t('app', 'Редактировать'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']);
            echo ' ';
            echo Html::a(\Yii::t('app', 'Удалить'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => \Yii::t('app', 'Вы действительно хотите удалить эту запись?'),
                    'method' => 'post',
                ],
            ]);
        }
        ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            'login',
            'description',
            'last_login_date',
            'login_attempts_count',
            [
                'attribute' => 'merchant_cashier_status_id',
                'value' => \yii\helpers\ArrayHelper::getValue(
                    $model->merchantCashierStatus,
                    'desc',
                    $model->merchant_cashier_status_id
                )
            ],
        ],
    ]) ?>

</div>
