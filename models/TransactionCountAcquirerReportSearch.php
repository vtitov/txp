<?php

namespace app\models;


use yii\base\Model;
use yii\data\ArrayDataProvider;
use yii\db\Expression;

class TransactionCountAcquirerReportSearch extends Model
{

    public $dateRange;
    public $ownerId;

    private $_data;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['dateRange'], 'required'],
            [
                ['dateRange'],
                'match',
                'pattern' => '/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[12][0-9]|3[01]) - [0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[12][0-9]|3[01])$/',
                'message' => \Yii::t('app', 'Неверный формат даты')
            ],
            [['ownerId'], 'integer'],
            [['ownerId'], 'exist', 'skipOnError' => true, 'targetClass' => Owners::className(), 'targetAttribute' => ['ownerId' => 'id']],
        ];
    }

    public function search($params)
    {
        $data = $this->getData($params);

        return [
            new ArrayDataProvider([
                'allModels' => $data[0],
                'sort' => false,
                'pagination' => false,
            ]),
            new ArrayDataProvider([
                'allModels' => $data[1],
                'sort' => false,
                'pagination' => false,
            ])
        ];
    }

    public function export($params)
    {
        $data = $this->getData($params);
        if ($this->hasErrors()) {
            return false;
        }
        $outputFile = tmpfile();
        fputcsv($outputFile, [$this->dateRange]);
        fputcsv($outputFile, [
            \Yii::t('app', 'Банк-эквайер'),
            \Yii::t('app', 'Дата'),
            \Yii::t('app', 'Количество оплат'),
            \Yii::t('app', 'Количество отмен'),
            \Yii::t('app', 'Количество возвратов'),
        ]);
        foreach ($data[0] as $row) {
            fputcsv($outputFile, $row);
        }

        fputcsv($outputFile, []);
        fputcsv($outputFile, []);
        fputcsv($outputFile, [\Yii::t('app', 'Итого:')]);
        fputcsv($outputFile, [
            \Yii::t('app', 'Банк-эквайер'),
            \Yii::t('app', 'Количество оплат'),
            \Yii::t('app', 'Количество отмен'),
            \Yii::t('app', 'Количество возвратов'),
        ]);
        foreach ($data[1] as $row) {
            fputcsv($outputFile, $row);
        }

        return $outputFile;
    }

    private function getData($params)
    {
        if (isset($this->_data)) {
            return ($this->_data);
        }

        $this->load($params);
        if (!$this->validate()) {
            $this->_data = [[], []];
            return $this->_data;
        }

        $dateArr = explode(' - ', $this->dateRange);
        $userOwnerId = \Yii::$app->user->identity->owner_id;
        $result = [];


        //1. считаем успешные транзакции
        $query = TransactionArchive::find()
            ->from(TransactionArchive::tableName() . ' ta')
            ->leftJoin(PaymentsTerminals::tableName() . ' pt', 'pt.`id` = ta.`payments_terminal_id`')
            ->leftJoin(IsoSettings::tableName() . ' iso', 'iso.`id` = pt.`iso_setting_id`')
            ->leftJoin(Owners::tableName() . ' o', 'iso.`owner_id` = o.`id` AND o.`locale` = \'' . \Yii::$app->language . '\'')
            ->select([
                'o.`name` AS `acquirer_name`',
                new Expression('DATE_FORMAT(ta.`date_time`, "%Y-%m-%d") AS `date`'),
                new Expression('COUNT(ta.`id`) AS `cnt`')
            ])
            ->where(['>=', 'ta.date_time', $dateArr[0] . ' 00:00:00'])
            ->andWhere(['<=', 'ta.date_time', $dateArr[1] . ' 23:59:59'])
            ->andWhere([
                'ta.payment_id' => 100000001,
                'ta.mpos_transaction_status_id' => 1,
            ])
            ->groupBy('`date`, `acquirer_name`');

        if ($userOwnerId != 1) {
            $query->andWhere(['o.id' => $userOwnerId]);
        } else {
            $query->andFilterWhere(['o.id' => $this->ownerId]);
        }

        $data = $query->asArray()->all();
        foreach ($data as $row) {
            if (isset($result[$row['acquirer_name']][$row['date']])) {
                $result[$row['acquirer_name']][$row['date']]['payment_count'] += $row['cnt'];
            } else {
                $result[$row['acquirer_name']][$row['date']] = [
                    'payment_count' => $row['cnt'],
                    'reversal_count' => 0,
                    'refund_count' => 0,
                ];
            }
        }


        //2. считаем успешные возвраты
        $query = TransactionArchive::find()
            ->from(TransactionArchive::tableName() . ' ta')
            ->leftJoin(PaymentsTerminals::tableName() . ' pt', 'pt.`id` = ta.`payments_terminal_id`')
            ->leftJoin(IsoSettings::tableName() . ' iso', 'iso.`id` = pt.`iso_setting_id`')
            ->leftJoin(Owners::tableName() . ' o', 'iso.`owner_id` = o.`id` AND o.`locale` = \'' . \Yii::$app->language . '\'')
            ->select([
                'o.`name` AS `acquirer_name`',
                new Expression('DATE_FORMAT(ta.`date_time`, "%Y-%m-%d") AS `date`'),
                new Expression('COUNT(ta.`id`) AS `cnt`')
            ])
            ->where(['>=', 'ta.date_time', $dateArr[0] . ' 00:00:00'])
            ->andWhere(['<=', 'ta.date_time', $dateArr[1] . ' 23:59:59'])
            ->andWhere([
                'ta.payment_id' => [200000002, 200000003, 200000004],
                'ta.mpos_transaction_status_id' => 1,
            ])
            ->groupBy('`date`, `acquirer_name`');

        if ($userOwnerId != 1) {
            $query->andWhere(['o.id' => $userOwnerId]);
        } else {
            $query->andFilterWhere(['o.id' => $this->ownerId]);
        }

        $data = $query->asArray()->all();
        foreach ($data as $row) {
            if (isset($result[$row['acquirer_name']][$row['date']])) {
                $result[$row['acquirer_name']][$row['date']]['refund_count'] += $row['cnt'];
            } else {
                $result[$row['acquirer_name']][$row['date']] = [
                    'payment_count' => 0,
                    'reversal_count' => 0,
                    'refund_count' => $row['cnt'],
                ];
            }
        }


        //3. Выбираем из базы оплаты со статусом "отменена"
        //это могут быть как простые отмены, так и возвраты по RRN
        $query = TransactionArchive::find()
            ->from(TransactionArchive::tableName() . ' ta')
            ->leftJoin(PaymentsTerminals::tableName() . ' pt', 'pt.`id` = ta.`payments_terminal_id`')
            ->leftJoin(IsoSettings::tableName() . ' iso', 'iso.`id` = pt.`iso_setting_id`')
            ->leftJoin(Owners::tableName() . ' o', 'iso.`owner_id` = o.`id` AND o.`locale` = \'' . \Yii::$app->language . '\'')
            ->select([
                'o.`name` AS `acquirer_name`',
                new Expression('DATE_FORMAT(ta.`date_time`, "%Y-%m-%d") AS `date`'),
                'pt.iso_setting_id',
                'ta.rrn'
            ])
            ->where(['>=', 'ta.date_time', $dateArr[0] . ' 00:00:00'])
            ->andWhere(['<=', 'ta.date_time', $dateArr[1] . ' 23:59:59'])
            ->andWhere([
                'ta.payment_id' => 100000001,
                'ta.mpos_transaction_status_id' => 3,
            ]);

        if ($userOwnerId != 1) {
            $query->andWhere(['o.id' => $userOwnerId]);
        } else {
            $query->andFilterWhere(['o.id' => $this->ownerId]);
        }

        $data = $query->asArray()->all();

        foreach ($data as $row) {

            if (isset($result[$row['acquirer_name']][$row['date']])) {
                $result[$row['acquirer_name']][$row['date']]['payment_count']++;
            } else {
                $result[$row['acquirer_name']][$row['date']] = [
                    'payment_count' => 1,
                    'reversal_count' => 0,
                    'refund_count' => 0,
                ];
            }

            //ищем был ли возврат по rrn
            $q = TransactionArchive::find()
                ->from(TransactionArchive::tableName() . ' ta')
                ->leftJoin(PaymentsTerminals::tableName() . ' pt', 'pt.`id` = ta.`payments_terminal_id`')
                ->where([
                    'ta.payment_id' => 200000003,
                    'ta.mpos_transaction_status_id' => 1,
                    'pt.iso_setting_id' => $row['iso_setting_id'],
                    'ta.rrn' => $row['rrn'],
                ]);

            if (!$q->exists()) {
                //нет возврата по rrn - простая отмена
                $result[$row['acquirer_name']][$row['date']]['reversal_count']++;
            } //else возврат уже подсчитан на втором этапе
        }


        //формируем итоговые массивы данных
        foreach ($result as $acquirer => $arr) {
            ksort($arr);
        }
        ksort($result);

        $dailyResult = [];
        $totalResult = [];
        foreach ($result as $acquirer => $arr1) {
            $acquirerSum = [
                'payment_count' => 0,
                'reversal_count' => 0,
                'refund_count' => 0
            ];
            foreach ($arr1 as $date => $arr2) {
                $dailyResult[] = [
                    'acquirer_name' => $acquirer,
                    'date' => $date,
                    'payment_count' => $arr2['payment_count'],
                    'reversal_count' => $arr2['reversal_count'],
                    'refund_count' => $arr2['refund_count']
                ];
                $acquirerSum['payment_count'] += $arr2['payment_count'];
                $acquirerSum['reversal_count'] += $arr2['reversal_count'];
                $acquirerSum['refund_count'] += $arr2['refund_count'];
            }
            $totalResult[] = [
                'acquirer_name' => $acquirer,
                'payment_count' => $acquirerSum['payment_count'],
                'reversal_count' => $acquirerSum['reversal_count'],
                'refund_count' => $acquirerSum['refund_count']
            ];
        }

        $this->_data = [$dailyResult, $totalResult];
        return $this->_data;
    }
} 