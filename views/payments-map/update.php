<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PaymentsMap */

$this->title = \Yii::t('app', 'Редактирование настроек платежа: ') . \yii\helpers\ArrayHelper::getValue(
        \app\models\PaymentLocale::findOne(['id' => $model->payments_id, 'locale' => Yii::$app->language]),
        'name'
    );
$this->params['breadcrumbs'][] = ['label' => \Yii::t('app', 'Справочник устройств'), 'url' => ['mpos-peds/index']];
$this->params['breadcrumbs'][] = ['label' => \yii\helpers\ArrayHelper::getValue(
    \app\models\MposPeds::findOne(['id' => $model->ped_id]),
    'serial_number'
), 'url' => ['mpos-peds/view', 'id' => $model->ped_id]];
$this->params['breadcrumbs'][] = \Yii::t('app', 'Редактирование настроек платежа');
?>
<div class="payments-map-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
