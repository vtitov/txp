<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PaymentTerminalSettlementLogSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'История закрытия бизнес-дней');
$this->params['breadcrumbs'][] = $this->title;

$batchStatusFilter = ArrayHelper::map(
    \app\models\BatchStatusLocale::find()->where(['locale' => Yii::$app->language])->all(),
    'id',
    'desc'
);
?>
<div class="payment-terminal-settlement-log-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'payment_terminal_id',
                'value' => function($model) {
                    if ($model->paymentTerminal) {
                        return Html::a(
                            Html::encode($model->paymentTerminal->name),
                            ['payments-terminals/view', 'id' => $model->payment_terminal_id]
                        );
                    } else {
                        return Html::encode($model->payment_terminal_id);
                    }
                },
                'format' => 'html',
            ],
            \app\helpers\DateColumnHelper::getColumn('bdd', $searchModel),
            'bddn',
            'trx_counter',
            \app\helpers\DateColumnHelper::getColumn('sett_date_time', $searchModel),
            //'sett_result',
            [
                'attribute' => 'batch_status_id',
                'value' => function ($model) use ($batchStatusFilter) {
                    return ArrayHelper::getValue($batchStatusFilter, $model->batch_status_id, $model->batch_status_id);
                },
                'filter' => $batchStatusFilter,
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}',
            ],
        ],
    ]); ?>


</div>
