<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "banks".
 *
 * @property string $mfo
 * @property string $name
 */
class Banks extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bank';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('mpos');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mfo', 'name'], 'required'],
            [['mfo'], 'unique'],
            [['mfo'], 'match', 'pattern' => '/^((\d{9})|([A-Z0-9]{4}[A-Z]{2}[A-Z0-9]{2}([A-Z0-9]{3})?))$/', 'message' => \Yii::t('app', 'Неверный формат кода банка')],
            [['name'], 'string', 'max' => 270]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'mfo' => \Yii::t('app', 'МФО'),
            'name' => \Yii::t('app', 'Название'),
        ];
    }
}
