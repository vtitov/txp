<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\PublishedReviews */
/* @var $form yii\widgets\ActiveForm */
?>

<style>
    .avatar-dropdown {
        width: 307px;
    }
</style>

<div class="published-reviews-form">

    <?php $form = ActiveForm::begin([
        'action' => $model->getIsNewRecord() ? Url::to(['published-reviews/create']) : Url::to(['published-reviews/update', 'id' => $model->id]),
    ]); ?>

    <div class="dropdown">
        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1"
                data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
            <div id="selected-avatar"><?= Html::img('@web' . $model->avatar) ?></div>
            <span class="caret"></span>
        </button>
        <ul class="dropdown-menu avatar-dropdown" aria-labelledby="dropdownMenu1">
            <li>
                <div>
                    <?= Html::img('@web/img/ava-m-11.png', ['class' => 'avatar']) ?>
                    <?= Html::img('@web/img/ava-m-21.png', ['class' => 'avatar']) ?>
                    <?= Html::img('@web/img/ava-m-31.png', ['class' => 'avatar']) ?>
                    <?= Html::img('@web/img/ava-m-12.png', ['class' => 'avatar']) ?>
                    <?= Html::img('@web/img/ava-m-22.png', ['class' => 'avatar']) ?>
                    <?= Html::img('@web/img/ava-m-32.png', ['class' => 'avatar']) ?>
                </div>
            </li>
            <li role="separator" class="divider"></li>
            <li>
                <div>
                    <?= Html::img('@web/img/ava-f-11.png', ['class' => 'avatar']) ?>
                    <?= Html::img('@web/img/ava-f-21.png', ['class' => 'avatar']) ?>
                    <?= Html::img('@web/img/ava-f-12.png', ['class' => 'avatar']) ?>
                    <?= Html::img('@web/img/ava-f-22.png', ['class' => 'avatar']) ?>
                </div>
            </li>
        </ul>
    </div>

    <?= $form->field($model, 'avatar')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'recieved_review_id')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'text')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'date_recieved')->widget(DatePicker::classname(), [
        'dateFormat' => 'php:Y-m-d',
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php
$this->registerJs('$(".avatar").click(function(){
        $("#selected-avatar").empty();
        $(this).clone().appendTo("#selected-avatar");
        $("#publishedreviews-avatar").val($(this).attr("src").substring($(this).attr("src").indexOf("/img/")));
    });

    ');
?>
