<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\helpers\DateColumnHelper;

/**
 * MposPedsArchiveSearch represents the model behind the search form about `app\models\MposPedsArchive`.
 */
class MposPedsArchiveSearch extends MposPedsArchive
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'ped_model_id', 'owner_id', 'ped_status_id'], 'integer'],
            [['serial_number', 'merchant_id', 'description', 'add_date', 'delete_date', 'payment_terminal_id', 'mcc', 'app_ver'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MposPedsArchive::find()
            ->joinWith(['mposMerchant', 'mposPedModel', 'mposPedStatus', 'paymentTerminal', 'owner']);

        if (Yii::$app->user->identity->owner_id != 1) {
            $query->andWhere([static::tableName() . '.owner_id' => Yii::$app->user->identity->owner_id]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]],
        ]);

        $dataProvider->sort->attributes['merchant_id'] = [
            'asc' => [MposMerchants::tableName() . '.name' => SORT_ASC],
            'desc' => [MposMerchants::tableName() . '.name' => SORT_DESC]
        ];

        $dataProvider->sort->attributes['ped_model_id'] = [
            'asc' => [MposPedModels::tableName() . '.name' => SORT_ASC],
            'desc' => [MposPedModels::tableName() . '.name' => SORT_DESC]
        ];

        $dataProvider->sort->attributes['owner'] = [
            'asc' => [Owners::tableName() . '.name' => SORT_ASC],
            'desc' => [Owners::tableName() . '.name' => SORT_DESC]
        ];

        $dataProvider->sort->attributes['ped_status_id'] = [
            'asc' => [PedStatusLocale::tableName() . '.name' => SORT_ASC],
            'desc' => [PedStatusLocale::tableName() . '.name' => SORT_DESC]
        ];

        $dataProvider->sort->attributes['payment_terminal_id'] = [
            'asc' => [PaymentsTerminals::tableName() . '.name' => SORT_ASC],
            'desc' => [PaymentsTerminals::tableName() . '.name' => SORT_DESC]
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            static::tableName() . '.id' => $this->id,
            static::tableName() . '.ped_model_id' => $this->ped_model_id,
            static::tableName() . '.owner_id' => $this->owner_id,
            static::tableName() . '.ped_status_id' => $this->ped_status_id,
        ]);

        $query->andFilterWhere(['like', static::tableName() . '.serial_number', $this->serial_number])
            ->andFilterWhere(['like', MposMerchants::tableName() . '.name', $this->merchant_id])
            ->andFilterWhere(['like', static::tableName() . '.description', $this->description])
            ->andFilterWhere(['like', PaymentsTerminals::tableName() . '.name', $this->payment_terminal_id])
            ->andFilterWhere(['like', static::tableName() . '.mcc', $this->mcc])
            ->andFilterWhere(['like', static::tableName() . '.app_ver', $this->app_ver]);

        DateColumnHelper::addFilterParams([static::tableName() . '.delete_date'], [$this->delete_date], $query);

        return $dataProvider;
    }
}
