<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model \app\modules\UserManagement\models\User */

$this->title = \Yii::t('app', 'Редактирование пользователя: ') . $model->username;
$this->params['breadcrumbs'][] = ['label' => \Yii::t('app', 'Пользователи'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->username, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = \Yii::t('app', 'Редактирование');
?>
<div class="user-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
