<?php

namespace app\controllers;

use Yii;
use app\models\PaymentsMap;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * PaymentsMapController implements the CRUD actions for PaymentsMap model.
 */
class PaymentsMapController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['update'],
                        'roles' => ['pedEdit'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Updates an existing PaymentsMap model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $ped_id
     * @param string $payments_id
     * @param integer $currency_id
     * @return mixed
     */
    public function actionUpdate($ped_id, $payments_id, $currency_id)
    {
        $model = $this->findModel($ped_id, $payments_id, $currency_id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['mpos-peds/view', 'id' => $model->ped_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    protected function findModel($ped_id, $payments_id, $currency_id)
    {
        if (($model = PaymentsMap::findOne(['ped_id' => $ped_id, 'payments_id' => $payments_id, 'currency_id' => $currency_id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
