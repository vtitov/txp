<?php

namespace app\modules\UserManagement\helpers;

use Aws\CognitoIdentityProvider\CognitoIdentityProviderClient;

class AwsCognitoHelper
{
    public static function getClient()
    {
        $awsParams = \Yii::$app->getModule('user-management')->params['awsParams'];
        $clientParams = [
            'region' => $awsParams['AWS_REGION'],
            'version' => $awsParams['AWS_VERSION'],
        ];

        if (isset($awsParams['AWS_ACCESS_KEY_ID']) && isset($awsParams['AWS_SECRET_ACCESS_KEY'])) {
            $clientParams['credentials'] = [
                'key'    => $awsParams['AWS_ACCESS_KEY_ID'],
                'secret' => $awsParams['AWS_SECRET_ACCESS_KEY'],
            ];
        }

        return new CognitoIdentityProviderClient($clientParams);
    }

    public static function getSignature($username)
    {
        return base64_encode(
            hash_hmac(
                'sha256',
                $username . \Yii::$app->getModule('user-management')->params['awsParams']['AWS_CLIENT_ID'],
                \Yii::$app->getModule('user-management')->params['awsParams']['AWS_APP_SECRET'],
                true
            )
        );
    }
} 