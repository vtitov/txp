<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TxpApiUser */
/* @var $ownerMap \app\modules\UserManagement\models\TxpApiUserOwnerMapBatch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="txp-api-user-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-8">
            <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
            <h3><?= Html::encode(Yii::t('app', 'Банки-эквайеры')) ?></h3>

            <p>
                <?= Html::checkbox(null, false, [
                    'label' => Yii::t('app', 'Выбрать все'),
                    'class' => 'check-all',
                ]) ?>
            </p>

            <?= $form->field($ownerMap, 'ownerIds')->checkboxList(
                \yii\helpers\ArrayHelper::map(\app\models\Owners::find()->select(['id', 'name'])->where(['locale' => Yii::$app->language])->asArray()->all(), 'id', 'name'),
                ['separator' => '<br/>']
            )->label(false) ?>
        </div>
    </div>



    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Сохранить'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php
$js = <<<JS
$('.check-all').click(function() {
    $('input:checkbox[name="TxpApiUserOwnerMapBatch[ownerIds][]"]').prop('checked', this.checked);
});

function updateCheckAllCheckboxState() {
    if ($('input:checkbox[name="TxpApiUserOwnerMapBatch[ownerIds][]"]:checked').length == $('input:checkbox[name="TxpApiUserOwnerMapBatch[ownerIds][]"]').length){
        $('.check-all').prop('checked',true);
    } else {
        $('.check-all').prop('checked',false);
    }
}

$('input:checkbox[name="TxpApiUserOwnerMapBatch[ownerIds][]"]').change(function() {
    updateCheckAllCheckboxState();
});

$(document).ready(function() {
    updateCheckAllCheckboxState()
});
JS;
$this->registerJs($js);
