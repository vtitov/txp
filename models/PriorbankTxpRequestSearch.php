<?php

namespace app\models;

use app\helpers\DateColumnHelper;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\PriorbankTxpRequest;

/**
 * PriorbankTxpRequestSearch represents the model behind the search form of `app\models\PriorbankTxpRequest`.
 */
class PriorbankTxpRequestSearch extends PriorbankTxpRequest
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'phone', 'currency', 'terminal_status', 'status'], 'integer'],
            [['rq_id', 'unp', 'name_legal_entity', 'registered_address', 'post_address', 'email', 'bic', 'iban', 'contract_id', 'contract_date', 'terminal_id', 'mcc', 'pt_description', 'pt_destination', 'parse_date'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PriorbankTxpRequest::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'currency' => $this->currency,
            'terminal_status' => $this->terminal_status,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'rq_id', $this->rq_id])
            ->andFilterWhere(['like', 'unp', $this->unp])
            ->andFilterWhere(['like', 'name_legal_entity', $this->name_legal_entity])
            ->andFilterWhere(['like', 'registered_address', $this->registered_address])
            ->andFilterWhere(['like', 'post_address', $this->post_address])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'bic', $this->bic])
            ->andFilterWhere(['like', 'iban', $this->iban])
            ->andFilterWhere(['like', 'contract_date', $this->contract_date])
            ->andFilterWhere(['like', 'contract_id', $this->contract_id])
            ->andFilterWhere(['like', 'terminal_id', $this->terminal_id])
            ->andFilterWhere(['like', 'mcc', $this->mcc])
            ->andFilterWhere(['like', 'pt_description', $this->pt_description])
            ->andFilterWhere(['like', 'pt_destination', $this->pt_destination]);

        DateColumnHelper::addFilterParams(['parse_date'], [$this->parse_date], $query);

        return $dataProvider;
    }
}
