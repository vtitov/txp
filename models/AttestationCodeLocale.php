<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "attestation_code_locale".
 *
 * @property int $attestation_code
 * @property string $name
 * @property string|null $description
 * @property string $locale
 */
class AttestationCodeLocale extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'attestation_code_locale';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('t2p_monitoring');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [];
    }
}
