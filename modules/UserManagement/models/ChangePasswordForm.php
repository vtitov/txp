<?php

namespace app\modules\UserManagement\models;

use app\modules\UserManagement\helpers\LoginHelper;
use Yii;
use yii\base\Model;
use yii\db\Expression;

class ChangePasswordForm extends Model
{
    public $username;
    public $currentPassword;
    public $password;
    public $repeatPassword;

    private $_user = false;

    public function rules()
    {
        return [
            [['currentPassword', 'password', 'repeatPassword'], 'required'],
            [['username'], 'required', 'when' => function ($model) {
                return Yii::$app->user->isGuest;
            }, 'whenClient' => false],
            [['username', 'currentPassword', 'password', 'repeatPassword'], 'string', 'max' => 255],
            ['repeatPassword', 'compare', 'compareAttribute' => 'password'],
            ['password', 'compare', 'compareAttribute' => 'username', 'operator' => '!='],
            ['password', 'compare', 'compareAttribute' => 'currentPassword', 'operator' => '!='],
            [['password'], 'match', 'pattern' => Yii::$app->getModule('user-management')->params['passwordPattern'], 'message' => Yii::$app->getModule('user-management')->params['weakPasswordMessage']],
            ['currentPassword', 'validateCurrentPassword'],
            ['password', 'validatePasswordHistory'],
        ];
    }

    public function validateCurrentPassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();

            if (!$user || !$user->validatePassword($this->currentPassword)) {
                $this->addError($attribute, \Yii::t('app', 'Неверное имя пользователя или пароль'));
            }
        }
    }

    public function validatePasswordHistory($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if ($user) {
                $passwordHistory = PasswordHistory::find()
                    ->where(['user_id' => $user->id])
                    ->orderBy(['date' => SORT_DESC])
                    ->limit(Yii::$app->getModule('user-management')->params['passwordHistoryLength'])
                    ->all();

                foreach ($passwordHistory as $pswd) {
                    if (Yii::$app->security->validatePassword($this->password, $pswd->password_hash)) {
                        $this->addError(
                            $attribute,
                            \Yii::t(
                                'app',
                                'Новый пароль не должен повторять один из {count} Ваших предыдущих паролей',
                                ['count' => Yii::$app->getModule('user-management')->params['passwordHistoryLength']]
                            )
                        );
                        break;
                    }
                }
            }
        }
    }

    public function changePassword()
    {
        $loginHelper = new LoginHelper(Yii::$app->user->isGuest ? $this->username : Yii::$app->user->identity->username);

        if ($loginHelper->tooManyAttempts()) {
            $this->addError('currentPassword', \Yii::t('app', 'Превышен лимит попыток авторизации'));
            return false;
        }

        if ($this->validate()) {

            $loginHelper->resetFailedAttemptsCounter();

            $user = $this->getUser();

            $transaction = Yii::$app->db->beginTransaction();
            try {
                $user->setPassword($this->password);
                $user->temp_password = 0;
                $user->save(false, ['password_hash', 'temp_password']);

                $passHistory = new PasswordHistory();
                $passHistory->user_id = $user->id;
                $passHistory->password_hash = $user->password_hash;
                $passHistory->date = new Expression('now()');
                $passHistory->save(false);

                $transaction->commit();
            } catch (\Exception $e) {
                $transaction->rollBack();
                throw $e;
            } catch (\Throwable $e) {
                $transaction->rollBack();
                throw $e;
            }

            if (Yii::$app->user->isGuest) {
                Yii::$app->user->login($user, 0);
                $loginHelper->saveAttempt(true);
            }
            return true;

        } else {
            $errors = $this->getErrors();
            if (array_key_exists('currentPassword', $errors)) {
                $loginHelper->incrementFailedAttemptsCounter();
            }
        }

        return false;
    }

    public function getUser()
    {
        if (Yii::$app->user->identity !== null) {
            $this->_user = Yii::$app->user->identity;
        } else if ($this->_user === false) {
            $this->_user = User::findByUsername($this->username);
        }

        return $this->_user;
    }

    public function attributeLabels()
    {
        return [
            'username' => \Yii::t('app', 'Логин'),
            'currentPassword' => \Yii::t('app', 'Текущий пароль'),
            'password' => \Yii::t('app', 'Пароль'),
            'repeatPassword' => \Yii::t('app', 'Повторите пароль'),
        ];
    }
}
