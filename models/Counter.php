<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "counter".
 *
 * @property int $id
 * @property string $name
 * @property int $counter_type
 * @property string|null $description
 * @property int $risk_severity_id
 */
class Counter extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'counter';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('t2p_monitoring');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            /*'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'counter_type' => Yii::t('app', 'Counter Type'),
            'description' => Yii::t('app', 'Description'),
            'risk_severity_id' => Yii::t('app', 'Risk Severity ID'),*/
        ];
    }
}
