<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use app\helpers\RandomImageHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\models\FitsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = \Yii::t('app', 'Справочник FIT');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fits-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <img src="<?= RandomImageHelper::getRandomImageFromDir('img/cards') ?>" height="180px" align="right" style="margin-top: -130px">
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(\Yii::t('app', 'Добавить FIT'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'fit',
            [
                'attribute' => 'ips_types_id',
                'value' => 'ipsTypes.name',
                'filter' => ArrayHelper::map(\app\models\IpsTypes::find()->asArray()->all(), 'id', 'name'),
            ],


            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
