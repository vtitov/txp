<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TransactionCountAcquirerReportSearch */
/* @var $dailyDataProvider yii\data\ArrayDataProvider */
/* @var $totalDataProvider yii\data\ArrayDataProvider */

$this->title = Yii::t('app', 'Отчет по количеству транзакций');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="transaction-count-acquirer-report">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="transaction-count-acquirer-report-search">

        <?php $form = ActiveForm::begin(); ?>

        <div class="row">
            <div class="col-md-6">
                <?= $form->field($searchModel, 'dateRange')->widget(
                    \kartik\daterange\DateRangePicker::className(),
                    \app\helpers\DateColumnHelper::getWidgetConfig())
                    ->label(Yii::t('app', 'Период'))
                ?>
            </div>
            <div class="col-md-6">

                <?php
                if (Yii::$app->user->identity->owner_id == 1) {
                    echo $form->field($searchModel, 'ownerId')
                        ->dropDownList(
                            \yii\helpers\ArrayHelper::map(
                                \app\models\Owners::find()
                                    ->select(['id', 'name'])
                                    ->where(['locale' => Yii::$app->language])
                                    ->asArray()
                                    ->all(),
                                'id',
                                'name'
                            ),
                            ['prompt' => '']
                        )
                        ->label(Yii::t('app', 'Банк-эквайер'));
                }
                ?>
            </div>
        </div>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Показать'), ['class' => 'btn btn-success', 'name' => 'action', 'value' => 'render']) ?>
            <?= Html::submitButton(Yii::t('app', 'Экспорт в CSV'), ['class' => 'btn btn-default', 'name' => 'action', 'value' => 'export']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

    <?php
    if ($dailyDataProvider != null) {
        echo GridView::widget([
            'dataProvider' => $dailyDataProvider,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                [
                    'attribute' => 'acquirer_name',
                    'label' => Yii::t('app', 'Банк-эквайер')
                ],
                [
                    'attribute' => 'date',
                    'label' => Yii::t('app', 'Дата')
                ],
                [
                    'attribute' => 'payment_count',
                    'label' => Yii::t('app', 'Количество оплат')
                ],
                [
                    'attribute' => 'reversal_count',
                    'label' => Yii::t('app', 'Количество отмен')
                ],
                [
                    'attribute' => 'refund_count',
                    'label' => Yii::t('app', 'Количество возвратов')
                ],
            ],
        ]);
    }

    if ($totalDataProvider != null) {
        echo '<h3>' . Html::encode(Yii::t('app', 'Итого:')) . '</h3>';
        echo GridView::widget([
            'dataProvider' => $totalDataProvider,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                [
                    'attribute' => 'acquirer_name',
                    'label' => Yii::t('app', 'Банк-эквайер')
                ],
                [
                    'attribute' => 'payment_count',
                    'label' => Yii::t('app', 'Количество оплат')
                ],
                [
                    'attribute' => 'reversal_count',
                    'label' => Yii::t('app', 'Количество отмен')
                ],
                [
                    'attribute' => 'refund_count',
                    'label' => Yii::t('app', 'Количество возвратов')
                ],
            ],
        ]);
    }
    ?>

</div>
