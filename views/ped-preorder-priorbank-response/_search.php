<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PedPreorderPriorbankResponseSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ped-preorder-priorbank-response-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'preorder_id') ?>

    <?= $form->field($model, 'file_number') ?>

    <?= $form->field($model, 'name_legal_entity') ?>

    <?= $form->field($model, 'unp') ?>

    <?php // echo $form->field($model, 'email') ?>

    <?php // echo $form->field($model, 'pos_info') ?>

    <?php // echo $form->field($model, 'contract') ?>

    <?php // echo $form->field($model, 'contract_date') ?>

    <?php // echo $form->field($model, 'trading_name') ?>

    <?php // echo $form->field($model, 'phone_trading_name') ?>

    <?php // echo $form->field($model, 'mcc') ?>

    <?php // echo $form->field($model, 'country') ?>

    <?php // echo $form->field($model, 'region') ?>

    <?php // echo $form->field($model, 'locality') ?>

    <?php // echo $form->field($model, 'address') ?>

    <?php // echo $form->field($model, 'terminal_id') ?>

    <?php // echo $form->field($model, 'merchant_id') ?>

    <?php // echo $form->field($model, 'parse_date') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
