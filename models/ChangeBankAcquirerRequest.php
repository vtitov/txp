<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "change_bank_acquirer_request".
 *
 * @property string $id
 * @property string $merchant_id
 * @property string $name
 * @property string $registered_address
 * @property string $post_address
 * @property string $unp
 * @property string $email
 * @property string $mobile_phone
 * @property string $organisation_phone
 * @property string $head_full_name
 * @property string $iban
 * @property string $bic
 * @property integer $device_number
 * @property string $serial_number
 * @property integer $status
 * @property string $add_date
 */
class ChangeBankAcquirerRequest extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'change_bank_acquirer_request';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('mpos');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['merchant_id', 'name', 'registered_address', 'post_address', 'email', 'mobile_phone', 'organisation_phone', 'head_full_name', 'iban', 'bic', 'serial_number'], 'required'],
            [['merchant_id', 'mobile_phone', 'organisation_phone', 'device_number', 'status'], 'integer'],
            [['add_date'], 'safe'],
            [['name', 'registered_address', 'post_address', 'head_full_name'], 'string', 'max' => 255],
            [['email'], 'string', 'max' => 50],
            [['iban'], 'string', 'max' => 34],
            [['bic'], 'string', 'max' => 11],
            [['unp'], 'string', 'min' => 9, 'max' => 9],
            [['serial_number'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'merchant_id' => 'Merchant ID',
            'name' => 'Наименование организации',
            'registered_address' => 'Юридический адрес',
            'post_address' => 'Почтовый адрес',
            'unp' => 'УНП',
            'email' => 'Email',
            'mobile_phone' => 'Мобильный телефон',
            'organisation_phone' => 'Телефон организации',
            'head_full_name' => 'ФИО руководителя',
            'iban' => 'Расчетный счет (в формате iban)',
            'bic' => 'Код банка (новый формат)',
            'device_number' => 'Device Number',
            'serial_number' => 'серийный номер ped\'а',
            'status' => 'Status',
            'add_date' => 'Дата подачи заявки',
        ];
    }
}
