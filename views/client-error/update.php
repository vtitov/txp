<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ClientError */

$this->title = Yii::t('app', 'Редактирование ошибки: ') . $model->id . ' (' . $model->locale . ')';
$this->params['breadcrumbs'][] = ['label' => \Yii::t('app', 'Справочник ошибок'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id . ' (' . $model->locale . ')', 'url' => ['view', 'id' => $model->id, 'locale' => $model->locale]];
$this->params['breadcrumbs'][] = \Yii::t('app', 'Редактирование');
?>
<div class="client-error-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
