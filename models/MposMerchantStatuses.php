<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mpos_merchant_statuses".
 *
 * @property string $id
 * @property string $name
 * @property string $description
 *
 * @property MposMerchants[] $mposMerchants
 */
class MposMerchantStatuses extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'merchant_status';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('mpos');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 40],
            ['name', 'required'],
            ['name', 'unique'],
            [['description'], 'string', 'max' => 255],
            ['color', 'string', 'max' => 7],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => \Yii::t('app', 'Название'),
            'description' => \Yii::t('app', 'Описание'),
            'color' => \Yii::t('app', 'Цвет'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMposMerchants()
    {
        return $this->hasMany(MposMerchants::className(), ['mpos_merchant_status_id' => 'id']);
    }
}
