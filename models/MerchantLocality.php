<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "merchant_locality".
 *
 * @property string $id
 * @property string $name
 * @property string $lat
 * @property string $lng
 */
class MerchantLocality extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'merchant_locality';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('paybycard');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'lat', 'lng'], 'required'],
            [['lat', 'lng'], 'number'],
            [['name'], 'string', 'max' => 255],
            ['name', 'unique'],
            ['name', 'trim'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название населенного пункта',
            'lat' => 'Широта',
            'lng' => 'Долгота',
        ];
    }
}
