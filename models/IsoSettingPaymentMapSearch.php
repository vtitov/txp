<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\IsoSettingPaymentMap;

/**
 * IsoSettingPaymentMapSearch represents the model behind the search form of `app\models\IsoSettingPaymentMap`.
 */
class IsoSettingPaymentMapSearch extends IsoSettingPaymentMap
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['payment_id', 'currency'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $iso_setting_id)
    {
        $query = IsoSettingPaymentMap::find()
            ->andWhere(['iso_setting_id' => $iso_setting_id]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => false,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'payment_id' => $this->payment_id,
            'currency' => $this->currency,
        ]);

        return $dataProvider;
    }
}
