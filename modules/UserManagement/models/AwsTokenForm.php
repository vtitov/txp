<?php

namespace app\modules\UserManagement\models;

use app\modules\UserManagement\helpers\AwsCognitoHelper;
use Aws\Exception\AwsException;
use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 */
class AwsTokenForm extends Model
{
    public $mfa_token;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['mfa_token'], 'required'],
        ];
    }


    /**
     * Запрос кода для привязки второго фактора
     * @param $awsSession string
     * @return string|bool возвращает строку для генерации qr кода, или false, если сессия истекла
     */
    public function sendAssociateSoftwareTokenRequest($awsSession)
    {
        Yii::info('Sending associateSoftwareToken request');

        try {
            $client = AwsCognitoHelper::getClient();
            $response = $client->associateSoftwareToken([
                'Session' => $awsSession,
            ]);

            if (YII_DEBUG) {
                Yii::info('AWS associateSoftwareToken response: ' . print_r($response, true));
            }
        } catch (AwsException $e) {
            if ($e->getAwsErrorCode() == 'NotAuthorizedException') {
                Yii::warning('AWS Exception: ' . $e->getAwsErrorMessage());
                Yii::$app->session->setFlash('awsRequestError', Yii::t('app', 'Сессия истекла'));
                return false;
            } else {
                Yii::error($e->getAwsErrorMessage());
                throw $e;
            }
        }

        Yii::$app->session->set('aws_cognito_session', $response['Session']);

        $qrString = 'otpauth://totp/psp-admin-panel:' . Yii::$app->session->get('aws_username')
            . '?secret=' . $response['SecretCode'] . '&issuer=psp-admin-panel';

        return $qrString;
    }

    /**
     * Подтверждение привязки второго фактора
     * @return bool надо ли переходить на страницу логина (в т.ч. в случае ошибки из-за истекшей сессии),
     * или остаться на странице привязки второго фактора
     */
    public function sendVerifySoftwareTokenRequest()
    {
        if (!$this->validate()) {
            return false;
        }

        Yii::info('Sending verifySoftwareToken request');

        try {
            $client = AwsCognitoHelper::getClient();
            $response = $client->verifySoftwareToken([
                'Session' => Yii::$app->session->get('aws_cognito_session'),
                'UserCode' => $this->mfa_token,
            ]);

            if (YII_DEBUG) {
                Yii::info('AWS verifySoftwareToken response: ' . print_r($response, true));
            }
        } catch (AwsException $e) {
            if ($e->getAwsErrorCode() == 'CodeMismatchException') {
                Yii::warning('AWS Exception: ' . $e->getAwsErrorMessage());
                $this->addError('mfa_token', \Yii::t('app', 'Неверный код авторизации'));
                return false;
            } else if ($e->getAwsErrorCode() == 'NotAuthorizedException') {
                Yii::warning('AWS Exception: ' . $e->getAwsErrorMessage());
                Yii::$app->session->setFlash('awsRequestError', Yii::t('app', 'Сессия истекла'));
                return true;
            } else {
                Yii::error($e->getAwsErrorMessage());
                throw $e;
            }
        }

        Yii::$app->session->set('aws_cognito_session', $response['Session']);

        if ($response['Status'] == 'SUCCESS') {
            return true;
        } else {
            //TODO перепроверить, когда может вернуться статус ERROR
            $this->addError('mfa_token', Yii::t('app', 'Неверный код авторизации'));
            return false;
        }
    }


    /**
     * Проверка второго фактора при логине
     * @return bool была ли проверка успешной
     */
    public function verifyToken()
    {
        if (!$this->validate()) {
            return false;
        }

        Yii::info('Sending aws mfa token');

        try {
            $client = AwsCognitoHelper::getClient();
            $response = $client->adminRespondToAuthChallenge([
                'ChallengeName' => 'SOFTWARE_TOKEN_MFA',
                'ChallengeResponses' => [
                    'USERNAME' => Yii::$app->session->get('aws_username'),
                    'SOFTWARE_TOKEN_MFA_CODE' => $this->mfa_token,
                    'SECRET_HASH' => AwsCognitoHelper::getSignature(Yii::$app->session->get('aws_username')),
                ],
                'ClientId' => Yii::$app->getModule('user-management')->params['awsParams']['AWS_CLIENT_ID'],
                'Session' => Yii::$app->session->get('aws_cognito_session'),
                'UserPoolId' => Yii::$app->getModule('user-management')->params['awsParams']['AWS_USERPOOL_ID'],
            ]);

            if (YII_DEBUG) {
                Yii::info('AWS adminRespondToAuthChallenge response: ' . print_r($response, true));
            }

        } catch (AwsException $e) {
            if ($e->getAwsErrorCode() == 'CodeMismatchException') {
                Yii::warning('AWS Exception: ' . $e->getAwsErrorMessage());
                $this->addError('mfa_token', \Yii::t('app', 'Неверный код авторизации'));
                return false;
            } else if ($e->getAwsErrorCode() == 'ExpiredCodeException') {
                Yii::warning('AWS Exception: ' . $e->getAwsErrorMessage());
                $this->addError('mfa_token', \Yii::t('app', 'Код авторизации недействителен'));
                return false;
            } else if ($e->getAwsErrorCode() == 'NotAuthorizedException') {
                Yii::warning('AWS Exception: ' . $e->getAwsErrorMessage());
                Yii::$app->session->setFlash('awsRequestError', Yii::t('app', 'Сессия истекла'));
                return false;
            } else {
                Yii::error($e->getAwsErrorMessage());
                throw $e;
            }
        }

        if (isset($response['AuthenticationResult']) && isset($response['AuthenticationResult']['AccessToken'])) {
            //если в ответе получили AccessToken - считаем что пользователь успешно авторизовался
            return true;
        } else {
            Yii::warning('AWS Access Token not received. AWS Response: ' . print_r($response, true));
            return false;
        }
    }

    public function attributeLabels()
    {
        return [
            'mfa_token' => \Yii::t('app', 'Код авторизации'),
        ];
    }
}
