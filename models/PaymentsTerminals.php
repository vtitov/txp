<?php

namespace app\models;

use Yii;
use yii\base\UserException;

/**
 * This is the model class for table "payments_terminals".
 *
 * @property string $id
 * @property int $iso_setting_id
 * @property string $name
 * @property string $description
 * @property string $zpk
 * @property string $zak
 * @property string $settl_time
 * @property int $status
 * @property int $merchant_id
 * @property int $ped_id
 * @property string $activation_code
 * @property int $is_softpos
 * @property int $counter_category_id
 * @property string $mcc
 *
 * @property IsoSettings $isoSettings
 * @property PaymentTerminalStatusLocale $paymentTerminalStatus
 * @property MposMerchants $merchant
 * @property MposPeds $ped
 * @property CounterCategory $counterCategory
 */
class PaymentsTerminals extends \yii\db\ActiveRecord
{
    public $unp;
    public $email;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payment_terminal';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('mpos');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['counter_category_id'], 'default', 'value' => 0],
            [['iso_setting_id', 'name', 'unp'], 'required'],
            [['iso_setting_id', 'status', 'counter_category_id'], 'integer'],
            [['name'], 'string', 'max' => 8],
            [['description'], 'string', 'max' => 255],
            [['zpk', 'zak'], 'string', 'max' => 128],
            [['zpk', 'zak'], 'default', 'value' => null],
            [['status'], 'exist', 'skipOnError' => true, 'targetClass' => PaymentTerminalStatus::className(), 'targetAttribute' => ['status' => 'id']],
            [['unp'], 'string', 'min' => 8, 'max' => 15],
            [['email'], 'email'],
            [['unp'], 'validateIsoNameUnp', 'skipOnError' => true],
            [['is_softpos'], 'in', 'range' => [0, 1]],
            [
                ['counter_category_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => CounterCategory::className(),
                'targetAttribute' => ['counter_category_id' => 'id'],
                'when' => function ($model) {
                    return ($model->counter_category_id != '0');
                },
                'message' => \Yii::t('app', 'Категория не найдена в базе')
            ],
            [['mcc'], 'default', 'value' => null],
            [['mcc'], 'match', 'pattern' => '/^\d{4}$/', 'message' => \Yii::t('app', 'MCC-код должен состоять из четырех цифр')],
            [['settl_time'], 'validateTime'],
        ];
    }

    public function validateTime($attribute)
    {
        if (!preg_match('/^\d{2,3}:[0-5][0-9]:[0-5][0-9]$/', $this->$attribute)) {
            $this->addError($attribute, Yii::t('app', 'Некорректный формат даты'));
            return;
        }

        $numbers = explode(':', $this->$attribute);
        $hours = intval($numbers[0], 10);
        if ($hours > 838) {
            $this->addError($attribute, Yii::t('app', 'Значение не должно превышать 838:59:59'));
        }
    }

    public function validateIsoNameUnp($attribute)
    {
        //валидация iso-настройки
        $isoSetting = IsoSettings::findOne($this->iso_setting_id);
        if ($isoSetting === null) {
            $this->addError('iso_setting_id', Yii::t('app', 'Неизвестная ISO-настройка'));
            return;
        } else if (!(Yii::$app instanceof Yii\console\Application)) {
            //если это не автоматическая регистрация по заявке от банка консольным скриптом, который не запускается под каким-то конкретным пользователем,
            //то проверяем, что пользователь регистрирует платежный терминал для своего банка-эквайера
            $owner_id = Yii::$app->user->identity->owner_id;
            if ($owner_id != 1 && $isoSetting->owner_id != $owner_id) {
                $this->addError('iso_setting_id', Yii::t('app', 'Неизвестная ISO-настройка'));
                return;
            }
        }

        //проверяем уникальность имени платежного терминала в рамках owner'а
        $isNameUniqeQuery = PaymentsTerminals::find()
            ->joinWith(['isoSettings'])
            ->where([PaymentsTerminals::tableName() . '.name' => $this->name])
            ->andWhere([IsoSettings::tableName() . '.owner_id' => $isoSetting->owner_id]);
        if (!$this->isNewRecord) {
            $isNameUniqeQuery->andWhere(['!=', PaymentsTerminals::tableName() . '.id', $this->id]);
        }
        if ($isNameUniqeQuery->exists()) {
            $this->addError('name', Yii::t('app', 'Для банка-эквайера выбранной ISO настройки уже заведен платежный терминал с таким идентификатором'));
        }


        //валидация данных торговца
        $query = MposMerchants::find()->where([
            'unp' => $this->$attribute,
            'owner_id' => $isoSetting->owner_id,
        ])->andFilterWhere(['email' => $this->email]);

        $merchants = $query->all();
        if (count($merchants) == 0) {
            if ($this->email != '') {
                $this->addError($attribute, Yii::t('app', 'Торговец с таким УНП и email не найден для банка-эквайера выбранной ISO-настройки'));
                $this->addError('email', Yii::t('app', 'Торговец с таким УНП и email не найден для банка-эквайера выбранной ISO-настройки'));
            } else {
                $this->addError($attribute, Yii::t('app', 'Торговец с таким УНП не найден для банка-эквайера выбранной ISO-настройки'));
            }
        } else if (count($merchants) > 1) {
            if ($this->email != '') {
                Yii::error('Multiple merchants found for unp "' . $this->unp . '" and email "' . $this->email . '"');
                throw new UserException(Yii::t('app', 'Для данных email и УНП найдено больше одного торговца'));
            } else {
                $this->addError('email', Yii::t('app', 'Для указанного УНП найдено несколько торговцев. Необходимо указать email, чтобы выбрать нужную учетную запись.'));
            }
        } else {
            $this->merchant_id = $merchants[0]->id;
        }

        if (!$this->isNewRecord && $this->merchant_id != $this->oldAttributes['merchant_id'] && $this->ped_id != 0) {
            $this->addError($attribute, Yii::t('app', 'Нельзя изменить Торговца, пока к платежному терминалу привязано устройство'));
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'iso_setting_id' => Yii::t('app', 'Настройка ISO'),
            'name' => Yii::t('app', 'Идентификатор'),
            'description' => Yii::t('app', 'Описание'),
            'zpk' => 'TPK',
            'zak' => 'TAK',
            'settl_time' => Yii::t('app', 'Интервал автоматического закрытия бизнес-дня'),
            'status' => Yii::t('app', 'Статус'),
            'merchant_id' => Yii::t('app', 'Торговец'),
            'ped_id' => Yii::t('app', 'Устройство'),
            'activation_code' => Yii::t('app', 'Код активации'),
            'is_softpos' => 'SoftPos',
            'unp' => Yii::t('app', 'УНП Торговца'),
            'counter_category_id' => \Yii::t('app', 'Категория лимитов'),
            'mcc' => 'MCC',
        ];
    }

    public function getIsoSettings()
    {
        return $this->hasOne(IsoSettings::className(), ['id' => 'iso_setting_id']);
    }

    public function getPaymentTerminalStatus()
    {
        return $this->hasOne(PaymentTerminalStatusLocale::className(), ['id' => 'status'])
            ->andOnCondition([PaymentTerminalStatusLocale::tableName() . '.locale' => Yii::$app->language]);
    }

    public function getMerchant()
    {
        return $this->hasOne(MposMerchants::className(), ['id' => 'merchant_id']);
    }

    public function getPed()
    {
        return $this->hasOne(MposPeds::className(), ['id' => 'ped_id']);
    }

    public function getCounterCategory()
    {
        return $this->hasOne(CounterCategory::className(), ['id' => 'counter_category_id']);
    }

    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        //при заведении платежного терминала или отвязке его от педа генерируем новый код активации
        if ($insert || ($this->ped_id == 0 && $this->oldAttributes['ped_id'] != 0)) {
            $this->generateActivationCode();
        }

        return true;
    }

    public function generateActivationCode()
    {
        $lastActivationCode = PaymentsTerminals::find()->where(['merchant_id' => $this->merchant_id])->max('activation_code');
        if ($lastActivationCode) {
            $this->activation_code = $lastActivationCode + rand(1, 100);
        } else {
            $this->activation_code = rand(100000, 500000);
        }
    }
}
