<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\MposMerchants */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => \Yii::t('app', 'Справочник Торговцев'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
    <div class="mpos-merchants-view">

        <h1><?= Html::encode($this->title) ?></h1>

        <?php
        $sentCodesCount = Yii::$app->session->getFlash('paymentTerminalActivationCodesSent');
        if ($sentCodesCount !== null) {
            if ($sentCodesCount > 0) {
                echo '<div class="alert alert-success">' . Yii::t('app', 'Отправлено кодов активации: ') . $sentCodesCount . '</div>';
            } else {
                echo '<div class="alert alert-warning">' . Yii::t('app', 'Нет свободных платежных терминалов') . '</div>';
            }
        }
        ?>

        <p>
            <?php if ($model->id != 0) {
                echo Html::a(
                    \Yii::t('app', 'Редактировать'),
                    ['update', 'id' => $model->id],
                    ['class' => 'btn btn-primary']
                );
                echo ' ';
                if ($model->owner_id != 6) {//не даем отправить смс с паролем казпочте, они не пользуются нашим личным кабинетом торговца
                    echo Html::a(
                        \Yii::t('app', 'Сгенерировать новый пароль'),
                        ['send-new-password', 'id' => $model->id],
                        ['class' => 'btn btn-success', 'data' => ['method' => 'post']]
                    );
                    echo ' ';
                }
                echo Html::a(
                    \Yii::t('app', 'Отослать коды активации'),
                    ['send-activation-codes', 'id' => $model->id],
                    ['class' => 'btn btn-default']
                );
            } ?>
        </p>

        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                //'id',
                [
                    'attribute' => 'owner_id',
                    'value' => ArrayHelper::getValue($model->owner, 'name', $model->owner_id),
                ],
                'name',
                'registered_address',
                'post_address',
                'unp',
                'bic',
                'iban',
                [
                    'attribute' => 'bank_account_currency_id',
                    'value' => ArrayHelper::getValue($model->currency, 'alpha_code'),
                ],
                'email:email',
                'phone',
                [
                    'attribute' => 'send_mail',
                    'value' => ArrayHelper::getValue(\app\models\MposMerchants::getNotificationChannels(), $model->send_mail),
                ],
                [
                    'attribute' => 'merchant_status_id',
                    'value' => ArrayHelper::getValue($model->mposMerchantStatus, 'name'),
                ],
                'contract',
                'contract_date',
                'iso_merchant_id',
                [
                    'attribute' => 'terminal_counter_category_id',
                    'value' => ArrayHelper::getValue($model->terminalCounterCategory, 'name'),
                ],
                [
                    'attribute' => 'merchant_counter_category_id',
                    'value' => ArrayHelper::getValue($model->merchantCounterCategory, 'name'),
                ],
                'created_at',
            ],
        ]) ?>

    </div>

<?php
$sm = new \app\models\MposTransactionsSearch();
$sm->mpos_merchant_id = $model->name;
$f = ActiveForm::begin([
    'action' => ['mpos-transactions/index'],
    'method' => 'post',
]);
echo $f->field($sm, 'mpos_merchant_id')->hiddenInput()->label(false);
echo Html::submitButton(\Yii::t('app', 'Просмотр транзакций Торговца'), ['class' => 'btn btn-primary', 'name' => 'action', 'value' => 'render']);
ActiveForm::end();
?>

    <div class="merchants-cashier-index">
        <h1><?= Html::encode(\Yii::t('app', 'Список кассиров данного Торговца')) ?></h1>

        <?= GridView::widget([
            'dataProvider' => $cashierDataProvider,
            'filterModel' => $cashierSearchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                //'id',
                //'merchant_id',
                [
                    'attribute' => 'login',
                    'value' => function ($cashier) use ($model) {
                        if ($cashier->id == $model->chief_cashier_id) {
                            return '<span class="glyphicon glyphicon-ok" title="'
                            . Yii::t('app', 'Главный кассир') .
                            '" style="margin-right: 3px"/>' .
                            Html::encode($cashier->login);
                        } else {
                            return Html::encode($cashier->login);
                        }
                    },
                    'format' => 'html',
                ],
                //'password',
                'description',
                //'last_login_date',
                //'login_attempts_count',
                [
                    'attribute' => 'merchant_cashier_status_id',
                    'value' => 'merchantCashierStatus.desc',
                    'filter' => ArrayHelper::map(
                        \app\models\MerchantCashierStatusLocale::find()
                            ->where(['locale' => Yii::$app->language])
                            ->asArray()
                            ->all(),
                        'id',
                        'desc'
                    ),
                ],

                [
                    'class' => 'yii\grid\ActionColumn',
                    'controller' => 'merchant-cashier',
                    'visibleButtons' => [
                        'update' => function($cashier) use ($model) {
                            return ($cashier->id != $model->chief_cashier_id);
                        },
                        'delete' => function($cashier) use ($model) {
                            return ($cashier->id != $model->chief_cashier_id);
                        }
                    ]
                ],
            ],
        ]); ?>

        <?= Html::a(\Yii::t('app', 'Добавить кассира'), ['merchant-cashier/create', 'merchant_id' => $model->id], ['class' => 'btn btn-success']) ?>

    </div>

    <div class="payment-terminals-index">

        <h1><?= Html::encode(\Yii::t('app', 'Список платежных терминалов данного Торговца')) ?></h1>
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <?= GridView::widget([
            'dataProvider' => $paymentTerminalsDataProvider,
            'filterModel' => $paymentTerminalsSearchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                //'id',
                'name',
                'description',
                [
                    'attribute' => 'status',
                    'value' => 'paymentTerminalStatus.name',
                    'filter' => \yii\helpers\ArrayHelper::map(
                        \app\models\PaymentTerminalStatusLocale::find()
                            ->where(['locale' => Yii::$app->language])
                            ->asArray()
                            ->all(),
                        'id',
                        'name'
                    ),
                ],
                [
                    'attribute' => 'ped_id',
                    'value' => function ($model) {
                        if ($model->ped) {
                            return Html::a(
                                Html::encode($model->ped->serial_number),
                                ['mpos-peds/view', 'id' => $model->ped_id]
                            );
                        } else {
                            return $model->ped_id;
                        }
                    },
                    'format' => 'html',
                ],
                [
                    'attribute' => 'iso_setting_id',
                    'value' => 'isoSettings.name',
                    'filter' => \yii\helpers\ArrayHelper::map(
                        \app\models\IsoSettings::find()
                            ->select(['id', 'name'])
                            ->orderBy(['name' => SORT_ASC])
                            ->asArray()
                            ->all(),
                        'id',
                        'name'
                    ),
                ],

                [
                    'class' => 'yii\grid\ActionColumn',
                    'options' => ['width' => '70px'],
                    'controller' => 'payments-terminals',
                    'template' => '{view} {update}',
                ],
            ],
        ]); ?>

    </div>

    <div class="mpos-peds-index">

        <h1><?= Html::encode(\Yii::t('app', 'Список зарегистрированных устройств для данного Торговца')) ?></h1>
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <?= GridView::widget([
            'dataProvider' => $pedsDataProvider,
            'filterModel' => $pedsSearchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                //'id',
                /*[
                    'attribute' => 'ped_model_id',
                    'value' => 'mposPedModel.name',
                    'filter' => ArrayHelper::map(\app\models\MposPedModels::find()->asArray()->all(), 'id', 'name'),
                ],*/
                'serial_number',
                //'merchant_id',
                [
                    'attribute' => 'ped_status_id',
                    'value' => 'mposPedStatus.name',
                    'filter' => ArrayHelper::map(
                        \app\models\PedStatusLocale::find()
                            ->where(['locale' => Yii::$app->language])
                            ->asArray()
                            ->all(),
                        'id',
                        'name'
                    )
                ],
                'description',
                [
                    'attribute' => 'payment_terminal_id',
                    'value' => function ($model) {
                        return Html::a(Html::encode($model->paymentTerminalName), ['payments-terminals/view', 'id' => $model->payment_terminal_id]) .
                        Html::img('@web/img/owners/' . $model->paymentTerminalOwner . '.png',
                            ['style' => 'float: right']);
                    },
                    'format' => 'html',
                    'label' => \Yii::t('app', 'Платежный терминал'),
                ],
                'app_ver',
                [
                    'class' => 'yii\grid\ActionColumn',
                    'controller' => 'mpos-peds',
                ],
            ],
        ]); ?>

    </div>

<?php if (Yii::$app->user->can('merchantPedAssign')): ?>
    <!--<div class="assign-ped-form">

        <h1><?/*= Html::encode(Yii::t('app', 'Добавление устройства Торговцу')) */?></h1>

        <?php /*$form = ActiveForm::begin(); */?>

        <?/*= $form->field($pedToBeAssigned, 'serial_number')->textInput(['autocomplete' => 'off'])->label(Yii::t('app', 'Введите серийный номер устройства')) */?>

        <div class="form-group">
            <?/*= Html::submitButton(\Yii::t('app', 'Привязать'), ['class' => 'btn btn-success']) */?>
        </div>

        <?php /*ActiveForm::end(); */?>

    </div>-->
<?php endif; ?>

<?php if ($model->id != 0 && ($model->merchant_status_id == 1 || $model->merchant_status_id == 4)): ?>

    <div class="send-message">
        <h1><?= Html::encode(Yii::t('app', 'Отправить сообщение Торговцу')) ?></h1>

        <?php $form = ActiveForm::begin(['action' => Url::to(['message-to-merchant', 'id' => $model->id])]); ?>

        <label class="radio-inline"><input type="radio" name="send_by" value="email" checked>Email</label>

        <label class="radio-inline"><input type="radio" name="send_by" value="sms">SMS</label>

        <label class="radio-inline"><input type="radio" name="send_by" value="both">Email + SMS</label>

        <input type="text" name="subject" class="form-control"
               placeholder="<?= Yii::t('app', 'Введите тему сообщения') ?>">

        <textarea name="text" class="form-control" rows="5"
                  placeholder="<?= Yii::t('app', 'Введите текст сообщения') ?>"
                  style="width: 100%"></textarea>

        <div class="form-group">
            <?= Html::submitButton(\Yii::t('app', 'Отправить сообщение'), ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>

<?php endif; ?>