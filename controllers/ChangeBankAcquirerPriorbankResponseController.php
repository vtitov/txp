<?php

namespace app\controllers;

use Yii;
use app\models\ChangeBankAcquirerPriorbankResponse;
use app\models\ChangeBankAcquirerPriorbankResponseSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ChangeBankAcquirerPriorbankResponseController implements the CRUD actions for ChangeBankAcquirerPriorbankResponse model.
 */
class ChangeBankAcquirerPriorbankResponseController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['view'],
                        'roles' => ['priorMposRequestView'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['change-status'],
                        'roles' => ['priorMposRequestEdit'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Displays a single ChangeBankAcquirerPriorbankResponse model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionChangeStatus($id)
    {
        $model = $this->findModel($id);
        $model->status = 1 - $model->status;
        $model->save();

        return $this->redirect(['view', 'id' => $id]);
    }

    /**
     * Finds the ChangeBankAcquirerPriorbankResponse model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return ChangeBankAcquirerPriorbankResponse the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ChangeBankAcquirerPriorbankResponse::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
