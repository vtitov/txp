<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "fits_map".
 *
 * @property string $fits_id
 * @property string $fits_groups_id
 *
 * @property Fits $fits
 * @property FitsGroups $fitsGroups
 */
class FitsMap extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'fit_map';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('mpos');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fits_id', 'fits_groups_id'], 'required'],
            [['fits_id', 'fits_groups_id'], 'integer'],
            ['fits_id', 'unique', 'targetAttribute' => ['fits_id', 'fits_groups_id'], 'message' => \Yii::t('app', 'FIT уже добавлен в группу')],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'fits_id' => 'Fits ID',
            'fits_groups_id' => 'Fits Groups ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFits()
    {
        return $this->hasOne(Fits::className(), ['id' => 'fits_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFitsGroups()
    {
        return $this->hasOne(FitsGroups::className(), ['id' => 'fits_groups_id']);
    }
}
