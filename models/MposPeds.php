<?php

namespace app\models;

use app\helpers\Notification;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * This is the model class for table "ped".
 *
 * @property int $id
 * @property int $ped_model_id
 * @property string $serial_number
 * @property int $owner_id
 * @property int $merchant_id
 * @property int $ped_status_id
 * @property string $description
 * @property string $add_date
 * @property int $payment_terminal_id
 * @property int $ped_conf_category_id
 * @property int $ped_caps
 * @property string $mcc
 * @property string $app_ver
 * @property int $os_id
 * @property int $counter_category_id
 *
 * @property PaymentsTerminals $paymentTerminal
 * @property MposMerchants $mposMerchant
 * @property MposPedModels $mposPedModel
 * @property PedStatusLocale $mposPedStatus
 * @property PedConfCategory $pedConfCategory
 * @property OsAllowed $os
 * @property CounterCategory $counterCategory
 */
class MposPeds extends \yii\db\ActiveRecord
{
    private $paymentTerminalName;

    public function  getPaymentTerminalName()
    {
        if (isset($this->paymentTerminalName)) {
            return $this->paymentTerminalName;
        } elseif ($this->paymentTerminal !== null) {
            return $this->paymentTerminalName = $this->paymentTerminal->name;
        } else {
            return '';
        }
    }

    public function setPaymentTerminalName($name)
    {
        $this->paymentTerminalName = $name;
    }


    private $_magStripe;

    public function getMagStripe()
    {
        if (isset($this->_magStripe)) {
            return $this->_magStripe;
        } else {
            return ($this->ped_caps & 1) == 0 ? 0 : 1;
        }
    }

    public function setMagStripe($magStripe)
    {
        $this->_magStripe = $magStripe;
    }

    private $_emv;

    public function getEmv()
    {
        if (isset($this->_emv)) {
            return $this->_emv;
        } else {
            return ($this->ped_caps & 2) == 0 ? 0 : 1;
        }
    }

    public function setEmv($emv)
    {
        $this->_emv = $emv;
    }

    private $_contactless;

    public function getContactless()
    {
        if (isset($this->_contactless)) {
            return $this->_contactless;
        } else {
            return ($this->ped_caps & 4) == 0 ? 0 : 1;
        }
    }

    public function setContactless($contactless)
    {
        $this->_contactless = $contactless;
    }

    private $_manualPanEntry;

    public function getManualPanEntry()
    {
        if (isset($this->_manualPanEntry)) {
            return $this->_manualPanEntry;
        } else {
            return ($this->ped_caps & 8) == 0 ? 0 : 1;
        }
    }

    public function setManualPanEntry($manualPanEntry)
    {
        $this->_manualPanEntry = $manualPanEntry;
    }


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ped';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('mpos');
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'add_date',
                ],
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['counter_category_id'], 'default', 'value' => 0],
            [['ped_model_id', 'serial_number', 'owner_id', 'ped_status_id', 'paymentTerminalName'], 'required'],
            [['ped_model_id', 'owner_id', 'merchant_id', 'ped_status_id', 'payment_terminal_id', 'counter_category_id'], 'integer'],
            [['paymentTerminalName'], 'validatePaymentTerminalName'],//не опускать ниже валидации owner_id - может его переопределить
            [['serial_number'], 'string', 'max' => 36],
            [['serial_number'], 'match', 'pattern' => '/^(\d{8}|[a-fA-F0-9]+_\d+)$/'],
            [['serial_number'], 'unique'],
            [['description', 'region', 'district', 'locality', 'location'], 'string', 'max' => 255],
            [['mcc'], 'match', 'pattern' => '/^\d{4}$/', 'message' => \Yii::t('app', 'MCC-код должен состоять из четырех цифр')],
            [['ped_status_id'], 'filter', 'filter' => 'intval'],//без этого целые значения попадают в $changedAttributes функции afterSave даже если они не менялись, как следствие отправляется смс-ка даже если статус устройства не менялся при редактировании
            [['owner_id'], 'exist', 'targetClass' => Owners::className(), 'targetAttribute' => ['owner_id' => 'id']],
            [['ped_conf_category_id'], 'exist', 'targetClass' => PedConfCategory::className(), 'targetAttribute' => ['ped_conf_category_id' => 'id']],
            [['magStripe', 'emv', 'contactless', 'manualPanEntry'], 'in', 'range' => [0, 1]],
            [['ped_status_id'], 'exist', 'skipOnError' => true, 'targetClass' => MposPedStatuses::className(), 'targetAttribute' => ['ped_status_id' => 'id']],
            [
                ['counter_category_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => CounterCategory::className(),
                'targetAttribute' => ['counter_category_id' => 'id'],
                'when' => function ($model) {
                    return ($model->counter_category_id != '0');
                },
                'message' => \Yii::t('app', 'Категория не найдена в базе')
            ],
        ];
    }

    public function validatePaymentTerminalName($attribute, $params)
    {
        /*
         * перестраховка, на случай если в POST запросе прилетит что-то другое
         * проверка !(Yii::$app instanceof Yii\console\Application) нужна, чтобы не ломать автоматическую регистрацию по заявкам от банка
         * консольные скрипты не запускаются под каким-то конкретным пользователем
         * */
        if (!(Yii::$app instanceof Yii\console\Application) && Yii::$app->user->identity->owner_id != 1) {
            $this->owner_id = Yii::$app->user->identity->owner_id;
        }

        $paymentTerminal = PaymentsTerminals::find()
            ->joinWith(['isoSettings'])
            ->where(['payment_terminal.name' => $this->$attribute])
            ->andWhere([
                'or',
                ['iso_setting.owner_id' => $this->owner_id],
                ['payment_terminal.id' => 1]
            ])
            ->one();

        if ($paymentTerminal === null) {
            $this->addError($attribute, \Yii::t('app', 'Платежный терминал с таким именем не найден для выбранного владельца'));
            return;
        }

        if ($paymentTerminal->status != 1) {
            $this->addError($attribute, Yii::t('app', 'Платежный терминал {status}', ['status' => $paymentTerminal->paymentTerminalStatus->name]));
            return;
        }

        if ($paymentTerminal->ped_id != 0 && $paymentTerminal->ped_id != $this->id) {
            $this->addError($attribute, \Yii::t('app', 'Платежный терминал привязан к другому устройству'));
            return;
        }

        $this->payment_terminal_id = $paymentTerminal->id;

        if ($paymentTerminal->id != 1 && $this->merchant_id != $paymentTerminal->merchant_id) {//'1' - платежный терминал "не задан"
            $this->addError($attribute, \Yii::t('app', 'Платежный терминал и устройство принадлежат разным торговцам'));
            return;
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ped_model_id' => \Yii::t('app', 'Модель'),
            'serial_number' => \Yii::t('app', 'Серийный номер'),
            'owner_id' => \Yii::t('app', 'Владелец'),
            'merchant_id' => \Yii::t('app', 'Торговец'),
            'ped_status_id' => \Yii::t('app', 'Статус'),
            'description' => \Yii::t('app', 'Описание'),
            'add_date' => \Yii::t('app', 'Дата добавления'),
            'payment_terminal_id' => \Yii::t('app', 'Платежный терминал'),
            'paymentTerminalName' => \Yii::t('app', 'Платежный терминал'),
            'mcc' => \Yii::t('app', 'MCC-код'),
            'region' => \Yii::t('app', 'Область'),
            'district' => \Yii::t('app', 'Район'),
            'locality' => \Yii::t('app', 'Населенный пункт'),
            'location' => \Yii::t('app', 'Место расположения'),
            'ped_conf_category_id' => \Yii::t('app', 'Группа конфигурации'),
            'app_ver' => \Yii::t('app', 'Версия приложения'),
            'os_id' => \Yii::t('app', 'Операционная система'),
            'counter_category_id' => \Yii::t('app', 'Категория лимитов'),
            'magStripe' => Yii::t('app', 'Магнитная полоса'),
            'emv' => Yii::t('app', 'Чип'),
            'contactless' => Yii::t('app', 'Бесконтакт'),
            'manualPanEntry' => Yii::t('app', 'Ручной ввод PAN'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMposMerchant()
    {
        return $this->hasOne(MposMerchants::className(), ['id' => 'merchant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMposPedModel()
    {
        return $this->hasOne(MposPedModels::className(), ['id' => 'ped_model_id']);
    }

    public function getOwner()
    {
        return $this->hasOne(Owners::className(), ['id' => 'owner_id'])
            ->andOnCondition([Owners::tableName() . '.locale' => Yii::$app->language]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMposPedStatus()
    {
        return $this->hasOne(PedStatusLocale::className(), ['id' => 'ped_status_id'])
            ->andOnCondition([PedStatusLocale::tableName() . '.locale' => Yii::$app->language]);
    }

    public function getPaymentTerminal()
    {
        return $this->hasOne(PaymentsTerminals::className(), ['id' => 'payment_terminal_id']);
    }

    public function getPedConfCategory()
    {
        return $this->hasOne(PedConfCategory::className(), ['id' => 'ped_conf_category_id']);
    }

    public function getOs()
    {
        return $this->hasOne(OsAllowed::className(), ['id' => 'os_id']);
    }

    public function getCounterCategory()
    {
        return $this->hasOne(CounterCategory::className(), ['id' => 'counter_category_id']);
    }

    public function getDeviceProfile()
    {
        return $this->hasOne(DeviceProfile::className(), ['deviceId' => 'serial_number']);
    }


    //Метод возвращает id'шник владельца iso-настройки, для выбора нужной иконки принадлежности платежного терминала
    public function getPaymentTerminalOwner()
    {
        if ($this->paymentTerminal !== null && $this->paymentTerminal->isoSettings !== null) {
            return $this->paymentTerminal->isoSettings->owner_id;
        } else {
            return 'unknown';
        }
    }


    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        $this->ped_caps = bindec($this->getManualPanEntry() . $this->getContactless() . $this->getEmv() . $this->getMagStripe());
        return true;
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        //Отправка SMS о смене статуса устройства
        if (array_key_exists('ped_status_id', $changedAttributes) && !$insert && $this->mposMerchant !== null) {

            if ($this->ped_status_id == 1) {
                $res = Notification::sendToMerchant(
                    $this->merchant_id,
                    'device.status.unlocked',
                    ['deviceId' => $this->serial_number]
                );
            } else {
                $res = Notification::sendToMerchant(
                    $this->merchant_id,
                    'device.status.locked',
                    ['deviceId' => $this->serial_number]
                );
            }

            if ($res !== true) {
                Yii::$app->session->addFlash(
                    'error',
                    Yii::t(
                        'app',
                        'Сообщение о смене статуса не отправлено: {errorMessage}',
                        ['errorMessage' => $res]
                    )
                );
            }
        }
    }
}
