<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "bb_clients_sending".
 *
 * @property string $id
 * @property string $phone
 * @property integer $status
 * @property string $date_sent
 */
class SmsSending extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sms_sending';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('paybycard');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['phone'], 'required'],
            [['status'], 'integer'],
            [['date_sent'], 'safe'],
            [['phone'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'phone' => 'Phone',
            'status' => 'Status',
            'date_sent' => 'Date Sent',
        ];
    }
}
