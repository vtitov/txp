<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\FitsGroups;

/**
 * FitsGroupsSearch represents the model behind the search form about `app\models\FitsGroups`.
 */
class FitsGroupsSearch extends FitsGroups
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'fits_groups_types_id'], 'integer'],
            [['name', 'description'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FitsGroups::find()
            ->joinWith('fitsGroupsTypes')
            ->with('fitsGroupsTypes');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['fits_groups_types_id'] = [
            'asc' => [FitsGroupsTypes::tableName() . '.fit_name' => SORT_ASC],
            'desc' => [FitsGroupsTypes::tableName() . '.fit_name' => SORT_DESC]
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'fits_groups_types_id' => $this->fits_groups_types_id,
        ]);

        $query->andFilterWhere(['like', 'fit_group.name', $this->name])
            ->andFilterWhere(['like', 'fit_group.description', $this->description]);

        return $dataProvider;
    }
}
