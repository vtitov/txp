<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AppKey */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="app-key-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'os_id')->dropDownList(
        \yii\helpers\ArrayHelper::map(
            \app\models\OsAllowed::find()->select(['id', 'name'])->all(),
            'id',
            'name'
        ),
        ['prompt' => '']
    ) ?>

    <?= $form->field($model, 'app_ver')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Сохранить'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
