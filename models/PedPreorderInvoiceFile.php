<?php
namespace app\models;

use yii\base\Model;
use yii\web\UploadedFile;

class PedPreorderInvoiceFile extends Model
{
    /**
     * @var UploadedFile
     */
    public $preorder_id;
    public $invoiceFile;

    public function rules()
    {
        return [
            ['preorder_id', 'required'],
            ['preorder_id', 'integer'],
            [['invoiceFile'], 'file', 'skipOnEmpty' => false, 'extensions' => 'xlsx, xls, pdf', 'maxSize' => 900*1024],//по умолчанию в mysql max_allowed_packet = 1M, php upload_max_filesize = 2M
            ['invoiceFile', 'fileNameLengthValidator', 'params' => ['max_file_name_length' => 255]],
        ];
    }

    public function fileNameLengthValidator($attribute, $params){
        if (strlen($this->$attribute->name) > $params['max_file_name_length']){
            $this->addError(
                $attribute,
                \Yii::t(
                    'app',
                    'Длина имени файла не должна превышать {maxFileNameLength} символов',
                    ['maxFileNameLength' => $params['max_file_name_length']]
                )
            );
        }
    }

    public function attributeLabels()
    {
        return [
            'invoiceFile' => \Yii::t('app', 'Файл счет-факуры'),
        ];
    }
}