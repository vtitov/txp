<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "merchant_cashier".
 *
 * @property integer $id
 * @property string $merchant_id
 * @property string $login
 * @property string $password
 * @property string $description
 * @property string $last_login_date
 * @property integer $login_attempts_count
 * @property integer $merchant_cashier_status_id
 *
 * @property MposMerchants $merchant
 */
class MerchantCashier extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'merchant_cashier';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('mpos');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['merchant_cashier_status_id'], 'integer'],
            [['login', 'description'], 'filter', 'filter' => 'trim'],
            [['login'], 'required'],
            [['login'], 'string', 'max' => 50],
            [['description'], 'string', 'max' => 80],
            [['login'], 'unique', 'targetAttribute' => ['login', 'merchant_id'], 'message' => Yii::t('app', 'Такой кассир уже существует у этого торговца')],
            [['merchant_cashier_status_id'], 'exist', 'skipOnError' => true, 'targetClass' => MerchantCashierStatus::className(), 'targetAttribute' => ['merchant_cashier_status_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'merchant_id' => Yii::t('app', 'Торговец'),
            'login' => Yii::t('app', 'Логин'),
            'password' => Yii::t('app', 'Пароль'),
            'description' => Yii::t('app', 'Описание'),
            'last_login_date' => Yii::t('app', 'Дата последнего входа в систему'),
            'login_attempts_count' => Yii::t('app', 'Количество неудачных попыток авторизации'),
            'merchant_cashier_status_id' => Yii::t('app', 'Статус'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMerchant()
    {
        return $this->hasOne(MposMerchants::className(), ['id' => 'merchant_id']);
    }

    public function getMerchantCashierStatus()
    {
        return $this->hasOne(MerchantCashierStatusLocale::className(), ['id' => 'merchant_cashier_status_id'])
            ->andOnCondition([MerchantCashierStatusLocale::tableName() . '.locale' => Yii::$app->language]);
    }
}
