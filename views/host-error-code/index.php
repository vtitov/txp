<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\HostErrorCodeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Справочник Response Code\'ов');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="host-error-code-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <!--<p>
        <?/*= Html::a('Create Host Error Code', ['create'], ['class' => 'btn btn-success']) */?>
    </p>-->

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'iso_settings_id',
                'value' => 'isoSettings.name',
                'filter' => \yii\helpers\ArrayHelper::map(
                    \app\models\IsoSettings::find()
                        ->select(['id', 'name'])
                        ->orderBy(['name' => SORT_ASC])
                        ->asArray()
                        ->all(),
                    'id',
                    'name'
                ),
            ],
            'descr',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}',
            ],
        ],
    ]); ?>

</div>
