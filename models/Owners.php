<?php

namespace app\models;

use app\modules\UserManagement\models\User;
use Yii;

/**
 * This is the model class for table "owners".
 *
 * @property string $id
 * @property string $name
 */
class Owners extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'owner';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('mpos');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentsTerminals()
    {
        return $this->hasMany(PaymentsTerminals::className(), ['id' => 'payments_terminals_id'])->viaTable('owners_payments_terminals_map', ['owners_id' => 'id']);
    }
}
