<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "event_code_locale".
 *
 * @property int $id
 * @property string $locale
 * @property string $name
 * @property string|null $desc
 */

class EventCodeLocale extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'event_code_locale';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('t2p_monitoring');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [];
    }
}
