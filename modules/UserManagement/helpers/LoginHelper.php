<?php

namespace app\modules\UserManagement\helpers;

use app\modules\UserManagement\models\FailedLoginAttemptCounter;
use Yii;
use app\modules\UserManagement\models\LoginHistory;
use yii\db\Expression;

class LoginHelper
{
    private $_username;
    private $_failedAttemptCounter = false;

    public function __construct($username)
    {
        $this->_username = $username;
    }

    private function getFailedAttemptCounter()
    {
        if ($this->_failedAttemptCounter === false) {
            $this->_failedAttemptCounter = FailedLoginAttemptCounter::find()->where(['username' => $this->_username])->one();
            if ($this->_failedAttemptCounter === null) {
                $this->_failedAttemptCounter = new FailedLoginAttemptCounter();
                $this->_failedAttemptCounter->username = $this->_username;
                $this->_failedAttemptCounter->attempt_count = 0;
            }
        }

        return $this->_failedAttemptCounter;
    }

    public function saveAttempt($wasSuccessful)
    {
        $loginHistory = new LoginHistory();
        $loginHistory->username = $this->_username;
        $loginHistory->ip = Yii::$app->request->getUserIP();
        $loginHistory->date = new Expression('NOW()');
        $loginHistory->status = $wasSuccessful ? 1 : 0;

        if (!$loginHistory->save()) {
            Yii::error('Login history saving error: ' . print_r($loginHistory->getErrors(), true));
        }
    }

    public function tooManyAttempts()
    {
        //проверяем только если включена блокировка пользователя после нескольких неверных попыток ввода пароля
        if (Yii::$app->getModule('user-management')->params['failedLoginAttemptsCountBeforeLock'] > 0) {
            $counter = $this->getFailedAttemptCounter();
            if ($counter->lock_date >= date('Y-m-d H:i:s')){
                $this->saveAttempt(false);
                return true;
            }
        }

        return false;
    }

    public function incrementFailedAttemptsCounter(){
        //инкрементируем счетчик только если включена блокировка пользователя после нескольких неверных попыток ввода пароля
        if (Yii::$app->getModule('user-management')->params['failedLoginAttemptsCountBeforeLock'] > 0){
            $counter = $this->getFailedAttemptCounter();
            $counter->attempt_count++;

            if ($counter->attempt_count >= Yii::$app->getModule('user-management')->params['failedLoginAttemptsCountBeforeLock']) {
                $counter->lock_date = date('Y-m-d H:i:s', strtotime('+' . (int)Yii::$app->getModule('user-management')->params['failedLoginAttemptsLockTime'] . ' seconds'));
                $counter->attempt_count = 0;
            }

            if (!$counter->save()) {
                Yii::error('Failed login counter saving error: ' . print_r($counter->getErrors(), true));
            }
        }
    }

    public function resetFailedAttemptsCounter()
    {
        $counter = $this->getFailedAttemptCounter();
        if ($counter->attempt_count != 0){
            $counter->attempt_count = 0;
            if (!$counter->save()) {
                Yii::error('Failed login counter saving error: ' . print_r($counter->getErrors(), true));
            }
        }
    }
}