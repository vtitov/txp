<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PublishedReviews */

$this->title = 'Размещение отзыва на сайте';
$this->params['breadcrumbs'][] = ['label' => 'Опубликованные отзывы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="published-reviews-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
