<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PaymentsMapSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Payments Maps';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payments-map-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Payments Map', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'ped_id',
            'payments_id',
            'currency_id',
            'payments_terminals_id',
            'fits_groups_id',
            // 'fits_groups_id_sign_allowed',
            // 'fits_groups_id_fallback_allowed',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
