<?php

namespace app\models;

use app\helpers\DateColumnHelper;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\RecievedReviews;

/**
 * RecievedReviewsSearch represents the model behind the search form about `app\models\RecievedReviews`.
 */
class RecievedReviewsSearch extends RecievedReviews
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status_id'], 'integer'],
            [['name', 'email', 'text', 'date_recieved'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RecievedReviews::find()
            ->with('reviewStatus');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['date_recieved' => SORT_DESC]],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'status_id' => $this->status_id,
            //'date_recieved' => $this->date_recieved,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'text', $this->text]);

        DateColumnHelper::addFilterParams(['date_recieved'], [$this->date_recieved], $query);

        return $dataProvider;
    }
}
