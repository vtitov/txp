<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\MerchantCashier;

/**
 * MerchantCashierSearch represents the model behind the search form about `app\models\MerchantCashier`.
 */
class MerchantCashierSearch extends MerchantCashier
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'merchant_id', 'login_attempts_count', 'merchant_cashier_status_id'], 'integer'],
            [['login', 'password', 'description', 'last_login_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $merchantId = null)
    {
        $query = MerchantCashier::find()
            ->joinWith('merchantCashierStatus');

        if ($merchantId !== null) {
            $query->andWhere([static::tableName() . '.merchant_id' => $merchantId]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['merchant_cashier_status_id'] = [
            'asc' => [MerchantCashierStatusLocale::tableName() . '.desc' => SORT_ASC],
            'desc' => [MerchantCashierStatusLocale::tableName() . '.desc' => SORT_DESC]
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            //static::tableName() . '.id' => $this->id,
            //static::tableName() . '.merchant_id' => $this->merchant_id,
            //static::tableName() . '.last_login_date' => $this->last_login_date,
            //static::tableName() . '.login_attempts_count' => $this->login_attempts_count,
            static::tableName() . '.merchant_cashier_status_id' => $this->merchant_cashier_status_id,
        ]);

        $query->andFilterWhere(['like', static::tableName() . '.login', $this->login])
            //->andFilterWhere(['like', static::tableName() . '.password', $this->password])
            ->andFilterWhere(['like', static::tableName() . '.description', $this->description]);

        return $dataProvider;
    }
}
