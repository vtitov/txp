<?php

namespace app\modules\UserManagement\controllers;

use Yii;
use app\modules\UserManagement\models\User;
use app\modules\UserManagement\models\UserSearch;
use yii\base\DynamicModel;
use yii\db\Expression;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view'],
                        'roles' => ['userView'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['create', 'update', 'change-status', 'manage-user-roles', 'delete'],
                        'roles' => ['userEdit'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single User model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        if (Yii::$app->getModule('user-management')->params['authChannel'] == 2) {
            $model->updateUserStatusFromAwsCognito();
        }

        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new User();
        $model->scenario = 'create';

        if ($model->load(Yii::$app->request->post())) {
            if (Yii::$app->getModule('user-management')->params['authChannel'] == 2) {
                $transaction = User::getDb()->beginTransaction();
                try {
                    //порядок вызова методов важен:
                    //если вначале успешно выполнится запрос на AWS, а при сохранении в нашу БД вывалится ошибка,
                    //то откатить изменения будет гораздо сложнее
                    if ($model->save() && $model->createAwsCognitoUser()) {
                        $transaction->commit();
                        return $this->redirect(['view', 'id' => $model->id]);
                    } else {
                        $transaction->rollBack();
                    }
                } catch (\Exception $e) {
                    $transaction->rollBack();
                    throw $e;
                } catch (\Throwable $e) {
                    $transaction->rollBack();
                    throw $e;
                }
            } else if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            if (Yii::$app->getModule('user-management')->params['authChannel'] == 2 && $model->password != '') {
                if ($model->save() && $model->setTemporaryPasswordForAwsCognito()) {
                    return $this->redirect(['view', 'id' => $model->id]);
                }
            } else if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionChangeStatus($id)
    {
        $model = $this->findModel($id);
        $model->status = 1 - $model->status;

        if ($model->status){
            $model->block_reason = new Expression('NULL');
        }

        if (Yii::$app->getModule('user-management')->params['authChannel'] == 2) {
            $transaction = User::getDb()->beginTransaction();
            try {
                //порядок вызова методов важен:
                //если вначале успешно выполнится запрос на AWS, а при сохранении в нашу БД вывалится ошибка,
                //то откатить изменения будет гораздо сложнее
                if ($model->save() && $model->changeAwsCognitoUserStatus($model->status)) {
                    $transaction->commit();
                } else {
                    $transaction->rollBack();
                }
            } catch (\Exception $e) {
                $transaction->rollBack();
                throw $e;
            } catch (\Throwable $e) {
                $transaction->rollBack();
                throw $e;
            }
        } else {
            $model->save();
        }

        return $this->redirect(['view', 'id' => $id]);
    }

    public function actionManageUserRoles($id)
    {
        $user = $this->findModel($id);
        $allRoles = Yii::$app->authManager->getRoles();

        if (Yii::$app->request->isPost) {

            $model = new DynamicModel(['roles']);
            $model->addRule('roles', 'each', ['rule' => ['in', 'range' => array_keys($allRoles)]]);

            if ($model->load(Yii::$app->request->post()) && $model->validate()){
                $user->setRoles($model->roles);
            } else {
                Yii::$app->session->setFlash('invalidDataRecieved');
            }
        }

        return $this->render('manage-user-roles', [
            'user' => $user,
            'allRoles' => $allRoles,
            'userRoles' => Yii::$app->authManager->getRolesByUser($id),
        ]);
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        if (Yii::$app->getModule('user-management')->params['authChannel'] == 2) {
            $transaction = User::getDb()->beginTransaction();
            try {
                if (($model->delete() !== false) && $model->deleteAwsCognitoUser()) {
                    $transaction->commit();
                } else {
                    $transaction->rollBack();
                }
            } catch (\Exception $e) {
                $transaction->rollBack();
                throw $e;
            } catch (\Throwable $e) {
                $transaction->rollBack();
                throw $e;
            }
        } else {
            $model->delete();
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
