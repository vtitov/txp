<?php
namespace app\commands;

use app\helpers\Notification;
use app\models\ChangeBankAcquirerPriorbankResponse;
use app\models\MerchantCashier;
use app\models\MerchantsArmUsers;
use app\models\MposMerchants;
use app\models\PaymentsTerminals;
use app\models\PedPreorderPriorbankResponse;
use app\models\PriorbankTxpRequest;
use Yii;
use yii\base\UserException;
use yii\console\Controller;
use yii\db\Expression;

class PriorbankResponseParseController extends Controller
{
    //здесь храним список файлов на ftp сервере из соответствующих директорий
    private $files = [
        'arch' => [],
        'error' => []
    ];
    private $tmpFile;
    private $ftpConn;

    public function actionIndex()
    {
        //временный файл, в который будем сохранять данные из файлов, расположенных на ftp
        $this->tmpFile = @fopen('php://temp', 'r+');
        if ($this->tmpFile === false) {
            $errorText = "Не удалось создать временный файл";
            Yii::error($errorText);
            $this->reportError($errorText);
            ftp_close($this->ftpConn);
            return;
        }

        $this->ftpConnect();

        $files = $this->getFtpFileList('out');
        foreach ($files as $file) {
            if ($file == 'out/arch' || $file == 'out/error') {
                continue;
            }

            $isPreorderResponse = preg_match('/^out\/(ansorder|anschange)_(\d+)~(\d+).xml$/', $file, $matches);
            $isTxpRequest = preg_match('/^out\/ansorder_(\d+).xml$/', $file);

            if (!$isPreorderResponse && !$isTxpRequest) {
                $this->reportError('Неизвестный формат имени файла: ' . $file);
            } else {
                $xmlString = $this->readXmlFile($file);
                //сталкивался с тем, что во время записи файла есть момент когда его содержимое пусто
                //хоть это было и не по ftp, но здесь эта проверка тоже мешать не будет
                if ($xmlString === "") {
                    continue;
                }

                try {
                    $xml = simplexml_load_string($xmlString);
                } catch (\Exception $e) {
                    Yii::error('XML parse error. File content: ' . $xmlString);
                    $this->reportError(
                        Yii::t(
                            'app',
                            'Не удалось распарсить XML-файл {fileName}: {errorMessage}',
                            ['fileName' => $file, 'errorMessage' => $e->getMessage()]
                        )
                    );
                    $this->moveResponseFile($file, 'error');
                    continue;
                } catch (\Throwable $e) {
                    Yii::error('XML parse error. File content: ' . $xmlString);
                    $this->reportError(
                        Yii::t(
                            'app',
                            'Не удалось распарсить XML-файл {fileName}: {errorMessage}',
                            ['fileName' => $file, 'errorMessage' => $e->getMessage()]
                        )
                    );
                    $this->moveResponseFile($file, 'error');
                    continue;
                }

                if ($xml === false) {
                    Yii::error('XML parse error. File content: ' . $xmlString);
                    $this->moveResponseFile($file, 'error');
                    $this->reportError("Не удалось распарсить XML-файл " . $file);
                    continue;
                }

                if ($isPreorderResponse) {
                    $this->parsePreorderResponse($file, $matches, $xml);
                } else {
                    $this->parseTxpRequest($file, $xml);
                }
            }
        }


    }

    private function parsePreorderResponse($file, $matches, $xml)
    {
        if ($matches[1] == 'ansorder') {
            $responseModel = new PedPreorderPriorbankResponse();
            $responseModel->preorder_id = $matches[2];
        } else {
            $responseModel = new ChangeBankAcquirerPriorbankResponse();
            $responseModel->request_id = $matches[2];
        }

        $responseModel->file_number = $matches[3];
        $responseModel->name_legal_entity = (string)$xml->LegalInfo->Namelegalentity;
        $responseModel->unp = (string)$xml->LegalInfo->UNP;
        $responseModel->email = (string)$xml->LegalInfo->Email;
        $responseModel->pos_info = (string)$xml->LegalInfo->POSInfo;
        $responseModel->contract = (string)$xml->LegalInfo->ContractId;
        if (preg_match('/^(0[1-9]|[12][0-9]|3[01])\.(0[1-9]|1[012])\.\d{4}$/', (string)$xml->LegalInfo->ContractDate)) {
            $responseModel->contract_date = date_format(date_create_from_format('d.m.Y', (string)$xml->LegalInfo->ContractDate), 'Y-m-d');
        } else {
            Yii::error("Некорректный формат даты в файле " . $file);
            $this->reportError("Некорректный формат даты в файле " . $file);
            $this->moveResponseFile($file, 'error');
            return;
        }
        $responseModel->trading_name = (string)$xml->Trading->Tradingname;
        $responseModel->phone_trading_name = preg_replace('/[^0-9]/', '', $xml->Trading->PhoneTradingname);
        $responseModel->mcc = (string)$xml->Trading->Mcc;
        $responseModel->country = (string)$xml->Trading->TradingDestination->Country;
        $responseModel->region = (string)$xml->Trading->TradingDestination->Region;
        $responseModel->locality = (string)$xml->Trading->TradingDestination->Locality;
        $responseModel->address = (string)$xml->Trading->TradingDestination->Address;
        $responseModel->terminal_id = (string)$xml->Trading->BankInfo->TerminalId;
        $responseModel->merchant_id = (string)$xml->Trading->BankInfo->MerchantId;
        $responseModel->parse_date = new Expression('now()');

        if ($responseModel->save()) {
            $this->moveResponseFile($file, 'arch');
        } else {
            Yii::error("Не удалось сохранить данные из файла " . $file . " в базу.\nОшибки валидации: " . print_r($responseModel->errors, true));
            $this->reportError("Не удалось сохранить данные из файла " . $file . " в базу.");
            $this->moveResponseFile($file, 'error');
        }
    }

    private function parseTxpRequest($file, $xml)
    {
        $isNewPaymentTerminal = false;//флаг, надо ли отправлять sms об активации платежного терминала, после коммита транзакции


        //сохраняем данные из заявки в базу
        $requestModel = $this->saveTxpRequestData($file, $xml);
        if ($requestModel === false) {
            return;
        }


        //проверяем пришла ли заявка по новым торговцу/терминалу или уже существующим
        $paymentTerminal = PaymentsTerminals::find()->where([
            'name' => $requestModel->terminal_id,
            'iso_setting_id' => Yii::$app->params['priorDefaultIsoSettingId'],
        ])->one();
        if ($paymentTerminal !== null) {
            $merchant = $paymentTerminal->merchant;
            //не даем автоматически перепривязать существующий терминал к другому торговцу
            if ($merchant === null) {
                $this->reportError(Yii::t(
                    'app',
                    'Ошибка при обработке заявки {rqId} из файла {fileName}. Не удалось обновить данные платежного терминала: терминал принадлежит неизвестному торговцу.',
                    ['fileName' => $file, 'rqId' => $requestModel->rq_id]
                ));
                $this->moveResponseFile($file, 'error');
                return;
            }
            if ($merchant->owner_id != 3 || $merchant->unp != $requestModel->unp) {
                $this->reportError(Yii::t(
                    'app',
                    'Ошибка при обработке заявки {rqId} из файла {fileName}. Не удалось обновить данные платежного терминала: терминал принадлежит другому торговцу.',
                    ['fileName' => $file, 'rqId' => $requestModel->rq_id]
                ));
                $this->moveResponseFile($file, 'error');
                return;
            }
        }
        if (!isset($merchant) || $merchant === null) {
            $merchant = MposMerchants::find()
                ->where([
                    'unp' => $requestModel->unp,
                    'owner_id' => 3,
                ])->one();
        }
        if ($paymentTerminal === null) {
            $isNewPaymentTerminal = true;
            $paymentTerminal = new PaymentsTerminals();
        }
        if ($merchant === null) {
            $merchant = new MposMerchants();
        }


        //обновляем данные по заявке
        $transaction = PriorbankTxpRequest::getDb()->beginTransaction();
        try {
            $pass = $this->saveMerchantData($requestModel, $merchant);//для новых торговцев метод возвращает сгенерированный пароль
            $this->savePaymentTerminalData($requestModel, $paymentTerminal);

            $requestModel->status = 1;//заявка обработана
            $requestModel->save(false, ['status']);

            $transaction->commit();
            $this->moveResponseFile($file, 'arch');
        } catch (\Exception $e) {
            $transaction->rollBack();
            $this->reportError(Yii::t(
                'app',
                'Ошибка при обработке заявки {rqId} из файла {fileName}: {errorMessage}',
                ['fileName' => $file, 'rqId' => $requestModel->rq_id, 'errorMessage' => $e->getMessage()]
            ));
            $this->moveResponseFile($file, 'error');
            return;
        } catch (\Throwable $e) {
            $transaction->rollBack();
            $this->reportError(Yii::t(
                'app',
                'Ошибка при обработке заявки {rqId} из файла {fileName}: {errorMessage}',
                ['fileName' => $file, 'rqId' => $requestModel->rq_id, 'errorMessage' => $e->getMessage()]
            ));
            $this->moveResponseFile($file, 'error');
            return;
        }


        //отсылаем регистрационные SMS'ки
        if ($pass) {//новый торговец
            //Yii::t('app', "Уважаемый клиент! Добро пожаловать в сервис Prior SoftPos (tapXphone)! Логином и МПИНом для активации приложения является Ваш email. Код активации будет направлен SMS-сообщением сразу после ввода логина и МПИНа. Ваш пароль для входа в личный кабинет на сайте paybycard.by - {password}, логином служит Ваш email\nВидеоинструкция по установке https://youtu.be/FIBrNmZ1YEc\nВидеоинструкция по запуску https://youtu.be/C7BqfJ_nsxI", ['password' => $pass])
            Notification::sendToMerchant($merchant->id, 'merchant.registration', ['password' => $pass]);
        }
        if ($isNewPaymentTerminal) {//новый платежный терминал
            //Yii::t('app', 'Уважаемый клиент! Ваше устройство Prior SoftPos (tapXphone) на {description} готово к работе. Для активации приложения введите логин и МПИН, которыми является Ваш email. Коды инициаизации и активации терминала придут после ввода логина и МПИН', ['description' => $paymentTerminal->description])
            Notification::sendToMerchant($merchant->id, 'terminal.registration', ['description' => $paymentTerminal->description]);
        }
    }


    private function saveTxpRequestData($file, $xml)
    {
        $requestModel = new PriorbankTxpRequest();

        $requestModel->rq_id = trim((string)$xml->RqId);

        $requestModel->unp = trim((string)$xml->Merchant->UNP);
        $requestModel->name_legal_entity = trim((string)$xml->Merchant->NameLegalEntity);
        $requestModel->registered_address = trim((string)$xml->Merchant->RegisteredAddress);
        $requestModel->post_address = trim((string)$xml->Merchant->PostAddress);
        $requestModel->phone = preg_replace('/[^0-9]/', '', $xml->Merchant->Phone);
        $requestModel->email = trim((string)$xml->Merchant->Email);
        $requestModel->bic = trim((string)$xml->Merchant->BIC);
        $requestModel->iban = trim((string)$xml->Merchant->IBAN);
        $requestModel->currency = trim((string)$xml->Merchant->Currency);
        $requestModel->contract_id = trim((string)$xml->Merchant->ContractId);
        if (preg_match('/^(0[1-9]|[12][0-9]|3[01])\.(0[1-9]|1[012])\.\d{4}$/', trim((string)$xml->Merchant->ContractDate))) {
            $requestModel->contract_date = date_format(date_create_from_format('d.m.Y', trim((string)$xml->Merchant->ContractDate)), 'Y-m-d');
        } else {
            Yii::error("Некорректный формат даты в файле " . $file);
            $this->reportError("Некорректный формат даты в файле " . $file);
            $this->moveResponseFile($file, 'error');
            return;
        }

        $requestModel->terminal_id = trim((string)$xml->Terminal->TerminalId);
        $requestModel->terminal_status = trim((string)$xml->Terminal->Status);
        $requestModel->mcc = trim((string)$xml->Terminal->MCC);
        $requestModel->pt_description = trim((string)$xml->Terminal->PTDescription);
        $requestModel->pt_destination = trim((string)$xml->Terminal->PTDestination);

        $requestModel->parse_date = new Expression('now()');
        $requestModel->status = 0;

        if ($requestModel->save()) {
            return $requestModel;
        } else {
            Yii::error("Failed saving request data from " . $file . " into database: " . print_r($requestModel->errors, true));
            $this->reportError(
                Yii::t(
                    'app',
                    'Не удалось сохранить данные из файла {fileName} в базу: {errorMessage}',
                    ['fileName' => $file, 'errorMessage' => print_r($requestModel->errors, true)]
                )
            );
            $this->moveResponseFile($file, 'error');
            return false;
        }
    }

    /**
     * Registers a new merchant or updates existing merchant data
     * @param PriorbankTxpRequest $requestModel
     * @param MposMerchants $merchant
     * @return string|false returns password for a new merchant or false for existing one
     * @throws UserException if there was some validation errors
     */
    private function saveMerchantData($requestModel, $merchant)
    {
        $pass = false;
        if ($merchant->isNewRecord) {

            $merchant->owner_id = 3;
            $merchant->unp = $requestModel->unp;
            $merchant->merchant_status_id = 1;//торговец активен

            //для нового торговца заводим кассира и учету для лк
            $cashier = new MerchantCashier();
            $cashier->login = $requestModel->email;
            $cashier->password = sha1($requestModel->email);
            $cashier->merchant_cashier_status_id = 1;//кассир активен

            $cabinetAccount = new MerchantsArmUsers();
            $cabinetAccount->username = $requestModel->email;
            $cabinetAccount->email = $requestModel->email;
            $pass = MposMerchants::generatePassword();
            $cabinetAccount->setPassword($pass);
            $cabinetAccount->generateAuthKey();
        } else {
            //ищем, нет ли у торговца кассира с логином равным присланному email'у
            //если есть - его надо сделать главным
            $cashier = MerchantCashier::find()->where([
                'merchant_id' => $merchant->id,
                'login' => $requestModel->email
            ])->one();
            //если нет - создаем
            if ($cashier === null) {
                $cashier = new MerchantCashier();
                $cashier->login = $requestModel->email;
                $cashier->password = sha1($requestModel->email);
            }
            $cashier->merchant_cashier_status_id = 1;//кассир активен

            if ($merchant->email != $requestModel->email) {
                //меняем логин для личного кабинета
                $cabinetAccount = MerchantsArmUsers::find()->where([
                    'merchant_id' => $merchant->id
                ])->one();
                if ($cabinetAccount === null) {
                    throw new UserException(\Yii::t('app', 'Не удалось обновить данные торговца: не найдена учетная запись для входа в личный кабинет'));
                }
                $cabinetAccount->username = $requestModel->email;
                $cabinetAccount->email = $requestModel->email;
            }
        }

        //поля которые надо и для нового торговца заполнить, и для старого обновить
        $merchant->name = $requestModel->name_legal_entity;
        $merchant->banks_mfo = 0;
        $merchant->bic = $requestModel->bic;
        $merchant->bank_account = 0;
        $merchant->iban = $requestModel->iban;
        $merchant->bank_account_currency_id = ($requestModel->currency == '' ? 933 : $requestModel->currency);
        $merchant->email = $requestModel->email;
        $merchant->phone = $requestModel->phone;
        $merchant->contract = $requestModel->contract_id;
        $merchant->contract_date = $requestModel->contract_date;
        $merchant->registered_address = $requestModel->registered_address;
        $merchant->post_address = $requestModel->post_address;

        if (!$merchant->save()) {
            throw new UserException(Yii::t(
                'app',
                'Не удалось сохранить данные торговца: {errorMessage}',
                ['errorMessage' => print_r($merchant->errors, true)]
            ));
        }

        if (isset($cashier)) {
            $cashier->merchant_id = $merchant->id;
            if (!$cashier->save()) {
                throw new UserException(Yii::t(
                    'app',
                    'Не удалось сохранить данные кассира: {errorMessage}',
                    ['errorMessage' => print_r($cashier->errors, true)]
                ));
            }

            $merchant->chief_cashier_id = $cashier->id;
            $merchant->save(false, ['chief_cashier_id']);
        }

        if (isset($cabinetAccount)) {
            $cabinetAccount->merchant_id = $merchant->id;
            if (!$cabinetAccount->save()) {
                throw new UserException(Yii::t(
                    'app',
                    'Не удалось сохранить авторизационные данные для личного кабинета торговца: {errorMessage}',
                    ['errorMessage' => print_r($cabinetAccount->errors, true)]
                ));
            }
        }

        return $pass;
    }

    /**
     * Registers a new payment terminal or updates an existing one data
     * @param PriorbankTxpRequest $requestModel
     * @param PaymentsTerminals $paymentTerminal
     * @throws UserException if there was some validation errors
     */
    private function savePaymentTerminalData($requestModel, $paymentTerminal)
    {
        if ($requestModel->terminal_status == '0') {
            if ($paymentTerminal->isNewRecord) {
                throw new UserException(Yii::t(
                    'app',
                    'Не удалось отключить платежный терминал: терминал не найден в базе'
                ));
            } else {
                $paymentTerminal->status = 3;//отключен
            }
        } else {
            $paymentTerminal->status = 1;//активен
        }

        if ($paymentTerminal->isNewRecord) {
            $paymentTerminal->iso_setting_id = Yii::$app->params['priorDefaultIsoSettingId'];
            $paymentTerminal->name = $requestModel->terminal_id;
            $paymentTerminal->is_softpos = 1;
        }

        //поля и для новых терминалов задаются, и для существующих обновляется
        $paymentTerminal->unp = $requestModel->unp;
        $paymentTerminal->email = $requestModel->email;
        $paymentTerminal->description = $requestModel->pt_destination;
        $paymentTerminal->mcc = $requestModel->mcc;

        if (!$paymentTerminal->save()) {
            throw new UserException(Yii::t(
                'app',
                'Не удалось обновить данные платежного терминала: {errorMessage}',
                ['errorMessage' => print_r($paymentTerminal->errors, true)]
            ));
        }
    }

    private function readXmlFile($file)
    {
        $r = ftp_fget($this->ftpConn, $this->tmpFile, $file, FTP_ASCII);
        if ($r === false) {
            $errorText = "Не удалось скачать файл с FTP сервера";
            Yii::error($errorText);
            $this->reportError($errorText);
            ftp_close($this->ftpConn);
            exit;
        }

        $r = rewind($this->tmpFile);
        if ($r === false) {
            $errorText = "Ошибка при сбросе курсора файлового указателя";
            Yii::error($errorText);
            $this->reportError($errorText);
            ftp_close($this->ftpConn);
            exit;
        }

        $xmlString = '';
        while (!feof($this->tmpFile)) {
            $xmlString .= fread($this->tmpFile, 1024);
        }

        return $xmlString;
    }

    private function reportError($errorText)
    {
        Yii::error($errorText);
        foreach (Yii::$app->params['xmlParseErrorEmailNotification'] as $email) {
            Notification::sendToAddress(
                Notification::TYPE_EMAIL,
                $email,
                $errorText,
                null,
                'Ошибка вчитывания xml-файла от Приорбанка'
            );
        }
    }

    private function ftpConnect()
    {
        // FTP connection
        $this->ftpConn = ftp_connect(Yii::$app->params['serverFtpForPriorbank']);
        if ($this->ftpConn === false) {
            $this->reportError(Yii::t('app', 'Не удалось подключиться к FTP серверу'));
            return;
        }

        // FTP login
        $r = @ftp_login($this->ftpConn, Yii::$app->params['userFtpForPriorbank'], Yii::$app->params['passwordFtpForPriorbank']);
        if ($r === false) {
            $this->reportError(Yii::t('app', 'Не удалось авторизоваться на FTP сервере'));
            ftp_close($this->ftpConn);
            return;
        }

        //passive mode
        $r = ftp_pasv($this->ftpConn, true);
        if ($r === false) {
            $this->reportError(Yii::t('app', 'Не удалось включить пассивный режим FTP'));
            ftp_close($this->ftpConn);
            return;
        }
    }

    private function getFtpFileList($folder)
    {
        $files = ftp_nlist($this->ftpConn, $folder);
        if ($files === false) {
            $this->reportError(Yii::t('app', 'Не удалось получить список файлов от FTP сервера'));
            ftp_close($this->ftpConn);
            exit;
        }
        return $files;
    }

    private function moveResponseFile($filePath, $to)
    {
        if (count($this->files[$to]) == 0) {//список файлов не был получен с сервера
            $this->files[$to] = ftp_nlist($this->ftpConn, 'out/' . $to);
            if ($this->files[$to] === false) {
                $this->reportError(Yii::t('app', 'Не удалось получить список файлов от FTP сервера'));
                ftp_close($this->ftpConn);
                fclose($this->tmpFile);
                die;
            }
        }

        $newFilePath = 'out/' . $to . substr($filePath, 3);//обрезаем старый путь ('out') и формируем новый ('out/arch' или 'out/error')
        if (!in_array($newFilePath, $this->files[$to])) {
            $this->moveFileOnFTP($filePath, $newFilePath, $to);
        } else {
            $i = 1;
            while (true) {
                $renamedFilePath = substr($newFilePath, 0, -4) . '(' . $i . ')' . '.xml';
                if (!in_array($renamedFilePath, $this->files[$to])) {
                    $this->moveFileOnFTP($filePath, $renamedFilePath, $to);
                    break;
                } else {
                    $i++;
                }
            }
        }
    }

    private function moveFileOnFTP($oldPath, $newPath, $newDirectory)
    {
        if (ftp_rename($this->ftpConn, $oldPath, $newPath)) {
            $this->files[$newDirectory][] = $newPath;//обновляем информацию о списке файлов на сервере
            $this->truncateTempFile();
        } else {
            $this->reportError(Yii::t('app', 'Не удалось переместить файл на FTP сервере'));
            ftp_close($this->ftpConn);
            fclose($this->tmpFile);
            die;
        }
    }

    private function truncateTempFile()
    {
        if (!ftruncate($this->tmpFile, 0)) {
            $this->reportError(Yii::t('app', 'Не удалось очистить временный файл'));
            ftp_close($this->ftpConn);
            fclose($this->tmpFile);
            die;
        }
    }
}
