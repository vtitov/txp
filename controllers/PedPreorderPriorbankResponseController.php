<?php

namespace app\controllers;

use app\models\ChangeBankAcquirerPriorbankResponseSearch;
use Yii;
use app\models\PedPreorderPriorbankResponse;
use app\models\PedPreorderPriorbankResponseSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PedPreorderPriorbankResponseController implements the CRUD actions for PedPreorderPriorbankResponse model.
 */
class PedPreorderPriorbankResponseController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view'],
                        'roles' => ['priorMposRequestView'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['change-status'],
                        'roles' => ['priorMposRequestEdit'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all PedPreorderPriorbankResponse models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PedPreorderPriorbankResponseSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageParam = 'preorder-page';
        $dataProvider->sort->sortParam = 'preorder-sort';

        $changeAcquirerSearchModel = new ChangeBankAcquirerPriorbankResponseSearch();
        $changeAcquirerDataProvider = $changeAcquirerSearchModel->search(Yii::$app->request->queryParams);
        $changeAcquirerDataProvider->pagination->pageParam = 'change-page';
        $changeAcquirerDataProvider->sort->sortParam = 'change-sort';

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'changeAcquirerSearchModel' => $changeAcquirerSearchModel,
            'changeAcquirerDataProvider' => $changeAcquirerDataProvider,
        ]);
    }

    /**
     * Displays a single PedPreorderPriorbankResponse model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionChangeStatus($id, $status)
    {
        $model = $this->findModel($id);
        if (!in_array($status, array_keys(PedPreorderPriorbankResponse::$_STATUS))) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        $model->status = $status;
        $model->save(false, ['status']);

        return $this->redirect(['view', 'id' => $id]);
    }

    /**
     * Finds the PedPreorderPriorbankResponse model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return PedPreorderPriorbankResponse the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PedPreorderPriorbankResponse::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
