<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TransactionIpsReportSearch */
/* @var $dataProvider yii\data\ArrayDataProvider */

$this->title = Yii::t('app', 'Транзакции в разрезе МПС');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="transaction-ips-report">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="transaction-ips-report-search">

        <?php $form = ActiveForm::begin(); ?>

        <div class="row">
            <div class="col-md-6">
                <?= $form->field($searchModel, 'isoSetting')
                    ->dropDownList(\app\models\TransactionIpsReportSearch::getIsoSettings())
                    ->label(Yii::t('app', 'Настройка ISO'))
                ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($searchModel, 'dateRange')->widget(
                    \kartik\daterange\DateRangePicker::className(),
                    \app\helpers\DateColumnHelper::getWidgetConfig())
                    ->label(Yii::t('app', 'Период'))
                ?>
            </div>
        </div>

        <div class="form-group">
            <?= Html::submitButton('Сформировать', ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

    <?php
    if ($dataProvider != null) {
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                [
                    'attribute' => 'currency',
                    'label' => Yii::t('app', 'Валюта')
                ],
                [
                    'attribute' => 'ips',
                    'label' => Yii::t('app', 'МПС')
                ],
                [
                    'attribute' => 'count',
                    'label' => Yii::t('app', 'Количество')
                ],
                [
                    'attribute' => 'amount',
                    'label' => Yii::t('app', 'Сумма')
                ],
            ],
        ]);
    }
    ?>

</div>
