<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\FitsGroupsTypes */

$this->title = \Yii::t('app', 'Добавление типа групп FIT');
$this->params['breadcrumbs'][] = ['label' => \Yii::t('app', 'Справочник типов групп FIT'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fits-groups-types-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
