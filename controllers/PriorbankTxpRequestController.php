<?php

namespace app\controllers;

use Yii;
use app\models\PriorbankTxpRequest;
use app\models\PriorbankTxpRequestSearch;
use yii\filters\AccessControl;
use yii\helpers\Html;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PriorbankTxpRequestController implements the CRUD actions for PriorbankTxpRequest model.
 */
class PriorbankTxpRequestController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view'],
                        'roles' => ['priorTxpRequestView'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['change-status'],
                        'roles' => ['priorTxpRequestEdit'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'change-status' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all PriorbankTxpRequest models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PriorbankTxpRequestSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single PriorbankTxpRequest model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $statusDropdownItems = [];
        foreach (PriorbankTxpRequest::getRequestStatuses() as $k => $v) {
            $statusDropdownItems[] = [
                'label' => Html::encode($v),
                'url' => ['change-status', 'id' => $id, 'status' => $k],
                'linkOptions' => ['data' => ['method' => 'POST']]
            ];
        }

        return $this->render('view', [
            'model' => $model,
            'statusDropdownItems' => $statusDropdownItems,
        ]);
    }

    public function actionChangeStatus($id, $status)
    {
        $model = $this->findModel($id);
        if (!in_array($status, array_keys(PriorbankTxpRequest::getRequestStatuses()))) {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }

        if ($model->status != $status) {
            $model->status = $status;
            $model->save(false, ['status']);
        }

        return $this->redirect(['view', 'id' => $id]);
    }

    /**
     * Finds the PriorbankTxpRequest model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PriorbankTxpRequest the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PriorbankTxpRequest::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
