<?php

namespace app\helpers;

use kartik\daterange\DateRangePicker;

class DateColumnHelper
{
    public static function getWidgetConfig()
    {
        return [
            'presetDropdown' => false,
            'initRangeExpr' => false,
            'hideInput' => true,
            'language' => \Yii::$app->language,
            'pluginOptions' => [
                'locale' => ['format' => 'YYYY-MM-DD'],
                'ranges' => [
                    \Yii::t('app', 'Сегодня') => [date('Y-m-d'), date('Y-m-d')],
                    \Yii::t('app', 'Вчера') => [date('Y-m-d', strtotime('-1 day')), date('Y-m-d', strtotime('-1 day'))],
                    \Yii::t('app', 'Последние 7 дней') => [date('Y-m-d', strtotime('-6 day')), date('Y-m-d')],
                    \Yii::t('app', 'Последние 30 дней') => [date('Y-m-d', strtotime('-29 day')), date('Y-m-d')],
                    \Yii::t('app', 'Этот месяц') => [date('Y-m') . '-01 00:00', date('Y-m-t') . ' 23:59'],
                    \Yii::t('app', 'Прошлый месяц') => [date('Y-m', strtotime('-1 month')) . '-01', date('Y-m-t', strtotime('-1 month'))]
                ],
            ],
        ];
    }

    public static function getColumn($attribute, $searchModel)
    {
        return [
            'attribute' => $attribute,
            'value' => $attribute,
            'headerOptions' => [
                'style' => 'min-width: 151px',
            ],
            'filter' => static::getFilter($attribute, $searchModel),
        ];
    }

    public static function getFilter($attribute, $searchModel)
    {
        return DateRangePicker::widget(array_merge([
            'model' => $searchModel,
            'attribute' => $attribute,
            'containerOptions' => [
                'class' => 'grid-date-range-filter-container',
            ],
        ], DateColumnHelper::getWidgetConfig()));
    }

    public static function addFilterParams($dbFields, $values, $query)
    {
        for ($i = 0; $i < count($dbFields); $i++) {
            $datesArray = explode(' - ', $values[$i]);
            if (count($datesArray) == 2) {
                $query->andFilterWhere(['>=', $dbFields[$i], $datesArray[0] . " 00:00:00"])
                    ->andFilterWhere(['<=', $dbFields[$i], $datesArray[1] . " 23:59:59"]);
            }
        }
    }
}