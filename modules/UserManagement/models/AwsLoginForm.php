<?php

namespace app\modules\UserManagement\models;

use app\modules\UserManagement\helpers\AwsCognitoHelper;
use Aws\Exception\AwsException;
use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 */
class AwsLoginForm extends Model
{
    public $username;
    public $password;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['username', 'password'], 'required'],
        ];
    }

    /**
     * Logs in a user using the provided username and password.
     * @return bool whether the user is logged in successfully
     */
    public function sendLoginRequest()
    {
        if (!$this->validate()) {
            return false;
        }

        Yii::info('Sending AWS adminInitiateAuth request');

        try {
            $client = AwsCognitoHelper::getClient();
            $response = $client->adminInitiateAuth([
                'AuthFlow' => 'ADMIN_USER_PASSWORD_AUTH',
                'ClientId' => Yii::$app->getModule('user-management')->params['awsParams']['AWS_CLIENT_ID'],
                'UserPoolId' => Yii::$app->getModule('user-management')->params['awsParams']['AWS_USERPOOL_ID'],
                'AuthParameters' => [
                    'USERNAME' => $this->username,
                    'PASSWORD' => $this->password,
                    'SECRET_HASH' => AwsCognitoHelper::getSignature($this->username),
                ]
            ]);

            if (YII_DEBUG) {
                Yii::info('AWS adminInitiateAuth response: ' . print_r($response, true));
            }

        } catch (AwsException $e) {
            if ($e->getAwsErrorCode() == 'NotAuthorizedException') {
                Yii::warning('AWS Exception: ' . $e->getAwsErrorMessage());
                //$this->addError('password', \Yii::t('app', 'Неверное имя пользователя или пароль'));
                //этот же exception валится и для заблокированного пользователя
                Yii::$app->session->setFlash('awsRequestError', $e->getAwsErrorMessage());
                return false;
            } else {
                Yii::error($e->getAwsErrorMessage());
                throw $e;
            }
        }

        Yii::$app->session->set('aws_cognito_session', $response['Session']);
        Yii::$app->session->set('aws_username', $response['ChallengeParameters']['USER_ID_FOR_SRP']);

        return $response;
    }

    public function attributeLabels()
    {
        return [
            'username' => \Yii::t('app', 'Логин'),
            'password' => \Yii::t('app', 'Пароль'),
        ];
    }
}
