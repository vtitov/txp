<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "payment_terminal_settlement_log".
 *
 * @property int $payment_terminal_id
 * @property string $bdd
 * @property int $bddn
 * @property int $trx_counter
 * @property string $sett_date_time
 * @property int $sett_result
 * @property int $batch_status_id
 *
 * @property PaymentsTerminals $paymentTerminal
 * @property BatchStatusLocale $batchStatus
 */
class PaymentTerminalSettlementLog extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'payment_terminal_settlement_log';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('mpos');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            /*[['payment_terminal_id', 'sett_date_time'], 'required'],
            [['payment_terminal_id', 'bddn', 'trx_counter', 'sett_result', 'batch_status_id'], 'integer'],
            [['bdd', 'sett_date_time'], 'safe'],
            [['payment_terminal_id', 'sett_date_time'], 'unique', 'targetAttribute' => ['payment_terminal_id', 'sett_date_time']],*/
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'payment_terminal_id' => Yii::t('app', 'Платежный терминал'),
            'bdd' => Yii::t('app', 'Бизнес-день'),
            'bddn' => Yii::t('app', 'Номер пачки в бизнес-дне'),
            'trx_counter' => Yii::t('app', 'Количество операций'),
            'sett_date_time' => Yii::t('app', 'Дата закрытия'),
            'sett_result' => Yii::t('app', 'Результат'),
            'batch_status_id' => Yii::t('app', 'Статус'),
        ];
    }

    public function getPaymentTerminal()
    {
        return $this->hasOne(PaymentsTerminals::className(), ['id' => 'payment_terminal_id']);
    }

    public function getBatchStatus()
    {
        return $this->hasOne(BatchStatusLocale::className(), ['id' => 'batch_status_id'])
            ->andOnCondition([BatchStatusLocale::tableName() . '.locale' => Yii::$app->language]);
    }
}
