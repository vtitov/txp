<?php

namespace app\controllers;

use app\helpers\Notification;
use app\models\MerchantCashier;
use app\models\MerchantCashierSearch;
use app\models\MerchantsArmUsers;
use app\models\MposMerchants;
use app\models\MposMerchantsSearch;
use app\models\MposPedsSearch;
use app\models\PaymentsTerminalsSearch;
use Yii;
use yii\base\UserException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * MposMerchantsController implements the CRUD actions for MposMerchants model.
 */
class MposMerchantsController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view'],
                        'roles' => ['merchantView'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['create', 'update', 'delete', 'send-new-password'],
                        'roles' => ['merchantEdit'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['message-to-merchant', 'send-activation-codes'],
                        'roles' => ['merchantMessage'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all MposMerchants models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MposMerchantsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single MposMerchants model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        $pedsSearchModel = new MposPedsSearch();
        $pedsDataProvider = $pedsSearchModel->search(Yii::$app->request->queryParams, $model->id);
        $pedsDataProvider->sort->sortParam = 'ped-sort';
        $pedsDataProvider->pagination->pageParam = 'ped-page';

        $cashierSearchModel = new MerchantCashierSearch();
        $cashierDataProvider = $cashierSearchModel->search(Yii::$app->request->queryParams, $model->id);
        $cashierDataProvider->sort->sortParam = 'cashier-sort';
        $cashierDataProvider->pagination->pageParam = 'cashier-page';

        $paymentTerminalsSearchModel = new PaymentsTerminalsSearch();
        $paymentTerminalsDataProvider = $paymentTerminalsSearchModel->search(Yii::$app->request->queryParams, $model->id);
        $paymentTerminalsDataProvider->sort->sortParam = 'payment-terminal-sort';
        $paymentTerminalsDataProvider->pagination->pageParam = 'payment-terminal-page';

        /*$pedToBeAssigned = new DynamicModel(['serial_number']);
        $pedToBeAssigned->addRule(['serial_number'], 'required');
        $pedToBeAssigned->addRule(['serial_number'], 'string', ['max' => 20]);
        if (Yii::$app->user->can('merchantPedAssign') && $pedToBeAssigned->load(Yii::$app->request->post()) && $pedToBeAssigned->validate()) {
            $p = MposPeds::find()->where([
                'serial_number' => $pedToBeAssigned->serial_number,
                'merchant_id' => 0
            ])->one();

            if ($p === null) {
                $pedToBeAssigned->addError('serial_number', \Yii::t('app', 'Устройство с таким серийным номером не найдено'));
            } else if ($p->payment_terminal_id != 1 && $p->paymentTerminal->merchant_id != $id) {
                $pedToBeAssigned->addError('serial_number', \Yii::t('app', 'Платежный терминал, к которому привязано это устройство, принадлежит другому Торговцу'));
            } else {
                $p->merchant_id = $id;
                $p->save(false, ['merchant_id']);
                $pedToBeAssigned->serial_number = '';
            }
        }*/

        return $this->render('view', [
            'model' => $model,

            'cashierSearchModel' => $cashierSearchModel,
            'cashierDataProvider' => $cashierDataProvider,

            'pedsSearchModel' => $pedsSearchModel,
            'pedsDataProvider' => $pedsDataProvider,

            'paymentTerminalsSearchModel' => $paymentTerminalsSearchModel,
            'paymentTerminalsDataProvider' => $paymentTerminalsDataProvider,

            //'pedToBeAssigned' => $pedToBeAssigned,
        ]);
    }

    /**
     * Creates a new MposMerchants model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new MposMerchants();
        $model->bank_account_currency_id = 933;
        $model->merchant_status_id = 2;

        //psp может сломаться, если в этих полях будет null
        $model->banks_mfo = 0;
        $model->bank_account = 0;

        if (Yii::$app->user->identity->owner_id != 1) {
            $model->owner_id = Yii::$app->user->identity->owner_id;
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            $transaction = MposMerchants::getDb()->beginTransaction();
            try {
                $model->save(false);

                $params = ArrayHelper::getValue(Yii::$app->params, 'ownerCashierLoginFieldMap');
                $cashierLoginField = ArrayHelper::getValue($params, $model->owner_id, 'email');

                $merchant_cashier = new MerchantCashier();
                $merchant_cashier->merchant_id = $model->id;
                $merchant_cashier->login = $model->$cashierLoginField;
                $merchant_cashier->password = sha1($model->$cashierLoginField);
                $merchant_cashier->merchant_cashier_status_id = 1;//кассир активен
                if (!$merchant_cashier->save()) {
                    Yii::error('Cashier not created: ' . print_r($merchant_cashier->errors, true));
                    throw new UserException(Yii::t('app', 'Не удалось создать кассира'));
                }

                $model->chief_cashier_id = $merchant_cashier->id;
                $model->save(false, ['chief_cashier_id']);

                $armUser = new MerchantsArmUsers();
                $armUser->username = $model->email;
                $armUser->email = $model->email;
                $armUser->merchant_id = $model->id;
                $pass = MposMerchants::generatePassword();
                $armUser->setPassword($pass);
                $armUser->generateAuthKey();
                if (!$armUser->save()) {
                    Yii::error('User for web portal not registered: ' . print_r($armUser->errors, true));
                    throw new UserException(Yii::t('app', 'Учетная запись для входа в личный кабинет не создана'));
                }

                $transaction->commit();

            } catch (\Exception $e) {
                $transaction->rollBack();
                throw $e;
            } catch (\Throwable $e) {
                $transaction->rollBack();
                throw $e;
            }

            //Yii::t('app', 'Ваш пароль для входа в личный кабинет {password} Логином служит Ваш email.', ['password' => $pass])
            $res = Notification::sendToMerchant($model->id, 'merchant.registration', ['password' => $pass]);
            if ($res !== true) {
                Yii::$app->session->addFlash(
                    'error',
                    Yii::t(
                        'app',
                        'Сообщение с паролем для личного кабинета не отправлено: {errorMessage}',
                        ['errorMessage' => $res]
                    )
                );
            }

            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing MposMerchants model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if ($id == 0) {//техническая запись, нельзя редактировать
            throw new NotFoundHttpException(\Yii::t('app', 'Торговец не найден.'));
        }

        $model = $this->findModel($id);
        $oldEmail = $model->email;

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            $transaction = MposMerchants::getDb()->beginTransaction();
            try {
                $model->save(false);
                if ($model->email != $oldEmail) {
                    $armUser = MerchantsArmUsers::findOne(['merchant_id' => $model->id]);

                    if ($armUser == null) {
                        throw new UserException(\Yii::t('app', 'Не найдена учетная запись для входа в личный кабинет'));
                    }

                    $armUser->username = $model->email;
                    $armUser->email = $model->email;

                    if (!$armUser->save()) {
                        Yii::error('Merchant account login update failed: ' . print_r($armUser->errors, true));
                        throw new UserException(\Yii::t('app', 'Не обновлен логин для входа в личный кабинет'));
                    }
                }

                //если меняется значение поля, которое использовалось в качестве логина старшего кассира,
                //обновляем и сам логин
                $chiefCashier = $model->chiefCashier;
                if ($chiefCashier === null) {
                    throw new UserException(\Yii::t('app', 'Не найдена учетная запись старшего кассира'));
                }

                $params = ArrayHelper::getValue(Yii::$app->params, 'ownerCashierLoginFieldMap');
                $cashierLoginField = ArrayHelper::getValue($params, $model->owner_id, 'email');

                if ($model->$cashierLoginField != $chiefCashier->login) {
                    //если установленный по-умолчанию пароль не менялся,
                    //то обновляем и его
                    if ($chiefCashier->password == sha1($chiefCashier->login)) {
                        $chiefCashier->password = sha1($model->$cashierLoginField);
                    }

                    $chiefCashier->login = $model->$cashierLoginField;

                    if (!$chiefCashier->save()) {
                        Yii::error('Chief cashier login update failed: ' . print_r($chiefCashier->errors, true));
                        throw new UserException(\Yii::t('app', 'Не обновлен логин старшего кассира'));
                    }
                }

                $transaction->commit();
            } catch (\Exception $e) {
                $transaction->rollBack();
                throw $e;
            } catch (\Throwable $e) {
                $transaction->rollBack();
                throw $e;
            }

            return $this->redirect(['view', 'id' => $model->id]);

        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionMessageToMerchant($id)
    {
        if ($id == 0) {//техническая запись, нет контактных данных, некуда слать сообщения
            throw new NotFoundHttpException(\Yii::t('app', 'Торговец не найден.'));
        }

        $merchant = $this->findModel($id);

        if (($_POST['send_by'] == 'email' || $_POST['send_by'] == 'both') && $_POST['text'] != '') {
            $res = Notification::sendToAddress(
                Notification::TYPE_EMAIL,
                $merchant->email,
                $_POST['text'],
                $merchant->owner_id,
                $_POST['subject']
            );
            if ($res !== true) {
                Yii::$app->session->addFlash(
                    'error',
                    Yii::t(
                        'app',
                        'Email не отправлен: {errorMessage}',
                        ['errorMessage' => $res]
                    )
                );
            }
        }

        if (($_POST['send_by'] == 'sms' || $_POST['send_by'] == 'both') && $_POST['text'] != '') {
            $res = Notification::sendToAddress(Notification::TYPE_SMS, $merchant->phone, $_POST['text'], $merchant->owner_id);
            if ($res !== true) {
                Yii::$app->session->addFlash(
                    'error',
                    Yii::t(
                        'app',
                        'SMS не отправлено: {errorMessage}',
                        ['errorMessage' => $res]
                    )
                );
            }
        }

        $errorText = '';
        if (isset($isEmailSent) && $isEmailSent === false) {
            $errorText .= Yii::t('app', 'Email не отправлен.') . "\n";
        }
        if (isset($isSmsSent) && $isSmsSent === false) {
            $errorText .= Yii::t('app', 'SMS не отправлено.');
        }
        if ($errorText != '') {
            throw new UserException($errorText);
        }

        return $this->redirect(['view', 'id' => $merchant->id]);
    }

    public function actionSendNewPassword($id)
    {
        if ($id == 0) {//техническая запись, у нее нет учетки для ЛК
            throw new NotFoundHttpException(\Yii::t('app', 'Торговец не найден.'));
        }

        $merchant = $this->findModel($id);
        if ($merchant->owner_id == 6) {
            //казпочта не пользуется нашим кабинетом торговца, для них пароль не отсылаем
            throw new NotFoundHttpException(\Yii::t('app', 'Страница не найдена'));
        }
        $merchantAccount = MerchantsArmUsers::findOne(['merchant_id' => $id]);
        if ($merchantAccount === null) {
            throw new NotFoundHttpException(\Yii::t('app', 'Торговец не найден.'));
        }

        $password = MposMerchants::generatePassword();
        $merchantAccount->setPassword($password);
        if ($merchantAccount->status == MerchantsArmUsers::STATUS_FAILED_PASSWORD_LOCK) {
            $merchantAccount->status = MerchantsArmUsers::STATUS_ACTIVE;
        }
        $merchantAccount->failed_password_count = 0;
        if ($merchantAccount->save()) {
            //Yii::t('app', 'Ваш пароль для входа в личный кабинет {password} Логином служит Ваш email.', ['password' => $password]),
            $res = Notification::sendToMerchant($merchant->id, 'merchant.password', ['password' => $password]);
            if ($res !== true) {
                Yii::$app->session->addFlash(
                    'error',
                    Yii::t(
                        'app',
                        'Сообщение с паролем для личного кабинета не отправлено: {errorMessage}',
                        ['errorMessage' => $res]
                    )
                );
            }

        } else {
            Yii::error('Merchant password change failed: ' . print_r($merchantAccount->errors, true));
            throw new UserException(Yii::t('app', 'Не удалось изменить пароль торговца'));
        }
        return $this->redirect(['view', 'id' => $merchant->id]);
    }

    public function actionSendActivationCodes($id)
    {
        if ($id == 0) {//техническая запись, у нее нет телефона, некуда слать коды активации
            throw new NotFoundHttpException(\Yii::t('app', 'Торговец не найден.'));
        }

        $model = $this->findModel($id);
        $model->sendActivationCodes();
        return $this->redirect(['view', 'id' => $id]);
    }

    /**
     * Finds the MposMerchants model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return MposMerchants the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        $owner_id = Yii::$app->user->identity->owner_id;
        $model = MposMerchants::findOne($id);

        if ($model !== null && ($owner_id == 1 || $model->owner_id == $owner_id)) {
            return $model;
        } else {
            throw new NotFoundHttpException(\Yii::t('app', 'Торговец не найден.'));
        }
    }
}
