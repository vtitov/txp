<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ped_status_locale".
 *
 * @property int $id Уникальный идентификатор
 * @property string $locale
 * @property string $name Название
 * @property string|null $description Описание
 * @property string|null $color
 */
class PedStatusLocale extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ped_status_locale';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('mpos');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [];
    }
}
