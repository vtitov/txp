<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\helpers\RandomImageHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\models\FitsGroupsTypesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = \Yii::t('app', 'Справочник типов групп FIT');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fits-groups-types-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <img src="<?= RandomImageHelper::getRandomImageFromDir('img/cards') ?>" height="180px" align="right" style="margin-top: -130px">
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(\Yii::t('app', 'Добавить тип групп FIT'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'fit_name',
            'description',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
