<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "batch_status".
 *
 * @property int $id
 * @property string $desc
 */
class BatchStatus extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'batch_status';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('mpos');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            /*[['id'], 'required'],
            [['id'], 'integer'],
            [['desc'], 'string', 'max' => 100],
            [['id'], 'unique'],*/
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'desc' => Yii::t('app', 'Desc'),
        ];
    }
}
