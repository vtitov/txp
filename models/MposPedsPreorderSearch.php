<?php

namespace app\models;

use app\helpers\DateColumnHelper;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\MposPedsPreorder;

/**
 * MposPedsPreorderSearch represents the model behind the search form about `app\models\MposPedsPreorder`.
 */
class MposPedsPreorderSearch extends MposPedsPreorder
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'okpo', 'mobile_phone', 'banks_mfo', 'ped_preorder_delivery_type_id', 'ped_preorder_delivery_point_id', 'ped_preorder_status_id', 'device_number'], 'integer'],
            [['name', 'registered_address', 'post_address', 'email', 'delivery_address', 'add_date', 'unp'], 'safe'],
            [['bank_account'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MposPedsPreorder::find()
            ->with('pedPreorderStatus');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['add_date'=>SORT_DESC]],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'okpo' => $this->okpo,
            'banks_mfo' => $this->banks_mfo,
            'bank_account' => $this->bank_account,
            'ped_preorder_delivery_type_id' => $this->ped_preorder_delivery_type_id,
            'ped_preorder_delivery_point_id' => $this->ped_preorder_delivery_point_id,
            'ped_preorder_status_id' => $this->ped_preorder_status_id,
            'device_number' => $this->device_number,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'registered_address', $this->registered_address])
            ->andFilterWhere(['like', 'post_address', $this->post_address])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'delivery_address', $this->delivery_address])
            ->andFilterWhere(['like', 'unp', $this->unp])
            ->andFilterWhere(['like', 'mobile_phone', $this->mobile_phone]);

        DateColumnHelper::addFilterParams(['add_date'], [$this->add_date], $query);

        return $dataProvider;
    }
}
