<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MposPeds */

$this->title = \Yii::t('app', 'Редактировать устройство: ') . $model->serial_number;
$this->params['breadcrumbs'][] = ['label' => \Yii::t('app', 'Справочник устройств'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->serial_number, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = \Yii::t('app', 'Редактировать');
?>
<div class="mpos-peds-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
