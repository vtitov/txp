<?php

namespace app\controllers;

use app\models\Fits;
use app\models\FitsMap;
use Yii;
use app\models\FitsGroups;
use app\models\FitsGroupsSearch;
use app\models\FitsMapSearch;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

/**
 * FitsGroupsController implements the CRUD actions for FitsGroups model.
 */
class FitsGroupsController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view'],
                        'roles' => ['fitGroupView'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['create', 'update', 'delete'],
                        'roles' => ['fitGroupEdit'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all FitsGroups models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new FitsGroupsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single FitsGroups model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        $fitsSearchModel = new FitsMapSearch();
        $fitsDataProvider = $fitsSearchModel->search(Yii::$app->request->queryParams, $id);
        //$unsignedFITsQuery = Fits::find()->leftJoin('fits_map', 'id = fits_map.fits_id')->andWhere('fits_map.fits_groups_id != '.$id);
        //$unsignedFITs = new ActiveDataProvider(['query' => $unsignedFITsQuery]);
        //$unsignedFITs = ArrayHelper::map(Fits::find()->asArray()->all(), 'id', 'fit');
        $fitToBeAssigned = new FitsMap();

        return $this->render('view', [
            'model' => $this->findModel($id),
            'fitsDataProvider' => $fitsDataProvider,
            'fitsSearchModel' => $fitsSearchModel,
            'fitToBeAssigned' => $fitToBeAssigned,
        ]);
    }

    /**
     * Creates a new FitsGroups model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new FitsGroups();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing FitsGroups model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing FitsGroups model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the FitsGroups model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return FitsGroups the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = FitsGroups::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
