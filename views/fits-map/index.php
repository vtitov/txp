<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\FitsMapSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Fits Maps';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fits-map-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Fits Map', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'fits_id',
            'fits_groups_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
