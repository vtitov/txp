<?php
namespace app\helpers;

class RandomImageHelper
{
    public static function getRandomImageFromDir($path)
    {
        $images = array();
        if ( $img_dir = @opendir($path) ) {
            while ( false !== ($img_file = readdir($img_dir)) ) {
                if ( preg_match("/\.gif$/", $img_file) ) {
                    $images[] = $img_file;
                }
            }
            closedir($img_dir);
        }

        return $path."/".$images[array_rand($images)];
    }
}