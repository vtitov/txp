<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\PaymentsTerminals */
/* @var $form yii\widgets\ActiveForm */

$isoSettingsQuery = \app\models\IsoSettings::find()
    ->select(['id', 'name']);
if (Yii::$app->user->identity->owner_id != 1) {
    $isoSettingsQuery->andWhere(['owner_id' => Yii::$app->user->identity->owner_id]);
}
$isoSettingsDropdown = ArrayHelper::map($isoSettingsQuery->asArray()->all(), 'id', 'name');
?>

<div class="payments-terminals-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'unp')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput() ?>

    <?= $form->field($model, 'iso_setting_id')->dropDownList($isoSettingsDropdown) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'zpk')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'zak')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'settl_time')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->dropDownList(
        ArrayHelper::map(
            \app\models\PaymentTerminalStatusLocale::find()
                ->where(['locale' => Yii::$app->language])
                ->asArray()
                ->all(),
            'id',
            'name'
        )
    ) ?>

    <?= $form->field($model, 'is_softpos')->checkbox() ?>

    <?= $form->field($model, 'counter_category_id')->dropDownList(
        ArrayHelper::map(app\models\CounterCategory::find()->select(['id', 'name'])->asArray()->all(), 'id', 'name'),
        ['prompt' => '']
    ) ?>

    <?= $form->field($model, 'mcc')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Сохранить'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
