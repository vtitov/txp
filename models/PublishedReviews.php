<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "published_reviews".
 *
 * @property string $id
 * @property string $recieved_review_id
 * @property string $name
 * @property string $text
 * @property string $date_recieved
 */
class PublishedReviews extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'published_reviews';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('paybycard');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'text'], 'required'],
            [['recieved_review_id'], 'integer'],
            [['text'], 'string'],
            [['date_recieved'], 'safe'],
            [['name', 'avatar'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'recieved_review_id' => 'Recieved Review ID',
            'name' => 'Имя',
            'text' => 'Текст',
            'date_recieved' => 'Дата получения',
        ];
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        if ($insert && isset($this->recieved_review_id)) {
            $recievedReview = RecievedReviews::findOne($this->recieved_review_id);
            if ($recievedReview) {
                $recievedReview->status_id = 2; //отзыв опубликован
                $recievedReview->save();
            }
        }
    }

    public function afterDelete()
    {
        parent::afterDelete();
        $recievedReview = RecievedReviews::findOne($this->recieved_review_id);
        if ($recievedReview) {
            $recievedReview->status_id = 4; //отзыв удален из опубликованных
            $recievedReview->save();
        }
    }
}
