<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mpos_ped_manufacturers".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 *
 * @property MposPedModels[] $mposPedModels
 */
class MposPedManufacturers extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ped_vendor';
    }
	
	public static function getDb()
    {
        return Yii::$app->get('mpos');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 40],
            ['name', 'required'],
            ['name', 'unique'],
            [['description'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => \Yii::t('app', 'Производитель'),
            'description' => \Yii::t('app', 'Описание'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMposPedModels()
    {
        return $this->hasMany(MposPedModels::className(), ['ped_vendor_id' => 'id']);
    }
}
