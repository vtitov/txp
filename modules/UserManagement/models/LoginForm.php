<?php

namespace app\modules\UserManagement\models;

use app\modules\UserManagement\helpers\LoginHelper;
use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class LoginForm extends Model
{
    public $username;
    public $password;
    public $rememberMe = true;

    private $_user = false;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['username', 'password'], 'required'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();

            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, \Yii::t('app', 'Неверное имя пользователя или пароль'));
            }
        }
    }

    /**
     * Logs in a user using the provided username and password.
     * @return bool whether the user is logged in successfully
     */
    public function login()
    {
        $loginHelper = new LoginHelper($this->username);

        if ($loginHelper->tooManyAttempts()){
            $this->addError('password', \Yii::t('app', 'Превышен лимит попыток авторизации'));
            return false;
        }

        if ($this->validate()) {//логин и пароль верны

            $loginHelper->resetFailedAttemptsCounter();

            $user = $this->getUser();

            //пользователь заблокирован
            if ($user->status == 0){
                $this->addError('username', \Yii::t('app', 'Пользователь заблокирован. Обратитесь к администратору.'));
                $loginHelper->saveAttempt(false);
                return false;
            }

            //временный пользователь
            if ($user->temp_user) {

                if ($user->temp_user_active_from > date('Y-m-d H:i:s')) {
                    $this->addError('username', \Yii::t('app', 'Пользователь еще не активен.'));
                    $loginHelper->saveAttempt(false);
                    return false;
                }

                if ($user->temp_user_active_to < date('Y-m-d H:i:s')) {
                    $this->addError('username', \Yii::t('app', 'Пользователь уже не активен.'));
                    $loginHelper->saveAttempt(false);
                    return false;
                }
            }

            //пароль установлен администратором, заставляем поменять
            if ($user->temp_password) {
                Yii::$app->response->redirect(['/user-management/auth/change-password']);
                Yii::$app->end();
            }

            //срок действия пароля истек
            if (Yii::$app->getModule('user-management')->params['passwordExpire'] > 0) {
                $lastPasswordChange = PasswordHistory::find()
                    ->where(['user_id' => $user->id])
                    ->orderBy(['date' => SORT_DESC])
                    ->limit(1)
                    ->one();

                if ($lastPasswordChange) {
                    $lastPasswordChangeDate = $lastPasswordChange->date;
                } else {
                    $lastPasswordChangeDate = $user->created_at;
                }

                if (date('Y-m-d', strtotime("-" . ((int)Yii::$app->getModule('user-management')->params['passwordExpire']) . " days")) > $lastPasswordChangeDate) {
                    Yii::$app->response->redirect(['/user-management/auth/change-password']);
                    Yii::$app->end();
                }
            }

            //пользователь все-таки авторизовался
            $loginHelper->saveAttempt(true);

            if (Yii::$app->user->enableAutoLogin && $this->rememberMe) {
                return Yii::$app->user->login($this->getUser(), 3600 * 24 * 30);
            } else {
                return Yii::$app->user->login($this->getUser());
            }


        } else {//пользователь с таким логином и паролем не найден
            $loginHelper->incrementFailedAttemptsCounter();
            $loginHelper->saveAttempt(false);
            return false;
        }
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = User::findByUsername($this->username);
        }

        return $this->_user;
    }

    public function attributeLabels()
    {
        return [
            'username' => \Yii::t('app', 'Логин'),
            'password' => \Yii::t('app', 'Пароль'),
            'rememberMe' => \Yii::t('app', 'Запомнить'),
        ];
    }
}
