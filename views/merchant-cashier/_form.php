<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MerchantCashier */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="merchant-cashier-form">

    <?php $form = ActiveForm::begin(); ?>

    <!--?= $form->field($model, 'merchant_id')->textInput(['maxlength' => 10]) ?-->

    <?= $form->field($model, 'login')->textInput(['maxlength' => 50]) ?>

    <!--?= $form->field($model, 'password')->passwordInput(['maxlength' => 40]) ?-->

    <?= $form->field($model, 'description')->textInput(['maxlength' => 80]) ?>

    <!--?= $form->field($model, 'last_login_date')->textInput() ?-->

    <!--?= $form->field($model, 'login_attempts_count')->textInput() ?-->

    <?= $form->field($model, 'merchant_cashier_status_id')->dropDownList(
        \yii\helpers\ArrayHelper::map(
            \app\models\MerchantCashierStatusLocale::find()
                ->where(['locale' => Yii::$app->language])
                ->asArray()
                ->all(),
            'id',
            'desc'
        )
    ) ?>

    <div class="form-group">
        <?= Html::submitButton(\Yii::t('app', 'Сохранить'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
