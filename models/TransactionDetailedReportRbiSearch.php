<?php

namespace app\models;

use app\helpers\DateColumnHelper;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\db\Query;

class TransactionDetailedReportRbiSearch extends Model
{

    public $dateRange;
    public $ownerId;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['dateRange'], 'required'],
            [
                ['dateRange'],
                'match',
                'pattern' => '/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[12][0-9]|3[01]) - [0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[12][0-9]|3[01])$/',
                'message' => \Yii::t('app', 'Неверный формат даты')
            ],
            [['ownerId'], 'integer'],
            [['ownerId'], 'exist', 'skipOnError' => true, 'targetClass' => Owners::className(), 'targetAttribute' => ['ownerId' => 'id']],
        ];
    }

    public function search($params)
    {
        $query = $this->getQuery($params);
        DateColumnHelper::addFilterParams(['ta.date_time'], [$this->dateRange], $query);

        return new ActiveDataProvider([
            'query' => $query,
            'db' => TransactionArchive::getDb(),
            'sort' => false,
        ]);
    }

    public function export($params)
    {
        $query = $this->getQuery($params);
        if ($this->hasErrors()) {
            return false;
        }
        $outputFile = tmpfile();
        fputcsv($outputFile, [$this->dateRange]);
        fputcsv($outputFile, [
            \Yii::t('app', 'Дата'),
            \Yii::t('app', 'Время'),
            \Yii::t('app', 'ID торговца в процессинговом центре'),
            \Yii::t('app', 'Платежный терминал'),
            'MCC',
            \Yii::t('app', 'Сумма'),
            \Yii::t('app', 'Валюта'),
            \Yii::t('app', 'Номер карты'),
            \Yii::t('app', 'Код ответа'),
            'RRN',
            \Yii::t('app', 'Код авторизации'),
        ]);

        $dates = explode(' - ', $this->dateRange);
        $timeFrom = strtotime($dates[0] . ' 00:00:00');
        $endTime = strtotime($dates[1] . ' 23:59:59');
        while ($timeFrom <= $endTime) {
            //выбираем записи пачками, чтобы не упереться в лимит памяти
            $timeTo = min($timeFrom + 24 * 60 * 60, $endTime);
            $q = clone $query;
            $q->andWhere(['>=', 'ta.date_time', date('Y-m-d H:i:s', $timeFrom)])
                ->andWhere(['<=', 'ta.date_time', date('Y-m-d H:i:s', $timeTo)]);

            foreach ($q->all(TransactionArchive::getDb()) as $row) {
                fputcsv($outputFile, $row);
            }

            $timeFrom = $timeTo + 1;//сдвигаемся на 1 секунду, чтобы не дублировались записи на стыке периодов
        }

        return $outputFile;
    }

    private function getQuery($params)
    {
        $query = (new Query())
            ->select([
                new Expression('DATE_FORMAT(ta.date_time, "%Y-%m-%d") as `date`'),
                new Expression('DATE_FORMAT(ta.date_time, "%H:%i:%s") as `time`'),
                'm.iso_merchant_id',
                'pt.name AS payment_terminal',
                'pt.mcc',
                'ta.amount',
                'c.alpha_code AS currency',
                'ta.card',
                'tla.response_code',
                'ta.rrn',
                'ta.authcode'
            ])
            ->from(TransactionArchive::tableName() . ' ta')
            ->leftJoin(MposMerchants::tableName() . ' m', 'm.`id` = ta.`mpos_merchant_id`')
            ->leftJoin(Currency::tableName() . ' c', 'c.`id` = ta.`currency_id`')
            ->leftJoin(PaymentsTerminals::tableName() . ' pt', 'pt.`id` = ta.`payments_terminal_id`')
            ->leftJoin(IsoSettings::tableName() . ' iso', 'iso.`id` = pt.`iso_setting_id`')
            ->leftJoin('transactions_log.' . TransactionslogArchive::tableName() . ' tla', 'tla.`id` = ta.`financial_original_trx_id`');

        //обязательные условия
        $userOwnerId = \Yii::$app->user->identity->owner_id;
        if ($userOwnerId != 1) {
            $query->andWhere(['iso.owner_id' => $userOwnerId]);
        }

        $this->load($params);
        if (!$this->validate()) {
            $query->andWhere('1=0');
            return $query;
        }

        //необязательные условия
        if ($userOwnerId == 1) {
            $query->andFilterWhere(['iso.owner_id' => $this->ownerId]);
        }

        return $query;
    }
} 