<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "app_key".
 *
 * @property int $id
 * @property int $os_id
 * @property string $app_ver
 * @property string $pub_key
 * @property string $priv_key
 * @property string $pub_modulus
 * @property string $pub_exp
 *
 * @property OsAllowed $os
 */
class AppKey extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'app_key';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('mpos');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['os_id'], 'integer'],
            [['app_ver'], 'filter', 'filter' => 'trim'],
            [['app_ver'], 'string', 'max' => 20],
            [['os_id', 'app_ver'], 'unique', 'targetAttribute' => ['os_id', 'app_ver']],
            [['os_id'], 'exist', 'skipOnError' => true, 'targetClass' => OsAllowed::className(), 'targetAttribute' => ['os_id' => 'id']],
            [['os_id'], 'default', 'value' => 0],
            [['app_ver'], 'default', 'value' => null],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'os_id' => Yii::t('app', 'ОС'),
            'app_ver' => Yii::t('app', 'Версия приложения'),
            'pub_key' => Yii::t('app', 'Публичный ключ'),
            'pub_modulus' => Yii::t('app', 'Модуль'),
            'pub_exp' => Yii::t('app', 'Экспонента'),
        ];
    }

    public function getOs()
    {
        return $this->hasOne(OsAllowed::className(), ['id' => 'os_id']);
    }
}
