<?php

namespace app\controllers;

use Yii;
use app\models\AuditLog;
use app\models\AuditLogSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * AuditLogController implements the CRUD actions for AuditLog model.
 */
class AuditLogController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view'],
                        'roles' => ['auditLogView'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all AuditLog models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AuditLogSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single AuditLog model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Finds the AuditLog model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return AuditLog the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        $owner_id = Yii::$app->user->identity->owner_id;
        $model = AuditLog::findOne($id);

        if ($model !== null) {
            if ($owner_id == 1) {
                return $model;
            }

            if ($model->mposPed !== null && $model->mposPed->owner_id == $owner_id) {
                return $model;
            }

            if ($model->mposPedArchive !== null && $model->mposPedArchive->owner_id == $owner_id) {
                return $model;
            }
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
