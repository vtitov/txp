<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use app\helpers\Notification;
use app\models\MerchantEmailSending;
use Yii;
use yii\console\Controller;
use yii\db\Expression;

class MerchantEmailController extends Controller
{
    public function actionIndex()
    {
        $lastSent = MerchantEmailSending::find()
            ->andFilterWhere(['status' => 1])
            ->orderBy(['date_send' => SORT_DESC])
            ->limit(1)
            ->one();

        $emailToBeSent = MerchantEmailSending::find()
            ->andFilterWhere(['status' => 0]);


        if ($lastSent) {
            $domain = substr($lastSent->email, strpos($lastSent->email, '@'));
            $emailToBeSent->andFilterWhere(['not like', 'email', '%' . $domain, false]);
        }

        $emailToBeSent = $emailToBeSent->limit(1)->one();

        if (!$emailToBeSent) {
            $emailToBeSent = MerchantEmailSending::find()
                ->andFilterWhere(['status' => 0])
                ->limit(1)
                ->one();
        }

        if (!$emailToBeSent) {
            return false;
        }

        $res = Notification::sendToAddress(
            Notification::TYPE_EMAIL,
            $emailToBeSent->email,
            $emailToBeSent->text,
            $emailToBeSent->owner_id,
            $emailToBeSent->subject
        );

        if ($res === true) {
            $emailToBeSent->date_send = new Expression('now()');
            $emailToBeSent->status = 1;
        } else {
            $emailToBeSent->status = 2;
        }

        if (!$emailToBeSent->save()) {
            Yii::error($emailToBeSent->getErrors());
        }
    }
}
