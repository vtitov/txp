<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "client_error".
 *
 * @property int $id
 * @property string $message
 * @property string $locale
 */
class ClientError extends \yii\db\ActiveRecord
{
    public static $_LOCALES = [
        'en' => 'en',
        'ru' => 'ru',
        'uk' => 'uk',
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'client_error';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('mpos');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'message', 'locale'], 'required'],
            [['id'], 'integer'],
            [['locale'], 'in', 'range' => array_keys(static::$_LOCALES)],
            [['id', 'locale'], 'unique', 'targetAttribute' => ['id', 'locale']],
            [['message'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'message' => \Yii::t('app', 'Текст ошибки'),
            'locale' => \Yii::t('app', 'Локализация'),
        ];
    }
}
