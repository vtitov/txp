<?php

namespace app\controllers;

use app\models\PublishedReviews;
use app\models\RecievedReviewsStatuses;
use Yii;
use app\models\RecievedReviews;
use app\models\RecievedReviewsSearch;
use yii\filters\AccessControl;
use yii\helpers\HtmlPurifier;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

/**
 * RecievedReviewsController implements the CRUD actions for RecievedReviews model.
 */
class RecievedReviewsController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view'],
                        'roles' => ['reviewView'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['change-status', 'publish-review'],
                        'roles' => ['reviewEdit'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all RecievedReviews models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RecievedReviewsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single RecievedReviews model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
            'statuses' => ArrayHelper::map(RecievedReviewsStatuses::find()->asArray()->all(), 'id', 'name'),
        ]);
    }

    public function actionChangeStatus($id, $status)
    {
        $model = $this->findModel($id);
        $model->status_id = $status;
        if (!$model->save()) {
            Yii::error($model->errors);
        }
        return $this->redirect(['view', 'id' => $model->id]);
    }

    public function actionPublishReview($id)
    {
        $modelRecieved = $this->findModel($id);
        $modelPublished = new PublishedReviews();
        $modelPublished->recieved_review_id = $id;
        $modelPublished->name = $modelRecieved->name;
        $modelPublished->text = $modelRecieved->text;
        $modelPublished->date_recieved = date('Y-m-d', strtotime($modelRecieved->date_recieved));
        $modelPublished->avatar = HtmlPurifier::process($modelRecieved->avatar);

        return $this->render('/published-reviews/create', [
            'model' => $modelPublished,
        ]);
    }

    /**
     * Finds the RecievedReviews model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return RecievedReviews the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = RecievedReviews::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
