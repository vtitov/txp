<?php

namespace app\models;

use app\helpers\DateColumnHelper;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\Query;

/**
 * MposTransactionsSearch represents the model behind the search form about `app\models\MposTransactions`.
 */
class MposTransactionsSearch extends MposTransactions
{
    public $bank;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'payments_terminal_id', 'merchant_cashier_id', 'mpos_transaction_status_id', 'batch', 'currency_id', 'financial_original_trx_id', 'financial_reverse_trx_id', 'payment_id', 'transaction_type_id', 'bank'], 'integer'],
            [['mpos_ped_id', 'date_time', 'bdd', 'stan', 'authcode', 'rrn', 'mpos_merchant_id', 'card'], 'safe'],
            [['amount'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query1 = MposTransactions::find()
            ->select('transaction.id, date_time, IFNULL(`ped`.`serial_number`, `ped_archive`.`serial_number`) as `serial_number`, mpos_merchant_id, merchant.name as merchant_name, authcode, transaction_status_locale.name as transaction_status, amount, card, payment_locale.name as payment_name, transaction_type_locale.desc as transaction_type')
            ->joinWith(['mposPed', 'mposPedArchive', 'mposMerchant', 'transactionStatus', 'transactionType', 'payment', 'paymentTerminal.isoSettings']);

        $query2 = TransactionArchive::find()
            ->select('transaction_archive.id, date_time, IFNULL(`ped`.`serial_number`, `ped_archive`.`serial_number`) as `serial_number`, mpos_merchant_id, merchant.name as merchant_name, authcode, transaction_status_locale.name as transaction_status, amount, card, payment_locale.name as payment_name, transaction_type_locale.desc as transaction_type')
            ->joinWith(['mposPed', 'mposPedArchive', 'mposMerchant', 'transactionStatus', 'transactionType', 'payment', 'paymentTerminal.isoSettings']);

        $owner_id = Yii::$app->user->identity->owner_id;
        if ($owner_id != 1) {
            $query1->andWhere([IsoSettings::tableName() . '.owner_id' => $owner_id]);
            $query2->andWhere([IsoSettings::tableName() . '.owner_id' => $owner_id]);
        }

        $query = new Query();
        $query->select('*')
            ->from(['t' => $query1->union($query2, true)]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'db' => 'mpos',
            'sort' => ['defaultOrder' => ['date_time' => SORT_DESC]]
        ]);

        $dataProvider->sort->attributes = [
            'date_time' => [
                'asc' => ['t.date_time' => SORT_ASC],
                'desc' => ['t.date_time' => SORT_DESC]
            ],
            'mpos_ped_id' => [
                'asc' => ['t.serial_number' => SORT_ASC],
                'desc' => ['t.serial_number' => SORT_DESC]
            ],
            'mpos_merchant_id' => [
                'asc' => ['t.merchant_name' => SORT_ASC],
                'desc' => ['t.merchant_name' => SORT_DESC]
            ],
            'authcode' => [
                'asc' => ['t.authcode' => SORT_ASC],
                'desc' => ['t.authcode' => SORT_DESC]
            ],
            'mpos_transaction_status_id' => [
                'asc' => ['t.description' => SORT_ASC],
                'desc' => ['t.description' => SORT_DESC]
            ],
            'amount' => [
                'asc' => ['t.amount' => SORT_ASC],
                'desc' => ['t.amount' => SORT_DESC]
            ],
            'card' => [
                'asc' => ['t.card' => SORT_ASC],
                'desc' => ['t.card' => SORT_DESC]
            ],
            'payment_id' => [
                'asc' => ['t.payment_name' => SORT_ASC],
                'desc' => ['t.payment_name' => SORT_DESC]
            ],

            'transaction_type_id' => [
                'asc' => ['t.transaction_type' => SORT_ASC],
                'desc' => ['t.transaction_type' => SORT_DESC]
            ],

        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query1->andFilterWhere([
            'id' => $this->id,
            'mpos_transaction_status_id' => $this->mpos_transaction_status_id,
            'batch' => $this->batch,
            'amount' => $this->amount,
            'currency_id' => $this->currency_id,
            'financial_original_trx_id' => $this->financial_original_trx_id,
            'financial_reverse_trx_id' => $this->financial_reverse_trx_id,
            'payment_id' => $this->payment_id,
            'transaction_type_id' => $this->transaction_type_id,
            IsoSettings::tableName() . '.owner_id' => $this->bank,
        ]);

        $query1->andFilterWhere(['like', 'stan', $this->stan])
            ->andFilterWhere(['like', 'authcode', $this->authcode])
            ->andFilterWhere(['like', 'rrn', $this->rrn])
            ->andFilterWhere(['or', ['like', 'ped.serial_number', $this->mpos_ped_id], ['like', 'ped_archive.serial_number', $this->mpos_ped_id]])
            ->andFilterWhere(['like', 'card', $this->card])
            ->andFilterWhere(['like', 'merchant.name', $this->mpos_merchant_id])
            ->andFilterWhere(['like', 'payment_terminal.name', $this->payments_terminal_id]);

        DateColumnHelper::addFilterParams(['date_time', 'bdd'], [$this->date_time, $this->bdd], $query1);


        $query2->andFilterWhere([
            'id' => $this->id,
            'mpos_transaction_status_id' => $this->mpos_transaction_status_id,
            'batch' => $this->batch,
            'amount' => $this->amount,
            'currency_id' => $this->currency_id,
            'financial_original_trx_id' => $this->financial_original_trx_id,
            'financial_reverse_trx_id' => $this->financial_reverse_trx_id,
            'payment_id' => $this->payment_id,
            'transaction_type_id' => $this->transaction_type_id,
            IsoSettings::tableName() . '.owner_id' => $this->bank,
        ]);

        $query2->andFilterWhere(['like', 'stan', $this->stan])
            ->andFilterWhere(['like', 'authcode', $this->authcode])
            ->andFilterWhere(['like', 'rrn', $this->rrn])
            ->andFilterWhere(['or', ['like', 'ped.serial_number', $this->mpos_ped_id], ['like', 'ped_archive.serial_number', $this->mpos_ped_id]])
            ->andFilterWhere(['like', 'card', $this->card])
            ->andFilterWhere(['like', 'merchant.name', $this->mpos_merchant_id])
            ->andFilterWhere(['like', 'payment_terminal.name', $this->payments_terminal_id]);

        DateColumnHelper::addFilterParams(['date_time', 'bdd'], [$this->date_time, $this->bdd], $query2);

        return $dataProvider;
    }
}
