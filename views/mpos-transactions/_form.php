<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MposTransactions */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mpos-transactions-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id')->textInput(['maxlength' => 10]) ?>

    <?= $form->field($model, 'date')->textInput() ?>

    <?= $form->field($model, 'time')->textInput(['maxlength' => 30]) ?>

    <?= $form->field($model, 'mpos_ped_id')->textInput(['maxlength' => 10]) ?>

    <?= $form->field($model, 'mpos_merchant_id')->textInput(['maxlength' => 10]) ?>

    <?= $form->field($model, 'payments_terminal_id')->textInput(['maxlength' => 10]) ?>

    <?= $form->field($model, 'bdd')->textInput() ?>

    <?= $form->field($model, 'stan')->textInput(['maxlength' => 6]) ?>

    <?= $form->field($model, 'authcode')->textInput(['maxlength' => 6]) ?>

    <?= $form->field($model, 'mpos_transaction_status_id')->textInput() ?>

    <?= $form->field($model, 'batch')->textInput(['maxlength' => 10]) ?>

    <?= $form->field($model, 'amount')->textInput(['maxlength' => 11]) ?>

    <?= $form->field($model, 'currency_id')->textInput() ?>

    <?= $form->field($model, 'card')->textInput(['maxlength' => 19]) ?>

    <?= $form->field($model, 'rrn')->textInput(['maxlength' => 12]) ?>

    <?= $form->field($model, 'financial_original_trx_id')->textInput(['maxlength' => 19]) ?>

    <?= $form->field($model, 'financial_reverse_trx_id')->textInput(['maxlength' => 19]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
