<?php

namespace app\models;

use app\helpers\DateColumnHelper;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * PedPreorderPriorbankResponseSearch represents the model behind the search form about `app\models\PedPreorderPriorbankResponse`.
 */
class PedPreorderPriorbankResponseSearch extends PedPreorderPriorbankResponse
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'preorder_id', 'file_number', 'phone_trading_name', 'status'], 'integer'],
            [['name_legal_entity', 'email', 'pos_info', 'contract', 'contract_date', 'trading_name', 'mcc', 'country', 'region', 'locality', 'address', 'terminal_id', 'merchant_id', 'parse_date', 'unp'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PedPreorderPriorbankResponse::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'preorder_id' => $this->preorder_id,
            'file_number' => $this->file_number,
            //'contract_date' => $this->contract_date,
            'phone_trading_name' => $this->phone_trading_name,
            //'parse_date' => $this->parse_date,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'name_legal_entity', $this->name_legal_entity])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'pos_info', $this->pos_info])
            ->andFilterWhere(['like', 'contract', $this->contract])
            ->andFilterWhere(['like', 'trading_name', $this->trading_name])
            ->andFilterWhere(['like', 'mcc', $this->mcc])
            ->andFilterWhere(['like', 'country', $this->country])
            ->andFilterWhere(['like', 'region', $this->region])
            ->andFilterWhere(['like', 'locality', $this->locality])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'terminal_id', $this->terminal_id])
            ->andFilterWhere(['like', 'merchant_id', $this->merchant_id])
            ->andFilterWhere(['like', 'unp', $this->unp]);

        DateColumnHelper::addFilterParams(['contract_date', 'parse_date'], [$this->contract_date, $this->parse_date], $query);

        return $dataProvider;
    }
}
