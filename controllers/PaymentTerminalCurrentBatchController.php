<?php

namespace app\controllers;

use app\models\PaymentTerminalCurrentBatch;
use Yii;
use app\models\PaymentTerminalCurrentBatchSearch;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * PaymentTerminalCurrentBatchController implements the CRUD actions for PaymentTerminalCurrentBatch model.
 */
class PaymentTerminalCurrentBatchController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view'],
                        'roles' => ['paymentTerminalCurrentBatchView'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['close'],
                        'roles' => ['paymentTerminalCurrentBatchClose'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'close' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all PaymentTerminalCurrentBatch models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PaymentTerminalCurrentBatchSearch();
        $previousDay = date_sub(new \DateTime(), new \DateInterval('P1D'));
        $searchModel->bdd = '2016-10-01 - '.$previousDay->format('Y-m-d');
        $searchModel->batch_status_id = 1;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single PaymentTerminalCurrentBatch model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }


    public function actionClose($id)
    {
        $model = $this->findModel($id);
        if ($model->batch_status_id != 4 && $model->batch_status_id != 5 && $model->batch_status_id != 6) {
            $model->batch_status_id = 6;
            $model->save(false, ['batch_status_id']);
            Yii::$app->session->addFlash(
                'success',
                Yii::t('app', 'Команда на закрытие бизнес-дня добавлена в очередь. Пожалуйста, проверьте статус позже.')
            );
        } else {
            Yii::$app->session->addFlash(
                'warning',
                Yii::t('app', 'Бизнес-день с таким статусом нельзя закрыть')
            );
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the PaymentTerminalCurrentBatch model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PaymentTerminalCurrentBatch the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        $owner_id = Yii::$app->user->identity->owner_id;
        $model = PaymentTerminalCurrentBatch::findOne($id);
        $isoSetting = ArrayHelper::getValue(ArrayHelper::getValue($model, 'paymentTerminal'), 'isoSettings');

        if ($model !== null && ($owner_id == 1 || ($isoSetting !== null && $isoSetting->owner_id == $owner_id))) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
