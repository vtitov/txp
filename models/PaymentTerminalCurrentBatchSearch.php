<?php

namespace app\models;

use app\helpers\DateColumnHelper;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\PaymentTerminalCurrentBatch;

/**
 * PaymentTerminalCurrentBatchSearch represents the model behind the search form about `app\models\PaymentTerminalCurrentBatch`.
 */
class PaymentTerminalCurrentBatchSearch extends PaymentTerminalCurrentBatch
{
    public $merchant_name;
    public $merchant_phone;
    public $ped_serial_number;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['bddn', 'trx_counter', 'batch_status_id', 'merchant_phone'], 'integer'],
            [['bdd', 'payment_terminal_id', 'merchant_name', 'ped_serial_number'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PaymentTerminalCurrentBatch::find()
            ->joinWith(['ped.mposMerchant', 'paymentTerminal', 'batchStatus']);

        $owner_id = Yii::$app->user->identity->owner_id;
        if ($owner_id != 1){
            $query->joinWith(['paymentTerminal.isoSettings'])
                ->andWhere([IsoSettings::tableName().'.owner_id' => $owner_id]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['bdd'=>SORT_ASC]],
        ]);

        $dataProvider->sort->attributes['payment_terminal_id'] = [
            'asc' => [PaymentsTerminals::tableName() . '.name' => SORT_ASC],
            'desc' => [PaymentsTerminals::tableName() . '.name' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['merchant_name'] = [
            'asc' => [MposMerchants::tableName() . '.name' => SORT_ASC],
            'desc' => [MposMerchants::tableName() . '.name' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['merchant_phone'] = [
            'asc' => [MposMerchants::tableName() . '.phone' => SORT_ASC],
            'desc' => [MposMerchants::tableName() . '.phone' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['ped_serial_number'] = [
            'asc' => [MposPeds::tableName() . '.serial_number' => SORT_ASC],
            'desc' => [MposPeds::tableName() . '.serial_number' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['batch_status_id'] = [
            'asc' => [BatchStatusLocale::tableName() . '.desc' => SORT_ASC],
            'desc' => [BatchStatusLocale::tableName() . '.desc' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'bddn' => $this->bddn,
            'trx_counter' => $this->trx_counter,
            'batch_status_id' => $this->batch_status_id,
        ]);

        $query->andFilterWhere(['<>', PaymentTerminalCurrentBatch::tableName().'.payment_terminal_id', '35'])
            ->andFilterWhere(['like', PaymentsTerminals::tableName().'.name', $this->payment_terminal_id])
            ->andFilterWhere(['like', MposMerchants::tableName().'.name', $this->merchant_name])
            ->andFilterWhere(['like', MposMerchants::tableName().'.phone', $this->merchant_phone])
            ->andFilterWhere(['like', MposPeds::tableName().'.serial_number', $this->ped_serial_number]);

        DateColumnHelper::addFilterParams(['bdd'], [$this->bdd], $query);

        return $dataProvider;
    }
}
