<?php

namespace app\modules\UserManagement\controllers;

use app\modules\UserManagement\models\AwsChangePasswordForm;
use app\modules\UserManagement\models\AwsTokenForm;
use app\modules\UserManagement\models\ChangePasswordForm;
use app\modules\UserManagement\models\LoginForm;
use app\modules\UserManagement\models\AwsLoginForm;
use app\modules\UserManagement\models\LoginOtpForm;
use app\modules\UserManagement\models\LoginOtpTokenForm;
use app\modules\UserManagement\models\User;
use Yii;
use yii\base\InvalidConfigException;
use yii\base\NotSupportedException;
use yii\base\UserException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\Response;

class AuthController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        if (Yii::$app->getModule('user-management')->params['authChannel'] == 0) {
            //авторизация через базу данных
            return $this->dbLogin();

        } else if (Yii::$app->getModule('user-management')->params['authChannel'] == 1) {
            //Двухфакторная авторизация через OpenOTP
            return $this->openOtpLogin();

        } else if (Yii::$app->getModule('user-management')->params['authChannel'] == 2) {
            //Двухфакторная авторизация через AWS Cognito
            return $this->awsCognitoLogin();

        } else {
            throw new InvalidConfigException(Yii::t('app', 'Некорректная конфигурация модуля автоизации'));
        }
    }

    private function dbLogin()
    {
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    private function openOtpLogin() {
        $loginOtpForm = new LoginOtpForm();
        $loginOtpForm->domain = 'pcidss';
        $loginOtpTokenForm = new LoginOtpTokenForm();

        if ($loginOtpForm->load(Yii::$app->request->post())) {
            $response = $loginOtpForm->sendSoapRequest();
            if ($response === false) {
                $loginOtpForm->password = '';
                return $this->render('login-otp', [
                    'model' => $loginOtpForm,
                ]);
            } else if ($response['code'] == '0') {
                Yii::$app->session->setFlash('loginFormError', $response['message']);
                $loginOtpForm->password = '';
                return $this->render('login-otp', [
                    'model' => $loginOtpForm,
                ]);
            } else if ($response['code'] == '2') {
                $loginOtpTokenForm->username = $loginOtpForm->username;
                $loginOtpTokenForm->domain = $loginOtpForm->domain;
                $loginOtpTokenForm->session = $response['session'];

                Yii::$app->session->setFlash('loginFormSuccess', $response['message']);

                return $this->render('login-otp-token', [
                    'model' => $loginOtpTokenForm,
                    'timeout' => $response['timeout'],
                ]);
            } else {
                Yii::error("SOAP response: " . print_r($response, true));
                Yii::$app->session->setFlash('loginFormError', \Yii::t('app', 'Некорректный ответ от сервера авторизации'));
                $loginOtpForm->password = '';
                return $this->render('login', [
                    'model' => $loginOtpForm,
                ]);
            }
        }

        if ($loginOtpTokenForm->load(Yii::$app->request->post())) {
            $response = $loginOtpTokenForm->sendSoapRequest();
            if ($response === false) {
                //do nothing
            } else if ($response['code'] == '0') {
                Yii::$app->session->setFlash('loginOtpFormError', $response['message']);
                return $this->render('login-otp-token', [
                    'model' => $loginOtpTokenForm,
                    'timeout' => 0,
                ]);
            } else if ($response['code'] == '1') {
                $user = User::findByUsername($loginOtpTokenForm->username);
                if ($user === null) {
                    throw new UserException(Yii::t('app', 'Не получен список прав пользователя'));
                } else if ($user->status == 0) {
                    throw new UserException(Yii::t('app', 'Пользователь заблокирован. Обратитесь к администратору.'));
                }
                if (Yii::$app->user->login($user)) {
                    Yii::$app->session->setFlash('loginOtpFormSuccess', $response['message']);
                    return $this->goBack();
                } else {
                    throw new UserException(Yii::t('app', 'Что-то пошло не так...'));
                }
            } else {
                Yii::error("SOAP response: " . print_r($response, true));
                Yii::$app->session->setFlash('loginOtpFormError', Yii::t('app', 'Некорректный ответ от сервера авторизации'));
                return $this->render('login-otp-token', [
                    'model' => $loginOtpTokenForm,
                    'timeout' => 0,
                ]);
            }
        }

        return $this->render('login-otp', [
            'model' => $loginOtpForm,
        ]);
    }

    private function awsCognitoLogin() {
        $loginForm = new AwsLoginForm();
        $tokenForm = new AwsTokenForm();
        $changePasswordForm = new AwsChangePasswordForm();

        if ($loginForm->load(Yii::$app->request->post())) {
            $response = $loginForm->sendLoginRequest();
            if ($response !== false) {
                if ($response['ChallengeName'] == 'SOFTWARE_TOKEN_MFA') {
                    return $this->render('aws-token',[
                        'model' => $tokenForm,
                    ]);
                } else if ($response['ChallengeName'] == 'MFA_SETUP') {
                    return $this->redirect(['aws-assign-token']);
                } else if ($response['ChallengeName'] == 'NEW_PASSWORD_REQUIRED') {
                    return $this->render('aws-change-password',[
                        'model' => $changePasswordForm,
                    ]);
                } else {
                    Yii::error('Unexpected AWS challenge in response for login request. AWS response: ' . print_r($response, true));
                    Yii::$app->session->setFlash('awsRequestError', Yii::t('app', 'Не удалось обработать ответ сервера авторизации'));
                }
            } //else login form would be rendered
        }

        if ($changePasswordForm->load(Yii::$app->request->post())) {
            $response = $changePasswordForm->requiredPasswordChange();
            if ($response !== false) {
                if ($response['ChallengeName'] == 'SOFTWARE_TOKEN_MFA') {
                    return $this->render('aws-token',[
                        'model' => $tokenForm,
                    ]);
                } else if ($response['ChallengeName'] == 'MFA_SETUP') {
                    return $this->redirect(['aws-assign-token']);
                } else {
                    Yii::$app->session->setFlash('awsRequestError', Yii::t('app', 'Не удалось обработать ответ сервера авторизации'));
                    Yii::error('Unexpected AWS challenge in response for change password request. AWS response: ' . print_r($response, true));
                    return $this->redirect(['login']);
                }
            } else if ($changePasswordForm->hasErrors()) {
                return $this->render('aws-change-password',[
                    'model' => $changePasswordForm,
                ]);
            } else {
                return $this->redirect(['login']);
            }
        }

        if ($tokenForm->load(Yii::$app->request->post())) {
            if ($tokenForm->verifyToken()) {
                $user = User::findByUsername(Yii::$app->session->get('aws_username'));
                if ($user === null) {
                    Yii::$app->session->setFlash('awsRequestError', Yii::t('app', 'Не получен список прав пользователя'));
                    return $this->redirect(['login']);
                } else if ($user->status == 0) {
                    Yii::$app->session->setFlash('awsRequestError', Yii::t('app', 'Пользователь заблокирован. Обратитесь к администратору.'));
                    return $this->redirect(['login']);
                } else {
                    if (Yii::$app->user->login($user)) {
                        return $this->goBack();
                    } else {
                        throw new UserException(Yii::t('app', 'Что-то пошло не так...'));
                    }
                }
            } else if ($tokenForm->hasErrors()) {
                return $this->render('aws-token',[
                    'model' => $tokenForm,
                ]);
            } else {
                return $this->redirect(['login']);
            }
        }

        return $this->render('aws-login', [
            'model' => $loginForm,
        ]);
    }


    public function actionAwsAssignToken()
    {
        $awsSession = Yii::$app->session->get('aws_cognito_session');
        if ($awsSession === null) {
            return $this->redirect(['login']);
        }

        $model = new AwsTokenForm();

        if ($model->load(Yii::$app->request->post()) && $model->sendVerifySoftwareTokenRequest()) {
            return $this->redirect(['login']);
        }

        $qrString = $model->sendAssociateSoftwareTokenRequest($awsSession);
        if ($qrString === false) {
            return $this->redirect(['login']);
        }

        return $this->render('aws-assign-token', [
            'qrString' => $qrString,
            'model' => $model,
        ]);
    }

    public function actionChangePassword()
    {
        if (Yii::$app->getModule('user-management')->params['authChannel'] == 0) {
            return $this->changeDbPassword();
        } else if (Yii::$app->getModule('user-management')->params['authChannel'] == 2) {
            return $this->changeAwsPassword();
        } else {
            throw new NotSupportedException(Yii::t('app', 'Для данного способа авторизации смена пароля через АРМ не поддерживается'));
        }
    }

    private function changeDbPassword()
    {
        $model = new ChangePasswordForm();
        if ($model->load(Yii::$app->request->post()) && $model->changePassword()) {
            return $this->goHome();
        }

        return $this->render('change-password', [
            'model' => $model,
        ]);
    }

    private function changeAwsPassword()
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['login']);
        }

        $model = new AwsChangePasswordForm();
        if ($model->load(Yii::$app->request->post())) {
            $model->userPasswordChange();
        }

        return $this->render('aws-change-password', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}
