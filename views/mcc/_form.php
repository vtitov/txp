<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Mcc */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mcc-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'range_start_code')->textInput(['maxlength' => 4]) ?>

    <?= $form->field($model, 'range_end_code')->textInput(['maxlength' => 4]) ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => 255]) ?>

    <div class="form-group">
        <?= Html::submitButton(\Yii::t('app', 'Сохранить'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
