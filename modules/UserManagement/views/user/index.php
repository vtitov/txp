<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel \app\modules\UserManagement\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = \Yii::t('app', 'Пользователи');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(\Yii::t('app', 'Добавить пользователя'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'username',
            //'auth_key',
            //'password_hash',
            //'temp_password',
            //'temp_user',
            //'temp_user_active_from',
            //'temp_user_active_to',
            [
                'attribute' => 'status',
                'value' => function ($model) {
                    return '<span class="glyphicon ' . ($model->status ? 'glyphicon-ok' : 'glyphicon-remove') . '">' .
                    \app\modules\UserManagement\models\User::getStatuses()[$model->status] . '</span>';
                },
                'format' => 'raw',
                'filter' => \app\modules\UserManagement\models\User::getStatuses(),
                'visible' => (Yii::$app->getModule('user-management')->params['authChannel'] != 2),
            ],
            //'block_reason',
            'description',
            //'phone',
            //'email:email',
            //'mfo',
            [
                'attribute' => 'id',
                'label' => \Yii::t('app', 'Роли'),
                'enableSorting' => false,
                'filter' => false,
                'value' => function ($model) {
                    $roles = [];
                    foreach ($model->roles as $role) {
                        $roles[] = $role->description;
                    }
                    return implode(', ', $roles) . ' ' . Html::a(
                        '<span class="glyphicon glyphicon-pencil"/>',
                        ['manage-user-roles', 'id' => $model->id],
                        ['title' => \Yii::t('app', 'Редактировать роли пользователя')]);
                },
                'format' => 'raw',
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'options' => ['style' => 'min-width: 70px'],
            ],
        ],
    ]); ?>
</div>
