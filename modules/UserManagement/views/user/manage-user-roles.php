<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */

$this->title = \Yii::t('app', 'Назначение ролей пользователю: ') . $user->username;
$this->params['breadcrumbs'][] = ['label' => \Yii::t('app', 'Пользователи'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $user->username, 'url' => ['view', 'id' => $user->id]];
$this->params['breadcrumbs'][] = \Yii::t('app', 'Роли');
?>

<?php
if (Yii::$app->session->hasFlash('userRolesAssigned')){
    echo '<div class="alert alert-success">'.\Yii::t('app', 'Роли пользователя сохранены').'</div>';
}
if (Yii::$app->session->hasFlash('invalidDataRecieved')){
    echo '<div class="alert alert-danger">'.\Yii::t('app', 'Получены некорректные данные').'</div>';
}
?>

<div class="user-roles-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="checkbox-list-container">

        <?php
        foreach ($allRoles as $role) {
            echo '<div class="checkbox-container">';
            echo Html::checkbox('DynamicModel[roles][]', isset($userRoles[$role->name]), ['value' => $role->name, 'label' => $role->description]);
            echo '</div>';
        }
        ?>

    </div>

    <div class="form-group">
        <?= Html::submitButton(\Yii::t('app', 'Сохранить'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>