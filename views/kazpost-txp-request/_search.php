<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\KazpostTxpRequestSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="kazpost-txp-request-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'rq_id') ?>

    <?= $form->field($model, 'unp') ?>

    <?= $form->field($model, 'name_legal_entity') ?>

    <?= $form->field($model, 'registered_address') ?>

    <?php // echo $form->field($model, 'post_address') ?>

    <?php // echo $form->field($model, 'phone') ?>

    <?php // echo $form->field($model, 'email') ?>

    <?php // echo $form->field($model, 'bic') ?>

    <?php // echo $form->field($model, 'iban') ?>

    <?php // echo $form->field($model, 'currency') ?>

    <?php // echo $form->field($model, 'contract_id') ?>

    <?php // echo $form->field($model, 'contract_date') ?>

    <?php // echo $form->field($model, 'terminal_id') ?>

    <?php // echo $form->field($model, 'mcc') ?>

    <?php // echo $form->field($model, 'pt_description') ?>

    <?php // echo $form->field($model, 'pt_destination') ?>

    <?php // echo $form->field($model, 'terminal_status') ?>

    <?php // echo $form->field($model, 'parse_date') ?>

    <?php // echo $form->field($model, 'status') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
