<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TxpApiUser */
/* @var $ownerMap \app\modules\UserManagement\models\TxpApiUserOwnerMapBatch */

$this->title = Yii::t('app', 'Добавление пользователя API');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Пользователи API'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="txp-api-user-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'ownerMap' => $ownerMap,
    ]) ?>

</div>
