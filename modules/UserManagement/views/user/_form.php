<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model \app\modules\UserManagement\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'owner_id')->dropDownList(
                \yii\helpers\ArrayHelper::map(
                    \app\models\Owners::find()
                        ->select(['id', 'name'])
                        ->where(['locale' => Yii::$app->language])
                        ->all(),
                    'id',
                    'name'
                ),
                ['prompt' => '']
            ) ?>

            <?= $form->field($model, 'username')->textInput([
                'maxlength' => true,
                //не даем редактировать логин, если включена авторизация через AWS
                'disabled' => !$model->isNewRecord && (Yii::$app->getModule('user-management')->params['authChannel'] == 2)
            ]) ?>

            <?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>


            <?php if (Yii::$app->getModule('user-management')->params['authChannel'] == 0): ?>
            <?= $form->field($model, 'temp_user')->checkbox() ?>

            <div id="temp-user-period-div">
                <?= $form->field($model, 'temp_user_active_from')->widget(\yii\jui\DatePicker::className(), [
                    'dateFormat' => 'yyyy-MM-dd',
                    'options' => ['class' => 'form-control'],
                ]) ?>

                <?= $form->field($model, 'temp_user_active_to')->widget(\yii\jui\DatePicker::className(), [
                    'dateFormat' => 'yyyy-MM-dd',
                    'options' => ['class' => 'form-control'],
                ]) ?>
            </div>
            <?php endif; ?>

        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'phone')->textInput() ?>

            <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton(\Yii::t('app', 'Сохранить'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>