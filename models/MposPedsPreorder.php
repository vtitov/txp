<?php

namespace app\models;

use Yii;
use yii\validators\RegularExpressionValidator;

/**
 * This is the model class for table "mpos_peds_preorder".
 *
 * @property string $id
 * @property string $name
 * @property string $registered_address
 * @property string $post_address
 * @property string $unp
 * @property integer $okpo
 * @property string $email
 * @property string $mobile_phone
 * @property string $landline_phone
 * @property string $banks_mfo
 * @property string $bank_account
 * @property string $fio
 * @property string $position
 * @property string $contact_phone
 * @property integer $contact_devices_number
 * @property integer $contactless_devices_number
 */
class MposPedsPreorder extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ped_preorder';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('mpos');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'registered_address', 'post_address', 'unp', 'email', 'mobile_phone', 'ped_preorder_delivery_type_id', 'device_number', 'delivery_contact_person_fio'], 'required'],//, 'captcha'
            [['okpo', 'banks_mfo', 'ped_preorder_delivery_point_id'], 'integer'],
            //['unp', 'match', 'pattern' => '|^\d{9}$|', 'message' => 'УНП должен состоять из девяти цифр'],
            [['unp'], 'string', 'min' => 9, 'max' => 9],
            [['mobile_phone'], 'match', 'pattern' => '/^375(29|33|44|25)\d{7}$/', 'message' => \Yii::t('app', 'Номер телефона должн быть введен в формате 375XXYYYYYYY')],
            [['name', 'registered_address', 'post_address', 'delivery_address', 'delivery_contact_person_fio'], 'string', 'max' => 255],
            ['name', 'string', 'min' => 5],
            [['email'], 'string', 'max' => 50],
            ['email', 'email'],
            ['device_number', 'integer', 'min' => 1, 'max' => 127],
            ['ped_preorder_delivery_point_id', 'required', 'when' => function ($model) {
                return $model->ped_preorder_delivery_type_id == '1';
            }, 'whenClient' => "function (attribute, value) {
                return $('#mpospedspreorder-ped_preorder_delivery_type_id').val() == '1';
            }"],
            ['delivery_address', 'required', 'when' => function ($model) {
                return $model->ped_preorder_delivery_type_id != '1';
            }, 'whenClient' => "function (attribute, value) {
                return $('#mpospedspreorder-ped_preorder_delivery_type_id').val() != '1';
            }"],
            //['captcha', 'captcha', 'message' => 'Неверно введен проверочный код. Кликните по картинке для загрузки другого изображения.'],
            [['banks_mfo', 'bank_account'], 'default', 'value' => 0],
            ['banks_mfo', 'match', 'pattern' => '/^(\d{9}|0)$/', 'message' => \Yii::t('app', 'МФО должен состоять из девяти цифр')],
            ['bank_account', 'match', 'pattern' => '/^(\d{13}|0)$/', 'message' => \Yii::t('app', 'Номер счета должен состоять из 13 цифр')],
            ['iban', 'validateIbanAccount'],
            ['bic', 'validateBic'],

            ['iban', 'required', 'when' =>  function($model) {
                return $model->bank_account == 0;
            }, 'whenClient' => "function (attribute, value) {
                return $('#mpospedspreorder-bank_account').val() == '0';
            }", 'message' => \Yii::t('app', 'Необходимо ввести номер счета хотя бы в одном из форматов')],

            ['bic', 'required', 'when' =>  function($model) {
                return $model->banks_mfo == 0;
            }, 'whenClient' => "function (attribute, value) {
                return $('#mpospedspreorder-banks_mfo').val() == '0';
            }", 'message' => \Yii::t('app', 'Необходимо ввести код банка хотя бы в одном из форматов')],

            //доп поля, которые нужны приору
            [['head_full_name'], 'string', 'max' => 255],
            [['organisation_phone'], 'match', 'pattern' => '|^375\d{9}$|', 'message' => \Yii::t('app', 'Номер должн быть введен в формате 375XXYYYYYYY')],
        ];
    }

    public function validateIbanAccount($attribute)
    {

        $modifiedNumber = $this->$attribute = strtoupper(str_replace(" ", "", $this->$attribute));

        if(substr($this->$attribute, 0, 2) == 'BY' && strlen($this->$attribute) != 28){
            $this->addError($attribute, \Yii::t('app', 'IBAN должен состоять из 28 символов'));
        }

        $regexpValidator = new RegularExpressionValidator(['pattern' => '|^[A-Z]{2}[0-9]{2}[A-Z0-9]{11,30}$|']);
        if (!$regexpValidator->validate($this->$attribute)){
            $this->addError($attribute, \Yii::t('app', 'Неверный формат номера счета IBAN'));
        }

        $modifiedNumber = substr($modifiedNumber, 4) . substr($modifiedNumber, 0, 4);
        $modifiedNumber = str_replace(['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'],
            ['10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31', '32', '33', '34', '35'],
            $modifiedNumber);
        $modifiedNumber = ltrim($modifiedNumber, '0');

        /*
         * Если попытаться привести слишком длинную строку цифр к целому числу, php заменит его на максимальное целое число поддерживаемое данной сборкой (обычно это 2147483647)
         *  поэтому, чтобы получить остаток от деления числа на 97, мы будем менять первые 9 цифр числа на остаток от их деления
         *
         * пример:
         * 999 999 999 999 999 999 999 999  ==
         * ==  (999 999 999 * 10 в 15-й степени)  +  999 999 999 999 999  ==
         * ==  (10 309 278 * 97 + 33) * 10 в 15-й степени  +  999 999 999 999 999  ==        //здесь 33 - остаток от деления 999 999 999 на 97
         * ==  (10 309 278 * 97 * 10 в 15-й степени)  +  (33 * 10 в 15-й степени) + 999 999 999 999 999  ==
         * ==  (10 309 278 * 97 * 10 в 15-й степени)  +  33 999 999 999 999 999
         *
         *  Здесь первое слагаемое делится на 97 без остатка, а значит остаток от деления суммы (исходного числа) будет равен остатку от деления второго слагаемого,
         * что и означает, что для получения остатка можно пользоваться такой заменой
         * */

        while (strlen($modifiedNumber) > 9) {
            $num = substr($modifiedNumber, 0, 9);
            $modifiedNumber = ($num % 97) . substr($modifiedNumber, 9);
            $modifiedNumber = ltrim($modifiedNumber, '0');//обрезаем ведущие ноли, если вдруг они есть
        }

        if (($modifiedNumber % 97) != 1) {
            $this->addError($attribute, \Yii::t('app', 'Неверно введен номер счета IBAN'));
        }
    }

    public function validateBic($attribute){
        $this->$attribute = strtoupper(str_replace(" ", "", $this->$attribute));
        $regexpValidator = new RegularExpressionValidator(['pattern' => '|^[A-Z0-9]{4}[A-Z]{2}[A-Z0-9]{2}([A-Z0-9]{3})?$|']);

        if (!$regexpValidator->validate($this->$attribute)){
            $this->addError($attribute, \Yii::t('app', 'Неверный формат BIC'));
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => \Yii::t('app', 'Наименование организации'),
            'registered_address' => \Yii::t('app', 'Юридический адрес'),
            'post_address' => \Yii::t('app', 'Почтовый адрес'),
            'unp' => \Yii::t('app', 'УНП'),
            'okpo' => \Yii::t('app', 'ОКПО'),
            'email' => 'Email',
            'mobile_phone' => \Yii::t('app', 'Мобильный телефон'),
            'banks_mfo' => \Yii::t('app', 'МФО'),
            'bank_account' => \Yii::t('app', 'Расчетный счет'),
            'ped_preorder_delivery_type_id' => \Yii::t('app', 'Тип доставки'),
            'ped_preorder_delivery_point_id' => \Yii::t('app', 'Пункт выдачи'),
            'delivery_address' => \Yii::t('app', 'Адрес доставки'),
            'add_date' => \Yii::t('app', 'Дата оформления предзаказа'),
            'ped_preorder_status_id' => \Yii::t('app', 'Статус'),
            'device_number' => \Yii::t('app', 'Количество устройств'),
            'delivery_contact_person_fio' => \Yii::t('app', 'ФИО контактного лица для доставки'),
            'bic' => \Yii::t('app', 'BIC (код банка - новый формат)'),
            'iban' => \Yii::t('app', 'Расчетный счет (IBAN)'),
            'organisation_phone' => \Yii::t('app', 'Телефон организации'),
            'head_full_name' => \Yii::t('app', 'ФИО руководителя'),
        ];
    }

    public function getPedPreorderStatus()
    {
        return $this->hasOne(PedPreorderStatus::className(), ['id' => 'ped_preorder_status_id']);
    }

    public function getPedPreorderDeliveryType()
    {
        return $this->hasOne(PedPreorderDeliveryType::className(), ['id' => 'ped_preorder_delivery_type_id']);
    }

    public function getPedPreorderDeliveryPoint()
    {
        return $this->hasOne(PedPreorderDeliveryPoint::className(), ['id' => 'ped_preorder_delivery_point_id']);
    }

    public function createPriorXmlFile()
    {
        $xml = new \DOMDocument('1.0', 'Windows-1251');
        $xml->formatOutput = true;

        $application = $xml->createElement('Application');
        $application = $xml->appendChild($application);

        $deviceType = $xml->createElement('DeviceType');
        $deviceType = $application->appendChild($deviceType);
        $text = $xml->createTextNode('Miura');
        $text = $deviceType->appendChild($text);

        $channalType = $xml->createElement('ChannalType');
        $channalType = $application->appendChild($channalType);
        $text = $xml->createTextNode('IBA');
        $text = $channalType->appendChild($text);

        $legalInfo = $xml->createElement('LegalInfo');
        $legalInfo = $application->appendChild($legalInfo);

        $nameLegalEntity = $xml->createElement('Namelegalentity');
        $nameLegalEntity = $legalInfo->appendChild($nameLegalEntity);
        $text = $xml->createTextNode($this->name);
        $text = $nameLegalEntity->appendChild($text);

        $addrLegalEntity = $xml->createElement('Addrlegalentity');
        $addrLegalEntity = $legalInfo->appendChild($addrLegalEntity);
        $text = $xml->createTextNode($this->registered_address);
        $text = $addrLegalEntity->appendChild($text);

        $nameCompany = $xml->createElement('NameCompany');
        $nameCompany = $legalInfo->appendChild($nameCompany);
        $text = $xml->createTextNode($this->name);
        $text = $nameCompany->appendChild($text);

        $unp = $xml->createElement('UNP');
        $unp = $legalInfo->appendChild($unp);
        $text = $xml->createTextNode($this->unp);
        $text = $unp->appendChild($text);

        $phone = $xml->createElement('Phone');
        $phone = $legalInfo->appendChild($phone);
        $text = $xml->createTextNode('+' . $this->organisation_phone);
        $text = $phone->appendChild($text);

        $fax = $xml->createElement('Fax');
        $fax = $legalInfo->appendChild($fax);
        $text = $xml->createTextNode('+');
        $text = $fax->appendChild($text);

        $mobilePhone = $xml->createElement('MobilePhone');
        $mobilePhone = $legalInfo->appendChild($mobilePhone);
        $text = $xml->createTextNode('+' . $this->mobile_phone);
        $text = $mobilePhone->appendChild($text);

        $email = $xml->createElement('Email');
        $email = $legalInfo->appendChild($email);
        $text = $xml->createTextNode($this->email);
        $text = $email->appendChild($text);

        $jobTitle = $xml->createElement('JobTitle');
        $jobTitle = $legalInfo->appendChild($jobTitle);
        $text = $xml->createTextNode('');
        $text = $jobTitle->appendChild($text);

        $fioBoss = $xml->createElement('FIIOBoss');
        $fioBoss = $legalInfo->appendChild($fioBoss);
        $text = $xml->createTextNode($this->head_full_name);
        $text = $fioBoss->appendChild($text);

        $numberOfTerminal = $xml->createElement('NumberOfterminal');
        $numberOfTerminal = $legalInfo->appendChild($numberOfTerminal);
        $text = $xml->createTextNode($this->device_number);
        $text = $numberOfTerminal->appendChild($text);

        $bankInfo = $xml->createElement('BankInfo');
        $bankInfo = $application->appendChild($bankInfo);

        $checkingAccountIban = $xml->createElement('Checkingaccount_iban');
        $checkingAccountIban = $bankInfo->appendChild($checkingAccountIban);
        $text = $xml->createTextNode($this->iban);
        $text = $checkingAccountIban->appendChild($text);

        $bankCheckingAccountName = $xml->createElement('BankCheckingaccountName');
        $bankCheckingAccountName = $bankInfo->appendChild($bankCheckingAccountName);
        $bank = Banks::findOne(['mfo' => $this->bic]);
        $text = $xml->createTextNode($bank === null ? '' : $bank->name);
        $text = $bankCheckingAccountName->appendChild($text);

        $bankCheckingAccountMfoIban = $xml->createElement('BankCheckingaccountMFO_iban');
        $bankCheckingAccountMfoIban = $bankInfo->appendChild($bankCheckingAccountMfoIban);
        $text = $xml->createTextNode($this->bic);
        $text = $bankCheckingAccountMfoIban->appendChild($text);

        $xmlString = $xml->saveXML();
        if ($xmlString === false) {
            Yii::error('XML creating failed');
            return false;
        }
        Yii::info("Preorder XML for Priorbank: " . iconv('Windows-1251', 'utf-8', $xmlString));

        try {
            // Create temporary file
            $tmpFile = fopen('php://temp', 'r+');
            if ($tmpFile === false) {
                Yii::error('Temporary file creating failed');
                return false;
            }
            $r = fwrite($tmpFile, $xmlString);
            if ($r === false) {
                Yii::error('Failed to write XML to temporary file');
                fclose($tmpFile);
                return false;
            }
            $r = rewind($tmpFile);
            if ($r === false) {
                Yii::error('Error resetting file pointer cursor');
                fclose($tmpFile);
                return false;
            }
            // FTP connection
            $ftpConn = ftp_connect(Yii::$app->params['serverFtpForPriorbank']);
            if ($ftpConn === false) {
                Yii::error('Failed to connect to FTP server');
                fclose($tmpFile);
                return false;
            }
            // FTP login
            $r = ftp_login($ftpConn, Yii::$app->params['userFtpForPriorbank'], Yii::$app->params['passwordFtpForPriorbank']);
            if ($r === false) {
                Yii::error('Failed to login to FTP server');
                fclose($tmpFile);
                ftp_close($ftpConn);
                return false;
            }
            //passive mode
            $r = ftp_pasv($ftpConn, true);
            if ($r === false) {
                Yii::error('Failed to enable passive FTP mode');
                fclose($tmpFile);
                ftp_close($ftpConn);
                return false;
            }
            // FTP upload
            $res = ftp_fput($ftpConn, 'in/preorder_' . $this->id . '.xml', $tmpFile, FTP_ASCII);

            ftp_close($ftpConn);
            fclose($tmpFile);

        } catch (\Exception $e) {
            Yii::error('Failed to save xml. ' . $e->getMessage());
            return false;
        }

        return $res;
    }
}
