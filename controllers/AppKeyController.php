<?php

namespace app\controllers;

use Yii;
use app\models\AppKey;
use app\models\AppKeySearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * AppKeyController implements the CRUD actions for AppKey model.
 */
class AppKeyController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view'],
                        'roles' => ['appKeyView'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['create', 'update'],
                        'roles' => ['appKeyEdit'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all AppKey models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AppKeySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single AppKey model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new AppKey model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $appKeyId = file_get_contents(Yii::$app->params['generateAppKeyUrl']);
        if ($appKeyId === false) {
            Yii::$app->session->setFlash('appKeyGenerationNoAnswer');
            return $this->redirect(['index']);
        } else if (preg_match('/^\d+$/', $appKeyId)) {
            return $this->redirect(['view', 'id' => $appKeyId]);
        } else {
            Yii::$app->session->setFlash('appKeyGenerationWrongAnswer');
            return $this->redirect(['index']);
        }
    }

    /**
     * Updates an existing AppKey model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Finds the AppKey model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return AppKey the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = AppKey::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
