<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PaymentsTerminalsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = \Yii::t('app', 'Справочник платежных терминалов');
$this->params['breadcrumbs'][] = $this->title;

$isoSettingsQuery = \app\models\IsoSettings::find()
    ->select(['id', 'name']);
if (Yii::$app->user->identity->owner_id != 1) {
    $isoSettingsQuery->andWhere(['owner_id' => Yii::$app->user->identity->owner_id]);
}
$isoSettingsFilter = ArrayHelper::map($isoSettingsQuery->asArray()->all(), 'id', 'name');
?>
<div class="payments-terminals-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(\Yii::t('app', 'Добавить платежный терминал'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            [
                'attribute' => 'ownerId',
                'value' => function($model){
                    return ArrayHelper::getValue(ArrayHelper::getValue(ArrayHelper::getValue($model, 'isoSettings'), 'owner'), 'name');
                },
                'filter' => ArrayHelper::map(
                    app\models\Owners::find()
                        ->where(['locale' => Yii::$app->language])
                        ->orderBy(['name' => SORT_ASC])
                        ->asArray()
                        ->all(),
                    'id',
                    'name'
                ),
                'label' => Yii::t('app', 'Владелец'),
                'visible' => (Yii::$app->user->identity->owner_id == 1),
            ],
            [
                'attribute' => 'iso_setting_id',
                'value' => function($model) use ($isoSettingsFilter) {
                    return ArrayHelper::getValue($isoSettingsFilter, $model->iso_setting_id, $model->iso_setting_id);
                },
                'filter' => $isoSettingsFilter,
            ],
            'name',
            [
                'attribute' => 'merchant_id',
                'value' => function($model){
                    if ($model->merchant) {
                        return Html::a(
                            Html::encode($model->merchant->name),
                            ['mpos-merchants/view', 'id' => $model->merchant_id]
                        );
                    } else {
                        return $model->merchant_id;
                    }
                },
                'format' => 'html',
            ],
            'description',
            [
                'attribute' => 'status',
                'value' => 'paymentTerminalStatus.name',
                'filter' => ArrayHelper::map(
                    \app\models\PaymentTerminalStatusLocale::find()
                        ->where(['locale' => Yii::$app->language])
                        ->asArray()
                        ->all(),
                    'id',
                    'name'
                ),
            ],
            [
                'attribute' => 'ped_id',
                'value' => function($model){
                    if ($model->ped) {
                        return Html::a(
                            Html::encode($model->ped->serial_number),
                            ['mpos-peds/view', 'id' => $model->ped_id]
                        );
                    } else {
                        return $model->ped_id;
                    }
                },
                'format' => 'html',
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'options' => ['width' => '70px'],
                'visibleButtons' => [
                    'update' => function($model) {
                        return $model->id != 1;
                    },
                    'delete' => function($model) {
                        return $model->id != 1;
                    },
                ]
            ],
        ],
    ]); ?>

</div>
