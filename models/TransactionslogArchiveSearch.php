<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TransactionslogArchive;

/**
 * TransactionslogArchiveSearch represents the model behind the search form about `app\models\TransactionslogArchive`.
 */
class TransactionslogArchiveSearch extends TransactionslogArchive
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'trx_statuses_id', 'trx_types_id', 'iso_setting_id', 'payment_id', 'payment_terminal_id', 'amount', 'batch', 'reconcilationFlag', 'MTI', 'rrn', 'expdate', 'acquircode', 'stan'], 'integer'],
            [['dt', 'currency', 'bdd', 'ksn', 'authcode', 'sred', 'device', 'response_code'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TransactionslogArchive::find()
            /*->select(TransactionslogArchive::tableName().'.*')
            ->from(TransactionslogArchive::tableName().' FORCE INDEX (`idx_dt`)')*/
            ->leftJoin(TrxStatuses::tableName(), TrxStatuses::tableName() . '.id = trx_statuses_id')
            ->leftJoin(TrxTypes::tableName(), TrxTypes::tableName() . '.id = trx_types_id')
            ->leftJoin('mpos.payment_locale', static::tableName() . '.payment_id = payment_locale.id AND payment_locale.locale = \'' . Yii::$app->language . '\'')
            ->with(['payment', 'mposPed', 'paymentTerminal', 'transactionArchive']);

        $owner_id = Yii::$app->user->identity->owner_id;
        if ($owner_id != 1) {
            $query->leftJoin('mpos.payment_terminal', 'payment_terminal_id = payment_terminal.id')
                ->leftJoin('mpos.iso_setting', 'payment_terminal.iso_setting_id = iso_setting.id')
                ->andWhere(['iso_setting.owner_id' => $owner_id]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['dt' => SORT_DESC, 'id' => SORT_DESC]]
        ]);

        $dataProvider->sort->attributes['trx_statuses_id'] = [
            'asc' => [TrxStatuses::tableName() . '.name' => SORT_ASC],
            'desc' => [TrxStatuses::tableName() . '.name' => SORT_DESC]
        ];

        $dataProvider->sort->attributes['trx_types_id'] = [
            'asc' => [TrxTypes::tableName() . '.name' => SORT_ASC],
            'desc' => [TrxTypes::tableName() . '.name' => SORT_DESC]
        ];

        $dataProvider->sort->attributes['payment_id'] = [
            'asc' => [PaymentLocale::tableName() . '.name' => SORT_ASC],
            'desc' => [PaymentLocale::tableName() . '.name' => SORT_DESC]
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            static::tableName() . '.id' => $this->id,
            static::tableName() . '.trx_statuses_id' => $this->trx_statuses_id,
            static::tableName() . '.trx_types_id' => $this->trx_types_id,
            static::tableName() . '.iso_setting_id' => $this->iso_setting_id,
            static::tableName() . '.payment_id' => $this->payment_id,
            static::tableName() . '.payment_terminal_id' => $this->payment_terminal_id,
            static::tableName() . '.amount' => $this->amount,
            static::tableName() . '.bdd' => $this->bdd,
            static::tableName() . '.batch' => $this->batch,
            static::tableName() . '.reconcilationFlag' => $this->reconcilationFlag,
            static::tableName() . '.MTI' => $this->MTI,
            static::tableName() . '.expdate' => $this->expdate,
            static::tableName() . '.acquircode' => $this->acquircode,
            static::tableName() . '.stan' => $this->stan,
        ]);

        $query->andFilterWhere(['like', static::tableName() . '.currency', $this->currency])
            ->andFilterWhere(['like', static::tableName() . '.rrn', $this->rrn])
            ->andFilterWhere(['like', static::tableName() . '.ksn', $this->ksn])
            ->andFilterWhere(['like', static::tableName() . '.authcode', $this->authcode])
            ->andFilterWhere(['like', static::tableName() . '.sred', $this->sred])
            ->andFilterWhere(['like', static::tableName() . '.device', $this->device])
            ->andFilterWhere(['like', static::tableName() . '.response_code', $this->response_code]);

        \app\helpers\DateColumnHelper::addFilterParams([static::tableName() . '.dt'], [$this->dt], $query);

        return $dataProvider;
    }
}
