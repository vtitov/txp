<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ped_model".
 *
 * @property integer $id
 * @property string $name
 * @property integer $ped_vendor_id
 * @property string $description
 *
 * @property MposPedManufacturers $mposPedManufacturer
 * @property MposPeds[] $mposPeds
 */
class MposPedModels extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ped_model';
    }
	
	public static function getDb()
    {
        return Yii::$app->get('mpos');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'ped_vendor_id'], 'required'],
            [['ped_vendor_id'], 'integer'],
            [['name'], 'string', 'max' => 40],
            ['name', 'unique', 'targetAttribute' => ['name', 'ped_vendor_id']],
            [['description'], 'string', 'max' => 100],
            ['pts_appr_number', 'string', 'max' => 7],
            ['pts_exp_date', 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => \Yii::t('app', 'Модель'),
            'ped_vendor_id' => \Yii::t('app', 'Производитель'),
            'description' => \Yii::t('app', 'Описание'),
            'pts_appr_number' => 'PTS SSC Approval Number',
            'pts_exp_date' => \Yii::t('app', 'Дата окончания сертификата'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMposPedManufacturer()
    {
        return $this->hasOne(MposPedManufacturers::className(), ['id' => 'ped_vendor_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMposPeds()
    {
        return $this->hasMany(MposPeds::className(), ['mpos_ped_model_id' => 'id']);
    }
}
