<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ChangeBankAcquirerPriorbankResponse */

$this->title = 'Update Change Bank Acquirer Priorbank Response: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Change Bank Acquirer Priorbank Responses', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="change-bank-acquirer-priorbank-response-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
