<?php

namespace app\models;


use app\helpers\DateColumnHelper;
use yii\base\Model;
use yii\data\ArrayDataProvider;
use yii\db\Expression;

class TransactionIpsReportSearch extends Model
{

    public $dateRange;
    public $isoSetting;

    public static function getIsoSettings()
    {
        return [
            25 => \Yii::t('app', 'Приорбанк (tapXphone)'),
            26 => \Yii::t('app', 'Казпочта'),
            28 => \Yii::t('app', 'Борисов транспорт'),
            42 => \Yii::t('app', 'Victoriabank'),
            41 => \Yii::t('app', 'Банк ЦентрКредит'),

        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['dateRange', 'isoSetting'], 'required'],
            [
                ['dateRange'],
                'match',
                'pattern' => '/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[12][0-9]|3[01]) - [0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[12][0-9]|3[01])$/',
                'message' => \Yii::t('app', 'Неверный формат даты')
            ],
            [['isoSetting'], 'in', 'range' => array_keys(static::getIsoSettings())],
        ];
    }

    public function search($params)
    {
        $this->load($params);

        if (!$this->validate()) {
            return new ArrayDataProvider();
        }

        $query = TransactionArchive::find()
            ->joinWith(['paymentTerminal'])
            ->select([
                TransactionArchive::tableName() . '.payments_terminal_id',
                TransactionArchive::tableName() . '.mpos_transaction_status_id',
                TransactionArchive::tableName() . '.payment_id',
                TransactionArchive::tableName() . '.amount',
                TransactionArchive::tableName() . '.currency_id',
                new Expression('SUBSTRING(' . TransactionArchive::tableName() . '.card, 1, 1) as ips'),
                TransactionArchive::tableName() . '.rrn',
            ])->where([PaymentsTerminals::tableName() . '.iso_setting_id' => $this->isoSetting]);

        DateColumnHelper::addFilterParams(
            [TransactionArchive::tableName() . '.date_time'],
            [$this->dateRange],
            $query
        );

        $data = $query->asArray()->all();
        $result = [];
        foreach ($data as $row) {
            if (!isset($result[$row['currency_id']][$row['ips']])) {
                $result[$row['currency_id']][$row['ips']] = [
                    'count' => 0,
                    'amount' => 0
                ];
            }
            if ($row['payment_id'] == 100000001) {

                if ($row['mpos_transaction_status_id'] == 1) {
                    //простая успешно завершенная оплата
                    $result[$row['currency_id']][$row['ips']]['count']++;
                    $result[$row['currency_id']][$row['ips']]['amount'] += $row['amount'];
                } else {

                    //отмененный платеж - ищем был ли возврат по rrn
                    $q = TransactionArchive::find()
                        ->joinWith(['paymentTerminal'])
                        ->where([
                            TransactionArchive::tableName() . '.payment_id' => 200000003,
                            TransactionArchive::tableName() . '.mpos_transaction_status_id' => 1,
                            PaymentsTerminals::tableName() . '.iso_setting_id' => $this->isoSetting,
                            TransactionArchive::tableName() . '.rrn' => $row['rrn'],
                        ]);

                    if ($q->exists()) {
                        //нашелся возврат по rrn для платежа
                        $result[$row['currency_id']][$row['ips']]['count']++;
                        $result[$row['currency_id']][$row['ips']]['amount'] += $row['amount'];
                    } else {
                        //нет возврата по rrn - простая отмена
                        $result[$row['currency_id']][$row['ips']]['count'] += 2;
                    }
                }
            } else if ($row['payment_id'] == 200000003) {
                if ($row['mpos_transaction_status_id'] == 1) {
                    //успешный возврат средств
                    $result[$row['currency_id']][$row['ips']]['count']++;
                    $result[$row['currency_id']][$row['ips']]['amount'] -= $row['amount'];
                } else {
                    $result[$row['currency_id']][$row['ips']]['count'] += 2;
                }
            } else {
                \Yii::$app->session->addFlash(
                    'warning',
                    \Yii::t(
                        'app',
                        'В выборку попал платеж {paymentCode}, его здесь быть не должно...',
                        ['paymentCode' => $row['payment_id']]
                    )
                );
            }
        }

        $flattenResult = [];
        foreach($result as $currency => $arr1) {
            foreach($arr1 as $ips => $arr2) {
                $flattenResult[] = [
                    'currency' => $currency,
                    'ips' => $ips,
                    'count' => $arr2['count'],
                    'amount' => $arr2['amount']
                ];
            }
        }
        usort($flattenResult, function($a, $b) {
            if ($a['currency'] < $b['currency']) {
                return -1;
            } else if ($a['currency'] > $b['currency']) {
                return 1;
            } else {
                if ($a['ips'] < $b['ips']) {
                    return -1;
                } else if ($a['ips'] > $b['ips']) {
                    return 1;
                } else {
                    return 0;
                }
            }
        });


        return new ArrayDataProvider([
            'allModels' => $flattenResult,
            'sort' => false,
            'pagination' => false,
        ]);
    }
} 