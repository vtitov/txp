<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model \app\modules\UserManagement\models\AuthItem */

$this->title = \Yii::t('app', 'Редактирование роли: ') . $model->name;
$this->params['breadcrumbs'][] = ['label' => \Yii::t('app', 'Роли'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->name]];
$this->params['breadcrumbs'][] = \Yii::t('app', 'Редактирование');
?>
<div class="auth-item-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
