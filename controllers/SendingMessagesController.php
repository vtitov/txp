<?php
namespace app\controllers;

use app\helpers\Notification;
use app\models\MerchantEmailSending;
use app\models\MposMerchants;
use app\models\SendSmsForm;
use app\modules\UserManagement\models\User;
use Yii;
use yii\base\UserException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\validators\EmailValidator;
use yii\web\Controller;

class SendingMessagesController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['message-to-merchants', 'send-message-to-merchants', 'view-unsend-messages'],
                        'roles' => ['messageMerchant'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['message-to-users', 'send-message-to-users'],
                        'roles' => ['messageUser'],
                    ],

                    [
                        'allow' => true,
                        'actions' => ['send-sms'],
                        'roles' => ['messageSms'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'sms-to-all' => ['post'],
                ],
            ],
        ];
    }

    public function actionMessageToMerchants()
    {
        return $this->render('message-to-merchants');
    }

    public function actionMessageToUsers()
    {
        return $this->render('message-to-users');
    }

    public function actionViewUnsendMessages()
    {
        $count = MerchantEmailSending::find()->andFilterWhere(['status' => 0])->count();
        return $this->render('view-unsend-messages', [
            'count' => $count,
        ]);
    }

    public function actionSendMessageToMerchants()
    {
        set_time_limit(0);

        $merchants = MposMerchants::find()
            ->select(['email', 'phone', 'owner_id'])
            ->distinct(true)
            ->andWhere(['merchant_status_id' => 1])
            ->asArray()
            ->all();

        if ($_POST['send_by'] == 'email' || $_POST['send_by'] == 'both') {
            $unsendMessagesCount = MerchantEmailSending::find()->andFilterWhere(['status' => 0])->count();
            if ($unsendMessagesCount > 0) {
                throw new UserException(
                    Yii::t('app', 'Предыдущая email-рассылка еще не завершена. Сообщений в очереди: ') . $unsendMessagesCount
                );
            } else {
                //Yii::$app->db->createCommand('truncate `paybycard`.`merchant_email_sending`')->execute(); //надо ли чистить таблицу от старых сообщений?
            }

            foreach ($merchants as $m) {
                $batchInsertArray[] = [$m['owner_id'], $m['email'], $_POST['subject'], $_POST['text']];
            }

            while(count($batchInsertArray) > 0){
                $batch = array_splice($batchInsertArray, 0, 100);
                MerchantEmailSending::getDb()->createCommand()
                    ->batchInsert(MerchantEmailSending::tableName(), ['owner_id', 'email', 'subject', 'text'], $batch)
                    ->execute();
            }
        }

        if ($_POST['send_by'] == 'sms' || $_POST['send_by'] == 'both') {
            foreach ($merchants as $m) {
                $res = Notification::sendToAddress(Notification::TYPE_SMS, $m['phone'], $_POST['text'], $m['owner_id']);
                if ($res !== true) {
                    Yii::$app->session->addFlash(
                        'error',
                        Yii::t(
                            'app',
                            'SMS на номер {phoneNumber} не отправлено: {errorMessage}',
                            ['phoneNumber' => $m['phone'], 'errorMessage' => $res]
                        )
                    );
                }
            }
        }

        Yii::$app->session->setFlash('messageToMerchantSent');
        return $this->redirect(['message-to-merchants']);
    }

    public function actionSendMessageToUsers()
    {

        set_time_limit(0);

        $users = User::find()
            ->select(['email', 'owner_id'])
            ->distinct(true)
            ->andFilterWhere(['status' => '1'])//только активным пользователям
            ->asArray()
            ->all();

        $validator = new EmailValidator();

        foreach ($users as $u) {

            if ($validator->validate($u['email'])) {
                $res = Notification::sendToAddress(
                    Notification::TYPE_EMAIL,
                    $u['email'],
                    $_POST['text'],
                    $u['owner_id'],
                    $_POST['subject']
                );
                if ($res !== true) {
                    Yii::$app->session->addFlash(
                        'error',
                        Yii::t(
                            'app',
                            'Email на адрес {email} не отправлен: {errorMessage}',
                            ['email' => $u['email'], 'errorMessage' => $res]
                        )
                    );
                }
            }
        }

        return $this->redirect(['site/index']);
    }

    public function actionSendSms()
    {
        $model = new SendSmsForm();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $res = Notification::sendToAddress(Notification::TYPE_SMS, $model->phone, $model->text);
            if ($res !== true) {
                Yii::$app->session->addFlash(
                    'error',
                    Yii::t(
                        'app',
                        'SMS не отправлено: {errorMessage}',
                        ['errorMessage' => $res]
                    )
                );
            }
            return $this->refresh();
        } else {
            return $this->render('send-sms', [
                'model' => $model,
            ]);
        }
    }
}
