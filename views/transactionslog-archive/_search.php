<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TransactionslogArchiveSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="transactionslog-archive-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'dt') ?>

    <?= $form->field($model, 'trx_statuses_id') ?>

    <?= $form->field($model, 'trx_types_id') ?>

    <?= $form->field($model, 'iso_setting_id') ?>

    <?php // echo $form->field($model, 'payments_id') ?>

    <?php // echo $form->field($model, 'payments_terminals_id') ?>

    <?php // echo $form->field($model, 'currency') ?>

    <?php // echo $form->field($model, 'amount') ?>

    <?php // echo $form->field($model, 'bdd') ?>

    <?php // echo $form->field($model, 'batch') ?>

    <?php // echo $form->field($model, 'reconcilationFlag') ?>

    <?php // echo $form->field($model, 'MTI') ?>

    <?php // echo $form->field($model, 'rrn') ?>

    <?php // echo $form->field($model, 'ksn') ?>

    <?php // echo $form->field($model, 'expdate') ?>

    <?php // echo $form->field($model, 'authcode') ?>

    <?php // echo $form->field($model, 'acquircode') ?>

    <?php // echo $form->field($model, 'stan') ?>

    <?php // echo $form->field($model, 'sred') ?>

    <?php // echo $form->field($model, 'device') ?>

    <?php // echo $form->field($model, 'response_code') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
