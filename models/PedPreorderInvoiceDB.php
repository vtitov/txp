<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ped_preorder_invoice".
 *
 * @property string $preorder_id
 * @property string $type
 * @property string $size
 * @property string $name
 * @property string $content
 * @property string $upload_date
 */
class PedPreorderInvoiceDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ped_preorder_invoice';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('mpos');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['preorder_id', 'type', 'size', 'name', 'content'], 'required'],
            [['preorder_id', 'size'], 'integer'],
            [['content'], 'string'],
            [['upload_date'], 'safe'],
            [['type'], 'string', 'max' => 255],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'preorder_id' => 'Preorder ID',
            'type' => 'Type',
            'size' => 'Size',
            'name' => 'Name',
            'content' => 'Content',
            'upload_date' => 'Upload Date',
        ];
    }
}
