<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "transaction_status_locale".
 *
 * @property int $id
 * @property string|null $name
 * @property string $locale
 */
class TransactionStatusLocale extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'transaction_status_locale';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('mpos');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [];
    }
}
