<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PedPreorderStatusSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = \Yii::t('app', 'Справочник статусов предзаказов');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ped-preorder-status-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(\Yii::t('app', 'Добавить статус'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'name',
                'value' => function($model){
                    return '<font color="' . Html::encode($model->color) . '">' . Html::encode($model->name) . '</font>';
                },
                'format' => 'html',
            ],
            //'color',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
