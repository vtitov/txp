<?php

namespace app\modules\UserManagement\models;

use Yii;

/**
 * This is the model class for table "login_history".
 *
 * @property string $id
 * @property string $username
 * @property string $ip
 * @property string $date
 * @property int $status
 */
class LoginHistory extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'login_history';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['username', 'status'], 'required'],
            [['date'], 'safe'],
            [['status'], 'integer'],
            [['username'], 'string', 'max' => 255],
            [['ip'], 'string', 'max' => 15],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'ip' => 'Ip',
            'date' => 'Date',
            'status' => 'Status',
        ];
    }
}
