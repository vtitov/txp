<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "merchant_email_sending".
 *
 * @property string $id
 * @property integer $owner_id
 * @property string $email
 * @property integer $status
 * @property string $subject
 * @property resource $text
 * @property string $date_send
 */
class MerchantEmailSending extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'merchant_email_sending';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('paybycard');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email', 'text'], 'required'],
            [['owner_id', 'status'], 'integer'],
            [['text'], 'string'],
            [['date_send'], 'safe'],
            [['email', 'subject'], 'string', 'max' => 255],
            ['email', 'email']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'owner_id' => 'Owner ID',
            'email' => 'Email',
            'status' => 'Status',
            'subject' => 'Subject',
            'text' => 'Text',
            'date_send' => 'Date Send',
        ];
    }
}
