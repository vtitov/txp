<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Tabs;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PedPreorderPriorbankResponseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Заявки на подключение mPOS Приорбанк';
$this->params['breadcrumbs'][] = $this->title;
?>

<style>
    .nav-tabs > li > a {
        background-color: #eee;
    }
    .nav-tabs > li.active > a {
        background-color: #fff;
        font-weight: bold;
    }
</style>

<div class="ped-preorder-priorbank-response-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= Tabs::widget([
        'items' => [
            [
                'label' => 'По предзаказам',
                'content' => GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

                        //'id',
                        'preorder_id',
                        'file_number',
                        'name_legal_entity',
                        //'unp',
                        //'email:email',
                        //'pos_info',
                        'contract',
                        \app\helpers\DateColumnHelper::getColumn('contract_date', $searchModel),
                        //'trading_name',
                        //'phone_trading_name',
                        //'mcc',
                        //'country',
                        //'region',
                        //'locality',
                        //'address',
                        'terminal_id',
                        //'merchant_id',
                        //'parse_date',
                        [
                            'attribute' => 'status',
                            'value' => function($model){
                                return \yii\helpers\ArrayHelper::getValue(
                                    \app\models\PedPreorderPriorbankResponse::$_STATUS,
                                    $model->status,
                                    $model->status
                                );
                            },
                            'filter' => \app\models\PedPreorderPriorbankResponse::$_STATUS,
                        ],

                        [
                            'class' => 'yii\grid\ActionColumn',
                            'template' => '{view}',
                        ],
                    ],
                ]),
                //'url' => '#preorder',
                'active' => is_null(Yii::$app->request->get('ChangeBankAcquirerPriorbankResponseSearch')),
            ],
            [
                'label' => 'По смене банка-эквайера',
                'content' => GridView::widget([
                    'dataProvider' => $changeAcquirerDataProvider,
                    'filterModel' => $changeAcquirerSearchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

                        //'id',
                        'request_id',
                        'file_number',
                        'name_legal_entity',
                        // 'unp',
                        // 'email:email',
                        // 'pos_info',
                        'contract',
                        \app\helpers\DateColumnHelper::getColumn('contract_date', $changeAcquirerSearchModel),
                        // 'trading_name',
                        // 'phone_trading_name',
                        // 'mcc',
                        // 'country',
                        // 'region',
                        // 'locality',
                        // 'address',
                        [
                            'attribute' => 'serialNumber',
                            'label' => 'Серийный номер',
                            'value' => 'changeBankAcquirerRequest.serial_number',
                        ],
                        'terminal_id',
                        // 'merchant_id',
                        // 'parse_date',
                        [
                            'attribute' => 'status',
                            'value' => function($model){
                                if($model->status){
                                    return 'Обработан';
                                } else {
                                    return 'Необработан';
                                }
                            },
                            'filter' => [
                                0 => 'Необработан',
                                1 => 'Обработан',
                            ],
                        ],

                        [
                            'class' => 'yii\grid\ActionColumn',
                            'template' => '{view}',
                            'controller' => 'change-bank-acquirer-priorbank-response',
                        ],
                    ],
                ]),
                //'url' => '#change',
                'active' => !is_null(Yii::$app->request->get('ChangeBankAcquirerPriorbankResponseSearch')),
            ],
        ]
    ])?>

</div>
