<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\color\ColorInput;

/* @var $this yii\web\View */
/* @var $model app\models\MposMerchantStatuses */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mpos-merchant-statuses-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => 40]) ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => 255]) ?>

    <?php echo $form->field($model, 'color')->widget(ColorInput::classname(), [
        'options' => ['placeholder' => \Yii::t('app', 'Выберите цвет...')],
    ]);?>

    <div class="form-group">
        <?= Html::submitButton(\Yii::t('app', 'Сохранить'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
