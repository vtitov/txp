<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ChangeBankAcquirerPriorbankResponse */

$this->title = "Заявка по смене банка-эквайера " . $model->request_id . "~" . $model->file_number;
$this->params['breadcrumbs'][] = ['label' => 'Заявки на подключение Приорбанк', 'url' => ['ped-preorder-priorbank-response/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="change-bank-acquirer-priorbank-response-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            'request_id',
            'file_number',
            'name_legal_entity',
            'unp',
            'email:email',
            'pos_info',
            'contract',
            'contract_date',
            'trading_name',
            'phone_trading_name',
            'mcc',
            'country',
            'region',
            'locality',
            'address',
            [
                'attribute' => 'request_id',
                'label' => 'Серийный номер',
                'value' => $model->changeBankAcquirerRequest->serial_number,
            ],
            'terminal_id',
            'merchant_id',
            'parse_date',
            [
                'attribute' => 'status',
                'value' => ($model->status ? 'Обработан' : 'Необработан') . ' ' . Html::a('Изменить статус', ['change-status', 'id' => $model->id], ['class' => 'btn btn-primary']),
                'format' => 'html',
            ],
        ],
    ]) ?>

</div>
