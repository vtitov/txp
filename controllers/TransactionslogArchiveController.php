<?php

namespace app\controllers;

use Yii;
use app\models\TransactionslogArchive;
use app\models\TransactionslogArchiveSearch;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * TransactionslogArchiveController implements the CRUD actions for TransactionslogArchive model.
 */
class TransactionslogArchiveController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view'],
                        'roles' => ['transactionslogArchiveView'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all TransactionslogArchive models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TransactionslogArchiveSearch();
        $searchModel->dt = date('Y-m-d') . ' - ' . date('Y-m-d');
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TransactionslogArchive model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Finds the TransactionslogArchive model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return TransactionslogArchive the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        $owner_id = Yii::$app->user->identity->owner_id;
        $model = TransactionslogArchive::findOne($id);
        $isoSetting = ArrayHelper::getValue(ArrayHelper::getValue($model, 'paymentTerminal'), 'isoSettings');

        if ($model !== null && ($owner_id == 1 || ($isoSetting !== null && $isoSetting->owner_id == $owner_id))) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
