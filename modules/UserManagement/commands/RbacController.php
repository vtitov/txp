<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\modules\UserManagement\commands;

use Yii;
use app\modules\UserManagement\models\LoginHistory;
use app\modules\UserManagement\models\User;
use yii\console\Controller;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class RbacController extends Controller
{
    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     * @return int Exit code
     */
    public function actionInit()
    {
        /*$user = new User();
        $user->username = 'user';
        $user->setPassword('password');
        $user->save();*/

        $auth = \Yii::$app->authManager;
        $auth->removeAll();

        //permissions
        $userView = $auth->createPermission('userView');
        $userView->description = Yii::t('app', 'Просмотр информации о пользователях');
        $auth->add($userView);

        $userEdit = $auth->createPermission('userEdit');
        $userEdit->description = Yii::t('app', 'Редактирование данных пользователей');
        $auth->add($userEdit);

        $roleView = $auth->createPermission('roleView');
        $roleView->description = Yii::t('app', 'Просмотр информации о ролях');
        $auth->add($roleView);

        $roleEdit = $auth->createPermission('roleEdit');
        $roleEdit->description = Yii::t('app', 'Редактирование ролей');
        $auth->add($roleEdit);

        $appKeyView = $auth->createPermission('appKeyView');
        $appKeyView->description = Yii::t('app', 'Просмотр ключей app key');
        $auth->add($appKeyView);

        $appKeyEdit = $auth->createPermission('appKeyEdit');
        $appKeyEdit->description = Yii::t('app', 'Редактирование ключей app key');
        $auth->add($appKeyEdit);

        $auditLogView = $auth->createPermission('auditLogView');
        $auditLogView->description = Yii::t('app', 'Просмотр журнала аудита');
        $auth->add($auditLogView);

        $bankView = $auth->createPermission('bankView');
        $bankView->description = Yii::t('app', 'Просмотр справочника банков');
        $auth->add($bankView);

        $bankEdit = $auth->createPermission('bankEdit');
        $bankEdit->description = Yii::t('app', 'Редактирование справочника банков');
        $auth->add($bankEdit);

        $priorMposRequestView = $auth->createPermission('priorMposRequestView');
        $priorMposRequestView->description = Yii::t('app', 'Просмотр заявок на подключение mPOS Приорбанк');
        $auth->add($priorMposRequestView);

        $priorMposRequestEdit = $auth->createPermission('priorMposRequestEdit');
        $priorMposRequestEdit->description = Yii::t('app', 'Смена статуса заявок на подключение mPOS Приорбанк');
        $auth->add($priorMposRequestEdit);

        $clientErrorView = $auth->createPermission('clientErrorView');
        $clientErrorView->description = Yii::t('app', 'Просмотр справочника ошибок');
        $auth->add($clientErrorView);

        $clientErrorEdit = $auth->createPermission('clientErrorEdit');
        $clientErrorEdit->description = Yii::t('app', 'Редактирование справочника ошибок');
        $auth->add($clientErrorEdit);

        $fitView = $auth->createPermission('fitView');
        $fitView->description = Yii::t('app', 'Просмотр справочника FIT');
        $auth->add($fitView);

        $fitEdit = $auth->createPermission('fitEdit');
        $fitEdit->description = Yii::t('app', 'Редактирование справочника FIT');
        $auth->add($fitEdit);

        $fitGroupView = $auth->createPermission('fitGroupView');
        $fitGroupView->description = Yii::t('app', 'Просмотр справочника групп FIT');
        $auth->add($fitGroupView);

        $fitGroupEdit = $auth->createPermission('fitGroupEdit');
        $fitGroupEdit->description = Yii::t('app', 'Редактирование справочника групп FIT');
        $auth->add($fitGroupEdit);

        $fitGroupTypeView = $auth->createPermission('fitGroupTypeView');
        $fitGroupTypeView->description = Yii::t('app', 'Просмотр справочника типов групп FIT');
        $auth->add($fitGroupTypeView);

        $fitGroupTypeEdit = $auth->createPermission('fitGroupTypeEdit');
        $fitGroupTypeEdit->description = Yii::t('app', 'Редактирование справочника типов групп FIT');
        $auth->add($fitGroupTypeEdit);

        $hostErrorCodeView = $auth->createPermission('hostErrorCodeView');
        $hostErrorCodeView->description = Yii::t('app', 'Просмотр справочника Response Code\'ов');
        $auth->add($hostErrorCodeView);

        $ipsTypeView = $auth->createPermission('ipsTypeView');
        $ipsTypeView->description = Yii::t('app', 'Просмотр справочника карт МПС');
        $auth->add($ipsTypeView);

        $ipsTypeEdit = $auth->createPermission('ipsTypeEdit');
        $ipsTypeEdit->description = Yii::t('app', 'Редактирование справочника карт МПС');
        $auth->add($ipsTypeEdit);

        $isoSettingView = $auth->createPermission('isoSettingView');
        $isoSettingView->description = Yii::t('app', 'Просмотр справочника настроек ISO');
        $auth->add($isoSettingView);

        $isoSettingEdit = $auth->createPermission('isoSettingEdit');
        $isoSettingEdit->description = Yii::t('app', 'Редактирование справочника настроек ISO');
        $auth->add($isoSettingEdit);

        $isoSettingPaymentEdit = $auth->createPermission('isoSettingPaymentEdit');
        $isoSettingPaymentEdit->description = Yii::t('app', 'Редактирование платежей настройки ISO');
        $auth->add($isoSettingPaymentEdit);

        $kazpostTxpRequestView = $auth->createPermission('kazpostTxpRequestView');
        $kazpostTxpRequestView->description = Yii::t('app', 'Просмотр заявок на подключение tapXphone Казпочта');
        $auth->add($kazpostTxpRequestView);

        $kazpostTxpRequestEdit = $auth->createPermission('kazpostTxpRequestEdit');
        $kazpostTxpRequestEdit->description = Yii::t('app', 'Смена статуса заявок на подключение tapXphone Казпочта');
        $auth->add($kazpostTxpRequestEdit);

        $mccView = $auth->createPermission('mccView');
        $mccView->description = Yii::t('app', 'Просмотр справочника MCC кодов');
        $auth->add($mccView);

        $mccEdit = $auth->createPermission('mccEdit');
        $mccEdit->description = Yii::t('app', 'Редактирование справочника MCC кодов');
        $auth->add($mccEdit);

        $cashierEdit = $auth->createPermission('cashierEdit');
        $cashierEdit->description = Yii::t('app', 'Редактирование кассиров Торговца');
        $auth->add($cashierEdit);

        $merchantLocalityView = $auth->createPermission('merchantLocalityView');
        $merchantLocalityView->description = Yii::t('app', 'Просмотр справочника населенных пунктов и MCC');
        $auth->add($merchantLocalityView);

        $merchantLocalityEdit = $auth->createPermission('merchantLocalityEdit');
        $merchantLocalityEdit->description = Yii::t('app', 'Редактирование справочника населенных пунктов и MCC');
        $auth->add($merchantLocalityEdit);

        $merchantView = $auth->createPermission('merchantView');
        $merchantView->description = Yii::t('app', 'Просмотр справочника Торговцев');
        $auth->add($merchantView);

        $merchantEdit = $auth->createPermission('merchantEdit');
        $merchantEdit->description = Yii::t('app', 'Редактирование справочника Торговцев');
        $auth->add($merchantEdit);

        $merchantPedAssign = $auth->createPermission('merchantPedAssign');
        $merchantPedAssign->description = Yii::t('app', 'Привязка устройства к Торговцу');
        $auth->add($merchantPedAssign);

        $merchantMessage = $auth->createPermission('merchantMessage');
        $merchantMessage->description = Yii::t('app', 'Отправка сообщений Торговцу');
        $auth->add($merchantMessage);

        $merchantStatusView = $auth->createPermission('merchantStatusView');
        $merchantStatusView->description = Yii::t('app', 'Просмотр справочника статусов Торговцев');
        $auth->add($merchantStatusView);

        $merchantStatusEdit = $auth->createPermission('merchantStatusEdit');
        $merchantStatusEdit->description = Yii::t('app', 'Редактирование справочника статусов Торговцев');
        $auth->add($merchantStatusEdit);

        $pedManufacturerView = $auth->createPermission('pedManufacturerView');
        $pedManufacturerView->description = Yii::t('app', 'Просмотр справочника производителей');
        $auth->add($pedManufacturerView);

        $pedManufacturerEdit = $auth->createPermission('pedManufacturerEdit');
        $pedManufacturerEdit->description = Yii::t('app', 'Редактирование справочника производителей');
        $auth->add($pedManufacturerEdit);

        $pedModelView = $auth->createPermission('pedModelView');
        $pedModelView->description = Yii::t('app', 'Просмотр справочника моделей');
        $auth->add($pedModelView);

        $pedModelEdit = $auth->createPermission('pedModelEdit');
        $pedModelEdit->description = Yii::t('app', 'Редактирование справочника моделей');
        $auth->add($pedModelEdit);

        $pedArchiveView = $auth->createPermission('pedArchiveView');
        $pedArchiveView->description = Yii::t('app', 'Просмотр архива устройств');
        $auth->add($pedArchiveView);

        $pedView = $auth->createPermission('pedView');
        $pedView->description = Yii::t('app', 'Просмотр справочника устройств');
        $auth->add($pedView);

        $pedEdit = $auth->createPermission('pedEdit');
        $pedEdit->description = Yii::t('app', 'Редактирование справочника устройств');
        $auth->add($pedEdit);

        $pedPreorderView = $auth->createPermission('pedPreorderView');
        $pedPreorderView->description = Yii::t('app', 'Просмотр предзаказов на устройства');
        $auth->add($pedPreorderView);

        $pedPreorderHandle = $auth->createPermission('pedPreorderHandle');
        $pedPreorderHandle->description = Yii::t('app', 'Обработка предзаказов на устройства');
        $auth->add($pedPreorderHandle);

        $pedStatusView = $auth->createPermission('pedStatusView');
        $pedStatusView->description = Yii::t('app', 'Просмотр справочника статусов устройств');
        $auth->add($pedStatusView);

        $pedStatusEdit = $auth->createPermission('pedStatusEdit');
        $pedStatusEdit->description = Yii::t('app', 'Редактирование справочника статусов устройств');
        $auth->add($pedStatusEdit);

        $transactionView = $auth->createPermission('transactionView');
        $transactionView->description = Yii::t('app', 'Просмотр журнала транзакций');
        $auth->add($transactionView);

        $newsView = $auth->createPermission('newsView');
        $newsView->description = Yii::t('app', 'Просмотр новостей сайта');
        $auth->add($newsView);

        $newsEdit = $auth->createPermission('newsEdit');
        $newsEdit->description = Yii::t('app', 'Редактирование новостей сайта');
        $auth->add($newsEdit);

        $paymentView = $auth->createPermission('paymentView');
        $paymentView->description = Yii::t('app', 'Просмотр справочника платежей');
        $auth->add($paymentView);

        $paymentTerminalView = $auth->createPermission('paymentTerminalView');
        $paymentTerminalView->description = Yii::t('app', 'Просмотр справочника платежных терминалов');
        $auth->add($paymentTerminalView);

        $paymentTerminalEdit = $auth->createPermission('paymentTerminalEdit');
        $paymentTerminalEdit->description = Yii::t('app', 'Редактирование справочника платежных терминалов');
        $auth->add($paymentTerminalEdit);

        $paymentTerminalCurrentBatchView = $auth->createPermission('paymentTerminalCurrentBatchView');
        $paymentTerminalCurrentBatchView->description = Yii::t('app', 'Просмотр незакрытых бизнес-дней');
        $auth->add($paymentTerminalCurrentBatchView);

        $paymentTerminalCurrentBatchClose = $auth->createPermission('paymentTerminalCurrentBatchClose');
        $paymentTerminalCurrentBatchClose->description = Yii::t('app', 'Закрытие бизнес-дней');
        $auth->add($paymentTerminalCurrentBatchClose);

        $paymentTerminalSettlementLogView = $auth->createPermission('paymentTerminalSettlementLogView');
        $paymentTerminalSettlementLogView->description = Yii::t('app', 'Просмотр истории закрытия бизнес-дней');
        $auth->add($paymentTerminalSettlementLogView);

        $pedPreorderStatusView = $auth->createPermission('pedPreorderStatusView');
        $pedPreorderStatusView->description = Yii::t('app', 'Просмотр справочника статусов предзаказов');
        $auth->add($pedPreorderStatusView);

        $pedPreorderStatusEdit = $auth->createPermission('pedPreorderStatusEdit');
        $pedPreorderStatusEdit->description = Yii::t('app', 'Редактирование справочника статусов предзаказов');
        $auth->add($pedPreorderStatusEdit);

        $priorTxpRequestView = $auth->createPermission('priorTxpRequestView');
        $priorTxpRequestView->description = Yii::t('app', 'Просмотр заявок на подключение tapXphone Приорбанк');
        $auth->add($priorTxpRequestView);

        $priorTxpRequestEdit = $auth->createPermission('priorTxpRequestEdit');
        $priorTxpRequestEdit->description = Yii::t('app', 'Смена статуса заявок на подключение tapXphone Приорбанк');
        $auth->add($priorTxpRequestEdit);

        $reviewView = $auth->createPermission('reviewView');
        $reviewView->description = Yii::t('app', 'Просмотр полученных и опубликованных отзывов');
        $auth->add($reviewView);

        $reviewEdit = $auth->createPermission('reviewEdit');
        $reviewEdit->description = Yii::t('app', 'Публикация полученных и редактирование опубликованных отзывов');
        $auth->add($reviewEdit);

        $runMonthSumReport = $auth->createPermission('runMonthSumReport');
        $runMonthSumReport->description = Yii::t('app', 'Генерация отчета по итоговой сумме за месяц');
        $auth->add($runMonthSumReport);

        $runTransactionIpsReport = $auth->createPermission('runTransactionIpsReport');
        $runTransactionIpsReport->description = Yii::t('app', 'Генерация отчета по транзакциям в разрезе МПС');
        $auth->add($runTransactionIpsReport);

        $runTransactionCountAcquirerReport = $auth->createPermission('runTransactionCountAcquirerReport');
        $runTransactionCountAcquirerReport->description = Yii::t('app', 'Генерация отчета по количеству транзакций');
        $auth->add($runTransactionCountAcquirerReport);

        $runTransactionDetailedReportRbi = $auth->createPermission('runTransactionDetailedReportRbi');
        $runTransactionDetailedReportRbi->description = Yii::t('app', 'Генерация детального отчета по транзакциям');
        $auth->add($runTransactionDetailedReportRbi);

        $messageMerchant = $auth->createPermission('messageMerchant');
        $messageMerchant->description = Yii::t('app', 'Рассылка сообщений всем Торговцам');
        $auth->add($messageMerchant);

        $messageUser = $auth->createPermission('messageUser');
        $messageUser->description = Yii::t('app', 'Рассылка сообщений всем пользователям АРМ');
        $auth->add($messageUser);

        $messageSms = $auth->createPermission('messageSms');
        $messageSms->description = Yii::t('app', 'Отправка SMS на произвольный номер');
        $auth->add($messageSms);

        $transactionslogView = $auth->createPermission('transactionslogView');
        $transactionslogView->description = Yii::t('app', 'Просмотр Transactionslog');
        $auth->add($transactionslogView);

        $transactionslogArchiveView = $auth->createPermission('transactionslogArchiveView');
        $transactionslogArchiveView->description = Yii::t('app', 'Просмотр TransactionslogArchive');
        $auth->add($transactionslogArchiveView);

        $txpApiUserView = $auth->createPermission('txpApiUserView');
        $txpApiUserView->description = Yii::t('app', 'Просмотр информации о пользователях API');
        $auth->add($txpApiUserView);

        $txpApiUserEdit = $auth->createPermission('txpApiUserEdit');
        $txpApiUserEdit->description = Yii::t('app', 'Редактирование данных пользователей API');
        $auth->add($txpApiUserEdit);


        //add "admin" role and give this role all permissions
        $admin = $auth->createRole('admin');
        $admin->description = Yii::t('app', 'Администратор');
        $auth->add($admin);

        $auth->addChild($admin, $userView);
        $auth->addChild($admin, $userEdit);
        $auth->addChild($admin, $roleView);
        $auth->addChild($admin, $roleEdit);
        $auth->addChild($admin, $appKeyView);
        $auth->addChild($admin, $appKeyEdit);
        $auth->addChild($admin, $auditLogView);
        $auth->addChild($admin, $bankView);
        $auth->addChild($admin, $bankEdit);
        $auth->addChild($admin, $priorMposRequestView);
        $auth->addChild($admin, $priorMposRequestEdit);
        $auth->addChild($admin, $clientErrorView);
        $auth->addChild($admin, $clientErrorEdit);
        $auth->addChild($admin, $fitView);
        $auth->addChild($admin, $fitEdit);
        $auth->addChild($admin, $fitGroupView);
        $auth->addChild($admin, $fitGroupEdit);
        $auth->addChild($admin, $fitGroupTypeView);
        $auth->addChild($admin, $fitGroupTypeEdit);
        $auth->addChild($admin, $hostErrorCodeView);
        $auth->addChild($admin, $ipsTypeView);
        $auth->addChild($admin, $ipsTypeEdit);
        $auth->addChild($admin, $isoSettingView);
        $auth->addChild($admin, $isoSettingEdit);
        $auth->addChild($admin, $isoSettingPaymentEdit);
        $auth->addChild($admin, $kazpostTxpRequestView);
        $auth->addChild($admin, $kazpostTxpRequestEdit);
        $auth->addChild($admin, $mccView);
        $auth->addChild($admin, $mccEdit);
        $auth->addChild($admin, $cashierEdit);
        $auth->addChild($admin, $merchantLocalityView);
        $auth->addChild($admin, $merchantLocalityEdit);
        $auth->addChild($admin, $merchantView);
        $auth->addChild($admin, $merchantEdit);
        $auth->addChild($admin, $merchantPedAssign);
        $auth->addChild($admin, $merchantMessage);
        $auth->addChild($admin, $merchantStatusView);
        $auth->addChild($admin, $merchantStatusEdit);
        $auth->addChild($admin, $pedManufacturerView);
        $auth->addChild($admin, $pedManufacturerEdit);
        $auth->addChild($admin, $pedModelView);
        $auth->addChild($admin, $pedModelEdit);
        $auth->addChild($admin, $pedArchiveView);
        $auth->addChild($admin, $pedView);
        $auth->addChild($admin, $pedEdit);
        $auth->addChild($admin, $pedPreorderView);
        $auth->addChild($admin, $pedPreorderHandle);
        $auth->addChild($admin, $pedStatusView);
        $auth->addChild($admin, $pedStatusEdit);
        $auth->addChild($admin, $transactionView);
        $auth->addChild($admin, $newsView);
        $auth->addChild($admin, $newsEdit);
        $auth->addChild($admin, $paymentView);
        $auth->addChild($admin, $paymentTerminalView);
        $auth->addChild($admin, $paymentTerminalEdit);
        $auth->addChild($admin, $paymentTerminalCurrentBatchView);
        $auth->addChild($admin, $paymentTerminalCurrentBatchClose);
        $auth->addChild($admin, $paymentTerminalSettlementLogView);
        $auth->addChild($admin, $pedPreorderStatusView);
        $auth->addChild($admin, $pedPreorderStatusEdit);
        $auth->addChild($admin, $priorTxpRequestView);
        $auth->addChild($admin, $priorTxpRequestEdit);
        $auth->addChild($admin, $reviewView);
        $auth->addChild($admin, $reviewEdit);
        $auth->addChild($admin, $runMonthSumReport);
        $auth->addChild($admin, $runTransactionIpsReport);
        $auth->addChild($admin, $runTransactionCountAcquirerReport);
        $auth->addChild($admin, $runTransactionDetailedReportRbi);
        $auth->addChild($admin, $messageMerchant);
        $auth->addChild($admin, $messageUser);
        $auth->addChild($admin, $messageSms);
        $auth->addChild($admin, $transactionslogView);
        $auth->addChild($admin, $transactionslogArchiveView);
        $auth->addChild($admin, $txpApiUserView);
        $auth->addChild($admin, $txpApiUserEdit);

        $auth->assign($admin, 1);
    }

    public function actionLockInactiveUsers()
    {
        $inactiveDaysPeriodBeforeLock = (int)\Yii::$app->getModule('user-management')->params['inactiveDaysPeriodBeforeLock'];
        if ($inactiveDaysPeriodBeforeLock > 0) {
            $notLockedUsers = User::find()->where(['status' => 1])->all();
            $lockDate = date('Y-m-d', strtotime('-' . $inactiveDaysPeriodBeforeLock . ' days'));
            foreach ($notLockedUsers as $user) {
                $lastSuccessfulLogin = LoginHistory::find()
                    ->where(['status' => 1, 'username' => $user->username])
                    ->orderBy(['date' => SORT_DESC])
                    ->limit(1)
                    ->one();
                if ($lastSuccessfulLogin === null) {//не было успешных авторизаций - смотрим дату создания
                    if ($user->created_at < $lockDate) {
                        $user->status = 0;
                        $user->save(false, ['status']);
                    }
                } elseif ($lastSuccessfulLogin->date < $lockDate) {
                    $user->status = 0;
                    $user->save(false, ['status']);
                }
            }
        }
    }
}
