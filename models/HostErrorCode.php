<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "host_error_code".
 *
 * @property integer $id
 * @property integer $iso_settings_id
 * @property string $descr
 *
 * @property IsoSetting $isoSettings
 */
class HostErrorCode extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'host_error_code';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('mpos');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'iso_settings_id'], 'required'],
            [['id', 'iso_settings_id'], 'integer'],
            [['descr'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'iso_settings_id' => \Yii::t('app', 'ISO настройка'),
            'descr' => \Yii::t('app', 'Описание'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIsoSettings()
    {
        return $this->hasOne(IsoSettings::className(), ['id' => 'iso_settings_id']);
    }
}
