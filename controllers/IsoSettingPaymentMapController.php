<?php

namespace app\controllers;

use app\models\IsoSettings;
use Yii;
use app\models\IsoSettingPaymentMap;
use app\models\IsoSettingPaymentMapSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * IsoSettingPaymentMapController implements the CRUD actions for IsoSettingPaymentMap model.
 */
class IsoSettingPaymentMapController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['create', 'update', 'delete'],
                        'roles' => ['isoSettingPaymentEdit'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Creates a new IsoSettingPaymentMap model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($iso_setting_id)
    {
        $isoSetting = IsoSettings::findOne($iso_setting_id);
        if ($isoSetting === null) {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }


        $model = new IsoSettingPaymentMap();
        $model->iso_setting_id = $iso_setting_id;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['iso-settings/view', 'id' => $iso_setting_id]);
        }

        return $this->render('create', [
            'isoSetting' => $isoSetting,
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing IsoSettingPaymentMap model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $iso_setting_id
     * @param integer $payment_id
     * @param integer $currency
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($iso_setting_id, $payment_id, $currency)
    {
        $isoSetting = IsoSettings::findOne($iso_setting_id);
        if ($isoSetting === null) {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }


        $model = $this->findModel($iso_setting_id, $payment_id, $currency);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['iso-settings/view', 'id' => $iso_setting_id]);
        }

        return $this->render('update', [
            'isoSetting' => $isoSetting,
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing IsoSettingPaymentMap model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $iso_setting_id
     * @param integer $payment_id
     * @param integer $currency
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($iso_setting_id, $payment_id, $currency)
    {
        $this->findModel($iso_setting_id, $payment_id, $currency)->delete();

        return $this->redirect(['iso-settings/view', 'id' => $iso_setting_id]);
    }

    /**
     * Finds the IsoSettingPaymentMap model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $iso_setting_id
     * @param integer $payment_id
     * @param integer $currency
     * @return IsoSettingPaymentMap the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($iso_setting_id, $payment_id, $currency)
    {
        if (($model = IsoSettingPaymentMap::findOne(['iso_setting_id' => $iso_setting_id, 'payment_id' => $payment_id, 'currency' => $currency])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
