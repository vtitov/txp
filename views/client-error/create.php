<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ClientError */

$this->title = \Yii::t('app', 'Добавление ошибки');
$this->params['breadcrumbs'][] = ['label' => \Yii::t('app', 'Справочник ошибок'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="client-error-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
