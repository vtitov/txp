<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\MposMerchants */

$this->title = \Yii::t('app', 'Добавить Торговца');
$this->params['breadcrumbs'][] = ['label' => \Yii::t('app', 'Справочник Торговцев'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mpos-merchants-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
