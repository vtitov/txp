<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $isoSetting app\models\IsoSettings */
/* @var $model app\models\IsoSettingPaymentMap */

$this->title = Yii::t('app', 'Добавление платежа');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Справочник ISO настроек'), 'url' => ['iso-settings/index']];
$this->params['breadcrumbs'][] = ['label' => $isoSetting->name, 'url' => ['iso-settings/view', 'id' => $isoSetting->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="iso-setting-payment-map-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
