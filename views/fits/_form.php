<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Fits */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="fits-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'fit')->textInput(['maxlength' => 19]) ?>

    <?= $form->field($model, 'ips_types_id')->dropDownList(ArrayHelper::map(\app\models\IpsTypes::find()->asArray()->all(), 'id', 'name')) ?>

    <div class="form-group">
        <?= Html::submitButton(\Yii::t('app', 'Сохранить'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
