<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AuditLogSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="audit-log-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'mpos_id') ?>

    <?= $form->field($model, 'audit_event_id') ?>

    <?= $form->field($model, 'audit_event_details') ?>

    <?= $form->field($model, 'rq_time') ?>

    <?php // echo $form->field($model, 'rs_time') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
