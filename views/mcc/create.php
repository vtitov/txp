<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Mcc */

$this->title = \Yii::t('app', 'Добавление MCC');
$this->params['breadcrumbs'][] = ['label' => \Yii::t('app', 'Справочник МСС-кодов для сайта payBYcard'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mcc-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
