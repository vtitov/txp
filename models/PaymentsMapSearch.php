<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\PaymentsMap;

/**
 * PaymentsMapSearch represents the model behind the search form about `app\models\PaymentsMap`.
 */
class PaymentsMapSearch extends PaymentsMap
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ped_id', 'payments_id', 'currency_id', 'fits_groups_id', 'fits_groups_id_sign_allowed', 'fits_groups_id_fallback_allowed'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $ped_id)
    {
        $query = PaymentsMap::find()
            ->with(['payments', 'currency', 'fitsGroups', 'fitsGroupsSign', 'fitsGroupsFallback'])
            ->andFilterWhere(['ped_id' => $ped_id])
            ->orderBy('payments_id, currency_id');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => false
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            //'ped_id' => $this->ped_id,
            'payments_id' => $this->payments_id,
            'currency_id' => $this->currency_id,
            'fits_groups_id' => $this->fits_groups_id,
            'fits_groups_id_sign_allowed' => $this->fits_groups_id_sign_allowed,
            'fits_groups_id_fallback_allowed' => $this->fits_groups_id_fallback_allowed,
        ]);

        return $dataProvider;
    }
}
