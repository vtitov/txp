<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\MerchantLocality */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Населенные пункты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="merchant-locality-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы действительно хотите удалить эту запись?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            'name',
            'lat',
            'lng',
        ],
    ]) ?>

    <h3>Категории торговцев</h3>

    <?= Html::a('Добавить MCC', ['add-mcc', 'merchant_locality_id' => $model->id], ['class' => 'btn btn-success']) ?>

    <?= \yii\grid\GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'mcc',
                'label' => 'MCC',
            ],
            [
                'attribute' => 'description',
                'label' => 'Описание',
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'buttons' => [
                    'delete' => function($url, $model){
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>',
                            \yii\helpers\Url::to(['delete-mcc', 'merchant_locality_id' => $model['merchant_locality_id'], 'mcc' => $model['mcc']]),
                            ['title' => 'Удалить', 'data' => ['method' => 'post']]);
                    },
                ],
                'template' => '{delete}',
            ],
        ],
    ]); ?>

</div>
