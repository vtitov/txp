<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\PedPreorderPriorbankResponse */

$this->title = "Заявка по предзаказу " . $model->preorder_id . "~" . $model->file_number;
$this->params['breadcrumbs'][] = ['label' => 'Заявки на подключение mPOS Приорбанк', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ped-preorder-priorbank-response-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?php foreach (\app\models\PedPreorderPriorbankResponse::$_STATUS as $key => $value): ?>
            <?= Html::a($value, ['change-status', 'id' => $model->id, 'status' => $key], ['class' => $model->status == $key ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?php endforeach; ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            'preorder_id',
            'file_number',
            'name_legal_entity',
            'unp',
            'email:email',
            'pos_info',
            'contract',
            'contract_date',
            'trading_name',
            'phone_trading_name',
            'mcc',
            'country',
            'region',
            'locality',
            'address',
            'terminal_id',
            'merchant_id',
            'parse_date',
            [
                'attribute' => 'status',
                'value' => \yii\helpers\ArrayHelper::getValue(
                    \app\models\PedPreorderPriorbankResponse::$_STATUS,
                    $model->status,
                    $model->status
                ),
            ],
        ],
    ]) ?>

</div>
