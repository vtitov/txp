<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PaymentsTerminals */

$this->title = \Yii::t('app', 'Добавление платежного терминала');
$this->params['breadcrumbs'][] = ['label' => \Yii::t('app', 'Справочник платежных терминалов'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payments-terminals-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
