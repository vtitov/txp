<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ips_types".
 *
 * @property string $id
 * @property string $name
 * @property string $description
 *
 * @property Fits[] $fits
 */
class IpsTypes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ip_type';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('mpos');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 20],
            [['description'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => \Yii::t('app', 'Название'),
            'description' => \Yii::t('app', 'Описание'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFits()
    {
        return $this->hasMany(Fits::className(), ['ips_types_id' => 'id']);
    }
}
