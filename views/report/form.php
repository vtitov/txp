<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model \yii\base\DynamicModel */
/* @var $form yii\widgets\ActiveForm */

$this->title = 'Итоговая сумма за месяц'
?>

<div class="report-form">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'bank')->dropDownList([
        '0' => 'Беларусбанк (mPOS)',
        '1' => 'Приорбанк (mPOS)',
        '2' => 'Приорбанк (SoftPOS)'
    ])->label('Банк') ?>

    <?= $form->field($model, 'year')->textInput(['maxlength' => 4])->label('Год') ?>

    <?= $form->field($model, 'month')->dropDownList([
        '01' => 'Январь',
        '02' => 'Февраль',
        '03' => 'Март',
        '04' => 'Апрель',
        '05' => 'Май',
        '06' => 'Июнь',
        '07' => 'Июль',
        '08' => 'Август',
        '09' => 'Сентябрь',
        '10' => 'Октябрь',
        '11' => 'Ноябрь',
        '12' => 'Декабрь',
    ])->label('Месяц') ?>

    <div class="form-group">
        <?= Html::submitButton('Получить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
