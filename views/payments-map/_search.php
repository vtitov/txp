<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PaymentsMapSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="payments-map-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'ped_id') ?>

    <?= $form->field($model, 'payments_id') ?>

    <?= $form->field($model, 'currency_id') ?>

    <?= $form->field($model, 'payments_terminals_id') ?>

    <?= $form->field($model, 'fits_groups_id') ?>

    <?php // echo $form->field($model, 'fits_groups_id_sign_allowed') ?>

    <?php // echo $form->field($model, 'fits_groups_id_fallback_allowed') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
