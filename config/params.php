<?php

return [
    'adminEmail' => 'info@paybycard.by',
    'adminName' => 'info@paybycard.by',

    'notificationUrl' => 'http://notification.tapxphone.com/notificator/',
    'generateAppKeyUrl' => 'http://pspinternal.tapxphone.com/tapx_gen_app_key.do',

    'xmlParseErrorEmailNotification' => ['aklimovich@iba.by', 'YauheniMelnikau@ibagroup.eu'],

    'priorDefaultIsoSettingId' => 25,
    'serverFtpForPriorbank' => '172.20.0.0',
    'userFtpForPriorbank' => 'iba',
    'passwordFtpForPriorbank' => 'pass',

    'kazpostDefaultIsoSettingId' => 26,
    'serverFtpForKazpost' => '172.20.0.0',
    'userFtpForKazpost' => 'iba',
    'passwordFtpForKazpost' => 'pass',

    'availableTranslations' => ['en', 'ru'],


    'ownerCashierLoginFieldMap' => [
        '6' => 'phone',
    ],
];
