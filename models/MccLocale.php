<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mcc_locale".
 *
 * @property string $code
 * @property string $locale
 * @property string $description
 */
class MccLocale extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mcc_locale';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('mpos');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['code', 'locale', 'description'], 'required'],
            [['code'], 'match', 'pattern' => '/^(0|\d{4})$/', 'message' => Yii::t('app', 'MCC должен состоять из 4-х цифр')],
            [['locale'], 'string', 'min' => 2, 'max' => 2],
            [['description'], 'string', 'max' => 255],
            [['code', 'locale'], 'unique', 'targetAttribute' => ['code', 'locale']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'code' => Yii::t('app', 'Код'),
            'locale' => Yii::t('app', 'Язык'),
            'description' => Yii::t('app', 'Описание'),
        ];
    }
}
