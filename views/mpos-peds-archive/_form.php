<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MposPedsArchive */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mpos-peds-archive-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'ped_model_id')->textInput() ?>

    <?= $form->field($model, 'serial_number')->textInput(['maxlength' => 20]) ?>

    <?= $form->field($model, 'merchant_id')->textInput(['maxlength' => 10]) ?>

    <?= $form->field($model, 'ped_status_id')->textInput() ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'add_date')->textInput() ?>

    <?= $form->field($model, 'delete_date')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
