<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "fits_groups".
 *
 * @property string $id
 * @property string $name
 * @property string $fits_groups_types_id
 * @property string $description
 *
 * @property FitsGroupsTypes $fitsGroupsTypes
 * @property FitsMap[] $fitsMaps
 * @property Fits[] $fits
 */
class FitsGroups extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'fit_group';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('mpos');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            ['name', 'unique'],
            [['fits_groups_types_id'], 'integer'],
            [['name', 'description'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => \Yii::t('app', 'Название'),
            'fits_groups_types_id' => \Yii::t('app', 'Тип'),
            'description' => \Yii::t('app', 'Описание'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFitsGroupsTypes()
    {
        return $this->hasOne(FitsGroupsTypes::className(), ['id' => 'fits_groups_types_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFitsMaps()
    {
        return $this->hasMany(FitsMap::className(), ['fits_groups_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFits()
    {
        return $this->hasMany(Fits::className(), ['id' => 'fits_id'])->viaTable('fit_map', ['fits_groups_id' => 'id']);
    }
}
