<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\IsoSettings */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="iso-settings-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'host')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'port')->textInput(['maxlength' => 10]) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'protocol_class')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'zpk')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'zak')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'zmk_tap_x')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sred_clear_period_days')->textInput() ?>

    <?= $form->field($model, 'owner_id')->dropDownList(
        \yii\helpers\ArrayHelper::map(
            \app\models\Owners::find()
                ->andWhere(['locale' => Yii::$app->language])
                ->all(),
            'id',
            'name'
        )
    ) ?>

    <?php
    $pedConfCategoryDropdown = [];
    foreach (\app\models\PedConfCategory::find()->all() as $category){
        $pedConfCategoryDropdown[$category->id] = $category->name;
        if($category->desc){
            $pedConfCategoryDropdown[$category->id] .= ' (' . $category->desc . ')';
        }
    }

    echo $form->field($model, 'ped_conf_category_id')->dropDownList($pedConfCategoryDropdown, ['prompt' => '']);

    echo $form->field($model, 'status')->dropDownList(\app\models\IsoSettings::getStatuses());
    ?>

    <div class="form-group">
        <?= Html::submitButton(\Yii::t('app', 'Сохранить'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
