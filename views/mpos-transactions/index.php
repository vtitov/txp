<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use app\helpers\RandomImageHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MposTransactionsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = \Yii::t('app', 'Журнал транзакций');
$this->params['breadcrumbs'][] = $this->title;
?>

<!--style>
    body {
        background: url(img/pimgpsh_fullsize_distr.jpg);
        background-size: 100%;
    }
</style-->

<div class="mpos-transactions-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <img src="<?= RandomImageHelper::getRandomImageFromDir('img/peds') ?>" height="140px" align="right"
         style="margin-top: -130px">

    <?php echo \yii\bootstrap\Collapse::widget([
        'items' => [
            [
                'label' => \Yii::t('app', 'Фильтр'),
                'content' => $this->render('_search', ['model' => $searchModel]),
            ],
        ]
    ]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //\app\helpers\DateColumnHelper::getColumn('date_time', $searchModel),
            [
                'attribute' => 'date_time',
                'label' => \Yii::t('app', 'Дата'),
            ],
            [
                'attribute' => 'mpos_ped_id',
                'value' => 'serial_number',
                'label' => \Yii::t('app', 'Устройство'),
            ],
            [
                'attribute' => 'mpos_merchant_id',
                'value' => function($model){
                    if ($model['merchant_name'] !== null) {
                        return Html::a(Html::encode($model['merchant_name']), ['mpos-merchants/view', 'id' => $model['mpos_merchant_id']]);
                    } else {
                        return null;
                    }
                },
                'label' => \Yii::t('app', 'Торговец'),
                'format' => 'html',
            ],
            [
                'attribute' => 'payment_id',
                'value' => 'payment_name',
                'label' => \Yii::t('app', 'Платеж'),
            ],
            'attribute' => 'authcode',
            [
                'attribute' => 'mpos_transaction_status_id',
                'value' => 'transaction_status',
                'label' => \Yii::t('app', 'Статус'),
            ],
            [
                'attribute' => 'amount',
                'label' => \Yii::t('app', 'Сумма'),
            ],
            [
                'attribute' => 'card',
                'label' => \Yii::t('app', 'Карта'),
            ],
            [
                'attribute' => 'transaction_type_id',
                'value' => 'transaction_type',
                'label' => \Yii::t('app', 'Тип'),
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}',
                'buttons' => [
                    'view' => function ($url, $model, $key) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', ['view', 'id' => $model['id']]);
                    },
                ],
            ],
        ],
    ]); ?>

</div>

<?php
//чтобы не сбрасывался фильтр при смене страницы/сортировки
$this->registerJs("
$('.pagination>li>a').click(function(e) {
    e.preventDefault();
    $('#filterForm').attr('action', this.href);
    $('#filterForm').submit();
});
$('table>thead>tr>th>a').click(function(e) {
    e.preventDefault();
    $('#filterForm').attr('action', this.href);
    $('#filterForm').submit();
});
");
?>