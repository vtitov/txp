<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\PaymentTerminalSettlementLog */

$this->title = ArrayHelper::getValue($model->paymentTerminal, 'name', $model->payment_terminal_id) . ', ' . $model->sett_date_time;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'История закрытия бизнес-дней'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="payment-terminal-settlement-log-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [

            [
                'attribute' => 'payment_terminal_id',
                'value' => $model->paymentTerminal ?
                    Html::a(Html::encode($model->paymentTerminal->name), ['payments-terminals/view', 'id' => $model->payment_terminal_id]) :
                    Html::encode($model->payment_terminal_id),
                'format' => 'html',
            ],
            'bdd',
            'bddn',
            'trx_counter',
            'sett_date_time',
            'sett_result',
            [
                'attribute' => 'batch_status_id',
                'value' => ArrayHelper::getValue($model->batchStatus, 'desc', $model->batch_status_id),
            ],
        ],
    ]) ?>

</div>
