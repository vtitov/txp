<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "change_bank_acquirer_priorbank_response".
 *
 * @property string $id
 * @property integer $request_id
 * @property integer $file_number
 * @property string $name_legal_entity
 * @property string $unp
 * @property string $email
 * @property string $pos_info
 * @property string $contract
 * @property string $contract_date
 * @property string $trading_name
 * @property string $phone_trading_name
 * @property string $mcc
 * @property string $country
 * @property string $region
 * @property string $locality
 * @property string $address
 * @property string $terminal_id
 * @property string $merchant_id
 * @property string $parse_date
 * @property integer $status
 */
class ChangeBankAcquirerPriorbankResponse extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'change_bank_acquirer_priorbank_response';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('mpos');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['request_id', 'file_number', 'name_legal_entity', 'unp', 'email', 'pos_info', 'contract', 'contract_date', 'trading_name', 'phone_trading_name', 'mcc', 'country', 'region', 'locality', 'address', 'terminal_id', 'merchant_id'], 'required'],
            [['request_id', 'file_number', 'phone_trading_name', 'status'], 'integer'],
            [['contract_date', 'parse_date'], 'safe'],
            [['name_legal_entity', 'email', 'pos_info', 'trading_name', 'country', 'region', 'locality', 'address', 'merchant_id'], 'string', 'max' => 255],
            [['contract'], 'string', 'max' => 50],
            [['mcc'], 'string', 'max' => 4],
            [['unp'], 'string', 'min' => 9, 'max' => 9],
            [['terminal_id'], 'string', 'max' => 8],
            [['request_id', 'file_number'], 'unique', 'targetAttribute' => ['request_id', 'file_number'], 'message' => 'The combination of Request ID and File Number has already been taken.'],
            ['status', 'in', 'range' => [0, 1]],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'request_id' => 'ID заявки',
            'file_number' => '№ файла',
            'name_legal_entity' => 'Юридическое наименование организации',
            'unp' => 'УНП',
            'email' => 'Email',
            'pos_info' => 'Описание',
            'contract' => '№ договора',
            'contract_date' => 'Договор заключен',
            'trading_name' => 'Название организации',
            'phone_trading_name' => 'Телефон',
            'mcc' => 'MCC-код',
            'country' => 'Страна',
            'region' => 'Район',
            'locality' => 'Населенный пункт',
            'address' => 'Адрес',
            'terminal_id' => 'Платежный терминал',
            'merchant_id' => 'Идентификатор торговца',
            'parse_date' => 'Дата вчитывания файла',
            'status' => 'Статус',
        ];
    }

    public function getChangeBankAcquirerRequest()
    {
        return $this->hasOne(ChangeBankAcquirerRequest::className(), ['id' => 'request_id']);
    }
}
