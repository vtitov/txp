<?php

namespace app\controllers;

use app\models\HostErrorCode;
use app\models\HostErrorCodeSearch;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * HostErrorCodeController implements the CRUD actions for HostErrorCode model.
 */
class HostErrorCodeController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view'],
                        'roles' => ['hostErrorCodeView'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all HostErrorCode models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new HostErrorCodeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single HostErrorCode model.
     * @param integer $id
     * @param integer $iso_settings_id
     * @return mixed
     */
    public function actionView($id, $iso_settings_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id, $iso_settings_id),
        ]);
    }

    /**
     * Finds the HostErrorCode model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @param integer $iso_settings_id
     * @return HostErrorCode the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id, $iso_settings_id)
    {
        if (($model = HostErrorCode::findOne(['id' => $id, 'iso_settings_id' => $iso_settings_id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
