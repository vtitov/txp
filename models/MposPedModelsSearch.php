<?php

namespace app\models;

use app\helpers\DateColumnHelper;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\MposPedModels;

/**
 * MposPedModelsSearch represents the model behind the search form about `app\models\MposPedModels`.
 */
class MposPedModelsSearch extends MposPedModels
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['name', 'ped_vendor_id', 'description', 'pts_appr_number', 'pts_exp_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MposPedModels::find()
            ->with('mposPedManufacturer')
			->leftJoin('ped_vendor', 'ped_model.ped_vendor_id = ped_vendor.id');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['ped_vendor_id'] = [
            'asc' => [MposPedManufacturers::tableName() . '.name' => SORT_ASC],
            'desc' => [MposPedManufacturers::tableName() . '.name' => SORT_DESC]
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            //'mpos_ped_manufacturer_id' => $this->mpos_ped_manufacturer_id,
        ]);

        $query->andFilterWhere(['like', 'ped_model.name', $this->name])
            ->andFilterWhere(['like', 'ped_model.description', $this->description])
			->andFilterWhere(['like', 'ped_vendor.name', $this->ped_vendor_id])
            ->andFilterWhere(['like', 'pts_appr_number', $this->pts_appr_number]);

        DateColumnHelper::addFilterParams(['pts_exp_date'], [$this->pts_exp_date], $query);

        return $dataProvider;
    }
}
