<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MposPedManufacturers */

$this->title = \Yii::t('app', 'Редактирование производителя: ') . $model->name;
$this->params['breadcrumbs'][] = ['label' => \Yii::t('app', 'Справочник производителей'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = \Yii::t('app', 'Редактирование');
?>
<div class="mpos-ped-manufacturers-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
