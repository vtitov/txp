<?php

namespace app\controllers;

use app\models\FitsGroups;
use Yii;
use app\models\FitsMap;
use app\models\FitsMapSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * FitsMapController implements the CRUD actions for FitsMap model.
 */
class FitsMapController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['create', 'delete'],
                        'roles' => ['fitGroupEdit'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Creates a new FitsMap model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new FitsMap();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['fits-groups/view', 'id' => $model->fits_groups_id]);
        } else {
            $fitsSearchModel = new FitsMapSearch();
            $fitsDataProvider = $fitsSearchModel->search(Yii::$app->request->queryParams, $model->fits_groups_id);
            return $this->render('//fits-groups/view', [
                'model' => FitsGroups::findOne(['id' => $model->fits_groups_id]),
                'fitsDataProvider' => $fitsDataProvider,
                'fitsSearchModel' => $fitsSearchModel,
                'fitToBeAssigned' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing FitsMap model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $fits_id
     * @param string $fits_groups_id
     * @return mixed
     */
    public function actionDelete($fits_id, $fits_groups_id)
    {
        $this->findModel($fits_id, $fits_groups_id)->delete();

        return $this->redirect(['fits-groups/view', 'id' => $fits_groups_id]);
    }

    /**
     * Finds the FitsMap model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $fits_id
     * @param string $fits_groups_id
     * @return FitsMap the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($fits_id, $fits_groups_id)
    {
        if (($model = FitsMap::findOne(['fits_id' => $fits_id, 'fits_groups_id' => $fits_groups_id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
