<?php

namespace app\models;

use app\helpers\DateColumnHelper;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ChangeBankAcquirerPriorbankResponse;

/**
 * ChangeBankAcquirerPriorbankResponseSearch represents the model behind the search form about `app\models\ChangeBankAcquirerPriorbankResponse`.
 */
class ChangeBankAcquirerPriorbankResponseSearch extends ChangeBankAcquirerPriorbankResponse
{
    public $serialNumber;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'request_id', 'file_number', 'phone_trading_name', 'status'], 'integer'],
            [['name_legal_entity', 'unp', 'email', 'pos_info', 'contract', 'contract_date', 'trading_name', 'mcc', 'country', 'region', 'locality', 'address', 'terminal_id', 'merchant_id', 'parse_date', 'serialNumber'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ChangeBankAcquirerPriorbankResponse::find()->joinWith('changeBankAcquirerRequest');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]],
        ]);

        $dataProvider->sort->attributes['serialNumber'] = [
            'asc' => [ChangeBankAcquirerRequest::tableName().'.serial_number' => SORT_ASC],
            'desc' => [ChangeBankAcquirerRequest::tableName().'.serial_number' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'request_id' => $this->request_id,
            'file_number' => $this->file_number,
            //'contract_date' => $this->contract_date,
            'phone_trading_name' => $this->phone_trading_name,
            'parse_date' => $this->parse_date,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'name_legal_entity', $this->name_legal_entity])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'pos_info', $this->pos_info])
            ->andFilterWhere(['like', 'contract', $this->contract])
            ->andFilterWhere(['like', 'trading_name', $this->trading_name])
            ->andFilterWhere(['like', 'mcc', $this->mcc])
            ->andFilterWhere(['like', 'country', $this->country])
            ->andFilterWhere(['like', 'region', $this->region])
            ->andFilterWhere(['like', 'locality', $this->locality])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'terminal_id', $this->terminal_id])
            ->andFilterWhere(['like', ChangeBankAcquirerRequest::tableName().'.serial_number', $this->serialNumber])
            ->andFilterWhere(['like', 'merchant_id', $this->merchant_id])
            ->andFilterWhere(['like', 'unp', $this->unp]);

        DateColumnHelper::addFilterParams(['contract_date'], [$this->contract_date], $query);

        return $dataProvider;
    }
}
