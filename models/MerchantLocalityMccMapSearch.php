<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\MerchantLocalityMccMap;
use yii\data\ArrayDataProvider;

/**
 * MerchantLocalityMccMapSearch represents the model behind the search form about `app\models\MerchantLocalityMccMap`.
 */
class MerchantLocalityMccMapSearch extends MerchantLocalityMccMap
{
    public $description;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mcc', 'description'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $localityId)
    {
        $query = MerchantLocalityMccMap::find()
            ->select('`paybycard`.`merchant_locality_mcc_map`.`mcc`, `paybycard`.`merchant_locality_mcc_map`.`merchant_locality_id`, `mpos`.`mcc`.`description`')
            ->from('`paybycard`.`merchant_locality_mcc_map`, `mpos`.`mcc`')
            ->where(['merchant_locality_id' => $localityId])
            ->andWhere('`merchant_locality_mcc_map`.`mcc` <= `mcc`.`range_end_code`')
            ->andWhere('`merchant_locality_mcc_map`.`mcc` >= `mcc`.`range_start_code`')
            ->orderBy('mcc')
            ->distinct()
            ->asArray();

        $this->load($params);

        if ($this->validate()) {
            $query->andFilterWhere(['like', 'mcc', $this->mcc])
                ->andFilterWhere(['like', '`mpos`.`mcc`.`description`', $this->description]);
        }

        return new ArrayDataProvider([
            'allModels' => $query->all(),
        ]);
    }
}
