<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;

class TxpApiUserOwnerMapBatch extends Model
{
    public $ownerIds;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ownerIds'], 'required', 'message' => Yii::t('app', 'Необходимо выбрать банки-эквайеры')],
            [['ownerIds'], 'each', 'rule' => ['filter', 'filter' => 'intval']],
            [['ownerIds'], 'validateOwnerIds'],
        ];
    }

    public function validateOwnerIds($attribute, $params, $validator)
    {
        $ownIds = ArrayHelper::getColumn(
            Owners::find()->select(['id'])->where(['locale' => Yii::$app->language])->asArray()->all(),
            'id'
        );
        foreach ($this->$attribute as $oid) {
            if (!in_array($oid, $ownIds)) {
                $this->addError($attribute, Yii::t('app', 'Получены некорректные данные'));
                return;
            }
        }
    }

    public static function deleteCurrentOwners($userId)
    {
        TxpApiUserOwnerMap::deleteAll(['user_id' => $userId]);
        return true;
    }

    public function insertNewOwners($userId)
    {
        if (!$this->validate()) {
            return false;
        }

        $batch = [];
        foreach ($this->ownerIds as $oid) {
            $batch[] = [$userId, $oid];
        }

        TxpApiUserOwnerMap::getDb()
            ->createCommand()
            ->batchInsert(
                TxpApiUserOwnerMap::tableName(),
                ['user_id', 'owner_id'],
                $batch
            )
            ->execute();

        return true;
    }
}
