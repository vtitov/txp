<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "audit_log".
 *
 * @property integer $id
 * @property integer $mpos_id
 * @property integer $audit_event_id
 * @property string $audit_event_details
 * @property string $rq_time
 * @property string $rs_time
 *
 * @property MposPeds|null $mposPed
 * @property MposPedsArchive|null $mposPedArchive
 */
class AuditLog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'audit_log';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('mpos');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mpos_id', 'audit_event_id'], 'integer'],
            [['rq_time', 'rs_time'], 'required'],
            [['rq_time', 'rs_time'], 'safe'],
            [['audit_event_details'], 'string', 'max' => 500]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'mpos_id' => \Yii::t('app', 'Устройство'),
            'audit_event_id' => \Yii::t('app', 'Событие'),
            'audit_event_details' => \Yii::t('app', 'Подробности'),
            'rq_time' => \Yii::t('app', 'Время запроса'),
            'rs_time' => \Yii::t('app', 'Время ответа'),
        ];
    }

    public function getAuditEvent()
    {
        return $this->hasOne(AuditEvent::className(), ['id' => 'audit_event_id']);
    }

    public function getMposPed()
    {
        return $this->hasOne(MposPeds::className(), ['id' => 'mpos_id']);
    }

    public function getMposPedArchive()
    {
        return $this->hasOne(MposPedsArchive::className(), ['id' => 'mpos_id']);
    }
}
