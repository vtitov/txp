<?php

namespace app\controllers;

use app\models\MposMerchants;
use app\models\PedPreorderStatus;
use Yii;
use app\models\MposPedsPreorder;
use app\models\MposPedsPreorderSearch;
use yii\base\UserException;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * MposPedsPreorderController implements the CRUD actions for MposPedsPreorder model.
 */
class MposPedsPreorderController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view', 'export-preorders-to-csv'],
                        'roles' => ['pedPreorderView'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['create', 'update', 'change-status', 'create-prior-xml', 'create-merchant'],
                        'roles' => ['pedPreorderHandle'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all MposPedsPreorder models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MposPedsPreorderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single MposPedsPreorder model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
            'statuses' => ArrayHelper::map(PedPreorderStatus::find()->asArray()->all(), 'id', 'name'),
        ]);
    }

    /**
     * Creates a new MposPedsPreorder model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new MposPedsPreorder();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing MposPedsPreorder model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionCreateMerchant($id)
    {
        $preorder = $this->findModel($id);

        $merchant = new MposMerchants();

        $merchant->name = $preorder->name;
        $merchant->registered_address = $preorder->registered_address;
        $merchant->post_address = $preorder->post_address;
        $merchant->unp = $preorder->unp;
        $merchant->banks_mfo = $preorder->banks_mfo;
        $merchant->bic = $preorder->bic;
        $merchant->bank_account = $preorder->bank_account;
        $merchant->iban = $preorder->iban;
        $merchant->bank_account_currency_id = 933;
        $merchant->email = $preorder->email;
        $merchant->address = $preorder->registered_address;
        $merchant->phone = $preorder->mobile_phone;

        return $this->render('/mpos-merchants/create', ['model' => $merchant]);
    }

    public function actionChangeStatus($id, $status)
    {
        $model = $this->findModel($id);

        //если статус "В доставке" или "Обработан" - формируем xml-файл о предзаказе
        if (in_array($status, [4, 5, 7, 8, 9]) && $model->priorbank_xml_created == 0 && $model->createPriorXmlFile()) {
            Yii::$app->session->setFlash('priorXmlFileCreated');
            $model->priorbank_xml_created = 1;//флаг, что файл сформировался
        }

        $model->ped_preorder_status_id = $status;
        if (!$model->save()) {
            Yii::error('Error while changing preorder status: ' . print_r($model->errors, true));
        }
        return $this->redirect(['view', 'id' => $model->id]);
    }

    public function actionCreatePriorXml($id)
    {
        $model = $this->findModel($id);
        if ($model->createPriorXmlFile()) {
            Yii::$app->session->setFlash('priorXmlFileCreated');
        } else {
            throw new UserException(Yii::t('app', 'Произошла ошибка. Файл не создан.'));
        };

        if ($model->priorbank_xml_created == 0) {
            $model->priorbank_xml_created = 1;
            $model->save(false, ['priorbank_xml_created']);
        }

        return $this->redirect(['view', 'id' => $model->id]);
    }

    public function actionExportPreordersToCsv()
    {
        $searchModel = new MposPedsPreorderSearch();

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination = false;

        header("Content-type: text/csv; charset=utf-8");
        header("Content-Disposition: attachment; filename=preorders_export.csv");
        header('Cache-Control: no-store, no-cache, must-revalidate, max-age=0');

        $outputBuffer = fopen("php://output", 'w');
        //fputs($outputBuffer, $bom =( chr(0xEF) . chr(0xBB) . chr(0xBF) ));
        //fputs($outputBuffer, "sep=,\n");
        fputcsv($outputBuffer, [
            'ID',
            Yii::t('app', 'Организация'),
            'Email',
            Yii::t('app', 'Мобильный телефон'),
            Yii::t('app', 'Количество'),
            Yii::t('app', 'Дата оформления'),
            Yii::t('app', 'Статус')
        ]);
        foreach ($dataProvider->getModels() as $model) {
            fputcsv($outputBuffer, [$model->id, $model->name, $model->email, $model->mobile_phone, $model->device_number, $model->add_date, $model->pedPreorderStatus->name]);
        }
        fclose($outputBuffer);
        exit();
    }

    /**
     * Finds the MposPedsPreorder model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return MposPedsPreorder the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MposPedsPreorder::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
