<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\FitsGroupsTypes */

$this->title = \Yii::t('app', 'Редактирование типа групп FIT: ') . $model->fit_name;
$this->params['breadcrumbs'][] = ['label' => \Yii::t('app', 'Справочник типов групп FIT'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->fit_name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = \Yii::t('app', 'Редактирование');
?>
<div class="fits-groups-types-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
