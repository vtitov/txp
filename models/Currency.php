<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "currency".
 *
 * @property integer $id
 * @property string $alpha_code
 * @property integer $minor_unit
 * @property string $description
 */
class Currency extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'currency';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('mpos');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'minor_unit'], 'required'],
            [['id', 'minor_unit'], 'integer'],
            [['alpha_code'], 'string', 'max' => 3],
            [['description'], 'string', 'max' => 45],
            [['id', 'alpha_code'], 'unique', 'targetAttribute' => ['id', 'alpha_code'], 'message' => 'The combination of ID and Alpha Code has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'alpha_code' => 'Alpha Code',
            'minor_unit' => 'Minor Unit',
            'description' => 'Description',
        ];
    }
}
