<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ChangeBankAcquirerPriorbankResponse */

$this->title = 'Create Change Bank Acquirer Priorbank Response';
$this->params['breadcrumbs'][] = ['label' => 'Change Bank Acquirer Priorbank Responses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="change-bank-acquirer-priorbank-response-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
