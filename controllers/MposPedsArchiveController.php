<?php

namespace app\controllers;

use Yii;
use app\models\MposPedsArchive;
use app\models\MposPedsArchiveSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * MposPedsArchiveController implements the CRUD actions for MposPedsArchive model.
 */
class MposPedsArchiveController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view'],
                        'roles' => ['pedArchiveView'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all MposPedsArchive models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MposPedsArchiveSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single MposPedsArchive model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Finds the MposPedsArchive model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return MposPedsArchive the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        $owner_id = Yii::$app->user->identity->owner_id;
        $model = MposPedsArchive::findOne($id);

        if ($model !== null && ($owner_id == 1 || $model->owner_id == $owner_id)) {
            return $model;
        } else {
            throw new NotFoundHttpException(\Yii::t('app', 'Устройство не найдено.'));
        }
    }
}
