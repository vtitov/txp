<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MposMerchantStatuses */

$this->title = \Yii::t('app', 'Редактировать статус: ') . $model->name;
$this->params['breadcrumbs'][] = ['label' => \Yii::t('app', 'Справочник статусов Торговцев'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = \Yii::t('app', 'Редактировать');
?>
<div class="mpos-merchant-statuses-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
