<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\FitsMap */

$this->title = 'Update Fits Map: ' . ' ' . $model->fits_id;
$this->params['breadcrumbs'][] = ['label' => 'Fits Maps', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->fits_id, 'url' => ['view', 'fits_id' => $model->fits_id, 'fits_groups_id' => $model->fits_groups_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="fits-map-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
