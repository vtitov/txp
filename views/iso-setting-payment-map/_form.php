<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\IsoSettingPaymentMap */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="iso-setting-payment-map-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'payment_id')->dropDownList(
        ArrayHelper::map(
            \app\models\PaymentLocale::find()
                ->select(['id', 'name'])
                ->where(['locale' => Yii::$app->language])
                ->asArray()
                ->all(),
            'id',
            'name'
        )
    ) ?>

    <?= $form->field($model, 'currency')->dropDownList(
        ArrayHelper::map(
            \app\models\Currency::find()->select(['id', 'alpha_code'])->all(),
            'id',
            'alpha_code'
        )
    ) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Сохранить'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
