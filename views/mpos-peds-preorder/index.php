<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MposPedsPreorderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = \Yii::t('app', 'Просмотр предзаказов устройств');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mpos-peds-preorder-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Добавить предзаказ'), ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a(\Yii::t('app', 'Экспорт в CSV-файл'),
            ['export-preorders-to-csv',
                'MposPedsPreorderSearch' => isset(Yii::$app->request->queryParams['MposPedsPreorderSearch']) ?
                    Yii::$app->request->queryParams['MposPedsPreorderSearch'] : []],
            ['target' => 'blanc', 'class' => 'btn btn-primary']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            'email:email',
            'mobile_phone',
            [
                'attribute' => 'device_number',
                'label' => \Yii::t('app', 'Кол-во'),
            ],

            \app\helpers\DateColumnHelper::getColumn('add_date', $searchModel),

            [
                'attribute' => 'ped_preorder_status_id',
                'value' => function ($model) {
                    return '<font color="' . Html::encode($model->pedPreorderStatus->color) . '">' . Html::encode($model->pedPreorderStatus->name) . '</font>';
                },
                'format' => 'html',
                'filter' => \yii\helpers\ArrayHelper::map(\app\models\PedPreorderStatus::find()->asArray()->all(), 'id', 'name'),
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update}'
            ],
        ],
    ]); ?>

</div>