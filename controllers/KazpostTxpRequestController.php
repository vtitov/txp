<?php

namespace app\controllers;

use Yii;
use app\models\KazpostTxpRequest;
use app\models\KazpostTxpRequestSearch;
use yii\filters\AccessControl;
use yii\helpers\Html;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * KazpostTxpRequestController implements the CRUD actions for KazpostTxpRequest model.
 */
class KazpostTxpRequestController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view'],
                        'roles' => ['kazpostTxpRequestView'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['change-status'],
                        'roles' => ['kazpostTxpRequestEdit'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'change-status' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all KazpostTxpRequest models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new KazpostTxpRequestSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single KazpostTxpRequest model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $statusDropdownItems = [];
        foreach (KazpostTxpRequest::getRequestStatuses() as $k => $v) {
            $statusDropdownItems[] = [
                'label' => Html::encode($v),
                'url' => ['change-status', 'id' => $id, 'status' => $k],
                'linkOptions' => ['data' => ['method' => 'POST']]
            ];
        }

        return $this->render('view', [
            'model' => $model,
            'statusDropdownItems' => $statusDropdownItems,
        ]);
    }

    public function actionChangeStatus($id, $status)
    {
        $model = $this->findModel($id);
        if (!in_array($status, array_keys(KazpostTxpRequest::getRequestStatuses()))) {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }

        if ($model->status != $status) {
            $model->status = $status;
            $model->save(false, ['status']);
        }

        return $this->redirect(['view', 'id' => $id]);
    }

    /**
     * Finds the KazpostTxpRequest model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return KazpostTxpRequest the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = KazpostTxpRequest::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
