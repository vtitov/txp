<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "transaction_archive".
 *
 * @property string $id
 * @property string $date
 * @property string $mpos_ped_id
 * @property string $mpos_merchant_id
 * @property string $payments_terminal_id
 * @property string $merchant_cashier_id
 * @property string $bdd
 * @property string $stan
 * @property string $authcode
 * @property integer $mpos_transaction_status_id
 * @property string $batch
 * @property string $amount
 * @property integer $currency_id
 * @property string $card
 * @property string $rrn
 * @property string $financial_original_trx_id
 * @property string $financial_reverse_trx_id
 * @property string $rquid
 * @property string $sred
 * @property string $sred_ksn
 * @property integer $payment_id
 * @property integer $transaction_type_id
 */
class TransactionArchive extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'transaction_archive';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('mpos');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'mpos_ped_id', 'mpos_merchant_id', 'payments_terminal_id', 'merchant_cashier_id', 'mpos_transaction_status_id', 'batch', 'currency_id', 'financial_original_trx_id', 'financial_reverse_trx_id', 'payment_id', 'transaction_type_id'], 'integer'],
            [['date_time', 'mpos_ped_id', 'mpos_merchant_id', 'payments_terminal_id', 'merchant_cashier_id', 'batch', 'amount', 'currency_id', 'card'], 'required'],
            [['date_time', 'bdd'], 'safe'],
            [['amount'], 'number'],
            [['stan', 'authcode'], 'string', 'max' => 6],
            [['card'], 'string', 'max' => 19],
            [['rrn'], 'string', 'max' => 12],
            [['rquid'], 'string', 'max' => 36],
            [['sred'], 'string', 'max' => 100],
            [['sred_ksn'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date_time' => \Yii::t('app', 'Дата'),
            'mpos_ped_id' => \Yii::t('app', 'Устройство'),
            'mpos_merchant_id' => \Yii::t('app', 'Торговец'),
            'payments_terminal_id' => \Yii::t('app', 'Платежный терминал'),
            'merchant_cashier_id' => \Yii::t('app', 'Кассир'),
            'bdd' => 'Bdd',
            'stan' => 'Stan',
            'authcode' => \Yii::t('app', 'Код авторизации'),
            'mpos_transaction_status_id' => \Yii::t('app', 'Статус'),
            'batch' => 'Batch',
            'amount' => \Yii::t('app', 'Сумма'),
            'currency_id' => \Yii::t('app', 'Валюта'),
            'card' => \Yii::t('app', 'Номер карты'),
            'rrn' => 'Rrn',
            'financial_original_trx_id' => 'Financial Original Trx ID',
            'financial_reverse_trx_id' => 'Financial Reverse Trx ID',
            'rquid' => 'Rquid',
            'sred' => 'Sred',
            'sred_ksn' => 'Sred Ksn',
            'payment_id' => \Yii::t('app', 'Платеж'),
            'transaction_type_id' => \Yii::t('app', 'Тип'),
        ];
    }

    public function getPayment()
    {
        return $this->hasOne(PaymentLocale::className(), ['id' => 'payment_id'])
            ->andOnCondition([PaymentLocale::tableName() . '.locale' => Yii::$app->language]);
    }

    public function getCurrency()
    {
        return $this->hasOne(Currency::className(), ['id' => 'currency_id']);
    }

    public function getTransactionStatus()
    {
        return $this->hasOne(TransactionStatusLocale::className(), ['id' => 'mpos_transaction_status_id'])
            ->andOnCondition([TransactionStatusLocale::tableName() . '.locale' => Yii::$app->language]);
    }

    public function getTransactionType()
    {
        return $this->hasOne(TransactionTypeLocale::className(), ['id' => 'transaction_type_id'])
            ->andOnCondition([TransactionTypeLocale::tableName() . '.locale' => Yii::$app->language]);
    }

    public function getMposPed()
    {
        return $this->hasOne(MposPeds::className(), ['id' => 'mpos_ped_id']);
    }

    public function getMposPedArchive()
    {
        return $this->hasOne(MposPedsArchive::className(), ['id' => 'mpos_ped_id']);
    }

    public function getMposMerchant()
    {
        return $this->hasOne(MposMerchants::className(), ['id' => 'mpos_merchant_id']);
    }

    public function getMerchantCashier()
    {
        return $this->hasOne(MerchantCashier::className(), ['id' => 'merchant_cashier_id']);
    }

    public function getSignature()
    {
        return $this->hasOne(Signatures::className(), ['transaction_id' => 'id']);
    }

    public function getPaymentTerminal()
    {
        return $this->hasOne(PaymentsTerminals::className(), ['id' => 'payments_terminal_id']);
    }

    public function getTransactionslogModel(){
        if (($tr_log = $this->hasOne(Transactionslog::className(), ['id' => 'financial_original_trx_id']))!==null){
            return $tr_log;
        }

        if (($tr_log_arch = $this->hasOne(TransactionslogArchive::className(), ['id' => 'financial_original_trx_id']))!==null){
            return $tr_log_arch;
        }

        return null;
    }
}
