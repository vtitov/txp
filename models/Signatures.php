<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "signatures".
 *
 * @property integer $transaction_id
 * @property string $image
 */
class Signatures extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'transasction_signature';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('mpos');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['transaction_id', 'image'], 'required'],
            [['transaction_id'], 'integer'],
            [['image'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'transaction_id' => 'Transaction ID',
            'image' => 'Image',
        ];
    }
}
