<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\MposPedModels */

$this->title = \Yii::t('app', 'Добавить модель');
$this->params['breadcrumbs'][] = ['label' => \Yii::t('app', 'Справочник моделей'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mpos-ped-models-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
