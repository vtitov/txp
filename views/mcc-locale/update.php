<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MccLocale */

$this->title = Yii::t('app', 'Редактирование MCC: {name}', [
    'name' => $model->code,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Справочник МСС-кодов платежных терминалов'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->code, 'url' => ['view', 'code' => $model->code, 'locale' => $model->locale]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Редактирование');
?>
<div class="mcc-locale-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
