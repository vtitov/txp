<?php

namespace app\controllers;

use Yii;
use app\models\MccLocale;
use app\models\MccLocaleSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * MccLocaleController implements the CRUD actions for MccLocale model.
 */
class MccLocaleController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view'],
                        'roles' => ['mccView'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['create', 'update', 'delete'],
                        'roles' => ['mccEdit'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all MccLocale models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MccLocaleSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single MccLocale model.
     * @param string $code
     * @param string $locale
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($code, $locale)
    {
        return $this->render('view', [
            'model' => $this->findModel($code, $locale),
        ]);
    }

    /**
     * Creates a new MccLocale model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new MccLocale();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'code' => $model->code, 'locale' => $model->locale]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing MccLocale model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $code
     * @param string $locale
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($code, $locale)
    {
        $model = $this->findModel($code, $locale);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'code' => $model->code, 'locale' => $model->locale]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing MccLocale model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $code
     * @param string $locale
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($code, $locale)
    {
        $this->findModel($code, $locale)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the MccLocale model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $code
     * @param string $locale
     * @return MccLocale the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($code, $locale)
    {
        if (($model = MccLocale::findOne(['code' => $code, 'locale' => $locale])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
