<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Fits */

$this->title = \Yii::t('app', 'Редактирование FIT: ') . $model->id;
$this->params['breadcrumbs'][] = ['label' => \Yii::t('app', 'Справочник FIT'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = \Yii::t('app', 'Редактирование');
?>
<div class="fits-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
