<?php

namespace app\models;

use Yii;

class MposPedsArchive extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ped_archive';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('mpos');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            /*[['ped_model_id', 'serial_number', 'ped_status_id'], 'required'],
            [['ped_model_id', 'merchant_id', 'ped_status_id'], 'integer'],
            [['add_date', 'delete_date'], 'safe'],
            [['serial_number'], 'string', 'max' => 20],
            [['description'], 'string', 'max' => 255]*/
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ped_model_id' => \Yii::t('app', 'Модель'),
            'serial_number' => \Yii::t('app', 'Серийный номер'),
            'owner_id' => \Yii::t('app', 'Владелец'),
            'merchant_id' => \Yii::t('app', 'Торговец'),
            'ped_status_id' => \Yii::t('app', 'Статус'),
            'description' => \Yii::t('app', 'Описание'),
            'add_date' => \Yii::t('app', 'Дата добавления'),
            'delete_date' => \Yii::t('app', 'Дата переноса в архив'),
            'payment_terminal_id' => \Yii::t('app', 'Платежный терминал'),
            'mcc' => \Yii::t('app', 'MCC-код'),
            'region' => \Yii::t('app', 'Область'),
            'district' => \Yii::t('app', 'Район'),
            'locality' => \Yii::t('app', 'Населенный пункт'),
            'location' => \Yii::t('app', 'Место расположения'),
            'app_ver' => \Yii::t('app', 'Версия приложения'),
            'os_id' => \Yii::t('app', 'Операционная система'),
            'counter_category_id' => \Yii::t('app', 'Категория лимитов'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMposMerchant()
    {
        return $this->hasOne(MposMerchants::className(), ['id' => 'merchant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMposPedModel()
    {
        return $this->hasOne(MposPedModels::className(), ['id' => 'ped_model_id']);
    }

    public function getOwner()
    {
        return $this->hasOne(Owners::className(), ['id' => 'owner_id'])
            ->andOnCondition([Owners::tableName() . '.locale' => Yii::$app->language]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMposPedStatus()
    {
        return $this->hasOne(PedStatusLocale::className(), ['id' => 'ped_status_id'])
            ->andOnCondition([PedStatusLocale::tableName() . '.locale' => Yii::$app->language]);
    }

    public function getPaymentTerminal()
    {
        return $this->hasOne(PaymentsTerminals::className(), ['id' => 'payment_terminal_id']);
    }

    public function getOs()
    {
        return $this->hasOne(OsAllowed::className(), ['id' => 'os_id']);
    }

    public function getCounterCategory()
    {
        return $this->hasOne(CounterCategory::className(), ['id' => 'counter_category_id']);
    }

    public function getDeviceProfile()
    {
        return $this->hasOne(DeviceProfile::className(), ['deviceId' => 'serial_number']);
    }
}
