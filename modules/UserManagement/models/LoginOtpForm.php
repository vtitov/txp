<?php

namespace app\modules\UserManagement\models;

use Yii;
use yii\base\Model;
use yii\base\UserException;

/**
 * LoginForm is the model behind the login form.
 */
class LoginOtpForm extends Model
{
    public $username;
    public $domain;
    public $password;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['username', 'domain', 'password'], 'required'],
        ];
    }

    /**
     * Logs in a user using the provided username and password.
     * @return bool whether the user is logged in successfully
     */
    public function sendSoapRequest()
    {
        if ($this->validate()) {

            ini_set("default_socket_timeout", 10);
            ini_set("soap.wsdl_cache_enabled", 0);

            if (!extension_loaded('soap')) {
                Yii::error("Soap extension is not loaded");
                throw new UserException(Yii::t('app', 'Не удалось отправить запрос автооризации'));
            }

            // Create the client instance
            $soap_client = new \SoapClient(
                Yii::$app->params['otpUrl'],
                [
                    'cache_wsdl' => WSDL_CACHE_NONE,
                    'stream_context' => stream_context_create([
                        'ssl' => [
                            'verify_peer' => false,
                            'verify_peer_name' => false,
                            'allow_self_signed' => true
                        ]
                    ])
                ]
            );
            if (!$soap_client) {
                Yii::error("Could not create a SOAP client");
                throw new UserException(Yii::t('app', 'Не удалось отправить запрос автооризации'));
            }

            $response = false;
            try {
                $response = $soap_client->openotpSimpleLogin($this->username, $this->domain, $this->password, "PSP_WEB_INT", $_SERVER['REMOTE_ADDR'], NULL);
            } catch (\Exception $e) {
                Yii::error("Could not send a SOAP request");
                throw new UserException(Yii::t('app', 'Не удалось отправить запрос автооризации'));
            }

            if (!$response) {
                Yii::error("Could not send a SOAP request");
                throw new UserException(Yii::t('app', 'Не удалось отправить запрос автооризации'));
            }

            return $response;

        } else {
            return false;
        }
    }

    public function attributeLabels()
    {
        return [
            'username' => \Yii::t('app', 'Логин'),
            'password' => \Yii::t('app', 'Пароль'),
            'domain' => \Yii::t('app', 'Домен'),
        ];
    }
}
