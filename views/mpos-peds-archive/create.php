<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\MposPedsArchive */

$this->title = 'Create Mpos Peds Archive';
$this->params['breadcrumbs'][] = ['label' => 'Mpos Peds Archives', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mpos-peds-archive-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
