<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\MposPeds */

$this->title = \Yii::t('app', 'Добавить устройство');
$this->params['breadcrumbs'][] = ['label' => \Yii::t('app', 'Справочник устройств'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="mpos-peds-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="barcode-scan">
        <p>
            <strong><?= Html::encode(\Yii::t('app', 'Для считывания данных со штрих-кода установите курсор в это поле:')) ?></strong>
            <input type="text" id="barcode" autofocus>
            <img src="barcode.gif" height="40px" style="margin-left: 40px">
        </p>
    </div>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>



<script type="text/javascript">
    var barcode_input = document.getElementById('barcode');
    var serial_input = document.getElementById('mpospeds-serial_number');
    var model_select = document.getElementById('name');
    var submit_button = document.getElementById('submit-button');

    barcode_input.oninput = function(){
        if (barcode_input.value.length == 20){

            var model = "M" + barcode_input.value.substr(0, 3);
            var serial = barcode_input.value.substr(-8);

            serial_input.value = serial;

            for (var i = 0; i < model_select.options.length; i++) {
                if (model_select.options[i].text== model) {
                    model_select.options[i].selected = true;
                    break;
                }
            }

            submit_button.click();
        }
    }
</script>