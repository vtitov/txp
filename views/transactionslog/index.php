<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TransactionslogSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'TransactionsLog';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="transactionslog-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'id',
                'value' => function($model){
                    if ($model->transaction){
                        return Html::a(Html::encode($model->id), ['mpos-transactions/view', 'id' => $model->transaction->id]);
                    } else {
                        return Html::encode($model->id);
                    }
                },
                'format' => 'html',
            ],
            \app\helpers\DateColumnHelper::getColumn('dt', $searchModel),
            'amount',
            [
                'attribute' => 'trx_statuses_id',
                'value' => 'trxStatus.name',
                'filter' => ArrayHelper::map(\app\models\TrxStatuses::find()->asArray()->all(), 'id', 'name'),
            ],
            [
                'attribute' => 'response_code',
                'value' => function ($model){
                    return $model->paymentTerminal ?
                        Html::a(Html::encode($model->response_code), ['host-error-code/view', 'id' => $model->response_code, 'iso_settings_id' => $model->paymentTerminal->iso_setting_id]) :
                        Html::encode($model->response_code);
                },
                'format' => 'html',
            ],
            [
                'attribute' => 'trx_types_id',
                'value' => 'trxType.name',
                'filter' => ArrayHelper::map(\app\models\TrxTypes::find()->asArray()->all(), 'id', 'name'),
            ],
            /*[
                'attribute' => 'iso_setting_id',
                'value' => 'isoSetting.name',
                'filter' => ArrayHelper::map(\app\models\IsoSettings::find()->asArray()->all(), 'id', 'name'),
            ],*/
            [
                'attribute' => 'payment_id',
                'value' => 'payment.name',
                'filter' => ArrayHelper::map(
                    \app\models\PaymentLocale::find()
                        ->select(['id', 'name'])
                        ->where(['locale' => Yii::$app->language])
                        ->asArray()
                        ->all(),
                    'id',
                    'name'
                ),
            ],
            // 'payment_terminal_id',
            // 'currency',
            // 'bdd',
            // 'batch',
            // 'reconcilationFlag',
            // 'MTI',
            'rrn',
            // 'ksn',
            // 'expdate',
            // 'authcode',
            // 'acquircode',
            // 'stan',
            // 'sred',
            [
                'attribute' => 'device',
                'value' => function($model){
                    $ped = $model->mposPed;
                    if ($ped){
                        return Html::a(Html::encode($model->device), ['mpos-merchants/view', 'id' => $ped->merchant_id]);
                    } else {
                        return Html::encode($model->device);
                    }
                },
                'format' => 'html',
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}',
            ],
        ],
    ]); ?>

</div>
