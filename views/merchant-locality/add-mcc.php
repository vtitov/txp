<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\MerchantLocalityMccMap */

$this->title = 'Добавление MCC населенному пункту';
$this->params['breadcrumbs'][] = ['label' => 'Населенные пункты', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => \app\models\MerchantLocality::findOne($model->merchant_locality_id)->name, 'url' => ['view', 'id' => $model->merchant_locality_id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="merchant-locality-mcc-map-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php $form = \yii\widgets\ActiveForm::begin(); ?>

    <?= $form->field($model, 'mcc')->textInput(['maxlength' => 4]) ?>

    <div class="form-group">
        <?= Html::submitButton('Добавить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php \yii\widgets\ActiveForm::end(); ?>

</div>
