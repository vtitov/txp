<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\AppKey;

/**
 * AppKeySearch represents the model behind the search form of `app\models\AppKey`.
 */
class AppKeySearch extends AppKey
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'os_id'], 'integer'],
            [['app_ver', 'pub_key'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AppKey::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'os_id' => $this->os_id,
        ]);

        $query->andFilterWhere(['like', 'app_ver', $this->app_ver])
            ->andFilterWhere(['like', 'pub_key', $this->pub_key]);

        return $dataProvider;
    }
}
