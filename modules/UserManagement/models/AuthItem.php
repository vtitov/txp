<?php

namespace app\modules\UserManagement\models;

use Yii;

/**
 * This is the model class for table "auth_item".
 *
 * @property string $name
 * @property int $type
 * @property string $description
 * @property string $rule_name
 * @property resource $data
 * @property int $created_at
 * @property int $updated_at
 */
class AuthItem extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'auth_item';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'description'], 'required'],
            [['name'], 'unique'],
            [['name'], 'string', 'max' => 64],
            [['description'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => \Yii::t('app', 'Название'),
            'type' => \Yii::t('app', 'Тип'),
            'description' => \Yii::t('app', 'Описание'),
            'rule_name' => \Yii::t('app', 'Имя правила'),
            'data' => 'Data',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    //просто чтобы запретить редактирование ролей напрямую в базе
    public function save($runValidation = true, $attributeNames = null){
        return false;
    }
    public function delete(){
        return false;
    }
}
