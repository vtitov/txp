<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;

class SiteController extends Controller
{
    public $freeAccessActions = ['index'];

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index'],
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionChangeLanguage($language)
    {
        if (in_array($language, Yii::$app->params['availableTranslations'])) {
            Yii::$app->response->cookies->add(new \yii\web\Cookie([
                'name' => 'mpos-arm-language',
                'value' => $language,
                'httpOnly' => true,
                'secure' => true,
            ]));
        }
        return $this->goBack();
    }
}
