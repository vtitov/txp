<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TransactionCountAcquirerReportSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Детальный отчет по транзакциям');
$this->params['breadcrumbs'][] = $this->title;

$this->registerCss('.grid-view th{white-space: normal}');
?>
<div class="transaction-detailed-report-rbi">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="transaction-detailed-report-rbi-search">

        <?php $form = ActiveForm::begin([
            'action' => ['transaction-detailed-report-rbi'],
            'method' => 'get'
        ]); ?>

        <div class="row">
            <div class="col-md-6">
                <?= $form->field($searchModel, 'dateRange')->widget(
                    \kartik\daterange\DateRangePicker::className(),
                    \app\helpers\DateColumnHelper::getWidgetConfig())
                    ->label(Yii::t('app', 'Период'))
                ?>
            </div>
            <div class="col-md-6">

                <?php
                if (Yii::$app->user->identity->owner_id == 1) {
                    echo $form->field($searchModel, 'ownerId')
                        ->dropDownList(
                            \yii\helpers\ArrayHelper::map(
                                \app\models\Owners::find()
                                    ->select(['id', 'name'])
                                    ->where(['locale' => Yii::$app->language])
                                    ->asArray()
                                    ->all(),
                                'id',
                                'name'
                            ),
                            ['prompt' => '']
                        )
                        ->label(Yii::t('app', 'Банк-эквайер'));
                }
                ?>
            </div>
        </div>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Показать'), ['class' => 'btn btn-success', 'name' => 'action', 'value' => 'render']) ?>
            <?= Html::submitButton(Yii::t('app', 'Экспорт в CSV'), ['class' => 'btn btn-default', 'name' => 'action', 'value' => 'export']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

    <?php
    if ($dataProvider != null) {
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                [
                    'attribute' => 'date',
                    'label' => Yii::t('app', 'Дата'),
                ],
                [
                    'attribute' => 'time',
                    'label' => Yii::t('app', 'Время'),
                ],
                [
                    'attribute' => 'iso_merchant_id',
                    'label' => Yii::t('app', 'ID торговца в процессинговом центре'),
                ],
                [
                    'attribute' => 'payment_terminal',
                    'label' => Yii::t('app', 'Платежный терминал'),
                ],
                [
                    'attribute' => 'mcc',
                    'label' => 'MCC',
                ],
                [
                    'attribute' => 'amount',
                    'label' => Yii::t('app', 'Сумма'),
                ],
                [
                    'attribute' => 'currency',
                    'label' => Yii::t('app', 'Валюта'),
                ],
                [
                    'attribute' => 'card',
                    'label' => Yii::t('app', 'Номер карты'),
                ],
                [
                    'attribute' => 'response_code',
                    'label' => Yii::t('app', 'Код ответа'),
                ],
                [
                    'attribute' => 'rrn',
                    'label' => 'RRN',
                ],
                [
                    'attribute' => 'authcode',
                    'label' => Yii::t('app', 'Код авторизации'),
                ],
            ],
        ]);
    }
    ?>

</div>
