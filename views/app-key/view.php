<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\AppKey */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Ключи app key'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
$css = <<<CSS
tr>td:nth-child(2) {
max-width: 450px;
overflow-wrap: break-word;
}
CSS;
$this->registerCss($css);
?>
<div class="app-key-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Редактировать'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?php /*echo Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ])*/ ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            [
                'attribute' => 'os_id',
                'value' => \yii\helpers\ArrayHelper::getValue($model->os, 'name', $model->os_id),
            ],
            'app_ver',
            'pub_key:ntext',
            'pub_modulus:ntext',
            'pub_exp',
        ],
    ]) ?>

</div>
