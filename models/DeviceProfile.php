<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "device_profile".
 *
 * @property int $id
 * @property string|null $deviceId
 * @property string|null $mask
 * @property string|null $mask_hash
 * @property int|null $status
 * @property string|null $app_ver
 * @property int|null $app_build
 * @property int $trust_level_id
 * @property string|null $current_locale
 * @property string $created_at
 *
 * @property TrustLevelLocale|null $trustLevel
 */
class DeviceProfile extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'device_profile';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('t2p_monitoring');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'deviceId' => Yii::t('app', 'ID устройства'),
            'mask' => Yii::t('app', 'Маска'),
            'mask_hash' => Yii::t('app', 'Хэш маски'),
            'status' => Yii::t('app', 'Статус'),
            'app_ver' => Yii::t('app', 'Версия мобильного приложения'),
            'app_build' => Yii::t('app', 'Номер сборки мобильного приложения'),
            'trust_level_id' => Yii::t('app', 'Уровень доверия'),
            'current_locale' => Yii::t('app', 'Выбранная локаль'),
            'created_at' => Yii::t('app', 'Дата создания профиля'),
        ];
    }

    public function getTrustLevel()
    {
        return $this->hasOne(TrustLevelLocale::className(), ['id' => 'trust_level_id'])
            ->andOnCondition([TrustLevelLocale::tableName() . '.locale' => Yii::$app->language]);
    }
}
