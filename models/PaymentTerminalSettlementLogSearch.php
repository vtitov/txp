<?php

namespace app\models;

use Yii;
use app\helpers\DateColumnHelper;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\PaymentTerminalSettlementLog;

/**
 * PaymentTerminalSettlementLogSearch represents the model behind the search form of `app\models\PaymentTerminalSettlementLog`.
 */
class PaymentTerminalSettlementLogSearch extends PaymentTerminalSettlementLog
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['bddn', 'trx_counter', 'sett_result', 'batch_status_id'], 'integer'],
            [['payment_terminal_id', 'bdd', 'sett_date_time'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PaymentTerminalSettlementLog::find()
            ->joinWith(['paymentTerminal', 'batchStatus']);

        // add conditions that should always apply here
        $owner_id = Yii::$app->user->identity->owner_id;
        if ($owner_id != 1) {
            $query->joinWith(['paymentTerminal.isoSettings'])
                ->andWhere([IsoSettings::tableName() . '.owner_id' => $owner_id]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['sett_date_time' => SORT_DESC]],
        ]);

        $dataProvider->sort->attributes['payment_terminal_id'] = [
            'asc' => [PaymentsTerminals::tableName() . '.name' => SORT_ASC],
            'desc' => [PaymentsTerminals::tableName() . '.name' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['batch_status_id'] = [
            'asc' => [BatchStatusLocale::tableName() . '.desc' => SORT_ASC],
            'desc' => [BatchStatusLocale::tableName() . '.desc' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            self::tableName() . '.bddn' => $this->bddn,
            self::tableName() . '.trx_counter' => $this->trx_counter,
            self::tableName() . '.sett_result' => $this->sett_result,
            self::tableName() . '.batch_status_id' => $this->batch_status_id,
        ]);

        $query->andFilterWhere(['like', PaymentsTerminals::tableName() . '.name', $this->payment_terminal_id]);

        DateColumnHelper::addFilterParams([self::tableName() . '.bdd', self::tableName() . '.sett_date_time'], [$this->bdd, $this->sett_date_time], $query);

        return $dataProvider;
    }
}
