<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PaymentTerminalCurrentBatchSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = \Yii::t('app', 'Незакрытые бизнес-дни');
$this->params['breadcrumbs'][] = $this->title;

$batchStatusFilter = ArrayHelper::map(
    \app\models\BatchStatusLocale::find()->where(['locale' => Yii::$app->language])->all(),
    'id',
    'desc'
);
?>
<div class="payment-terminal-current-batch-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'payment_terminal_id',
                'value' => 'paymentTerminal.name',
            ],
            \app\helpers\DateColumnHelper::getColumn('bdd', $searchModel),
            //'bddn',
            //'trx_counter',
            [
                'attribute' => 'ped_serial_number',
                'value' => function ($model) {
                    if ($model->ped) {
                        return Html::a(
                            Html::encode($model->ped->serial_number),
                            ['mpos-peds/view', 'id' => $model->ped->id]
                        );
                    } else {
                        return null;
                    }

                },
                'label' => \Yii::t('app', 'Устройство'),
                'format' => 'html',
            ],
            [
                'attribute' => 'merchant_name',
                'value' => function ($model) {
                    if ($model->ped && $model->ped->mposMerchant) {
                        return Html::a(
                            Html::encode($model->ped->mposMerchant->name),
                            ['mpos-merchants/view', 'id' => $model->ped->mposMerchant->id]
                        );
                    } else {
                        return null;
                    }

                },
                'label' => \Yii::t('app', 'Торговец'),
                'format' => 'html',
            ],
            [
                'attribute' => 'merchant_phone',
                'value' => 'ped.mposMerchant.phone',
                'label' => \Yii::t('app', 'Телефон'),
            ],
            [
                'attribute' => 'batch_status_id',
                'value' => function ($model) use ($batchStatusFilter) {
                    return ArrayHelper::getValue($batchStatusFilter, $model->batch_status_id, $model->batch_status_id);
                },
                'filter' => $batchStatusFilter,
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}',
            ],
        ],
    ]); ?>

</div>
