<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AuditLogSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = \Yii::t('app', 'Журнал аудита');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="audit-log-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <!--?= Html::a('Create Audit Log', ['create'], ['class' => 'btn btn-success']) ?-->
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            [
                'attribute' => 'mpos_id',
                'value' => function ($model){
                    $ped = \app\models\MposPeds::findOne(['id' => $model->mpos_id]);
                    if ($ped){
                        return Html::a(Html::encode($ped->serial_number), ['mpos-peds/view', 'id' => $model->mpos_id]);
                    }

                    $pedArchive = \app\models\MposPedsArchive::findOne(['id' => $model->mpos_id]);
                    if ($pedArchive) {
                        return Html::a(Html::encode($pedArchive->serial_number), ['mpos-peds-archive/view', 'id' => $model->mpos_id]);
                    }

                    return null;
                },
                'format' => 'html',
            ],
            [
                'attribute' => 'audit_event_id',
                'value' => 'auditEvent.name',
                'filter' => \yii\helpers\ArrayHelper::map(\app\models\AuditEvent::find()->asArray()->all(), 'id', 'name'),
            ],

            //'audit_event_details',

            \app\helpers\DateColumnHelper::getColumn('rq_time', $searchModel),
            \app\helpers\DateColumnHelper::getColumn('rs_time', $searchModel),

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}'
            ],
        ],
    ]); ?>

</div>
