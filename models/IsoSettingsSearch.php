<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\IsoSettings;

/**
 * IsoSettingsSearch represents the model behind the search form about `app\models\IsoSettings`.
 */
class IsoSettingsSearch extends IsoSettings
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'port', 'owner_id', 'ped_conf_category_id', 'sred_clear_period_days', 'status'], 'integer'],
            [['host', 'name', 'protocol_class', 'zpk', 'zak', 'zmk_tap_x'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = IsoSettings::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'port' => $this->port,
            'owner_id' => $this->owner_id,
            'ped_conf_category_id' => $this->ped_conf_category_id,
            'sred_clear_period_days' => $this->sred_clear_period_days,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'host', $this->host])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'protocol_class', $this->protocol_class])
            ->andFilterWhere(['like', 'zpk', $this->zpk])
            ->andFilterWhere(['like', 'zak', $this->zak])
            ->andFilterWhere(['like', 'zmk_tap_x', $this->zmk_tap_x]);

        return $dataProvider;
    }
}
