<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\IsoSettings */

$this->title = \Yii::t('app', 'Добавить ISO-настройку');
$this->params['breadcrumbs'][] = ['label' => \Yii::t('app', 'Справочник ISO настроек'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="iso-settings-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
