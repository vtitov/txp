<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MerchantCashier */

$this->title = \Yii::t('app', 'Редактирование данных кассира: ') . $model->description;
$this->params['breadcrumbs'][] = ['label' => \Yii::t('app', 'Справочник Торговцев'), 'url' => ['mpos-merchants/index']];
$this->params['breadcrumbs'][] = ['label' => $model->merchant->name, 'url' => ['mpos-merchants/view', 'id' => $model->merchant_id]];
$this->params['breadcrumbs'][] = \Yii::t('app', 'Редактирование');
?>

<div class="merchant-cashier-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
