<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\FitsMap */

$this->title = 'Create Fits Map';
$this->params['breadcrumbs'][] = ['label' => 'Fits Maps', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fits-map-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
