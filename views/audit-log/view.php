<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\MposPeds;
use app\models\MposPedsArchive;

/* @var $this yii\web\View */
/* @var $model app\models\AuditLog */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => \Yii::t('app', 'Журнал аудита'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="audit-log-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <!--?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?-->
        <!--?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?-->
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            [
                'attribute' => 'mpos_id',
                'value' => ($ped = MposPeds::findOne(['id' => $model->mpos_id])) ?
                    Html::a($ped->serial_number, ['mpos-peds/view', 'id' => $model->mpos_id]) :
                    (($pedArchive = MposPedsArchive::findOne(['id' => $model->mpos_id])) ?
                        Html::a($pedArchive->serial_number, ['mpos-peds-archive/view', 'id' => $model->mpos_id]) : null),
                'format' => 'html',
            ],
            [
                'attribute' => 'audit_event_id',
                'value' => $model->auditEvent->name,
            ],

            'audit_event_details',
            'rq_time',
            'rs_time',
        ],
    ]) ?>

</div>
