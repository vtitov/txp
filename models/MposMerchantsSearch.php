<?php

namespace app\models;

use app\helpers\DateColumnHelper;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

/**
 * MposMerchantsSearch represents the model behind the search form about `app\models\MposMerchants`.
 */
class MposMerchantsSearch extends MposMerchants
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'owner_id', 'bank_account_currency_id', 'phone', 'merchant_status_id'], 'integer'],
            [['name', 'email', 'banks_mfo', 'bank_account', 'unp', 'contract', 'created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MposMerchants::find()
            ->joinWith(['owner', 'mposMerchantStatus']);

        if (Yii::$app->user->identity->owner_id != 1){
            $query->andWhere([MposMerchants::tableName().'.owner_id' => Yii::$app->user->identity->owner_id]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $dataProvider->sort->attributes['owner_id'] = [
            'asc' => [Owners::tableName() . '.name' => SORT_ASC],
            'desc' => [Owners::tableName() . '.name' => SORT_DESC]
        ];

        $dataProvider->sort->attributes['merchant_status_id'] = [
            'asc' => [MerchantStatusLocale::tableName() . '.name' => SORT_ASC],
            'desc' => [MerchantStatusLocale::tableName() . '.name' => SORT_DESC]
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            static::tableName() . '.id' => $this->id,
            static::tableName() . '.owner_id' => $this->owner_id,
            static::tableName() . '.bank_account_currency_id' => $this->bank_account_currency_id,
            static::tableName() . '.merchant_status_id' => $this->merchant_status_id,
        ]);

        $query->andFilterWhere(['like', static::tableName() . '.name', $this->name])
            ->andFilterWhere(['like', static::tableName() . '.email', $this->email])
            ->andFilterWhere(['like', static::tableName() . '.unp', $this->unp])
            ->andFilterWhere(['like', static::tableName() . '.contract', $this->contract])
            ->andFilterWhere(['like', static::tableName() . '.phone', $this->phone]);

        DateColumnHelper::addFilterParams([static::tableName() . '.created_at'], [$this->created_at], $query);

        return $dataProvider;
    }
}
