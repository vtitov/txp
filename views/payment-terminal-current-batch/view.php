<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\PaymentTerminalCurrentBatch */

$this->title = \yii\helpers\ArrayHelper::getValue($model->paymentTerminal, 'name', $model->payment_terminal_id);
$this->params['breadcrumbs'][] = ['label' => \Yii::t('app', 'Незакрытые бизнес-дни'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="payment-terminal-current-batch-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?php if ($model->batch_status_id != 4 && $model->batch_status_id != 5 && $model->batch_status_id != 6) {
            echo Html::a(Yii::t('app', 'Закрыть бизнес-день'), ['close', 'id' => $model->payment_terminal_id], [
                'class' => 'btn btn-primary',
                'data' => [
                    'confirm' => Yii::t('app', 'Вы действительно хотите закрыть этот бизнес-день?'),
                    'method' => 'post',
                ],
            ]);
        } ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'payment_terminal_id',
                'value' => \yii\helpers\ArrayHelper::getValue($model->paymentTerminal, 'name', $model->payment_terminal_id),
            ],
            'bdd',
            'bddn',
            'trx_counter',
            [
                'attribute' => 'batch_status_id',
                'value' => \yii\helpers\ArrayHelper::getValue($model->batchStatus, 'desc', $model->batch_status_id),
            ],
            //'host_batch_id',
        ],
    ]) ?>

</div>
