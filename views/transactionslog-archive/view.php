<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\TransactionslogArchive */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Transactionslog Archive', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="transactionslog-archive-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'id',
                'value' => ($tr = \app\models\TransactionArchive::findOne(['financial_original_trx_id' => $model->id])) ?
                    Html::a(Html::encode($model->id), ['mpos-transactions/view', 'id' => $tr->id]) : Html::encode($model->id),
                'format' => 'html',
            ],
            'dt',
            [
                'attribute' => 'trx_statuses_id',
                'value' => ArrayHelper::getValue($model->trxStatus, 'name', $model->trx_statuses_id),
            ],
            [
                'attribute' => 'trx_types_id',
                'value' => ArrayHelper::getValue($model->trxType, 'name', $model->trx_types_id),
            ],
            /*[
                'attribute' => 'iso_setting_id',
                'value' => $model->isoSetting->name,
            ],*/
            /*
             * [10:38:33] Yauhen Klim: это ваще не iso настройка
[10:38:53] Yauhen Klim: это константа, описывающая протокол

 static int PROTOCOL_TYPE_TIETO = 1;
    static int PROTOCOL_TYPE_OW = 2;
    static int PROTOCOL_TYPE_POS_TIETO = 3;
    static int PROTOCOL_TYPE_BASE24 = 4;
    static int PROTOCOL_TYPE_TPII = 5;
    static int PROTOCOL_TYPE_CORTEX = 6; // Metavante*/
            [
                'attribute' => 'payment_id',
                'value' => ArrayHelper::getValue($model->payment, 'name', $model->payment_id),
            ],
            [
                'attribute' => 'payment_terminal_id',
                'value' => ArrayHelper::getValue($model->paymentTerminal, 'name', $model->payment_terminal_id),
            ],
            [
                'attribute' => 'payment_terminal_id',
                'label' => \Yii::t('app', 'Торговец'),
                'value' => ($model->mposPed && $model->mposPed->mposMerchant) ? Html::a(Html::encode($model->mposPed->mposMerchant->name), ['mpos-merchants/view', 'id' => $model->mposPed->merchant_id]) : \Yii::t('app', 'Торговец не найден'),
                'format' => 'html',
            ],
            [
                'attribute' => 'currency',
                'value' => ArrayHelper::getValue($model->currencyModel, 'alpha_code', $model->currency),
            ],
            'amount',
            'bdd',
            'batch',
            'reconcilationFlag',
            'MTI',
            'rrn',
            'ksn',
            'expdate',
            'authcode',
            'acquircode',
            'stan',
            //'sred',
            'device',
            [
                'attribute' => 'response_code',
                'value' => $model->paymentTerminal ?
                    Html::a(Html::encode($model->response_code), ['host-error-code/view', 'id' => $model->response_code, 'iso_settings_id' => $model->paymentTerminal->iso_setting_id]) :
                    Html::encode($model->response_code),
                'format' => 'html',
            ],
        ],
    ]) ?>

</div>
