<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MposMerchantsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = \Yii::t('app', 'Справочник Торговцев');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mpos-merchants-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(\Yii::t('app', 'Добавить Торговца'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            [
                'attribute' => 'owner_id',
                'value' => function($model){
                    if ($model->owner) {
                        return Html::encode($model->owner->name);
                    } else if ($model->owner_id != '') {
                        return '<span class="not-set">(' . Html::encode($model->owner_id) . ')</span>';
                    } else {
                        return null;
                    }
                },
                'filter' => ArrayHelper::map(
                    app\models\Owners::find()
                        ->where(['locale' => Yii::$app->language])
                        ->orderBy(['name' => SORT_ASC])
                        ->asArray()
                        ->all(),
                    'id',
                    'name'
                ),
                'format' => 'html',
                'visible' => (Yii::$app->user->identity->owner_id == 1),
            ],
            [
                'attribute' => 'name',
                'value' => function($model){
                    return Html::a(Html::encode($model->name), ['view', 'id' => $model->id]);
                },
                'format'=>'html',
                'label' => \Yii::t('app', 'Наименование предприятия'),
            ],
            'unp',
            /*[
                'attribute' => 'banks_mfo',
                'label' => \Yii::t('app', 'Банк'),
                'value' => function($model){
                    return $model->banks_mfo ? $model->banks_mfo : $model->bic;
                },
            ],
            [
                'attribute' => 'bank_account',
                'value' => function($model){
                    return $model->bank_account ? $model->bank_account : $model->iban;
                },
            ],
            [
				'attribute' => 'bank_account_currency_id',
				'value' => 'currency.alpha_code',
				'options' => [
					'width' => 100,
				],
				'filter' => ArrayHelper::map(app\models\Currency::find()->asArray()->all(), 'id', 'alpha_code'),
			],*/
            'email:email',
            // 'password',
            [
                'attribute' => 'phone',
                'label' => \Yii::t('app', 'Телефон'),
            ],
            [
				'attribute' => 'merchant_status_id',
				'value' => function($model){
                    if ($model->mposMerchantStatus) {
                        return '<font color="' . Html::encode($model->mposMerchantStatus->color) . '">' . Html::encode($model->mposMerchantStatus->name) . '</font>';
                    } else {
                        return null;
                    }
                },
                'format' => 'html',
				'options' => [
					'width' => 100,
                ],
                'filter' => ArrayHelper::map(
                    app\models\MerchantStatusLocale::find()
                        ->where(['locale' => Yii::$app->language])
                        ->asArray()
                        ->all(),
                    'id',
                    'name'
                ),
			],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update}',
                'visibleButtons' => [
                    'update' => function ($model) {
                        return $model->id != 0;
                    },
                ],
            ],
        ],
    ]); ?>

</div>
