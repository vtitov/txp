<?php

namespace app\models;

use app\helpers\DateColumnHelper;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\AuditLog;

/**
 * AuditLogSearch represents the model behind the search form about `app\models\AuditLog`.
 */
class AuditLogSearch extends AuditLog
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'audit_event_id'], 'integer'],
            [['mpos_id', 'audit_event_details', 'rq_time', 'rs_time'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AuditLog::find()
            ->joinWith(['mposPed', 'mposPedArchive', 'auditEvent']);

        if (Yii::$app->user->identity->owner_id != 1){
            $query->andWhere(['or',
                [MposPeds::tableName().'.owner_id' => Yii::$app->user->identity->owner_id],
                [MposPedsArchive::tableName().'.owner_id' => Yii::$app->user->identity->owner_id]
            ]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        //серийник может быть как действующего ped'а так и архивного
        /*$dataProvider->sort->attributes['mpos_id'] = [
            'asc' => [MposPeds::tableName() . '.serial_number' => SORT_ASC],
            'desc' => [MposPeds::tableName() . '.serial_number' => SORT_DESC]
        ];*/

        $dataProvider->sort->attributes['audit_event_id'] = [
            'asc' => [AuditEvent::tableName() . '.name' => SORT_ASC],
            'desc' => [AuditEvent::tableName() . '.name' => SORT_DESC]
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'audit_event_id' => $this->audit_event_id,
        ]);

        $query->andFilterWhere(['like', 'audit_event_details', $this->audit_event_details]);
        $query->andFilterWhere(['or', ['like', 'ped.serial_number', $this->mpos_id], ['like', 'ped_archive.serial_number', $this->mpos_id]]);

        DateColumnHelper::addFilterParams(['rq_time', 'rs_time'], [$this->rq_time, $this->rs_time], $query);

        return $dataProvider;
    }
}
