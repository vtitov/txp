<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\PaymentsMap */

$this->title = $model->ped_id;
$this->params['breadcrumbs'][] = ['label' => 'Payments Maps', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payments-map-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'ped_id' => $model->ped_id, 'payments_id' => $model->payments_id, 'currency_id' => $model->currency_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'ped_id' => $model->ped_id, 'payments_id' => $model->payments_id, 'currency_id' => $model->currency_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'ped_id',
            'payments_id',
            'currency_id',
            'payments_terminals_id',
            'fits_groups_id',
            'fits_groups_id_sign_allowed',
            'fits_groups_id_fallback_allowed',
        ],
    ]) ?>

</div>
