<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\MposPedsArchive */

$this->title = $model->serial_number;
$this->params['breadcrumbs'][] = ['label' => \Yii::t('app', 'Архив устройств'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mpos-peds-archive-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="row">
        <div class="col-md-6">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    //'id',
                    'serial_number',
                    [
                        'attribute' => 'ped_model_id',
                        'value' => $model->mposPedModel->name,
                    ],
                    /*[
                        'attribute' => 'merchant_id',
                        'value' => is_null($model->merchant_id) ? null : $model->mposMerchant->name,
                    ],*/
                    [
                        'attribute' => 'ped_status_id',
                        'value' => $model->mposPedStatus->name,
                    ],
                    'description',
                    'add_date',
                    'delete_date',
                    'payment_terminal_id',
                    'mcc',
                    [
                        'attribute' => 'mcc',
                        'value' => $model->mcc ? (($mcc = \app\models\Mcc::find()->andWhere("range_start_code <= $model->mcc")->andWhere("range_end_code >= $model->mcc")->one()) ? $mcc->description : '') : '',
                        'label' => \Yii::t('app', 'Описание MCC-кода'),
                    ],
                    'region',
                    'district',
                    'locality',
                    'location',
                    'app_ver',
                    [
                        'attribute' => 'os_id',
                        'value' => \yii\helpers\ArrayHelper::getValue($model->os, 'name', $model->os_id),
                    ],
                    [
                        'attribute' => 'counter_category_id',
                        'value' => \yii\helpers\ArrayHelper::getValue($model->counterCategory, 'name'),
                    ],
                ],
            ]) ?>
        </div>
        <div class="col-md-6">
            <?php if ($model->deviceProfile === null): ?>
                <div class="alert alert-warning">
                    <?= Yii::t('app', 'Профиль устройства не найден') ?>
                </div>
            <?php else: ?>
                <?= DetailView::widget([
                    'model' => $model->deviceProfile,
                    'attributes' => [
                        [
                            'attribute' => 'mask',
                            'value' => str_replace(';', ";\n", $model->deviceProfile->mask),
                            'format' => 'ntext',
                        ],
                        //'mask_hash',
                        //'status',
                        'current_locale',
                        'created_at',
                        [
                            'attribute' => 'trust_level_id',
                            'value' => \yii\helpers\ArrayHelper::getValue(
                                $model->deviceProfile->trustLevel,
                                'short_desc',
                                $model->deviceProfile->trust_level_id
                            ),
                        ],
                    ],
                ]) ?>
            <?php endif; ?>
        </div>
    </div>



</div>
