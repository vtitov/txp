FROM yiisoftware/yii2-php:7.4-apache

RUN apt-get update -y

COPY . /app
RUN chgrp www-data /app/web/assets
RUN chmod g+w /app/web/assets
