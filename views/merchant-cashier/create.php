<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\MerchantCashier */

$this->title = \Yii::t('app', 'Добавление кассира');
$this->params['breadcrumbs'][] = ['label' => \Yii::t('app', 'Справочник Торговцев'), 'url' => ['mpos-merchants/index']];
$this->params['breadcrumbs'][] = ['label' => \yii\helpers\ArrayHelper::getValue($model->merchant, 'name'), 'url' => ['mpos-merchants/view', 'id' => $model->merchant_id]];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="spacer"></div>
<div class="merchant-cashier-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
