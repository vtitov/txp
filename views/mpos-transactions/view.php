<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\MposTransactions */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => \Yii::t('app', 'Журнал транзакций'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mpos-transactions-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <!--?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?-->
        <!--?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?-->
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            'date_time',
            [
                'attribute' => 'mpos_ped_id',
                'value' => $model->mposPed ?
                    Html::a(Html::encode($model->mposPed->serial_number), ['mpos-peds/view', 'id' => $model->mpos_ped_id]) :
                    ($model->mposPedArchive ?
                        Html::a(Html::encode($model->mposPedArchive->serial_number), ['mpos-peds-archive/view', 'id' => $model->mpos_ped_id]) :
                        null
                    ),
                'format' => 'html',
            ],
            [
                'attribute' => 'mpos_merchant_id',
                'value' => $model->mposMerchant ?
                    Html::a(Html::encode($model->mposMerchant->name), ['mpos-merchants/view', 'id' => $model->mpos_merchant_id]) :
                    null,
                'format' => 'html',
            ],
            [
                'attribute' => 'merchant_cashier_id',
                'value' => $model->merchantCashier ?
                    ($model->merchantCashier->description != '' ? $model->merchantCashier->description : $model->merchantCashier->login) :
                    null,
            ],
            [
                'attribute' => 'payments_terminal_id',
                'value' => ArrayHelper::getValue($model->paymentTerminal, 'name'),
            ],
            'bdd',
            'stan',
            'authcode',
            [
                'attribute' => 'mpos_transaction_status_id',
                'value' => ArrayHelper::getValue($model->transactionStatus, 'description'),
            ],
            [
                'attribute' => 'transaction_type_id',
                'value' => ArrayHelper::getValue($model->transactionType, 'desc'),
            ],
            'batch',
            'amount',
            [
                'attribute' => 'currency_id',
                'value' => ArrayHelper::getValue($model->currency, 'alpha_code'),
            ],
            'card',
            'rrn',
            [
                'attribute' => 'financial_original_trx_id',
                'value' => is_null($model->transactionslogModel) ? Html::encode($model->financial_original_trx_id) : Html::a(Html::encode($model->financial_original_trx_id), ['/transactionslog/view', 'id' => $model->financial_original_trx_id]),
                'format' => 'html',
            ],

            'financial_reverse_trx_id',
            [
                'attribute' => 'id',
                'label' => \Yii::t('app', 'Подпись'),
                'value' => is_null($model->signature) ? null : Html::img('data:image/jpg;base64,' . base64_encode($model->signature->image)),
                //'value' => Html::img('/mpos/web/index.php?r=mpos-transactions%2Fview-signature&id='.$model->id),
                'format' => 'raw',
            ],
        ],
    ]) ?>

    <?= Html::a(\Yii::t('app', 'Просмотр и печать чека'), ['receipt-view', 'id' => $model->id], ['class' => 'btn btn-primary', 'target' => 'blanc']) ?>

</div>
