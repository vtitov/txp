<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\MposPedsPreorder */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => \Yii::t('app', 'Просмотр предзаказов устройств'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<style>
    .status-button-container{
        display: inline-block;
        margin-bottom: 5px;
    }
</style>

<div class="mpos-peds-preorder-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php if(Yii::$app->session->hasFlash('priorXmlFileCreated')): ?>
        <div class="alert alert-success">
            <?= Yii::t('app', 'XML-файл с информацией о предзаказе был успешно создан') ?>
        </div>
    <?php endif; ?>

    <p>
        <?php foreach ($statuses as $key => $value): ?>

    <div class="status-button-container">
        <?= Html::a($value, ['change-status', 'id' => $model->id, 'status' => $key], ['class' => $model->ped_preorder_status_id == $key ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    <?php endforeach; ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            'name',
            'registered_address',
            'post_address',
            [
                'attribute' => 'unp',
                'value' => Html::encode($model->unp) . " " . Html::a(Yii::t('app', 'Проверить данные'), 'http://www.portal.nalog.gov.by/grp/getData?unp=' . Html::encode($model->unp), ['class' => 'btn btn-primary', 'target' => 'blank']),
                'format' => 'raw',
            ],
            'okpo',
            'email:email',
            'mobile_phone',
            'banks_mfo',
            'bic',
            'bank_account',
            'iban',
            [
                'attribute' => 'ped_preorder_delivery_type_id',
                'value' => $model->pedPreorderDeliveryType->name,
            ],
            [
                'attribute' => 'ped_preorder_delivery_point_id',
                'value' => $model->ped_preorder_delivery_type_id == 1 ? $model->pedPreorderDeliveryPoint->address : NULL,
            ],
            [
                'attribute' => 'delivery_address',
                'value' => $model->ped_preorder_delivery_type_id != 1 ? $model->delivery_address : NULL,
            ],
            [
                'attribute' => 'ped_preorder_status_id',
                'value' => $model->pedPreorderStatus->name,
            ],
            'delivery_contact_person_fio',
            'device_number',
            'add_date',

            //доп поля, информация для приора
            'head_full_name',
            'organisation_phone',
        ],
    ]) ?>

    <p>
        <?= Html::a(\Yii::t('app', 'Зарегистрировать торговца'), ['create-merchant', 'id' => $model->id], ['class' => 'btn btn-success']) ?>
        <?= Html::a(\Yii::t('app', 'Создать XML-файл для Приорбанка'), ['create-prior-xml', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>

</div>