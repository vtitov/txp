<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Mcc */

$this->title = \Yii::t('app', 'Редактирование МСС: ') . $model->range_start_code;
$this->params['breadcrumbs'][] = ['label' => \Yii::t('app', 'Справочник МСС-кодов для сайта payBYcard'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->range_start_code, 'url' => ['view', 'id' => $model->range_start_code]];
$this->params['breadcrumbs'][] = \Yii::t('app', 'Редактирование');
?>
<div class="mcc-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
