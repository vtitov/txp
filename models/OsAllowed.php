<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "os_allowed".
 *
 * @property int $id
 * @property string $name
 * @property int $allow
 * @property string $minimal_client_ver
 */
class OsAllowed extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'os_allowed';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('mpos');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            /*[['id', 'name'], 'required'],
            [['id', 'allow'], 'integer'],
            [['name'], 'string', 'max' => 50],
            [['minimal_client_ver'], 'string', 'max' => 20],
            [['id'], 'unique'],*/
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'allow' => 'Allow',
            'minimal_client_ver' => 'Minimal Client Ver',
        ];
    }
}
