<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model \app\modules\UserManagement\models\User */

$this->title = $model->username;
$this->params['breadcrumbs'][] = ['label' => \Yii::t('app', 'Пользователи'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(\Yii::t('app', 'Редактировать'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(\Yii::t('app', 'Удалить'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => \Yii::t('app', 'Вы действительно хотите удалить данного пользователя?'),
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a(\Yii::t('app', 'Назначить роли'), ['manage-user-roles', 'id' => $model->id], ['class' => 'btn btn-default']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            [
                'attribute' => 'owner_id',
                'value' => ($owner = \app\models\Owners::find()->where(['id' => $model->owner_id, 'locale' => Yii::$app->language])->one()) ? $owner->name : $model->owner_id,
            ],
            'username',
            //'auth_key',
            //'password_hash',
            //'temp_password',
            [
                'attribute' => 'temp_user',
                'value' => $model->temp_user ? \Yii::t('app', 'Да') : \Yii::t('app', 'Нет'),
            ],
            'temp_user_active_from',
            'temp_user_active_to',
            [
                'attribute' => 'status',
                'value' => \app\modules\UserManagement\models\User::getStatuses()[$model->status] . ' ' .
                    Html::a($model->status ? \Yii::t('app', 'Заблокировать') : \Yii::t('app', 'Разблокировать'),
                        ['change-status', 'id' => $model->id],
                        ['class' => $model->status ? 'btn btn-warning' : 'btn btn-success']
                    ),
                'format' => 'raw',
            ],
            'block_reason',
            'description',
            'phone',
            'email:email',
        ],
    ]) ?>

</div>
