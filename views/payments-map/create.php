<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PaymentsMap */

$this->title = 'Create Payments Map';
$this->params['breadcrumbs'][] = ['label' => 'Payments Maps', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payments-map-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
