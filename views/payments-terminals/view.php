<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\PaymentsTerminals */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => \Yii::t('app', 'Справочник платежных терминалов'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payments-terminals-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?php if ($model->id != 1) {//не даем редактировать платежный терминал "не задан"
            echo Html::a(
                \Yii::t('app', 'Редактировать'),
                ['update', 'id' => $model->id],
                ['class' => 'btn btn-primary']
            );
            echo ' ';
            echo Html::a(
                \Yii::t('app', 'Удалить'),
                ['delete', 'id' => $model->id],
                [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => \Yii::t('app', 'Вы действительно хотите удалить эту запись?'),
                        'method' => 'post',
                    ],
                ]
            );
            echo ' ';
            echo Html::a(
                \Yii::t('app', 'История закрытия бизнес-дней'),
                ['payment-terminal-settlement-log/index', 'PaymentTerminalSettlementLogSearch[payment_terminal_id]' => $model->name],
                ['class' => 'btn btn-default']
            );
        } ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            [
                'attribute' =>'merchant_id',
                'value' => $model->merchant ?
                    Html::a(Html::encode($model->merchant->name), ['mpos-merchants/view', 'id' => $model->merchant_id]) :
                    null,
                'format' => 'html',
            ],
            [
                'attribute' =>'iso_setting_id',
                'value' => $model->isoSettings->name,
            ],
            'name',
            'description',
            'zpk',
            'zak',
            'settl_time',
            [
                'attribute' => 'status',
                'value' => $model->paymentTerminalStatus->name,
            ],
            [
                'attribute' => 'ped_id',
                'value' => $model->ped ?
                    Html::a(Html::encode($model->ped->serial_number), ['mpos-peds/view', 'id' => $model->ped_id]) :
                    null,
                'format' => 'html',
            ],
            'activation_code',
            [
                'attribute' => 'is_softpos',
                'value' => $model->is_softpos ? '<span class="glyphicon glyphicon-ok"/>' : '<span class="glyphicon glyphicon-remove"/>',
                'format' => 'html',
            ],
            [
                'attribute' => 'counter_category_id',
                'value' => \yii\helpers\ArrayHelper::getValue($model->counterCategory, 'name'),
            ],
            'mcc',
        ],
    ]) ?>

</div>
