<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\MposMerchants */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mpos-merchants-form">

    <?php $form = ActiveForm::begin([
        'action' => $model->getIsNewRecord() ? Url::to(['mpos-merchants/create']) : Url::to(['mpos-merchants/update', 'id' => $model->id]),
    ]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'owner_id')->dropDownList(
        ArrayHelper::map(
            \app\models\Owners::find()
                ->select(['id', 'name'])
                ->where(['locale' => Yii::$app->language])
                ->asArray()
                ->all(),
            'id',
            'name'
        ),
        ['disabled' => Yii::$app->user->identity->owner_id != 1]
    ) ?>

    <?= $form->field($model, 'registered_address')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'post_address')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'unp')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'bic')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'iban')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'bank_account_currency_id')->dropDownList(
        ArrayHelper::map(app\models\Currency::find()->asArray()->all(), 'id', 'alpha_code')
    ) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'send_mail')->dropDownList(\app\models\MposMerchants::getNotificationChannels()) ?>

    <?php if (!$model->isNewRecord): ?>

        <?= $form->field($model, 'merchant_status_id')->dropDownList(
            ArrayHelper::map(
                app\models\MerchantStatusLocale::find()
                    ->where(['locale' => Yii::$app->language])
                    ->asArray()
                    ->all(),
                'id',
                'name'
            )
        ) ?>

    <?php endif; ?>

    <?= $form->field($model, 'contract')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'contract_date')->widget(\yii\jui\DatePicker::classname(), [
        'dateFormat' => 'php:Y-m-d',
    ]) ?>

    <?= $form->field($model, 'iso_merchant_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'terminal_counter_category_id')->dropDownList(
        ArrayHelper::map(app\models\CounterCategory::find()->select(['id', 'name'])->asArray()->all(), 'id', 'name'),
        ['prompt' => '']
    ) ?>

    <?= $form->field($model, 'merchant_counter_category_id')->dropDownList(
        ArrayHelper::map(app\models\CounterCategory::find()->select(['id', 'name'])->asArray()->all(), 'id', 'name'),
        ['prompt' => '']
    ) ?>

    <div class="form-group">
        <?= Html::submitButton(\Yii::t('app', 'Сохранить'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
