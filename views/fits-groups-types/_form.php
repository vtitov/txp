<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\FitsGroupsTypes */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="fits-groups-types-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'fit_name')->textInput(['maxlength' => 15]) ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => 100]) ?>

    <div class="form-group">
        <?= Html::submitButton(\Yii::t('app', 'Сохранить'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
