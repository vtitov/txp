<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "payments_map".
 *
 * @property string $ped_id
 * @property string $payments_id
 * @property integer $currency_id
 * @property string $fits_groups_id
 * @property string $fits_groups_id_sign_allowed
 * @property string $fits_groups_id_fallback_allowed
 *
 * @property PaymentLocale $payments
 * @property MposPeds $ped
 */
class PaymentsMap extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payment_map';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('mpos');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ped_id', 'payments_id'], 'required'],
            [['ped_id', 'payments_id', 'currency_id', 'fits_groups_id', 'fits_groups_id_sign_allowed', 'fits_groups_id_fallback_allowed'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ped_id' => \Yii::t('app', 'Устройство'),
            'payments_id' => \Yii::t('app', 'Платеж'),
            'currency_id' => \Yii::t('app', 'Валюта'),
            'fits_groups_id' => \Yii::t('app', 'Группа FIT'),
            'fits_groups_id_sign_allowed' => \Yii::t('app', 'Верификация подписью'),
            'fits_groups_id_fallback_allowed' => \Yii::t('app', 'Возможность fallback'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPayments()
    {
        return $this->hasOne(PaymentLocale::className(), ['id' => 'payments_id'])
            ->andOnCondition([PaymentLocale::tableName() . '.locale' => Yii::$app->language]);
    }

    public function getFitsGroups()
    {
        return $this->hasOne(FitsGroups::className(), ['id' => 'fits_groups_id']);
    }

    public function getFitsGroupsSign()
    {
        return $this->hasOne(FitsGroups::className(), ['id' => 'fits_groups_id_sign_allowed']);
    }

    public function getFitsGroupsFallback()
    {
        return $this->hasOne(FitsGroups::className(), ['id' => 'fits_groups_id_fallback_allowed']);
    }

    public function getCurrency()
    {
        return $this->hasOne(Currency::className(), ['id' => 'currency_id']);
    }
}
