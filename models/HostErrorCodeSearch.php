<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\HostErrorCode;

/**
 * HostErrorCodeSearch represents the model behind the search form about `app\models\HostErrorCode`.
 */
class HostErrorCodeSearch extends HostErrorCode
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'iso_settings_id'], 'integer'],
            [['descr'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = HostErrorCode::find()
            ->with(['isoSettings']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'iso_settings_id' => $this->iso_settings_id,
        ]);

        $query->andFilterWhere(['like', 'descr', $this->descr]);

        return $dataProvider;
    }
}
