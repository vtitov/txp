<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\PublishedReviews;

/**
 * PublishedReviewsSearch represents the model behind the search form about `app\models\PublishedReviews`.
 */
class PublishedReviewsSearch extends PublishedReviews
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'recieved_review_id'], 'integer'],
            [['name', 'text', 'date_recieved'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PublishedReviews::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['date_recieved' => SORT_DESC]],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'recieved_review_id' => $this->recieved_review_id,
            'date_recieved' => $this->date_recieved,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'text', $this->text]);

        return $dataProvider;
    }
}
