<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model \yii\base\DynamicModel */
/* @var $result array|false */

$this->title = 'Итоговая сумма за месяц';
if ($model->bank == '0') {
    $subheading = 'Беларусбанк (mPOS) ';
} else if ($model->bank == '1') {
    $subheading = 'Приорбанк (mPOS) ';
} else if ($model->bank == '2') {
    $subheading = 'Приорбанк (SoftPOS) ';
} else {
    //вообще контроллер здесь должен выкинуть UserException, так что этот заголовок никогда не должен быть виден
    $subheading = 'Unknown ';
}

$subheading .= $model->year . '-' . $model->month;
?>

<div class="report-result">

    <h1><?= Html::encode($this->title) ?></h1>
    <h2><?= Html::encode($subheading) ?></h2>

    <?php if ($result){
        echo \yii\widgets\DetailView::widget([
            'model' => $result,
            'attributes' => [
                [
                    'attribute' => 'total_income',
                    'label' => 'Сумма',
                ],
                [
                    'attribute' => 'curr',
                    'label' => 'Валюта',
                ],
                [
                    'attribute' => 'ops_counter',
                    'label' => 'Счетчик операций',
                ],
            ]
        ]);
    } else {
        echo '<div class="alert alert-warning">Данные за указанный месяц не найдены</div>';
    } ?>


</div>