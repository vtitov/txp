<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MposMerchants */

$this->title = \Yii::t('app', 'Редактировать Торговца: ') . $model->name;
$this->params['breadcrumbs'][] = ['label' => \Yii::t('app', 'Справочник Торговцев'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = \Yii::t('app', 'Редактировать');
?>
<div class="mpos-merchants-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
