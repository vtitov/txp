<?php

use yii\helpers\Html;

?>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode(Yii::t('app', 'Чек')) ?></title>
    <style>
        .receipt {
            border: 1px solid #000000;
            display: inline-block;
            margin: 10px;
            padding: 10px;
        }

        #print-button {
            display: inline-block;
            margin-top: 10px;
            margin-left: 10px;
            padding: 10px 20px;
            border-radius: 5px;
            cursor: pointer;
            background-color: rgb(40, 96, 144);
            color: white;
            font-size: 1.1em;
        }
    </style>
</head>
<body>

<div class="receipt-view">

    <div class="receipt">
        <div class="body-container">
            <?php
            if (trim($receipt->email) != '') {
                echo nl2br($receipt->email);
            } else {
                echo nl2br($receipt->sms);
            }
            ?>
        </div>

    </div>

</div>

<div id="print-button"><?= Yii::t('app', 'Распечатать') ?></div>

<script>
    document.getElementById("print-button").onclick = function () {
        var restorepage = document.body.innerHTML;
        var printcontent = document.getElementsByClassName("receipt-view")[0].innerHTML;
        document.body.innerHTML = printcontent;
        window.print();
        document.body.innerHTML = restorepage;
    };
</script>
</body>
</html>