<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PedPreorderStatus */

$this->title = \Yii::t('app', 'Редактирование статуса предзаказа: ') . $model->name;
$this->params['breadcrumbs'][] = ['label' => \Yii::t('app', 'Справочник статусов предзаказов'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = \Yii::t('app', 'Редактирование');
?>
<div class="ped-preorder-status-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
