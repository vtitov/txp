<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "email_sending".
 *
 * @property string $id
 * @property string $name
 * @property string $unp
 * @property string $address
 * @property string $contact_info
 * @property string $merchant_type
 * @property string $url
 * @property string $email
 * @property string $additional_email
 * @property integer $status
 * @property string $date_send
 * @property resource $message_text
 */
class EmailSending extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'email_sending';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('paybycard');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'email'], 'required'],//, 'unp'
            [['status'], 'integer'],
            [['date_send'], 'safe'],
            [['message_text'], 'string'],
            [['unp'], 'string', 'min' => 9, 'max' => 9],
            [['name', 'address', 'contact_info', 'merchant_type'], 'string', 'max' => 255],
            [['url', 'email', 'additional_email'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'unp' => 'Unp',
            'address' => 'Address',
            'contact_info' => 'Contact Info',
            'merchant_type' => 'Merchant Type',
            'url' => 'Url',
            'email' => 'Email',
            'additional_email' => 'Additional Email',
            'status' => 'Status',
            'date_send' => 'Date Send',
            'message_text' => 'Message Text',
        ];
    }
}
