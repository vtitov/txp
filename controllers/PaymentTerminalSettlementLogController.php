<?php

namespace app\controllers;

use app\models\PaymentTerminalSettlementLog;
use app\models\PaymentTerminalSettlementLogSearch;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * PaymentTerminalSettlementLogController implements the CRUD actions for PaymentTerminalSettlementLog model.
 */
class PaymentTerminalSettlementLogController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view'],
                        'roles' => ['paymentTerminalSettlementLogView'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all PaymentTerminalSettlementLog models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PaymentTerminalSettlementLogSearch();
        $searchModel->sett_date_time = date('Y-m-d', strtotime('-29 days')) . ' - ' . date('Y-m-d');
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single PaymentTerminalSettlementLog model.
     * @param integer $payment_terminal_id
     * @param string $sett_date_time
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($payment_terminal_id, $sett_date_time)
    {
        return $this->render('view', [
            'model' => $this->findModel($payment_terminal_id, $sett_date_time),
        ]);
    }

    /**
     * Finds the PaymentTerminalSettlementLog model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $payment_terminal_id
     * @param string $sett_date_time
     * @return PaymentTerminalSettlementLog the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($payment_terminal_id, $sett_date_time)
    {
        $owner_id = Yii::$app->user->identity->owner_id;
        $model = PaymentTerminalSettlementLog::findOne(['payment_terminal_id' => $payment_terminal_id, 'sett_date_time' => $sett_date_time]);
        $isoSetting = ArrayHelper::getValue(ArrayHelper::getValue($model, 'paymentTerminal'), 'isoSettings');
        if ($model !== null && ($owner_id == 1 || ($isoSetting !== null && $isoSetting->owner_id == $owner_id))) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
