<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PaymentTerminalSettlementLogSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="payment-terminal-settlement-log-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'payment_terminal_id') ?>

    <?= $form->field($model, 'bdd') ?>

    <?= $form->field($model, 'bddn') ?>

    <?= $form->field($model, 'trx_counter') ?>

    <?= $form->field($model, 'sett_date_time') ?>

    <?php // echo $form->field($model, 'sett_result') ?>

    <?php // echo $form->field($model, 'batch_status_id') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
