<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PedPreorderPriorbankResponse */

$this->title = 'Create Ped Preorder Priorbank Response';
$this->params['breadcrumbs'][] = ['label' => 'Ped Preorder Priorbank Responses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ped-preorder-priorbank-response-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
