<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\MposPedManufacturers */

$this->title = \Yii::t('app', 'Добавление производителя');
$this->params['breadcrumbs'][] = ['label' => \Yii::t('app', 'Производители устройств'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mpos-ped-manufacturers-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
