<?php

namespace app\modules\UserManagement\models;

use app\modules\UserManagement\helpers\AwsCognitoHelper;
use Aws\Exception\AwsException;
use Aws\Result;
use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 */
class AwsChangePasswordForm extends Model
{
    public $password;
    public $password_repeat;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['password', 'password_repeat'], 'required'],
            [
                ['password'],
                'match',
                'pattern' => '|^\S*(?=\S{8,})(?=\S*[A-ZА-ЯЁ])(?=\S*[a-zа-яё])(?=\S*[\d])(?=\S*[\=\+\-\^\$\*\.\[\]\{\}\(\)\?\"\!\@\#\%\&\/\\\,\>\<\'\:\;\|\_\~\`])\S*$|',
                'message' => Yii::t('app', 'Пароль должен быть не короче 8 символов, содержать буквы верхнего и нижнего регистра, цифры и спецсимволы')
            ],
            [['password_repeat'], 'compare', 'compareAttribute' => 'password'],
        ];
    }


    /**
     * смена пароля по требованию при логине
     * @return Result|false
     */
    public function requiredPasswordChange()
    {
        if (!$this->validate()) {
            return false;
        }

        Yii::info('AWS password change required');

        try {
            $client = AwsCognitoHelper::getClient();
            $response = $client->adminRespondToAuthChallenge([
                'ChallengeName' => 'NEW_PASSWORD_REQUIRED',
                'ChallengeResponses' => [
                    'USERNAME' => Yii::$app->session->get('aws_username'),
                    'NEW_PASSWORD' => $this->password,
                    'SECRET_HASH' => AwsCognitoHelper::getSignature(Yii::$app->session->get('aws_username')),
                ],
                'ClientId' => Yii::$app->getModule('user-management')->params['awsParams']['AWS_CLIENT_ID'],
                'Session' => Yii::$app->session->get('aws_cognito_session'),
                'UserPoolId' => Yii::$app->getModule('user-management')->params['awsParams']['AWS_USERPOOL_ID'],
            ]);

            if (YII_DEBUG) {
                Yii::info('AWS adminRespondToAuthChallenge response: ' . print_r($response, true));
            }

        } catch (AwsException $e) {
            if ($e->getAwsErrorCode() == 'NotAuthorizedException') {
                Yii::warning('AWS Exception: ' . $e->getAwsErrorMessage());
                Yii::$app->session->setFlash('awsRequestError', Yii::t('app', 'Сессия истекла'));
                return false;
            } else if ($e->getAwsErrorCode() == 'InvalidPasswordException') {
                Yii::warning('AWS Exception: ' . $e->getAwsErrorMessage());
                Yii::$app->session->setFlash('awsRequestError', Yii::t('app', $e->getAwsErrorMessage()));
                $this->addError('password', Yii::t('app', 'Пароль не удовлетворяет требованиям безопасности'));
                return false;
            } else {
                Yii::error($e->getAwsErrorMessage());
                throw $e;
            }
        }

        Yii::$app->session->set('aws_cognito_session', $response['Session']);

        return $response;
    }


    /**
     * смена пароля по желанию пользователя
     */
    public function userPasswordChange()
    {
        if (!$this->validate()) {
            return false;
        }

        Yii::info('AWS user changes his password');

        try {
            $client = AwsCognitoHelper::getClient();
            $response = $client->adminSetUserPassword([
                'Password' => $this->password,
                'Permanent' => true,
                'UserPoolId' => Yii::$app->getModule('user-management')->params['awsParams']['AWS_USERPOOL_ID'],
                'Username' => Yii::$app->user->identity->username,
            ]);

        } catch (AwsException $e) {
            if ($e->getAwsErrorCode() == 'InvalidPasswordException') {
                Yii::warning('AWS Exception: ' . $e->getAwsErrorMessage());
                Yii::$app->session->setFlash('awsRequestError', Yii::t('app', $e->getAwsErrorMessage()));
                $this->addError('password', Yii::t('app', 'Пароль не удовлетворяет требованиям безопасности'));
                return false;
            } else {
                Yii::error($e->getAwsErrorMessage());
                throw $e;
            }
        }

        Yii::$app->session->setFlash('awsUserPasswordChanged', Yii::t('app', 'Пароль успешно изменен'));
        $this->password = '';
        $this->password_repeat = '';

        return $response;
    }



    public function attributeLabels()
    {
        return [
            'password' => \Yii::t('app', 'Пароль'),
            'password_repeat' => \Yii::t('app', 'Подтверждение пароля'),
        ];
    }
}
