<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "payment_terminal_current_batch".
 *
 * @property int $payment_terminal_id
 * @property string $bdd
 * @property int $bddn
 * @property int $trx_counter
 * @property int $batch_status_id
 *
 * @property PaymentsTerminals $paymentTerminal
 * @property BatchStatusLocale $batchStatus
 * @property MposPeds $ped
 */
class PaymentTerminalCurrentBatch extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payment_terminal_current_batch';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('mpos');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            /*[['payment_terminal_id', 'bdd', 'bddn'], 'required'],
            [['payment_terminal_id', 'bddn', 'trx_counter', 'batch_status_id'], 'integer'],
            [['bdd'], 'safe']*/
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'payment_terminal_id' => \Yii::t('app', 'Платежный терминал'),
            'bdd' => \Yii::t('app', 'Бизнес-день'),
            'bddn' => \Yii::t('app', 'Номер пачки в бизнес-дне'),
            'trx_counter' => \Yii::t('app', 'Количество операций'),
            'batch_status_id' => \Yii::t('app', 'Статус'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentTerminal()
    {
        return $this->hasOne(PaymentsTerminals::className(), ['id' => 'payment_terminal_id']);
    }

    public function getBatchStatus()
    {
        return $this->hasOne(BatchStatusLocale::className(), ['id' => 'batch_status_id'])
            ->andOnCondition([BatchStatusLocale::tableName() . '.locale' => Yii::$app->language]);
    }

    public function getPed()
    {
        return $this->hasOne(MposPeds::className(), ['payment_terminal_id' => 'payment_terminal_id']);
    }
}
