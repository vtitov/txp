<?php

namespace app\controllers;

use app\models\MposTransactions;
use app\models\TransactionArchive;
use Yii;
use app\models\MerchantCashier;
use app\models\MerchantCashierSearch;
use yii\base\UserException;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * MerchantCashierController implements the CRUD actions for MerchantCashier model.
 */
class MerchantCashierController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['view'],
                        'roles' => ['merchantView'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['create', 'update', 'reset-password', 'delete'],
                        'roles' => ['cashierEdit'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'reset-password' => ['POST'],
                ],
            ],
        ];
    }

    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new MerchantCashier model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($merchant_id)
    {
        $model = new MerchantCashier();
        $model->merchant_id = $merchant_id;

        if ($model->load(Yii::$app->request->post())) {
            $model->password = sha1($model->login);
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing MerchantCashier model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        //старшего кассира редактировать нельзя
        if ($model->merchant->chief_cashier_id == $id) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionResetPassword($id)
    {
        $model = $this->findModel($id);
        $model->password = sha1($model->login);
        $model->login_attempts_count = 0;
        $model->merchant_cashier_status_id = 1; //статус Active
        if ($model->save()) {
            Yii::$app->session->addFlash('success', Yii::t('app', 'Пароль успешно изменен'));
        } else {
            Yii::$app->session->addFlash('error', Yii::t('app', 'Произошла ошибка, пароль не изменен'));
        }

        return $this->redirect(['view', 'id' => $id]);
    }

    /**
     * Deletes an existing MerchantCashier model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        //старшего кассира удалять нельзя
        if ($model->merchant->chief_cashier_id == $id) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        if (
            MposTransactions::find()->where(['merchant_cashier_id' => $id])->exists() ||
            TransactionArchive::find()->where(['merchant_cashier_id' => $id])->exists()
        ) {
            Yii::$app->session->addFlash('error', Yii::t('app', 'Кассира нельзя удалить. В базе существуют проведенные им транзакции.'));
            return $this->redirect(['view', 'id' => $id]);
        }

        $model->delete();

        return $this->redirect(['mpos-merchants/view', 'id' => $model->merchant_id]);
    }

    /**
     * Finds the MerchantCashier model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MerchantCashier the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        $owner_id = Yii::$app->user->identity->owner_id;
        $model = MerchantCashier::findOne($id);

        if ($model !== null && $model->merchant !== null && ($owner_id == 1 || $model->merchant->owner_id == $owner_id)) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
