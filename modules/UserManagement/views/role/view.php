<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model \app\modules\UserManagement\models\AuthItem */

$this->title = $role->name;
$this->params['breadcrumbs'][] = ['label' => \Yii::t('app', 'Роли'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="auth-item-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(\Yii::t('app', 'Редактировать'), ['update', 'id' => $role->name], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(\Yii::t('app', 'Удалить'), ['delete', 'id' => $role->name], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => \Yii::t('app', 'Вы действительно хотите удалить эту роль?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $role,
        'attributes' => [
            [
                'attribute' => 'name',
                'label' => \Yii::t('app', 'Название'),
            ],
            [
                'attribute' => 'description',
                'format' => 'ntext',
                'label' => \Yii::t('app', 'Описание'),
            ],
        ],
    ]) ?>

</div>

<div class="role-permissions-form">

    <h3><?= \Yii::t('app', 'Права') ?></h3>

    <?php
    if (Yii::$app->session->hasFlash('rolePermissionsAssigned')) {
        echo '<div class="alert alert-success">'.\Yii::t('app', 'Права роли изменены').'</div>';
    }
    if (Yii::$app->session->hasFlash('invalidDataRecieved')) {
        echo '<div class="alert alert-danger">'.\Yii::t('app', 'Получены некорректные данные').'</div>';
    }
    ?>

    <?php $form = ActiveForm::begin([
        'action' => ['manage-role-permissions', 'id' => $role->name],
    ]); ?>

    <div class="checkbox-list-container">

        <?php
        echo \yii\bootstrap\Collapse::widget([
            'items' => [
                [
                    'label' => 'mPOS',
                    'content' => renderPermissionCheckBoxes([
                        'pedView' => \Yii::t('app', 'Просмотр справочника устройств'),
                        'pedEdit' => \Yii::t('app', 'Редактирование справочника устройств'),
                        'pedArchiveView' => \Yii::t('app', 'Просмотр архива устройств'),
                        'merchantView' => \Yii::t('app', 'Просмотр справочника Торговцев'),
                        'merchantEdit' => \Yii::t('app', 'Редактирование справочника Торговцев'),
                        'cashierEdit' => \Yii::t('app', 'Редактирование кассиров Торговца'),
                        'merchantPedAssign' => \Yii::t('app', 'Привязка устройства к Торговцу'),
                        'merchantMessage' => \Yii::t('app', 'Отправка сообщений Торговцу'),
                        'paymentTerminalView' => \Yii::t('app', 'Просмотр справочника платежных терминалов'),
                        'paymentTerminalEdit' => \Yii::t('app', 'Редактирование справочника платежных терминалов'),
                        'transactionView' => \Yii::t('app', 'Просмотр журнала транзакций'),
                        'transactionslogView' => \Yii::t('app', 'Просмотр Transactionslog'),
                        'transactionslogArchiveView' => \Yii::t('app', 'Просмотр TransactionslogArchive'),
                        'paymentTerminalCurrentBatchView' => \Yii::t('app', 'Просмотр незакрытых бизнес-дней'),
                        'paymentTerminalCurrentBatchClose' => \Yii::t('app', 'Закрытие бизнес-дней'),
                        'paymentTerminalSettlementLogView' => \Yii::t('app', 'Просмотр истории закрытия бизнес-дней'),
                        'auditLogView' => \Yii::t('app', 'Просмотр журнала аудита'),
                    ], $rolePermissions),
                ],
                [
                    'label' => Yii::t('app', 'Заявки'),
                    'content' => renderPermissionCheckBoxes([
                        'pedPreorderView' => Yii::t('app', 'Просмотр предзаказов на устройства'),
                        'pedPreorderHandle' => Yii::t('app', 'Обработка предзаказов на устройства'),
                        'kazpostTxpRequestView' => Yii::t('app', 'Просмотр заявок на подключение tapXphone Казпочта'),
                        'kazpostTxpRequestEdit' => Yii::t('app', 'Смена статуса заявок на подключение tapXphone Казпочта'),
                        'priorTxpRequestView' => Yii::t('app', 'Просмотр заявок на подключение tapXphone Приорбанк'),
                        'priorTxpRequestEdit' => Yii::t('app', 'Смена статуса заявок на подключение tapXphone Приорбанк'),
                        'priorMposRequestView' => Yii::t('app', 'Просмотр заявок на подключение mPOS Приорбанк'),
                        'priorMposRequestEdit' => Yii::t('app', 'Смена статуса заявок на подключение mPOS Приорбанк'),
                    ], $rolePermissions),
                ],
                [
                    'label' => \Yii::t('app', 'Конфигурация'),
                    'content' => renderPermissionCheckBoxes([
                        'pedManufacturerView' => Yii::t('app', 'Просмотр справочника производителей'),
                        'pedManufacturerEdit' => Yii::t('app', 'Редактирование справочника производителей'),
                        'pedModelView' => Yii::t('app', 'Просмотр справочника моделей'),
                        'pedModelEdit' => Yii::t('app', 'Редактирование справочника моделей'),
                        'pedStatusView' => \Yii::t('app', 'Просмотр справочника статусов устройств'),
                        'pedStatusEdit' => \Yii::t('app', 'Редактирование справочника статусов устройств'),
                        'bankView' => \Yii::t('app', 'Просмотр справочника банков'),
                        'bankEdit' => \Yii::t('app', 'Редактирование справочника банков'),
                        'merchantStatusView' => \Yii::t('app', 'Просмотр справочника статусов Торговцев'),
                        'merchantStatusEdit' => \Yii::t('app', 'Редактирование справочника статусов Торговцев'),
                        'isoSettingView' => \Yii::t('app', 'Просмотр справочника настроек ISO'),
                        'isoSettingEdit' => \Yii::t('app', 'Редактирование справочника настроек ISO'),
                        'isoSettingPaymentEdit' => \Yii::t('app', 'Редактирование платежей настройки ISO'),
                        'paymentView' => \Yii::t('app', 'Просмотр справочника платежей'),
                        'ipsTypeView' => Yii::t('app', 'Просмотр справочника карт МПС'),
                        'ipsTypeEdit' => Yii::t('app', 'Редактирование справочника карт МПС'),
                        'fitView' => Yii::t('app', 'Просмотр справочника FIT'),
                        'fitEdit' => Yii::t('app', 'Редактирование справочника FIT'),
                        'fitGroupView' => Yii::t('app', 'Просмотр справочника групп FIT'),
                        'fitGroupEdit' => Yii::t('app', 'Редактирование справочника групп FIT'),
                        'fitGroupTypeView' => Yii::t('app', 'Просмотр справочника типов групп FIT'),
                        'fitGroupTypeEdit' => Yii::t('app', 'Редактирование справочника типов групп FIT'),
                        'pedPreorderStatusView' => Yii::t('app', 'Просмотр справочника статусов предзаказов'),
                        'pedPreorderStatusEdit' => Yii::t('app', 'Редактирование справочника статусов предзаказов'),
                        'clientErrorView' => \Yii::t('app', 'Просмотр справочника ошибок'),
                        'clientErrorEdit' => \Yii::t('app', 'Редактирование справочника ошибок'),
                        'hostErrorCodeView' => \Yii::t('app', 'Просмотр справочника Response Code\'ов'),
                        'mccView' => \Yii::t('app', 'Просмотр справочника MCC кодов'),
                        'mccEdit' => \Yii::t('app', 'Редактирование справочника MCC кодов'),
                        'appKeyView' => \Yii::t('app', 'Просмотр ключей app key'),
                        'appKeyEdit' => \Yii::t('app', 'Редактирование ключей app key'),
                    ], $rolePermissions),
                ],
                [
                    'label' => \Yii::t('app', 'Оповещение'),
                    'content' => renderPermissionCheckBoxes([
                        'messageMerchant' => \Yii::t('app', 'Рассылка сообщений всем Торговцам'),
                        'messageUser' => \Yii::t('app', 'Рассылка сообщений всем пользователям АРМ'),
                        'messageSms' => \Yii::t('app', 'Отправка SMS на произвольный номер'),
                    ], $rolePermissions),
                ],
                [
                    'label' => \Yii::t('app', 'Отчеты'),
                    'content' => renderPermissionCheckBoxes([
                        'runMonthSumReport' => Yii::t('app', 'Генерация отчета по итоговой сумме за месяц'),
                        'runTransactionIpsReport' => Yii::t('app', 'Генерация отчета по транзакциям в разрезе МПС'),
                        'runTransactionCountAcquirerReport' => Yii::t('app', 'Генерация отчета по количеству транзакций'),
                        'runTransactionDetailedReportRbi' => Yii::t('app', 'Генерация детального отчета по транзакциям'),
                    ], $rolePermissions),
                ],
                [
                    'label' => \Yii::t('app', 'payBYcard'),
                    'content' => renderPermissionCheckBoxes([
                        'newsView' => Yii::t('app', 'Просмотр новостей сайта'),
                        'newsEdit' => Yii::t('app', 'Редактирование новостей сайта'),
                        'reviewView' => Yii::t('app', 'Просмотр полученных и опубликованных отзывов'),
                        'reviewEdit' => Yii::t('app', 'Публикация полученных и редактирование опубликованных отзывов'),
                        'merchantLocalityView' => Yii::t('app', 'Просмотр справочника населенных пунктов и MCC'),
                        'merchantLocalityEdit' => Yii::t('app', 'Редактирование справочника населенных пунктов и MCC'),
                    ], $rolePermissions),
                ],
                [
                    'label' => \Yii::t('app', 'Пользователи'),
                    'content' => renderPermissionCheckBoxes([
                        'userView' => \Yii::t('app', 'Просмотр информации о пользователях'),
                        'userEdit' => \Yii::t('app', 'Редактирование данных пользователей'),
                        'roleView' => \Yii::t('app', 'Просмотр информации о ролях'),
                        'roleEdit' => \Yii::t('app', 'Редактирование ролей'),
                        'txpApiUserView' => \Yii::t('app', 'Просмотр информации о пользователях API'),
                        'txpApiUserEdit' => \Yii::t('app', 'Редактирование данных пользователей API'),
                    ], $rolePermissions),
                ],
            ]
        ]);
        ?>

    </div>

    <div class="form-group">
        <?= Html::submitButton(\Yii::t('app', 'Сохранить'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php
function renderPermissionCheckBoxes($permissionNames, $rolePermissions) {
    $str = '';
    foreach ($permissionNames as $name => $description) {
        $str .= '<div class="checkbox-container">';
        $str .= Html::checkbox('DynamicModel[permissions][]',
            isset($rolePermissions[$name]),
            ['value' => $name, 'label' => $description]);
        $str .= '</div>';
    }
    return $str;
}
?>
