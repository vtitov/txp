<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "payment_terminal_status_locale".
 *
 * @property int $id
 * @property string $locale
 * @property string $name
 */
class PaymentTerminalStatusLocale extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'payment_terminal_status_locale';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('mpos');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [];
    }
}
