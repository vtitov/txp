<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PublishedReviewsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Опубликованные отзывы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="published-reviews-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить новый отзыв', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            //'recieved_review_id',
            'name',
            'text:ntext',
            'date_recieved',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
