<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\MposMerchantStatuses */

$this->title = \Yii::t('app', 'Добавить статус');
$this->params['breadcrumbs'][] = ['label' => \Yii::t('app', 'Справочник статусов Торговцев'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mpos-merchant-statuses-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
