<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;

$this->title = \Yii::t('app', 'Отправка SMS на произвольный номер');
?>

    <div class="send-sms">

        <h1><?= Html::encode($this->title) ?></h1>

        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'phone')->textInput(['placeholder' => 'ZZZXXYYYYYYY']) ?>

        <?= $form->field($model, 'text')->textInput() ?>

        <span id="sms-length-counter" style="color: #808080; margin-bottom: 15px"></span>

        <div class="form-group">
            <?= Html::submitButton(\Yii::t('app', 'Отправить'), ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

<?php
$js = "$('#sendsmsform-text').keyup(function(){
    l = $('#sendsmsform-text').val().length;
    if (l == 0) {
        $('#sms-length-counter').html('');
    } else if (l <= 70) {
        $('#sms-length-counter').html('" . Yii::t('app', 'Количество символов в сообщении: ') . "' + l + ' (1 SMS)');
    } else {
        $('#sms-length-counter').html('" . Yii::t('app', 'Количество символов в сообщении: ') . "' + l + ' ('+ Math.ceil(l/67) + ' SMS)');
    }
});";
$this->registerJs($js);
?>