<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mcc".
 *
 * @property string $range_start_code
 * @property string $range_end_code
 * @property string $description
 */
class Mcc extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mcc';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('mpos');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['range_start_code', 'range_end_code'], 'required'],
            [['range_start_code', 'range_end_code'], 'string', 'max' => 4],
            [['range_start_code', 'range_end_code'], 'match', 'pattern' => '/^\d{4}$/', 'message' => \Yii::t('app', 'MCC-код должен состоять из четырех цифр')],
            [['description'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'range_start_code' => \Yii::t('app', 'Начало диапазона'),
            'range_end_code' => \Yii::t('app', 'Конец диапазона'),
            'description' => \Yii::t('app', 'Описание'),
        ];
    }
}
