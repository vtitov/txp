<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mpos_ped_statuses".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 *
 * @property MposPeds[] $mposPeds
 */
class MposPedStatuses extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ped_status';
    }
	
	public static function getDb()
    {
        return Yii::$app->get('mpos');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 40],
            ['name', 'required'],
            ['name', 'unique'],
            [['description'], 'string', 'max' => 255],
            ['color', 'string', 'max' => 7]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => \Yii::t('app', 'Название'),
            'description' => \Yii::t('app', 'Описание'),
            'color' => \Yii::t('app', 'Цвет'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMposPeds()
    {
        return $this->hasMany(MposPeds::className(), ['mpos_ped_status_id' => 'id']);
    }
}
