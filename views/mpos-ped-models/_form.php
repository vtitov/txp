<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\MposPedModels */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mpos-ped-models-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => 40]) ?>

    <?= $form->field($model, 'ped_vendor_id')->dropDownList(
		ArrayHelper::map(app\models\MposPedManufacturers::find()->asArray()->all(), 'id', 'name')
	) ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'pts_appr_number')->textInput(['maxlength' => 7]) ?>

    <?= $form->field($model, 'pts_exp_date')->widget(\yii\jui\DatePicker::classname(), [
        'dateFormat' => 'php:Y-m-d',
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton(\Yii::t('app', 'Сохранить'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
