<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\IsoSettings */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => \Yii::t('app', 'Справочник ISO настроек'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$paymentFilter = ArrayHelper::map(
    \app\models\PaymentLocale::find()
        ->select(['id', 'name'])
        ->where(['locale' => Yii::$app->language])
        ->asArray()
        ->all(),
    'id',
    'name'
);
$currencyFilter = ArrayHelper::map(
    \app\models\Currency::find()
        ->select(['id', 'alpha_code'])
        ->asArray()
        ->all(),
    'id',
    'alpha_code'
);
?>
<div class="iso-settings-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(\Yii::t('app', 'Редактировать'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(\Yii::t('app', 'Удалить'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => \Yii::t('app', 'Вы действительно хотите удалить эту запись?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            'host',
            'port',
            'name',
            'protocol_class',
            'zpk',
            'zak',
            'zmk_tap_x',
            'sred_clear_period_days',
            [
                'attribute' => 'owner_id',
                'value' => ArrayHelper::getValue($model->owner, 'name', $model->owner_id),
            ],
            [
                'attribute' => 'ped_conf_category_id',
                'value' => $model->pedConfCategory ?
                    $model->pedConfCategory->name . ($model->pedConfCategory->desc ? ' (' . $model->pedConfCategory->desc . ')' : '') :
                    '<span class="not-set">(' . \Yii::t('app', 'неизвестная группа конфигурации') . ')</span>',
                'format' => 'html',
            ],
            [
                'attribute' => 'status',
                'value' => ArrayHelper::getValue(\app\models\IsoSettings::getStatuses(), $model->status, $model->status),
            ],
            'date_mod',
        ],
    ]) ?>


    <div class="iso-setting-payment-map-index">

        <h3><?= Html::encode(Yii::t('app', 'Платежи')) ?></h3>

        <p>
            <?= Html::a(Yii::t('app', 'Добавить платеж'), ['iso-setting-payment-map/create', 'iso_setting_id' => $model->id], ['class' => 'btn btn-success']) ?>
        </p>

        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                //'iso_setting_id',
                [
                    'attribute' => 'payment_id',
                    'value' => function($model) use ($paymentFilter) {
                        return ArrayHelper::getValue($paymentFilter, $model->payment_id, $model->payment_id);
                    },
                    'filter' => $paymentFilter,
                ],
                [
                    'attribute' => 'currency',
                    'value' => function($model) use ($currencyFilter) {
                        return ArrayHelper::getValue($currencyFilter, $model->currency, $model->currency);
                    },
                    'filter' => $currencyFilter,
                ],

                [
                    'class' => 'yii\grid\ActionColumn',
                    'controller' => 'iso-setting-payment-map',
                    'template' => '{update} {delete}',
                ],
            ],
        ]); ?>


    </div>

</div>
