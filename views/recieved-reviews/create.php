<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\RecievedReviews */

$this->title = 'Create Recieved Reviews';
$this->params['breadcrumbs'][] = ['label' => 'Recieved Reviews', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="recieved-reviews-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
