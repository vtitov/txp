<?php

/* @var $this yii\web\View */
/* @var $qrString string */
/* @var $model \yii\base\DynamicModel */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = \Yii::t('app', 'Добавление второго фактора');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="auth-assign-software-token">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="row">
        <div class="col-md-6">
            <p><?= Yii::t('app', 'Просканируйте QR код в мобильном приложении двухфакторной аутентификации (напр. Google Authenticator) и введите сгенерированный код авторизации:') ?></p>

            <?php $form = ActiveForm::begin([]); ?>

            <?= $form->field($model, 'mfa_token')->textInput(['autofocus' => true, 'autocomplete' => 'off']) ?>

            <div class="form-group">
                <?= Html::submitButton(\Yii::t('app', 'Привязать'), ['class' => 'btn btn-primary']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
        <div class="col-md-6">
            <?php
            $qrCode = (new \Da\QrCode\QrCode($qrString))
                ->setSize(250)
                ->setMargin(5);

            echo '<img src="' . $qrCode->writeDataUri() . '"><br>';
            ?>
        </div>
    </div>

</div>
