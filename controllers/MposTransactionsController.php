<?php

namespace app\controllers;

use app\models\Receipt;
use app\models\TransactionArchive;
use Yii;
use app\models\MposTransactions;
use app\models\MposTransactionsSearch;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * MposTransactionsController implements the CRUD actions for MposTransactions model.
 */
class MposTransactionsController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view', 'receipt-view'],
                        'roles' => ['transactionView'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all MposTransactions models.
     * @return mixed
     */
    public function actionIndex()
    {
        set_time_limit(300);
        $searchModel = new MposTransactionsSearch();
        $searchModel->date_time = date('Y-m-d') . ' - ' . date('Y-m-d');

        if (Yii::$app->request->isPost) {
            if (Yii::$app->request->post('action') == 'render') {
                $dataProvider = $searchModel->search(Yii::$app->request->post());
            } else if (Yii::$app->request->post('action') == 'export') {
                $dataProvider = $searchModel->search(Yii::$app->request->post());
                if (!$searchModel->hasErrors()) {
                    $dataProvider->pagination = false;

                    header("Content-type: text/csv; charset=utf-8");
                    header("Content-Disposition: attachment; filename=transactions_export.csv");
                    header('Cache-Control: no-store, no-cache, must-revalidate, max-age=0');

                    $outputBuffer = fopen("php://output", 'w');
                    //fputs($outputBuffer, $bom =( chr(0xEF) . chr(0xBB) . chr(0xBF) ));
                    //fputs($outputBuffer, "sep=,\n");
                    fputcsv($outputBuffer, [
                        'ID',
                        Yii::t('app', 'Дата'),
                        Yii::t('app', 'Серийный номер терминала'),
                        Yii::t('app', 'Торговец'),
                        Yii::t('app', 'Код авторизации'),
                        Yii::t('app', 'Статус'),
                        Yii::t('app', 'Сумма'),
                        Yii::t('app', 'Карта'),
                        Yii::t('app', 'Платеж'),
                        Yii::t('app', 'Тип')
                    ]);
                    foreach ($dataProvider->getModels() as $model){
                        //$model['amount'] = str_replace('.', ',', $model['amount']);
                        fputcsv($outputBuffer, $model);
                    }
                    fclose($outputBuffer);
                    exit();
                }
            } else {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
        } else {
            $dataProvider = $searchModel->search('');
        }
        $dataProvider = $searchModel->search(Yii::$app->request->post());

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single MposTransactions model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionReceiptView($id)
    {
        $this->findModel($id);//проверяем, имеет ли пользователь право запрашивать чек транзакции
        $receipt = Receipt::findOne($id);
        return $this->renderPartial('receipt-view', [
            'receipt' => $receipt,
        ]);
    }

    /**
     * Finds the MposTransactions model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return MposTransactions the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        $owner_id = Yii::$app->user->identity->owner_id;
        $model = MposTransactions::findOne($id);
        if ($model === null) {
            $model = TransactionArchive::findOne(['id' => $id]);
        }
        $isoSetting = ArrayHelper::getValue(ArrayHelper::getValue($model, 'paymentTerminal'), 'isoSettings');

        if ($model !== null && ($owner_id == 1 || ($isoSetting !== null && $isoSetting->owner_id == $owner_id))) {
            return $model;
        } else {
            throw new NotFoundHttpException(\Yii::t('app', 'Транзакция не найдена.'));
        }
    }
}
