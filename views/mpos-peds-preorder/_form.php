<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\MposPedsPreorder */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mpos-peds-preorder-form">

    <?php $form = ActiveForm::begin(); ?>

    <h2><?= Yii::t('app', 'Информация о заказчике') ?></h2>

    <?= $form->field($model, 'name')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'registered_address')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'post_address')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'unp')->textInput(['maxlength' => 9]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => 50]) ?>

    <?= $form->field($model, 'mobile_phone')->textInput(['maxlength' => 12, 'placeholder' => '375291234567']) ?>

    <?= $form->field($model, 'organisation_phone')->textInput(['maxlength' => 12, 'placeholder' => '375172222222']) ?>

    <?= $form->field($model, 'head_full_name')->textInput(['maxlength' => 255]) ?>

    <h2><?= Yii::t('app', 'Банковские реквизиты') ?></h2>

    <?= $form->field($model, 'banks_mfo')->textInput(['maxlength' => 9]) ?>

    <?= $form->field($model, 'bank_account')->textInput(['maxlength' => 25]) ?>

    <?= $form->field($model, 'bic')->textInput(['maxlength' => 11]) ?>

    <?= $form->field($model, 'iban')->textInput(['maxlength' => 42]) ?>

    <h2><?= Yii::t('app', 'Доставка') ?></h2>

    <?= $form->field($model, 'ped_preorder_delivery_type_id')->dropDownList(
        ArrayHelper::map(app\models\PedPreorderDeliveryType::find()->asArray()->all(), 'id', 'name')) ?>

    <?= $form->field($model, 'delivery_contact_person_fio')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'ped_preorder_delivery_point_id', [
        'options' => ['id' => 'delivery-point-div', 'class' => 'form-group']
    ])->dropDownList(ArrayHelper::map(app\models\PedPreorderDeliveryPoint::find()->asArray()->all(), 'id', 'address')) ?>

    <?= $form->field($model, 'delivery_address', [
        'options' => ['id' => 'delivery-address-div', 'style' => 'display:none', 'class' => 'form-group'],
        'template' => '<span style="margin-right: 20px">{label}</span><label class="checkbox-inline"><input type="checkbox" id="delivery-address-checkbox"/>' . Yii::t('app', 'совпадает с почтовым') . '</label>{input}{hint}{error}'
    ])->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'device_number')->textInput(['type' => 'number', 'min' => '1']) ?>

    <div class="form-group">
        <?= Html::submitButton(\Yii::t('app', 'Сохранить'), ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<script>
    document.getElementById('mpospedspreorder-ped_preorder_delivery_type_id').onchange = function () {
        var selectedDeliveryType = document.getElementById('mpospedspreorder-ped_preorder_delivery_type_id');
        document.getElementById('delivery-point-div').style.display = selectedDeliveryType.value == 1 ? 'block' : 'none';
        document.getElementById('delivery-address-div').style.display = selectedDeliveryType.value != 1 ? 'block' : 'none';
        document.getElementById('delivery-address-checkbox-label').style.display = selectedDeliveryType.value != 1 ? 'block' : 'none';
    }

    document.getElementById('delivery-address-checkbox').onchange = function () {
        if (this.checked) {
            document.getElementById('mpospedspreorder-delivery_address').value = document.getElementById('mpospedspreorder-post_address').value;
            document.getElementById('mpospedspreorder-delivery_address').readOnly = true;
        } else {
            document.getElementById('mpospedspreorder-delivery_address').readOnly = false;
        }
    }

    document.getElementById('mpospedspreorder-post_address').onchange = function () {
        if (document.getElementById('delivery-address-checkbox').checked) {
            document.getElementById('mpospedspreorder-delivery_address').value = document.getElementById('mpospedspreorder-post_address').value;
        }
    }
</script>
